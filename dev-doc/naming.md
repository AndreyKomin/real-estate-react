# Naming

## Files

Every file should have the same name as default export's value 
 (React component, function). If file have no default export it
  should be named with name (plural) of entities it exports. 

## Constants

It mean config constants not things that declared with `const`.
  Constants should be named with UPPER_CASE. If you have a constant that
  is an object/array/function - use deep freeze on it.

Constants that are not config part should be named in camelCase.

```jsx harmony
// bad
function badFunction(filePath) {
  const exts = ['js', 'jsx'];
  const EXT = getExt(filePath);
  return exts.includes(EXT);
}


// good
const EXTS = ['js', 'jsx'];

function goodFunction(filePath) {  
  const ext = getExt(filePath);
  return EXTS.includes(ext);
}
```


## Components

If component is layout (have no logic) it should be named with `{OptionalLabel}Layout`.
If component has logic it should be named as `{Label}`. 

### Methods

If method renders something (returns array list's components for example) 
  it should be named as `render{Something}`. If it returns element, it's
  better to move it into own component in most cases.
  
```jsx harmony
// bad
class Comp1 extends PureComponent {
  getElem1() {
    if (!this.props.elem1) return null;
    return <div>{this.props.elem1}</div>;
  }
  
  getRows() {
    return this.props.items.map(row => <div key={row.id}>{row.name}: {row.value}</div>)
  }

  render(){
    const elem1 = this.getElem1();
    const rows = this.getRows();
    
    return (
      <div>
        {elem1}
        {rows}
      </div>
    );
  }
}

// good
// Elem1.js
const Elem1 = ({ elem1 }) => {
  if (!elem1) return null;
  return <div>{elem1}</div>;
};


// Comp2.js
class Comp2 extends PureComponent {  
  renderRows() {
    return this.props.items.map(row => <div key={row.id}>{row.name}: {row.value}</div>)
  }

  render(){
    const { elem1 } = this.props;
    const rows = this.renderRows();
    
    return (
      <div>
        <Elem1 elem1={elem1} />
        {rows}
      </div>
    );
  }
}
```




