const cp = require('child_process');
const path = require('path');

const moment = require('moment');
const { getNpm } = require('./utils');

const MODULE_NAME = '[PREPUSH]';
const FORMAT = '\\[hh:mm:ss\\]';

const OPTIONS = {
  stdio: ['pipe', process.stdout, process.stderr],
};

const log = (string, ...rest) => {
  console.log(`${moment().format(FORMAT)} ${MODULE_NAME} - ${string}`, ...rest);
};

const checkBranch = () => {
  if (process.env.SKIP_PREPUSH) return process.exit(0);
  return Promise.resolve();
};

const buildDll = () => {
  const COMMAND = getNpm();
  const ARGS = ['run', 'build:target:dll'];
  return new Promise((resolve) => {
    log(`[DLL] start "${COMMAND} ${ARGS.join(' ')}"`);
    cp.spawn(COMMAND, ARGS, OPTIONS)
      .once('exit', (code) => {
        log('[DLL] end');
        if (code !== 0) {
          process.exit(code);
        } else {
          resolve();
        }
      });
  });
};

const buildDllIfNeed = () => new Promise((resolve) => {
  require(path.join(__dirname, '../build/dll/chunk-map.json'));
  resolve();
}).catch(err => {
  console.warn(err);
  return buildDll();
});

const typeCheck = () => {
  const COMMAND = getNpm();
  const ARGS = ['run', 'flow'];
  return new Promise((resolve) => {
    log(`[TYPE_CHECK] start "${COMMAND} ${ARGS.join(' ')}"`);
    cp.spawn(COMMAND, ARGS, OPTIONS)
      .once('exit', (code) => {
        log('[TYPE_CHECK] end');
        if (code !== 0) {
          process.exit(code);
        } else {
          resolve();
        }
      });
  });
};

const lint = () => {
  const COMMAND = getNpm();
  const ARGS = ['run', 'lint'];
  return new Promise((resolve) => {
    log(`[LINT] start "${COMMAND} ${ARGS.join(' ')}"`);
    cp.spawn(COMMAND, ARGS, OPTIONS)
      .once('exit', (code) => {
        log('[LINT] end');
        if (code !== 0) {
          process.exit(code);
        } else {
          resolve();
        }
      });
  });
};

const stylelint = () => {
  const COMMAND = getNpm();
  const ARGS = ['run', 'stylelint'];
  return new Promise((resolve) => {
    log(`[STYLELINT] start "${COMMAND} ${ARGS.join(' ')}"`);
    cp.spawn(COMMAND, ARGS, OPTIONS)
      .once('exit', (code) => {
        log('[STYLELINT] end');
        if (code !== 0) {
          process.exit(code);
        } else {
          resolve();
        }
      });
  });
};

const test = () => {
  const COMMAND = getNpm();
  const ARGS = ['run', 'test'];
  return new Promise((resolve) => {
    log(`[TEST] start "${COMMAND} ${ARGS.join(' ')}"`);
    cp.spawn(COMMAND, ARGS, OPTIONS)
      .once('exit', (code) => {
        log('[TEST] end');
        if (code !== 0) {
          process.exit(code);
        } else {
          resolve();
        }
      });
  });
};

checkBranch()
  .then(buildDllIfNeed)
  .then(() => Promise.all([
    lint(),
    stylelint(),
    test(),
    typeCheck(),
  ]));
