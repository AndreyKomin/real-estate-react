import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
import chaiImmutable from 'chai-immutable';
import sinonChai from 'sinon-chai';

chai.use(chaiImmutable);
chai.use(sinonChai);
chai.use(chaiAsPromised);

window.expect = chai.expect;
