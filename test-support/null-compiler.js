function noop() {
  return null;
}

const EXTENSIONS = [
  '.scss',
  '.css',
];

EXTENSIONS.forEach((ext) => {
  require.extensions[ext] = noop;
});

