/** @flow */
import type { Map } from 'immutable';
import { combineReducers } from 'redux-immutable';
import { reducer as form } from 'redux-form/immutable';

import app from 'app/reducer';
import search from 'data/search';
import map from 'data/map';
import listing from 'data/listing';
import property from 'data/property';
import user from 'data/user';
import statistic from 'data/statistic';
import bing from 'data/bing';
import contracts from 'data/contracts';
import campaigns from 'data/campaigns';
import errors from 'data/errors';
import analyses from 'data/analyses';
import contacts from 'data/contacts';
import reducer from 'data/map/factory';

import routing from './routing';

import * as ActionType from './tempCompMap';


const comparableMap = reducer(ActionType, 'PROPERTIES_DETAILS_');

const treeReducer = combineReducers({
  form,
  app,
  listing,
  property,
  search,
  map,
  bing,
  routing,
  user,
  statistic,
  contracts,
  campaigns,
  errors,
  analyses,
  contacts,
  comparableMap,
});

const order = [treeReducer];

export default function rootReducer(state: Map<string, *>, action: Object): Map<string, *> {
  return order.reduce((currentState, reducer) => reducer(currentState, action), state);
}

