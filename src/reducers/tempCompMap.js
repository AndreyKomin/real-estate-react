/** @flow */
import { List } from 'immutable';


export const PROPERTIES_DETAILS_ZOOM_IN = Symbol('PROPERTIES_DETAILS_ZOOM_IN');

export function zoomIn() {
  return {
    type: PROPERTIES_DETAILS_ZOOM_IN,
  };
}

export const PROPERTIES_DETAILS_ZOOM_OUT = Symbol('PROPERTIES_DETAILS_ZOOM_OUT');

export function zoomOut() {
  return {
    type: PROPERTIES_DETAILS_ZOOM_OUT,
  };
}

export const PROPERTIES_DETAILS_SET_ZOOM = Symbol('PROPERTIES_DETAILS_SET_ZOOM');

export function setZoom(value: number) {
  return {
    type: PROPERTIES_DETAILS_SET_ZOOM,
    value,
  };
}

export const PROPERTIES_DETAILS_TRIGGER_PUSHPIN_POPOVER = Symbol('PROPERTIES_DETAILS_TRIGGER_PUSHPIN_POPOVER');

export function triggerActivePushpin(value: number | string) {
  return {
    type: PROPERTIES_DETAILS_TRIGGER_PUSHPIN_POPOVER,
    value,
  };
}

export const PROPERTIES_DETAILS_SET_POLYGON_CORDS = Symbol('PROPERTIES_DETAILS_SET_POLYGON_CORDS');

export function setPolygonCords(cords: List<string> | Array<string>) {
  return {
    type: PROPERTIES_DETAILS_SET_POLYGON_CORDS,
    cords,
  };
}

export const PROPERTIES_DETAILS_ADD_POLYGON_CORD = Symbol('PROPERTIES_DETAILS_ADD_POLYGON_CORD');

export function addPolygonCord(cord: string) {
  return {
    type: PROPERTIES_DETAILS_ADD_POLYGON_CORD,
    cord,
  };
}
