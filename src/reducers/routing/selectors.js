import { createSelector } from 'reselect';


const checkState = state => state;

export const selectRouting = createSelector(
  checkState,
  state => state.get('routing'),
);

export const selectLocationBeforeTransitions = createSelector(
  selectRouting,
  routing => routing.get('locationBeforeTransitions'),
);

export const selectPathname = createSelector(
  selectLocationBeforeTransitions,
  locationBeforeTransitions => locationBeforeTransitions.get('pathname'),
);
