/** @flow */
export const PropertyTypeValues: Array<{ label: string, value: string }> = [
  { label: 'Investment', value: 'INVESTMENT' },
  { label: 'Personal', value: 'PERSONAL' },
];

export const BooleanValues: Array<{ label: string, value: boolean }> = [
  { label: 'Yes', value: true },
  { label: 'No', value: false },
];

export const MortgageTypes: Object = {
  fixedInterest: 'FIXED_INTEREST',
  interestOnly: 'INTEREST_ONLY',
  combination: 'COMBINATION',
  accelerated: 'ACCELERATED',
};

export const MortgageTypeValues: Array<{ label: string, value: string }> = [
  { label: 'Fixed Interest', value: MortgageTypes.fixedInterest },
  { label: 'Interest Only', value: MortgageTypes.interestOnly },
  { label: 'Combination', value: MortgageTypes.combination },
  { label: 'Accelerated', value: MortgageTypes.accelerated },
];

export const MortgageTypeInputs: Object = {
  FIXED_INTEREST: { fixedInterest: true, interestOnly: false },
  INTEREST_ONLY: { fixedInterest: false, interestOnly: true },
  COMBINATION: { fixedInterest: true, interestOnly: true },
  ACCELERATED: { fixedInterest: true, interestOnly: false },
};

export const MortgageFrequencyValues : Array<{ label: string, value: string }> = [
  { label: 'Monthly', value: 'MONTHLY' },
  { label: 'Quarterly', value: 'QUARTERLY' },
  { label: 'Semi-annually', value: 'SEMI_ANNUALLY' },
  { label: 'Annually', value: 'ANNUALLY' },
];

export const ArmTypeValues: Array<{ label: string, value: string }> = [
  { label: 'Standard', value: 'STANDARD' },
  { label: 'Hybrid', value: 'HYBRID' },
];

export const ArmTypes: Object = {
  standard: 'STANDARD',
  hybrid: 'HYBRID',
};

export const ArmHybridCustomType: string = 'CUSTOM';

export const ArmHybridTypeValues: Array<{ label: string, value: string }> = [
  { label: '10/1 ARM', value: 'ARM_10_1' },
  { label: '7/1 ARM', value: 'ARM_7_1' },
  { label: '5/1 ARM', value: 'ARM_5_1' },
  { label: '3/1 ARM', value: 'ARM_3_1' },
  { label: '1 Year ARM', value: 'ARM_1_YEAR' },
  { label: 'Custom', value: 'CUSTOM' },
];

export const ArmRateCapTypeValues: Array<{ label: string, value: string }> = [
  { label: 'Limit Rate', value: 'RATE', field: 'armRateMax' },
  { label: 'Limit Increase', value: 'INCREASE', field: 'armIncreaseRateMax' },
];

export const AnalysisFieldsTypes: Object = {
  closingCosts: 'CLOSING_COST',
  improvements: 'IMPROVEMENT',
  expenses: 'EXPENSE',
  incomes: 'INCOME',
};

export const DepreciationMethodValues: Array<{ label: string, value: string, realValue: ?string }> = [
  { label: 'None', value: 'NONE', realValue: null },
  { label: 'Straightline', value: 'STRAIGHT_LINE', realValue: 'STRAIGHT_LINE' },
  { label: 'Double-Declining Balance', value: 'DOUBLE_DECLINING_BALANCE', realValue: 'DOUBLE_DECLINING_BALANCE' },
];

export const RateAmountTypeValues: Array<{ label: string, value: string }> = [
  { label: 'Percentage', value: 'PERCENTAGE' },
  { label: 'Fixed Dollar Amount', value: 'DOLLAR_AMOUNT' },
];

export const RateAmountNoneTypeValues: Array<{ label: string, value: ?string, realValue: ?string }> = [
  { label: 'None', value: 'NONE', realValue: null },
  { label: 'Percentage', value: 'PERCENTAGE', realValue: 'PERCENTAGE' },
  { label: 'Fixed Dollar Amount', value: 'DOLLAR_AMOUNT', realValue: 'DOLLAR_AMOUNT' },
];

export const ExpenseIncreaseTypeValues: Array<{ label: string, value: string }> = [
  { label: 'Percentage', value: 'PERCENTAGE' },
  { label: 'Fixed Dollar Amount', value: 'DOLLAR_AMOUNT' },
];

export const PropertyValueBasisValues: Array<{ label: string, value: string }> = [
  { label: 'Purchase Price', value: 'PURCHASE_PRICE' },
  { label: 'Market Value', value: 'MARKET_VALUE' },
  { label: 'Other', value: 'DOLLAR_AMOUNT' },
];

export const PropertyValueIncreaseTypeValues: Array<{ label: string, value: string }> = [
  { label: 'Market Value Increase Rate', value: 'MARKET_VALUE_INCREASE_RATE' },
  { label: 'Cap Rate', value: 'CAP_RATE' },
  { label: 'Inflation Rate', value: 'INFLATION_RATE' },
  { label: 'Fixed Selling Price', value: 'DOLLAR_AMOUNT' },
];

export const TermTypeValues: Array<{ label: string, value: string }> = [
  { label: 'One Year', value: 'ONE_YEAR' },
  { label: 'Three Years', value: 'THREE_YEAR' },
  { label: 'Five Years', value: 'FIVE_YEAR' },
  { label: 'Ten Years', value: 'TEN_YEAR' },
  { label: 'Mortgage Term', value: 'MORTGAGE' },
  { label: 'Months(Short Sale)', value: 'MANUAL' },
];

export const IncomeIncreaseTypeValues: Array<{ label: string, value: string }> = [
  { label: 'Percentage', value: 'PERCENTAGE' },
  { label: 'Fixed Dollar Amount', value: 'DOLLAR_AMOUNT' },
];
