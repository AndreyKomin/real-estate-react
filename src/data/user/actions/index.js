/** @flow */
import { CALL_API } from 'store/middleware/api';
import { setCode } from 'store/middleware/api/activate';


export const AUTHENTICATE = Symbol('AUTHENTICATE');
export const AUTHENTICATION_SUCCESS = Symbol('AUTHENTICATION_SUCCESS');
export const AUTHENTICATION_ERROR = Symbol('AUTHENTICATION_ERROR');

type TAuthParams = {|
  activationCode: string,
  afterSuccess: Function,
  afterError: Function,
|};

export function authenticate({ activationCode = '', afterSuccess, afterError }: TAuthParams): Object {
  const result: any = {};
  result[CALL_API] = {
    method: 'get',
    path: '/resource/auth/ps4/user/authentication',
    query: { ActivationCode: activationCode, dev: process.env.NODE_ENV !== 'production' },
    startType: AUTHENTICATE,
    successType: AUTHENTICATION_SUCCESS,
    errorType: AUTHENTICATION_ERROR,
    afterSuccess: ({ response, ...objRest }, ...rest) => {
      setCode(response.activationCode);
      afterSuccess({ response, ...objRest }, ...rest);
    },
    afterError,
  };

  return result;
}

export const AUTHENTICATION_CHECK = Symbol('AUTHENTICATION_CHECK');

export function checkAuth({ afterError, afterSuccess }: { afterSuccess?: Function, afterError: Function }): Object {
  const result: any = {};
  result[CALL_API] = {
    method: 'get',
    path: '/resource/auth/ps4/user/authentication',
    unifier: 'checkAuth',
    startType: AUTHENTICATION_CHECK,
    successType: AUTHENTICATION_SUCCESS,
    errorType: AUTHENTICATION_ERROR,
    skipGlobalErrorHandler: true,
    afterSuccess: ({ response, ...objRest }, ...rest) => {
      setCode(response.activationCode);
      if (typeof afterSuccess === 'function') {
        afterSuccess({ response, ...objRest }, ...rest);
      }
    },
    afterError,
  };

  return result;
}


export const USER_SET_BACK_URL = Symbol('USER_SET_BACK_URL');

export function setBackUrl({ pathname, search }: { pathname: string, search: string }): Object {
  return {
    type: USER_SET_BACK_URL,
    pathname,
    search,
  };
}


export const SAVE_PROFILE = Symbol('SAVE_PROFILE');
export const SAVE_PROFILE_SUCCESS = Symbol('SAVE_PROFILE_SUCCESS');
export const SAVE_PROFILE_ERROR = Symbol('SAVE_PROFILE_ERROR');

export function saveProfile(body: Object): Object {
  const result: any = {};
  result[CALL_API] = {
    caseTransform: true,
    method: 'post',
    body,
    path: '/resource/auth/ps4/user/profile',
    startType: SAVE_PROFILE,
    successType: SAVE_PROFILE_SUCCESS,
    errorType: SAVE_PROFILE_ERROR,
  };

  return result;
}


export const REMOVE_ERROR = Symbol('REMOVE_ERROR');

export function removeError(): Object {
  return {
    type: REMOVE_ERROR,
  };
}


export const PAYMENT = Symbol('PAYMENT');
export const PAYMENT_SUCCESS = Symbol('PAYMENT_SUCCESS');
export const PAYMENT_ERROR = Symbol('PAYMENT_ERROR');

export function payment(body: Object, callbacks: Object): Object {
  const result: any = {};
  result[CALL_API] = {
    ...callbacks,
    method: 'post',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body,
    path: '/resource/auth/ps4/user/payment',
    startType: PAYMENT,
    successType: PAYMENT_SUCCESS,
    errorType: PAYMENT_ERROR,
  };

  return result;
}
