/** @flow */
import { fromJS, Map } from 'immutable';

import API from 'config/constants/API';

import * as ActionType from '../actions';


const defaultProfile = {
  username: '',
  password: '',
  cardNumber: '',
  maskedCardNumber: '',
  cardName: '',
  cardExpMonth: 1,
  cardExpYear: new Date().getFullYear(),
  billingStreetAddress: '',
  billingCity: '',
  billingState: '',
  billingZip: '',
  billingCountry: '',
  logoUrl: '',
  logoHash: Date.now(),
  favoritePropertyAddLimit: 0,
  marketingListAddLimit: 0,
  appendPhoneRate: 0,
  appendEmailRate: 0,
  countySearchEnabled: false,
};

const defaultState = fromJS({
  isLoading: false,
  profile: defaultProfile,
  reports: [],
  permissions: [],
  pages: [],
  backUrl: '/',
});

function setProfile(state, response) {
  const { profile } = response;
  profile.logoUrl = `${API.BASE_URL}${response.profile.logoUrl}`;
  profile.cardNumberMasked = profile.cardNumber;
  profile.cardNumber = '';

  return state.mergeDeep({
    isLoading: false,
    profile,
    reports: response.reports,
    permissions: response.permissions,
    pages: response.pages,
    analysisFields: response.analysisFields,
  });
}

function saveProfile(state) {
  return state.merge({
    isLoading: true,
  });
}

export default function reducer(state: Map<string, *> = defaultState, action: Object): Map<string, *> {
  switch (action.type) {
    case ActionType.AUTHENTICATE:
    case ActionType.AUTHENTICATION_CHECK:
      return state.set('isLoading', true);

    case ActionType.AUTHENTICATION_SUCCESS:
      return setProfile(state, action.response);

    case ActionType.AUTHENTICATION_ERROR:
      return state.set('isLoading', false);

    case ActionType.USER_SET_BACK_URL:
      return state.set('backUrl', `${action.pathname}${action.search}`);

    case ActionType.SAVE_PROFILE:
      return saveProfile(state);

    case ActionType.SAVE_PROFILE_SUCCESS:
      return state.mergeDeep({
        isLoading: false,
        profile: action.response,
      });

    case ActionType.SAVE_PROFILE_ERROR:
      return state.mergeDeep({
        isLoading: false,
      });

    case ActionType.REMOVE_ERROR:
      return state.mergeDeep({
        profile: {
          success: null,
          message: null,
        },
      });

    default:
      return state;
  }
}
