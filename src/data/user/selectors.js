/** @flow */
import { createSelector } from 'reselect';
import { List } from 'immutable';
import { ReportTypes, Reports } from './constants';


const emptyList = List();

const checkState = state => state;

const selectUser = createSelector(
  checkState,
  state => state.get('user'),
);

export const selectProfile = createSelector(
  selectUser,
  user => user.get('profile'),
);

export const selectPermissions = createSelector(
  selectUser,
  user => user.get('permissions'),
);

export const selectPages = createSelector(
  selectUser,
  user => user.get('pages', emptyList),
);

export const selectReports = createSelector(
  selectUser,
  user => user.get('reports', emptyList).sort((a, b) => a.get('seq') - b.get('seq')),
);

export const selectPremiumReports = createSelector(
  selectReports,
  reports => reports.filter(report => report.get('type') === ReportTypes.premium),
);

export const selectPropertyReports = createSelector(
  selectReports,
  reports => reports.filter(report => (report.get('type') === ReportTypes.property || report.get('type') === ReportTypes.analysis) && report.get('code') !== Reports.comparativeMarketAnalysis),
);

export const selectAnalysisReports = createSelector(
  selectReports,
  reports => reports.filter(report => report.get('type') === ReportTypes.analysis),
);

export const selectIsAuthed = createSelector(
  selectUser,
  user => user.get('profile').size !== 0,
);

export const selectIsLoading = createSelector(
  selectUser,
  user => user.get('isLoading'),
);

export const selectBackUrl = createSelector(
  selectUser,
  user => user.get('backUrl'),
);
