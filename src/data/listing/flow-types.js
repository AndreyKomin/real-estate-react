/** @flow */
export type ListingTypeCustom = 'CUSTOM';
export type FullTableType = 'FULL';
export type LiensTableType = 'LIENS';
export type ActivePinType = 'ACTIVE';
export type NeighborsColorType = 'NEIGHBORS';
export type DefaultColorType = 'DEFAULT_COLOR';
export type DefaultType = 'DEFAULT';


export type ListingType = 'M' | 'P' | 'A' | 'F' | 'C' | 'V' | 'E' | 'R' | 'L' | 'D' | 'B';
export type PropertyType = 'SFR' | 'CONDO' | 'MFR' | 'LAND' | 'OTHER';
export type DataTypeType = 'PRICE' | 'DATE' | null;

export type MlsListingStatusType = 'ACTIVE' | 'CONTINGENT' | 'FAIL' | 'PENDING' | 'SOLD' | 'UNKNOWN';
export type DocumentTypeCodeType = 'NT' | 'ND' | 'LP' | 'NR' | 'TD' | 'OTHER' | null;
export type ForeclosureStatusType = 'PRE_FORECLOSURE' | 'FORECLOSURE' | 'ANY' | 'NONE';
export type LienTypeCodeType = 'TAX' | 'HOA' | 'MECHANIC' | 'UTILITY' | 'OTHER' | null;
export type EstimatedValueGrowthPeriodType = 'ONE_MONTH' | 'THREE_MONTH' | 'SIX_MONTH' | 'ONE_YEAR';

export type DisplayType = 'PICTURE' | 'LIST' | null;

export type OverlayType = 'CHARTS' | 'TABLE' | null;

export type ListingSearchParams = {
  cityId?: number,
  saleAmountMax?: number | string,
  saleAmountMin?: number | string,
  bathroomsMax?: number,
  bathroomsMin?: number,
  bedroomsMax?: number,
  bedroomsMin?: number,
  listingType: ListingType[],
  mlsListingStatus?: MlsListingStatusType[],
  ownerCorporate?: boolean | void,
  ownerOccupied?: boolean,
  squareFeetMin?: number,
  squareFeetMax?: number,
  lotSquareFeetMin?: number,
  lotSquareFeetMax?: number,
  garageAvailable?: boolean,
  yearBuiltMin?: number,
  yearBuiltMax?: number,
  poolAvailable?: boolean,
  listingDateMin?: Date,
  listingDateMax?: Date,
  ownershipLengthMax?: number,
  ownershipLengthMin?: number,
  shapeDefinition: string[],
  vacant?: boolean,

  page?: number,
  limit?: number,
}
