/** @flow */
import { CALL_API } from 'store/middleware/api';
import formatRequestQuery from 'utils/formatRequestQuery';
import getRequestParams from 'data/utils/getSearchRequestParams';
import SearchTransform from 'data/search/Transform';

import type { ListingSearchParams, OverlayType, DisplayType } from '../flow-types';


export const LOAD_LISTINGS = Symbol('LOAD_LISTINGS');
export const LOAD_LISTINGS_SUCCESS = Symbol('LOAD_LISTINGS_SUCCESS');
export const LOAD_LISTINGS_ERROR = Symbol('LOAD_LISTINGS_ERROR');

export const LOAD_ALL_LISTINGS = Symbol('LOAD_ALL_LISTINGS');
export const LOAD_ALL_LISTINGS_SUCCESS = Symbol('LOAD_ALL_LISTINGS_SUCCESS');
export const LOAD_ALL_LISTINGS_ERROR = Symbol('LOAD_ALL_LISTINGS_ERROR');


function getSearchParams(params) {
  return SearchTransform.toAPI(params);
}

export function loadListings(listingInfo: Object): Object {
  const requestParams = getRequestParams(listingInfo);
  const params = getSearchParams(requestParams);
  const query = formatRequestQuery(params);

  const request: any = {
    searchIdentifier: JSON.stringify({ ...params }),
    page: params.page,
    limit: params.limit,
    rangeFrom: params.rangeFrom,
    rangeTo: params.rangeTo,
    rangeEnabled: params.rangeEnabled,
    searchQuery: query,
  };

  request[CALL_API] = {
    method: 'get',
    path: '/resource/auth/ps4/listing',
    unifier: 'search listing resource',
    query: { ...query },
    dislodging: true,
    startType: LOAD_LISTINGS,
    successType: LOAD_LISTINGS_SUCCESS,
    errorType: LOAD_LISTINGS_ERROR,
  };

  return request;
}

export function loadAllListings(listingInfo: Object, rangeFrom: number, total: number, callbacks: Object = {}): Object {
  const requestParams: ListingSearchParams = getRequestParams({ ...listingInfo, resultLimit: total });
  const query = formatRequestQuery(getSearchParams(requestParams));
  const request: any = {};

  request[CALL_API] = {
    method: 'get',
    path: '/resource/auth/ps4/listing',
    query: { ...query, distinct: true, resultOffset: rangeFrom },
    dislodging: true,
    startType: LOAD_ALL_LISTINGS,
    successType: LOAD_ALL_LISTINGS_SUCCESS,
    errorType: LOAD_ALL_LISTINGS_ERROR,
    afterSuccess: callbacks.afterSuccess,
  };
  return request;
}

export const SAVE_LISTINGS = Symbol('SAVE_LISTINGS');
export const SAVE_LISTINGS_SUCCESS = Symbol('SAVE_LISTINGS_SUCCESS');
export const SAVE_LISTINGS_ERROR = Symbol('SAVE_LISTINGS_ERROR');

export function saveListings(listingInfo: Object, rangeFrom: number, rangeTo: number, allSelected: boolean, selection: Array<*>, groupType: string, groupName: string, afterSuccess: Function): Object {
  const requestParams: ListingSearchParams = getRequestParams({ ...listingInfo, resultOffset: rangeFrom, resultLimit: rangeTo });
  const query = formatRequestQuery(getSearchParams(requestParams));
  const request: any = {};

  request[CALL_API] = {
    method: 'post',
    path: '/resource/auth/ps4/user/listings',
    body: { ...query, allSelected, selection },
    query: { groupType, groupName },
    dislodging: true,
    startType: SAVE_LISTINGS,
    successType: SAVE_LISTINGS_SUCCESS,
    errorType: SAVE_LISTINGS_ERROR,
    afterSuccess,
  };
  return request;
}

export const LOAD_PROPERTIES = Symbol('LOAD_PROPERTIES');
export const LOAD_PROPERTIES_SUCCESS = Symbol('LOAD_PROPERTIES_SUCCESS');
export const LOAD_PROPERTIES_ERROR = Symbol('LOAD_PROPERTIES_ERROR');

export function loadProperties(searchParams: Object): Object {
  const { cityName, stateCode, zip, streetAddress, apn, countyId } = searchParams;

  const params = getSearchParams({ cityName, stateCode, zip, streetAddress, apn, countyId });
  const query = formatRequestQuery(params);
  const request: any = {
    searchIdentifier: JSON.stringify({ ...params }),
  };

  request[CALL_API] = {
    method: 'get',
    path: '/resource/auth/ps4/property',
    unifier: 'search properties resource',
    query,
    dislodging: true,
    startType: LOAD_PROPERTIES,
    successType: LOAD_PROPERTIES_SUCCESS,
    errorType: LOAD_PROPERTIES_ERROR,
  };

  return request;
}

export const CLEAR_PROPERTIES = Symbol('CLEAR_PROPERTIES');

export function clearProperties() {
  return {
    type: CLEAR_PROPERTIES,
  };
}

export const CHANGE_PROPERTY_SORTING = Symbol('CHANGE_PROPERTY_SORTING');

export function changePropertySorting(data: Object) {
  return {
    type: CHANGE_PROPERTY_SORTING,
    data,
  };
}

export const SET_ACTIVE_TAB = Symbol('SET_ACTIVE_TAB');

export function setActiveTab(tabIndex: Number, tabName: string) {
  return {
    type: SET_ACTIVE_TAB,
    tabIndex,
    tabName,
  };
}

export const TRIGGER_LISTING_SELECT = Symbol('TRIGGER_LISTING_SELECT');

export function triggerSelect(id: *): Object {
  return {
    type: TRIGGER_LISTING_SELECT,
    id,
  };
}

export const MARK_ALL_LISTINGS_AS_SELECTED = Symbol('MARK_ALL_LISTINGS_AS_SELECTED');

export function markAllListingsAsSelected(): Object {
  return {
    type: MARK_ALL_LISTINGS_AS_SELECTED,
  };
}

export const CLEAR_SELECTED_LISTINGS = Symbol('CLEAR_SELECTED_LISTINGS');

export function clearSelectedListings(id: *): Object {
  return {
    type: CLEAR_SELECTED_LISTINGS,
    id,
  };
}

export const REMOVE_LISTING_SELECT = Symbol('REMOVE_LISTING_SELECT');

export function removeAllSelect(): Object {
  return {
    type: REMOVE_LISTING_SELECT,
  };
}

export const CHANGE_DISPLAY_TYPE = Symbol('CHANGE_DISPLAY_TYPE');

export function changeDisplayType(displayType: DisplayType) {
  return {
    type: CHANGE_DISPLAY_TYPE,
    displayType,
  };
}

export const CHANGE_OVERLAY_TYPE = Symbol('CHANGE_OVERLAY_TYPE');

export function changeOverlayType(overlayType: OverlayType) {
  return {
    type: CHANGE_OVERLAY_TYPE,
    overlayType,
  };
}


export const HIDE_OVERLAY = Symbol('HIDE_OVERLAY');

export function hideOverlay() {
  return {
    type: HIDE_OVERLAY,
  };
}

export const CHANGE_PAGE = Symbol('CHANGE_PAGE');

export function changePage(page: number) {
  return {
    type: CHANGE_PAGE,
    page,
  };
}

export const CHANGE_RANGE = Symbol('CHANGE_RANGE');

export function changeRange(range: Object) {
  return {
    type: CHANGE_RANGE,
    range,
  };
}

export const CLEAR_LISTINGS = Symbol('CLEAR_LISTINGS');

export function clear() { // TODO rename to clearListing
  return {
    type: CLEAR_LISTINGS,
  };
}
