/** @flow */
import { List, Map } from 'immutable';
import { createSelector } from 'reselect';
import { getBingUrl } from '../property';


const checkState = state => state;

export const selectItems = createSelector(
  checkState,
  map => map.getIn(['listing', 'list', 'items']),
);

const selectListing = createSelector(
  checkState,
  state => state.get('listing'),
);

export const selectDetailsState = createSelector(
  selectListing,
  properties => properties.getIn(['details']),
);

export const selectSorting = (state: any) => {
  const sorting = state.getIn(['listing', 'details', 'sorting']);
  return sorting.map((sort) => {
    const field = sort.get('field');
    const fields = field ? List([field]) : List();
    return sort.set('fields', fields);
  });
};

export const selectIsLoading = createSelector(
  selectDetailsState,
  details => details.get('propertiesLoading'),
);

export const selectActiveTabIndex = createSelector(
  selectDetailsState,
  details => details.get('activeTabIndex'),
);

export const selectSearchResultBingMapImageUrl = createSelector(
  [selectDetailsState, (state, index) => index],
  (details, propertyIndex) => {
    const properties = details.getIn(['data', 'properties'], List());
    const propertyDetails = properties.get(propertyIndex, Map());
    if (!propertyDetails.has('latitude')) return null;
    if (!propertyDetails.has('longitude')) return null;
    return getBingUrl(propertyDetails.get('latitude'), propertyDetails.get('longitude'));
  },
);

export default selectItems;
