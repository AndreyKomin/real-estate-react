/** @flow */
import { fromJS, Map } from 'immutable';
import * as ActionType from '../actions';


const defaultState = fromJS({
  data: {
    properties: [],
  },
  activeTabIndex: 0,
  activeTab: 'neighbors',
  propertiesLoading: false,
  propertiesError: null,
  sorting: {
    nearbyForeclosures: {
      ascending: false,
      field: 'type',
    },
    nearbyMlsListings: {
      ascending: false,
      field: 'type',
    },
    nearbyPreForeclosures: {
      ascending: false,
      field: 'type',
    },
    neighbors: {
      ascending: false,
      field: 'type',
    },
    properties: {
      ascending: false,
      field: 'type',
    },
  },
});

const propertiesData = ({ properties = [], ...rest }) => (
  fromJS(rest).set('properties', fromJS(properties).map((property, i) => {
    const lastSaleAmount = property.getIn(['lastSale', 'saleAmount'], null);
    const lastSaleDate = property.getIn(['lastSale', 'saleDate'], null);
    return property.merge({
      resultIndex: i + 1,
      lastSaleAmount,
      lastSaleDate,
    });
  }))
);

const getFieldValue = (data, fieldName = null) => {
  if (!fieldName) return null;

  if (fieldName === 'streetAddress') {
    const streetAddress = data.getIn(['address', 'streetAddress'], '');
    const cityName = data.getIn(['address', 'cityName'], '');
    const address = `${streetAddress} ${cityName}`;
    return address.trim();
  }

  if (fieldName === 'unitNumber') {
    return data.getIn(['address', 'unitNumber'], '');
  }

  return data.get(fieldName, null);
};

const sortData = (data, fieldName, ascending = true) => (
  data.sort((a, b) => {
    const left: any = getFieldValue(a, fieldName);
    const right: any = getFieldValue(b, fieldName);

    if (left < right) { return ascending ? -1 : 1; }
    if (left > right) { return ascending ? 1 : -1; }

    return 0;
  })
);

const propertySorting = (state, data) => {
  const newState = state.updateIn(
    ['sorting', data.tableName],
    (sorting) => {
      let ascending = data.ascending || sorting.get('ascending');
      if (!data.ascending && sorting.get('field') === data.fieldName) {
        ascending = !ascending;
      }

      return fromJS({
        ascending,
        field: data.fieldName,
      });
    });

  return newState.updateIn(
    ['data', data.tableName],
    table => (
      sortData(table, data.fieldName, newState.getIn(['sorting', data.tableName, 'ascending']))
    ),
  );
};

export default function (state: Map<string, *> = defaultState, action: Object): Map<string, *> {
  switch (action.type) {
    case ActionType.LOAD_PROPERTIES:
      return state.merge({
        data: {
          properties: [],
        },
        activeTabIndex: 0,
        activeTab: 'neighbors',
        propertiesLoading: true,
        propertiesError: null,
      });

    case ActionType.LOAD_PROPERTIES_SUCCESS:
      return state.merge({
        propertiesLoading: false,
        propertiesError: null,
        data: propertiesData(action.response),
      });

    case ActionType.LOAD_PROPERTIES_ERROR:
      return state.merge({
        propertiesLoading: false,
        propertiesError: action.error,
      });

    case ActionType.CHANGE_PROPERTY_SORTING:
      return propertySorting(state, action.data);

    case ActionType.CLEAR_PROPERTIES:
      return defaultState;

    case ActionType.SET_ACTIVE_TAB:
      return state.set('activeTabIndex', action.tabIndex).set('activeTab', action.tabName);

    default:
      return state;
  }
}
