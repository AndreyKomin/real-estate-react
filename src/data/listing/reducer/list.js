/** @flow */
import { Map, fromJS, is, List } from 'immutable';

import { triggerByFilter } from 'utils/triggerValueInList';
import getSearchQueryId from 'data/utils/getSearchQueryId';

import * as ActionType from '../actions';
import { Overlay, Display } from '../constants';


const DEFAULT_LIMIT = 10;
const emptyList = List();

const defaultState = fromJS({
  items: [],
  lastRequestedSearch: '',
  searchQueryId: '',
  selected: [],
  allSelectedState: false,
  overlay: Overlay.NONE,
  display: Display.PICTURE,
  page: 1,
  rangeFrom: 1,
  rangeTo: 4,
  rangeEnabled: false,
  limit: DEFAULT_LIMIT,
  totalCount: 0,
  isAllPropertiesLoading: false,
});

function getTotalCount(action) {
  const { response }: { limit: number, response: Array<Object> } = action;
  if (!response.length) return 0;
  const { resultCount = 0 }: { resultCount: number } = response[0];
  return resultCount;
}

function unifyId(items) {
  return fromJS(items).map(item => item.set('id', `${item.get('type')}${item.get('id')}`));
}

function updateSelected(state, action) {
  const searchQueryId = getSearchQueryId(action.searchQuery);
  if (state.get('searchQueryId') === searchQueryId) return state;
  return state.set('selected', defaultState.get('selected')).set('searchQueryId', searchQueryId);
}

function filterSelected(fromList, value) {
  return fromList.get('id') === value.get('id');
}

function getSelected(items: Map<string, *>, selected: List<Map<string, *>> = List(), propertyId) {
  return selected.find(item => item.get('id') === propertyId) || items.find(item => item.get('id') === propertyId);
}

function listingListReducer(state: Map<string, *> = defaultState, action: Object): Map<string, *> {
  switch (action.type) {
    case ActionType.LOAD_LISTINGS:
      return updateSelected(state.merge({
        isLoading: true,
        error: null,
        items: [],
        lastRequestedSearch: action.searchIdentifier,
        limit: action.limit,
        page: action.page,
        rangeFrom: action.rangeFrom || null,
        rangeTo: action.rangeTo || null,
        rangeEnabled: action.rangeEnabled || false,
      }), action);


    case ActionType.LOAD_LISTINGS_SUCCESS: {
      if (!is(fromJS(action.searchIdentifier), state.get('lastRequestedSearch'))) {
        return state;
      }

      const listhubIds = (action.response || []).filter(l => l.listhubId).map(l => ({ lkey: l.listhubId }));
      if (listhubIds.length) window.listHub('submit', 'SEARCH_DISPLAY', listhubIds);

      return state.merge({
        items: unifyId(action.response),
        totalCount: getTotalCount(action),
        isLoading: false,
        error: null,
      });
    }
    case ActionType.LOAD_LISTINGS_ERROR:
      return state.merge({
        isLoading: false,
        error: action.error,
      });

    case ActionType.REMOVE_LISTING_SELECT:
      return state.merge({ selected: [] });

    case ActionType.LOAD_ALL_LISTINGS:
      return state.set('isAllPropertiesLoading', true);

    case ActionType.LOAD_ALL_LISTINGS_SUCCESS:
      return state.merge({
        selected: fromJS(action.response),
        isAllPropertiesLoading: false,
      });

    case ActionType.LOAD_ALL_LISTINGS_ERROR:
      return state.set('isAllPropertiesLoading', false);

    case ActionType.MARK_ALL_LISTINGS_AS_SELECTED:
      return state.set('allSelectedState', true);

    case ActionType.CLEAR_SELECTED_LISTINGS:
      return state.merge({
        selected: emptyList,
        allSelectedState: false,
      });

    case ActionType.TRIGGER_LISTING_SELECT:
      return state.update('selected', selected => triggerByFilter(
        ((selected: any): List<*>),
        filterSelected,
        getSelected(((state.get('items'): any): Map<string, *>), ((selected: any): List<*>), action.id),
      ));

    case ActionType.CHANGE_DISPLAY_TYPE:
      return state.set('display', action.displayType);

    case ActionType.CHANGE_OVERLAY_TYPE:
      return state.update('overlay', overlay => (action.overlayType === overlay ? Overlay.NONE : action.overlayType));

    case ActionType.HIDE_OVERLAY:
      return state.set('overlay', Overlay.NONE);

    case ActionType.CHANGE_PAGE:
      return state.set('page', action.page);

    case ActionType.CHANGE_RANGE:
      return state.merge(action.range);

    case ActionType.CLEAR_LISTINGS:
      return defaultState;

    default:
      return state;
  }
}

export default listingListReducer;
