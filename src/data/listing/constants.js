/** @flow */
import { fromJS } from 'immutable';

import type {
  ListingType,
  PropertyType,
  ListingTypeCustom,
  MlsListingStatusType,
  DataTypeType,
  DocumentTypeCodeType,
  ForeclosureStatusType,
  LienTypeCodeType,
  FullTableType,
  LiensTableType,
  DefaultType,
  ActivePinType,
  NeighborsColorType,
  DefaultColorType,
  OverlayType,
  DisplayType,
  EstimatedValueGrowthPeriodType,
} from './flow-types';


export type {
  ListingType,
  PropertyType,
  ListingTypeCustom,
  FullTableType,
  LiensTableType,
  DefaultType,
  ActivePinType,
  NeighborsColorType,
  DefaultColorType,
  OverlayType,
  DisplayType,
  DataTypeType,
};

export const Type: { [ListingType]: ListingType } = {
  M: 'M',
  P: 'P',
  F: 'F',
  L: 'L',
  C: 'C',
  E: 'E',
  V: 'V',
  R: 'R',
  B: 'B',
  D: 'D',
  A: 'A',
//  W: 'W',
};

export const PropTypeCode: { [PropertyType]: PropertyType } = {
  SFR: 'SFR',
  CONDO: 'CONDO',
  MFR: 'MFR',
  LAND: 'LAND',
  OTHER: 'OTHER',
};

export const PhotoComingSoonUrl: string = 'http://images.listing.realestate.com/photo_coming_soon_sm8.jpg';

export const TypeCustom: ListingTypeCustom = 'CUSTOM';
export const FullTable: FullTableType = 'FULL';
export const LiensTable: LiensTableType = 'LIENS';
export const Active: ActivePinType = 'ACTIVE';
export const Neighbors: NeighborsColorType = 'NEIGHBORS';
export const DefaultColor: DefaultColorType = 'DEFAULT_COLOR';
export const Default: DefaultType = 'DEFAULT';

export const Name: { [ListingType | ListingTypeCustom]: string } = {
  M: 'MLS',
  P: 'Pre-Foreclosures',
  F: 'Foreclosures',
  L: 'Liens',
  C: 'Cash Buyers',
  E: 'High Equity',
  V: 'Vacant',
  R: 'Free & Clear',
  B: 'Bankruptcy',
  D: 'Divorce',
  A: 'Auctions',
//  W: 'Non-Owner Occupied',
  [TypeCustom]: 'Custom',
};

export const PropName: { [PropertyType]: string } = {
  SFR: 'Single Family',
  CONDO: 'Condo/Townhouse',
  MFR: 'Multi-Family',
  LAND: 'Land',
  OTHER: 'Other',
};

export const OwnerCorporate: Array<{ label: string, value: ?boolean }> = [
  { label: 'Corporate', value: true },
  { label: 'Individual', value: false },
  { label: 'Any', value: null },
];

export const MlsListingStatus: Array<{ label: string, value: MlsListingStatusType }> = [
  { label: 'Active', value: 'ACTIVE' },
  { label: 'Contingent', value: 'CONTINGENT' },
  { label: 'Fail', value: 'FAIL' },
  { label: 'Pending', value: 'PENDING' },
  { label: 'Sold', value: 'SOLD' },
  { label: 'Unknown', value: 'UNKNOWN' },
];

export const ListingTypes: Array<{ label: string, value: string }> = [
  { label: 'MLS', value: 'M' },
  { label: 'Pre-Foreclosures', value: 'P' },
  { label: 'Foreclosures', value: 'F' },
  { label: 'Liens', value: 'L' },
  { label: 'Cash Buyers', value: 'C' },
  { label: 'High Equity', value: 'E' },
  { label: 'Vacant', value: 'V' },
  { label: 'Free & Clear', value: 'R' },
  { label: 'Bankruptcy', value: 'B' },
  { label: 'Divorce', value: 'D' },
  { label: 'Auctions', value: 'A' },
//  { label: 'Non-Owner Occupied', value: 'W' },
];

export const LoanType = [
  { label: 'ARM', value: 'ARM' },
  { label: 'Assumption', value: 'ASSUMPTION' },
  { label: 'Balloon', value: 'BALLOON' },
  { label: 'Closed-End Mortgage', value: 'CLOSED_END' },
  { label: 'Commercial', value: 'COMMERCIAL' },
  { label: 'Credit Line (Revolving)', value: 'CREDIT' },
  { label: 'Farmers Home Administration', value: 'FARMER' },
  { label: 'FHA', value: 'FHA' },
  { label: 'Land Contract', value: 'LAND_CONTRACT' },
  { label: 'Mortgage Modification', value: 'MORTGAGE_MODIFICATION' },
  { label: 'Negative Amortization', value: 'NEGATIVE_AMORTIZATION' },
  { label: 'New Conventional', value: 'CONVENTIONAL' },
  { label: 'Reverse Mortgage', value: 'REVERSE_MORTGAGE' },
  { label: 'Seller Take-Back', value: 'SELLER_TAKE_BACK' },
  { label: 'Stand Alone First', value: 'STAND_ALONE_FIRST' },
  { label: 'Stand Alone Refi', value: ' STAND_ALONE_REFI' },
  { label: 'Stand Alone Second', value: 'STAND_ALONE_SECOND' },
  { label: 'State Veterans', value: 'STATE_VETERANS' },
  { label: 'Trade', value: 'TRADE' },
  { label: 'USDA', value: 'USDA' },
  { label: 'VA', value: 'VA' },
];

export const MlsListingStatusList = fromJS(MlsListingStatus);
export const MlsListingStatusNames = MlsListingStatus.reduce((m, s) => ({ ...m, [s.value]: s.label }), {});
export const LoanTypeList = fromJS(LoanType);
export const MlsListingStatusNoneList = fromJS([{ label: 'Off Market', value: 'NONE' }].concat(MlsListingStatus));
export const ListingTypesList = fromJS(ListingTypes);
export const ListingTypesSortedList = fromJS(ListingTypes.sort((a, b) => a.label.toLowerCase().localeCompare(b.label.toLowerCase())));

export const DocumentTypeCode: Array<{ label: string, value: DocumentTypeCodeType }> = [
  { label: 'Notice of Trustee\'s Sale', value: 'NT' },
  { label: 'Notice of Default', value: 'ND' },
  { label: 'Lis Pendens', value: 'LP' },
  { label: 'Notice of Rescission', value: 'NR' },
  { label: 'Trustee\'s Deed', value: 'TD' },
  { label: 'Other', value: 'OTHER' },
];

export const ForeclosureStatus: Array<{ label: string, value: ForeclosureStatusType }> = [
  { label: 'Pre-Foreclosure', value: 'PRE_FORECLOSURE' },
  { label: 'Foreclosure', value: 'FORECLOSURE' },
  { label: 'None', value: 'NONE' },
];

export const LienTypeCode: Array<{ label: string, value: LienTypeCodeType }> = [
  { label: 'Tax Lien', value: 'TAX' },
  { label: 'HOA Lien', value: 'HOA' },
  { label: 'Mechanics Lien', value: 'MECHANIC' },
  { label: 'Utility Lien', value: 'UTILITY' },
  { label: 'Other', value: 'OTHER' },
];

export const EstimatedValueGrowthPeriod: Array<{ label: string, value: EstimatedValueGrowthPeriodType }> = [
  { label: 'One month', value: 'ONE_MONTH' },
  { label: 'Three month', value: 'THREE_MONTH' },
  { label: 'Six months', value: 'SIX_MONTH' },
  { label: 'One year', value: 'ONE_YEAR' },
];

export const OwnerOccupied: Array<{ label: string, value: ?boolean }> = [
  { label: 'Yes', value: true },
  { label: 'No', value: false },
  { label: 'Any', value: null },
];

export const BelowMarketPrice: Array<{ label: string, value: ?boolean }> = [
  { label: 'Yes', value: true },
  { label: 'No', value: false },
  { label: 'Any', value: null },
];

export const LienTypeCodes: Array<{ label: string, value: ?string }> = [
  { label: 'Lean Type', value: null },
  { label: 'Tax Lien', value: 'TAX' },
  { label: 'Bankruptcy', value: 'BANKRUPTCY' },
  { label: 'Divorce', value: 'DIVORCE' },
  { label: 'Utility Lien', value: 'UTILITY' },
  { label: 'Other', value: 'OTHER' },
];

export const Color: { [ListingType | ActivePinType | DefaultColorType]: string } = {
  M: '#FFA715',
  P: '#FFDC55',
  A: '#7CB247',
  F: '#39D4A9',
  C: '#0FB474',
  V: '#AF66BD',
  E: '#8360EF',
  R: '#666666',
  L: '#FF6060',
  D: '#B01024',
  B: '#DD33FF',
  [Neighbors]: '#7FEAFF',
  [Active]: '#0000FF',
  [DefaultColor]: '#666666',
};

export const Display: { [string]: DisplayType } = {
  PICTURE: 'PICTURE',
  LIST: 'LIST',
  NONE: null,
};

export const Overlay: { [string]: OverlayType } = {
  CHARTS: 'CHARTS',
  TABLE: 'TABLE',
  NONE: null,
};

export const DataType: { [string]: DataTypeType } = {
  PRICE: 'PRICE',
  DATE: 'DATE',
  NONE: null,
};

export const Fields: { [string]: string } = {
  TYPE: 'type',
  ADDRESS: 'address',
  UNIT: 'unitNumber',
  ADDRESS_FIELD_NAME: 'streetAddress',
  BEDROOMS: 'bedrooms',
  BATHROOMS: 'bathrooms',
  LISTING_TYPE: 'listingType',
  LISTING_PRICE: 'listingAmount',
  LISTING_DATE: 'listingDate',
  STATUS: 'status',
  DOCUMENT_TYPE: 'documentType',
  DEFAULT_AMOUNT: 'defaultAmount',
  LISTING_AMOUNT: 'listingAmount',
  UPDATE_DATE: 'listingDate',
  ESTIMATE_VALUE: 'estimatedValue',
  ESTIMATE_EQUITY: 'estimatedEquity',
  SALE_PRICE: 'lastSaleAmount',
  SALE_DATE: 'lastSaleDate',
  CITY: 'address.cityName',
  CITY_FIELD_NAME: 'cityName',
  STATE: 'address.stateCode',
  STATE_FIELD_NAME: 'stateCode',
  ZIP: 'address.zip',
  ZIP_FIELD_NAME: 'zip',
  SQUARE_FEET: 'squareFeet',
  SQUARE_FEET_FIELD_NAME: 'squareFeet',
  PROPERTY_TYPE: 'propertyType',
  PROPERTY_TYPE_FIELD_NAME: 'propertyTypeCode',
  COUNTER: 'resultIndex',
  LIEN_TYPE: 'lienDocumentType',
  LIEN_DATE: 'listingDate',
  LIEN_AMOUNT: 'listingAmount',
  MLS_LISTING_DATE: 'mlsListingDate',
  MLS_LISTING_AMOUNT: 'mlsListingAmount',
};

export const Tables: { [ListingType | ListingTypeCustom | FullTableType | LiensTableType | DefaultType ]: string[] } = {
  M: [
    'ADDRESS',
    'BEDROOMS',
    'BATHROOMS',
    'LISTING_PRICE',
    'LISTING_DATE',
    'STATUS',
  ],
  P: [
    'DOCUMENT_TYPE',
    'ADDRESS',
    'BEDROOMS',
    'BATHROOMS',
    'DEFAULT_AMOUNT',
    'UPDATE_DATE',
  ],
  F: [
    'DOCUMENT_TYPE',
    'ADDRESS',
    'BEDROOMS',
    'BATHROOMS',
    'DEFAULT_AMOUNT',
    'UPDATE_DATE',
  ],
  C: [
    'ADDRESS',
    'BEDROOMS',
    'BATHROOMS',
    'ESTIMATE_VALUE',
    'SALE_PRICE',
    'SALE_DATE',
  ],
  V: [
    'ADDRESS',
    'BEDROOMS',
    'BATHROOMS',
    'ESTIMATE_VALUE',
    'ESTIMATE_EQUITY',
    'SALE_DATE',
  ],
  E: [
    'ADDRESS',
    'BEDROOMS',
    'BATHROOMS',
    'ESTIMATE_VALUE',
    'ESTIMATE_EQUITY',
    'SALE_DATE',
  ],
  R: [
    'ADDRESS',
    'BEDROOMS',
    'BATHROOMS',
    'ESTIMATE_VALUE',
    'SALE_PRICE',
    'SALE_DATE',
  ],
  A: [
    'ADDRESS',
    'BEDROOMS',
    'BATHROOMS',
    'ESTIMATE_VALUE',
    'DEFAULT_AMOUNT',
    'UPDATE_DATE',
  ],
  L: [
    'LIEN_TYPE',
    'ADDRESS',
    'BEDROOMS',
    'BATHROOMS',
    'LIEN_AMOUNT',
    'LIEN_DATE',
  ],
  [Default]: [
    'ADDRESS',
    'BEDROOMS',
    'BATHROOMS',
    'ESTIMATE_VALUE',
    'ESTIMATE_EQUITY',
    'SALE_DATE',
  ],
  [TypeCustom]: [
    'TYPE',
    'ADDRESS',
    'BEDROOMS',
    'BATHROOMS',
    'ESTIMATE_VALUE',
    'SALE_DATE',
  ],
  [FullTable]: [
    'TYPE',
    'ADDRESS',
    'CITY',
    'STATE',
    'ZIP',
    'BEDROOMS',
    'BATHROOMS',
    'PROPERTY_TYPE',
    'ESTIMATE_VALUE',
    'MLS_LISTING_DATE',
    'MLS_LISTING_AMOUNT',
    'STATUS',
    'DOCUMENT_TYPE',
    'DEFAULT_AMOUNT',
    'UPDATE_DATE',
    'SALE_PRICE',
    'SALE_DATE',
    'ESTIMATE_EQUITY',
  ],
  [LiensTable]: [
    'LIEN_TYPE',
    'ADDRESS',
    'CITY',
    'STATE',
    'ZIP',
    'BEDROOMS',
    'BATHROOMS',
    'PROPERTY_TYPE',
    'ESTIMATE_VALUE',
    'LIEN_AMOUNT',
    'LIEN_DATE',
    'STATUS',
    'DOCUMENT_TYPE',
    'DEFAULT_AMOUNT',
    'UPDATE_DATE',
    'SALE_PRICE',
    'SALE_DATE',
    'ESTIMATE_EQUITY',
  ],
};

export const PicViewFields: {
  [ListingType | ListingTypeCustom | FullTableType ]: Array<{ label: string, value: string }>
} = {
  M: [
    {
      label: 'Listing Price',
      value: 'LISTING_PRICE',
      type: 'PRICE',
    },
    {
      label: 'Listing Date',
      value: 'LISTING_DATE',
      type: 'DATE',
    },
    {
      label: 'Status',
      value: 'STATUS',
      type: 'NONE',
    },
  ],
  F: [
    {
      label: 'Doc. Type',
      value: 'DOCUMENT_TYPE',
      type: 'NONE',
    },
    {
      label: 'Default Amount',
      value: 'DEFAULT_AMOUNT',
      type: 'PRICE',
    },
    {
      label: 'Last Updated',
      value: 'UPDATE_DATE',
      type: 'DATE',
    },
  ],
  A: [
    {
      label: 'Est. Value',
      value: 'ESTIMATE_VALUE',
      type: 'PRICE',
    },
    {
      label: 'Default Amount',
      value: 'DEFAULT_AMOUNT',
      type: 'PRICE',
    },
    {
      label: 'Last Updated',
      value: 'UPDATE_DATE',
      type: 'DATE',
    },
  ],
  P: [
    {
      label: 'Doc. Type',
      value: 'DOCUMENT_TYPE',
      type: 'NONE',
    },
    {
      label: 'Default Amount',
      value: 'DEFAULT_AMOUNT',
      type: 'PRICE',
    },
    {
      label: 'Last Updated',
      value: 'UPDATE_DATE',
      type: 'DATE',
    },
  ],
  C: [
    {
      label: 'Est. Value',
      value: 'ESTIMATE_VALUE',
      type: 'PRICE',
    },
    {
      label: 'Sale Price',
      value: 'SALE_PRICE',
      type: 'PRICE',
    },
    {
      label: 'Sale Date',
      value: 'SALE_DATE',
      type: 'DATE',
    },
  ],
  R: [
    {
      label: 'Est. Value',
      value: 'ESTIMATE_VALUE',
      type: 'PRICE',
    },
    {
      label: 'Sale Price',
      value: 'SALE_PRICE',
      type: 'PRICE',
    },
    {
      label: 'Sale Date',
      value: 'SALE_DATE',
      type: 'DATE',
    },
  ],
  E: [
    {
      label: 'Est. Value',
      value: 'ESTIMATE_VALUE',
      type: 'PRICE',
    },
    {
      label: 'Est. Equity',
      value: 'ESTIMATE_EQUITY',
      type: 'PRICE',
    },
    {
      label: 'Sale Date',
      value: 'SALE_DATE',
      type: 'DATE',
    },
  ],
  V: [
    {
      label: 'Est. Value',
      value: 'ESTIMATE_VALUE',
      type: 'PRICE',
    },
    {
      label: 'Est. Equity',
      value: 'ESTIMATE_EQUITY',
      type: 'PRICE',
    },
    {
      label: 'Sale Date',
      value: 'SALE_DATE',
      type: 'DATE',
    },
  ],
  L: [
    {
      label: 'Lien Type',
      value: 'LIEN_TYPE',
      type: 'NONE',
    },
    {
      label: 'Lien Date',
      value: 'LISTING_DATE',
      type: 'DATE',
    },
    {
      label: 'Lien Amount',
      value: 'LISTING_PRICE',
      type: 'PRICE',
    },
  ],
  [TypeCustom]: [
    {
      label: 'Est. Value',
      value: 'ESTIMATE_VALUE',
      type: 'PRICE',
    },
    {
      label: 'Est. Equity',
      value: 'ESTIMATE_EQUITY',
      type: 'PRICE',
    },
    {
      label: 'Sale Date',
      value: 'SALE_DATE',
      type: 'DATE',
    },
  ],
};
