/** @flow */
export type BingPointType = {
  type: 'Point',
  coordinates: [
    number,
    number,
    ],
};

export type BingBboxType = Array<number>;

export type BingAddressType = {
  addressLine?: string,
  locality?: string,
  adminDistrict?: string,
  adminDistrict2?: string,
  formattedAddress: string,
  postalCode?: string,
  countryRegion?: string,
  countryRegionIso2?: string,
  landmark?: string,
};

export type BingEntityType = 'CountryRegion'
  | 'AdminDivision1'
  | 'AdminDivision2'
  | 'Postcode1'
  | 'Postcode2'
  | 'Postcode3'
  | 'Postcode4'
  | 'Neighborhood'
  | 'PopulatedPlace'
  | 'River' // undocumented
  | 'PointOfInterest' // undocumented
  | 'Island' // undocumented
  | 'Plateau'; // undocumented

export type BingQueryParseValueType = 'AddressLine'
  | 'Locality'
  | 'AdminDistrict'
  | 'AdminDistrict2'
  | 'PostalCode'
  | 'CountryRegion'
  | 'Landmark';

export type BingConfidenceType = 'High'
  | 'Low'
  | 'Medium';

export type BingMatchCodesType = 'Good'
  | 'Ambiguous'
  | 'UpHierarchy';

export type BingLocationType = {
  name?: string,
  point: BingPointType,
  bbox?: BingBboxType,
  entityType: BingEntityType,
  address: BingAddressType,
  confidence: BingConfidenceType,
  matchCodes: BingMatchCodesType[],
  queryParseValues?: BingQueryParseValueType,
  geocodePoints: Array<*>
};

