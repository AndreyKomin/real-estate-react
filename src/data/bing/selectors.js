/** @flow */
import type { Map, List } from 'immutable';
import { createSelector } from 'reselect';


export const getBing = (state: Map<string, Map<string, *>>): Map<string, *> => state.get('bing');

export const getIsRecent = createSelector(
  getBing,
  bing => ((bing.get('isRecent'): any): boolean),
);

export const getIsLoading = createSelector(
  getBing,
  bing => ((bing.get('isLoading'): any): boolean),
);

export const getValue = createSelector(
  getBing,
  bing => ((bing.get('value'): any): string),
);

export const selectLabel = createSelector(
  getBing,
  bing => ((bing.get('label'): any): string),
);

export const getOptions = createSelector(
  getBing,
  bing => ((bing.get('options'): any): List<Map<string, string | Object>>),
);

export const getSearchValue = createSelector(
  getBing,
  bing => (bing.get('searchValue'): any),
);
