/** @flow */
import type { Map } from 'immutable';
import { BING_ACTION } from 'store/middleware/bing';


export const CHANGE_BING_SEARCH_VALUE = Symbol('CHANGE_BING_SEARCH_VALUE');

export function changeValue({ value }: { value: Object | string }) {
  return {
    type: CHANGE_BING_SEARCH_VALUE,
    value,
  };
}


export const CHANGE_BING_SEARCH_LABEL = Symbol('CHANGE_BING_SEARCH_LABEL');

export function changeLabel(label: string) {
  return {
    type: CHANGE_BING_SEARCH_LABEL,
    label,
  };
}


export const SUGGESTIONS_LOAD = Symbol('SUGGESTIONS_LOAD');
export const SUGGESTIONS_LOAD_SUCCESS = Symbol('SUGGESTIONS_LOAD_SUCCESS');
export const SUGGESTIONS_LOAD_ERROR = Symbol('SUGGESTIONS_LOAD_ERROR');

export function getSuggestions({ query }: { query: ?string }): Object {
  const result: any = {};
  result[BING_ACTION] = {
    method: 'Location.getSuggestions',
    query: `${query || ''}`,
    startType: SUGGESTIONS_LOAD,
    successType: SUGGESTIONS_LOAD_SUCCESS,
    errorType: SUGGESTIONS_LOAD_ERROR,
    successParams: { query },
    maxCount: 2,
  };

  return result;
}


export const SAVE_RECENT_BING_SEARCH = Symbol('SAVE_RECENT_BING_SEARCH');

export function saveRecent({ recent }: { recent: Array<Object> }) {
  return {
    type: SAVE_RECENT_BING_SEARCH,
    recent,
  };
}


export const SHOW_RECENT_BING_SEARCH = Symbol('SHOW_RECENT_BING_SEARCH');

export function showRecent() {
  return {
    type: SHOW_RECENT_BING_SEARCH,
  };
}


export const CHANGE_BING_SEARCH_PARAMETER = Symbol('CHANGE_BING_SEARCH_PARAMETER');

export function changeSearchParameter({ name, value }: { name: string, value: * }) {
  return {
    type: CHANGE_BING_SEARCH_PARAMETER,
    name,
    value,
  };
}


export const SET_BING_SEARCH_VALUE = Symbol('SET_BING_SEARCH_VALUE');

export function setSearchValue(value: Map<string, *>) {
  return {
    type: SET_BING_SEARCH_VALUE,
    value,
  };
}

export const CLEAR_BING_SEARCH = Symbol('CLEAR_BING_SEARCH');

export function clearBingSearch() {
  return {
    type: CLEAR_BING_SEARCH,
  };
}
