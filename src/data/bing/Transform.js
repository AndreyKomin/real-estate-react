/** @flow */
import type { SuggestedLocationType } from 'types';
import ParamTransform from 'data/utils/ParamTransform';
import KeyTransform from 'data/utils/KeyTransform';

import getSearchSource from 'data/search/getSearchSource';

import type { BingLocationType } from './flow-types';


const keyTransform = new KeyTransform([
  { apiName: 'point', jsName: 'point' },
]);

const addressKeyTransform = new KeyTransform([
  { apiName: 'postalCode', jsName: 'zip' },
  { apiName: 'locality', jsName: 'cityName' },
  { apiName: 'adminDistrict', jsName: 'stateCode' },
  { apiName: 'addressLine', jsName: 'streetAddress' },
]);

const paramTransform = new ParamTransform([
  {
    test: /^bbox$/,
    toJS: JSON.stringify,
    toAPI: JSON.parse,
  },
  {
    test: /^point$/,
    toJS: (value: { coordinates: number[] }) => ({ latitude: value.coordinates[0], longitude: value.coordinates[1] }),
    toAPI: ({ latitude, longitude }: { latitude: number, longitude: number }) => ({ type: 'Point', coordinates: [latitude, longitude] }),
  },
  {
    test: /^address$/,
    toJS: address => addressKeyTransform.toJS(address),
    toAPI: address => addressKeyTransform.toAPI(address),
  },
]);

export default {
  toAPI: (value: SuggestedLocationType): BingLocationType => {
    const { zip, formattedAddress, cityName, stateCode, streetAddress, ...rest } = (value: any);
    rest.address = rest.address || {};
    rest.address.zip = zip;
    rest.address.cityName = cityName;
    rest.address.stateCode = stateCode;
    rest.address.streetAddress = streetAddress;
    rest.address.formattedAddress = formattedAddress;
    return paramTransform.toAPI(keyTransform.toAPI(rest));
  },
  toJS: (value: BingLocationType): SuggestedLocationType => {
    const { address, ...rest } = keyTransform.toJS(paramTransform.toJS(value));
    const source = getSearchSource(value);

    return { ...rest, source, ...address };
  },
};
