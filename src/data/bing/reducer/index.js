/** @flow */
import { fromJS, Map } from 'immutable';
import { countyMap } from 'data/search/constants';
import { AUTHENTICATION_SUCCESS } from 'data/user';

// import triggerValueInList from 'utils/triggerValueInList';
import type { BingLocationType } from '../flow-types';
import Transform from '../Transform';

import getItemUnifier from '../getItemUnifier';
import * as ActionType from '../actions';


const defaultState = fromJS({
  isLoading: false,
  isRecent: false,
  options: [],
  recent: [],
  value: '',
  fullData: '',
  label: '',
  searchValue: {},
  countySearchEnabled: false,
});

function getLabel(state, action) {
  return ((action.value || {}).address || {}).formattedAddress || '';
}

function getOptions(state, options: Array<BingLocationType>, label: string = '', query: string = '') {
  const bingResults = options.map((fullData: BingLocationType) => {
    const { address } = fullData;

    // Alter response for multi zip searches.
    if (label !== '') {
      if (/^\d{5}(,\d{5})+$/.test(label.replace(/\s/g, ''))) {
        address.postalCode = label.replace(/,\s*/g, ', ');
        address.formattedAddress = label;
      } else {
        // See if there was a unit number in the original query, and add it back in if so.
        const match = /#[\w-]+\b/i.exec(label);
        if (match) {
          address.addressLine = `${address.addressLine || ''} ${match[0]}`;
          address.formattedAddress = `${address.addressLine}, ${address.locality || ''}, ${address.adminDistrict || ''} ${address.postalCode || ''}`;
        }
      }
    }

    return fullData;
  });

  const localResults = [];

  // Check for county matches
  if (query !== '') {
    const match = /^((#[a-z0-9\s-]+),)?([a-z\s'-]+)(,\s*([a-z]{2}))?$/i.exec(query);

    if (match && match[3] && match[5]) {
      const county = countyMap[`${match[3].trim().replace(/\s+county$/i, '')}-${match[5].trim()}`.toLowerCase()];
      if (county) {
        const apn = match[2] ? match[2].trim() : null;
        if (apn || state.get('countySearchEnabled')) {
          const name = `${apn ? `${apn}, ` : ''}${county.name}`;
          localResults.push({ name, address: { countyId: county.id, apn: apn ? apn.substr(1) : null, formattedAddress: name } });
        }
      }
    }
  }

  // localResults.push({ name: 'The OC, CA', address: { countyId: 256, formattedAddress: 'Orange County, CA' } });
  // localResults.push({ name: 'APN: blah, The OC, CA', address: { countyId: 256, apn: 'blah', formattedAddress: 'APN: blah, Orange County, CA' } });

  return fromJS(bingResults.concat(localResults).map(fullData => ({
    label: fullData.address.formattedAddress,
    value: getItemUnifier(fullData),
    fullData,
  })));
}

export default function reducer(state: Map<string, *> = defaultState, action: Object): Map<string, *> {
  switch (action.type) {
    case AUTHENTICATION_SUCCESS:
      return state.merge({ countySearchEnabled: action.response.profile.countySearchEnabled });

    case ActionType.CHANGE_BING_SEARCH_VALUE:
      return state.set('value', getItemUnifier(action.value))
        .set('fullData', fromJS(action.value))
        .set('label', getLabel(state, action))
        .merge({ searchValue: (Transform.toJS(action.value): any) });

    case ActionType.SUGGESTIONS_LOAD:
      return state.set('isLoading', true);

    case ActionType.SUGGESTIONS_LOAD_SUCCESS:
      return state.set('isRecent', false)
        .set('isLoading', false)
        .set('options', getOptions(state, action.response, state.get('label') || '', action.query));

    case ActionType.SUGGESTIONS_LOAD_ERROR:
      return state.set('isLoading', false);

    case ActionType.CHANGE_BING_SEARCH_LABEL:
      return state.set('label', action.label);

    case ActionType.SAVE_RECENT_BING_SEARCH:
      return state.set('recent', fromJS(action.recent));

    case ActionType.SHOW_RECENT_BING_SEARCH:
      return state.set('isRecent', true)
        .set('options', getOptions(state, state.getIn(['recent'], defaultState.get('recent')).toJS()));

    case ActionType.SET_BING_SEARCH_VALUE:
      return state.set('searchValue', action.value)
        .merge((Transform.toAPI(action.value.toJS()): any));

    case ActionType.CLEAR_BING_SEARCH:
      return defaultState
        .set('options', state.get('options'))
        .set('recent', state.get('recent'))
        .set('countySearchEnabled', state.get('countySearchEnabled'))
        .set('isRecent', state.get('isRecent'));

    default:
      return state;
  }
}
