/** @flow */
export const NO_UNIFIER = Symbol('NO_UNIFIER');

export default function getItemUnifier(value: ?{ bbox?: Array<number>, name?: string } | ''): string | typeof NO_UNIFIER {
  if (value === '' || value === undefined || value === null || !value.name) return NO_UNIFIER;
  return JSON.stringify(value.bbox || value.name);
}
