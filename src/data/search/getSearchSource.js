/** @flow */
import { isZipAddress, isPropertyAddress } from 'Bing/Location';

import { SearchType } from './constants';


export default function getSearchSource(address: *) {
  if (typeof address === 'string') {
    if (address === 'PROPERTY') return SearchType.PROPERTY;
    if (address === 'LISTING') return SearchType.LISTING;
  } else {
    if (isPropertyAddress(address)) return SearchType.PROPERTY;
    if (isZipAddress(address)) return SearchType.LISTING;
  }
  return SearchType.NONE;
}
