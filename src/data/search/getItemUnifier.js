/** @flow */
export default function getItemUnifier(value: { bbox?: Array<number>, name: string }): string {
  if (!value) return 'NOT_UNIQ';
  return JSON.stringify(value.bbox || value.name);
}
