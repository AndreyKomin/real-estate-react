/** @flow */
import { type Map } from 'immutable';


export default function getSearchQuery(query: ?Map<string, *>) {
  const result = {
    searchParams: {},
    searchValue: {},
  };

  if (query) {
    const jsQuery = query.toJS();
    const { searchValue = jsQuery, searchParams = jsQuery } = jsQuery;
    result.searchValue = searchValue;
    result.searchParams = searchParams;
  }

  return result;
}
