/** @flow */
import { Map, fromJS, List } from 'immutable';
import { createSelector } from 'reselect';

import { SearchSource, PositionFields } from './constants';


const defaultSorting = fromJS({
  ascending: false,
  field: 'type', // TODO remove magic string
});

const emptyMap = Map({});
const emptyList = List();

export const selectSearch = createSelector(
  state => state,
  state => state.getIn(['search'], emptyMap),
);

export const selectCurrentSearch = createSelector(
  selectSearch,
  search => search.get('current', emptyMap),
);

export const selectAvailableListingTypes = createSelector(
  selectCurrentSearch,
  search => search.get('availableListingTypes'),
);

export const selectCurrentSearchPosition = createSelector(
  selectCurrentSearch,
  search => search.filter((value, field) => PositionFields.includes(field)),
);

export const selectSorting = (state: any) => {
  const sorting = state.getIn(['search', 'current', 'sorting'], defaultSorting);
  const field = sorting.get('field') === undefined ? defaultSorting.get('field') : sorting.get('field');
  const fields = field ? List([field]) : emptyList;
  return { ...sorting.toJS(), fields };
};

export const selectSource = createSelector(
  selectCurrentSearch,
  current => current.get('source', SearchSource.NONE),
);

export const selectType = selectSource;

export const selectAllListingTypesIsChecked = createSelector(
  selectCurrentSearch,
  current => current.get('allListingTypesIsChecked', true),
);

export const selectListingType = createSelector(
  selectCurrentSearch,
  current => current.get('listingType', emptyList),
);

export const selectPropertyType = createSelector(
  selectCurrentSearch,
  current => current.get('propertyTypeCode', emptyList),
);

export const selectOwnershipInfo = createSelector(
  selectCurrentSearch,
  (current) => {
    const sortedSaleDate = {
      min: current.get('saleDateMin', null),
      max: current.get('saleDateMax', null),
    };
    const saleDateMin = sortedSaleDate.min;
    const saleDateMax = sortedSaleDate.max;
    const sortedSaleAmount = {
      min: current.get('saleAmountMin', null),
      max: current.get('saleAmountMax', null),
    };
    const saleAmountMin = sortedSaleAmount.min;
    const saleAmountMax = sortedSaleAmount.max;
    const sortedOwnershipLength = {
      min: current.get('ownershipLengthMin', null),
      max: current.get('ownershipLengthMax', null),
    };
    const ownershipLengthMin = sortedOwnershipLength.min;
    const ownershipLengthMax = sortedOwnershipLength.max;
    const ownerOutOfState = current.get('ownerOutOfState', false);
    const vacant = current.get('vacant', false);
    const ownerCorporate = current.get('ownerCorporate', null);
    const ownerOccupied = current.get('ownerOccupied', null);
    return {
      saleDateMin,
      saleDateMax,
      saleAmountMin,
      saleAmountMax,
      ownershipLengthMin,
      ownershipLengthMax,
      ownerOutOfState,
      vacant,
      ownerCorporate,
      ownerOccupied,
    };
  },
);

export const selectLienInfo = createSelector(
  selectCurrentSearch,
  (current) => {
    const sortedLienRecordingDate = {
      min: current.get('lienRecordingDateMin', null),
      max: current.get('lienRecordingDateMax', null),
    };
    const lienRecordingDateMin = sortedLienRecordingDate.min;
    const lienRecordingDateMax = sortedLienRecordingDate.max;
    const sortedLienAmount = {
      min: current.get('lienAmountMin', null),
      max: current.get('lienAmountMax', null),
    };
    const lienAmountMin = sortedLienAmount.min;
    const lienAmountMax = sortedLienAmount.max;
    const lienTypeCode = current.get('lienTypeCode');
    const lienActive = current.get('lienActive', false);
    return {
      lienRecordingDateMin,
      lienRecordingDateMax,
      lienAmountMin,
      lienAmountMax,
      lienTypeCode,
      lienActive,
      divorceActive: current.get('divorceActive', false),
      bankruptcyActive: current.get('bankruptcyActive', false),
    };
  },
);

export const selectSquareFeet = createSelector(
  selectCurrentSearch,
  (current) => {
    const min = current.get('squareFeetMin', '');
    const max = current.get('squareFeetMax', '');
    return {
      min,
      max,
    };
  },
);

export const selectEstimatedValue = createSelector(
  selectCurrentSearch,
  (current) => {
    const min = current.get('estimatedValueMin', '');
    const max = current.get('estimatedValueMax', '');
    return {
      min,
      max,
    };
  },
);

export const selectEstimatedRentalIncome = createSelector(
  selectCurrentSearch,
  (current) => {
    const min = current.get('estimatedRentalIncomeMin', '');
    const max = current.get('estimatedRentalIncomeMax', '');
    return {
      min,
      max,
    };
  },
);

export const selectBedrooms = createSelector(
  selectCurrentSearch,
  (current) => {
    const min = current.get('bedroomsMin', '');
    const max = current.get('bedroomsMax', '');
    return {
      min,
      max,
    };
  },
);

export const selectBathrooms = createSelector(
  selectCurrentSearch,
  (current) => {
    const min = current.get('bathroomsMin', '');
    const max = current.get('bathroomsMax', '');
    return {
      min,
      max,
    };
  },
);

export const selectEstimatedValueGrowth = createSelector(
  selectCurrentSearch,
  (current) => {
    const min = current.get('estimatedValueGrowthMin', '');
    const max = current.get('estimatedValueGrowthMax', '');
    return {
      min,
      max,
    };
  },
);

export const selectEstimatedValueGrowthPeriod = createSelector(
  selectCurrentSearch,
  current => current.get('estimatedValueGrowthPeriod', null),
);

export const selectYearBuilt = createSelector(
  selectCurrentSearch,
  (current) => {
    const min = current.get('yearBuiltMin', '');
    const max = current.get('yearBuiltMax', '');
    return {
      min,
      max,
    };
  },
);

export const selectLotSize = createSelector(
  selectCurrentSearch,
  (current) => {
    const min = current.get('lotSquareFeetMin', '');
    const max = current.get('lotSquareFeetMax', '');
    return {
      min,
      max,
    };
  },
);

export const selectHomeFeatures = createSelector(
  selectCurrentSearch,
  (current) => {
    const poolAvailable = current.get('poolAvailable', false);
    const airConditioningAvailable = current.get('airConditioningAvailable', false);
    const fireplaceAvailable = current.get('fireplaceAvailable', false);
    const garageAvailable = current.get('garageAvailable', false);
    return {
      poolAvailable,
      airConditioningAvailable,
      fireplaceAvailable,
      garageAvailable,
    };
  },
);

export const selectDaysOnMarket = createSelector(
  selectCurrentSearch,
  (current) => {
    const min = current.get('daysOnMarketMin', '');
    const max = current.get('daysOnMarketMax', '');
    return {
      min,
      max,
    };
  },
);

export const selectLienRecordingDate = createSelector(
  selectCurrentSearch,
  (current) => {
    const min = current.get('lienRecordingDateMin', '');
    const max = current.get('lienRecordingDateMax', '');
    return {
      min,
      max,
    };
  },
);

export const selectLienAmount = createSelector(
  selectCurrentSearch,
  (current) => {
    const min = current.get('lienAmountMin', '');
    const max = current.get('lienAmountMax', '');
    return {
      min,
      max,
    };
  },
);

export const selectAmountOfDefault = createSelector(
  selectCurrentSearch,
  (current) => {
    const min = current.get('defaultAmountMin', '');
    const max = current.get('defaultAmountMax', '');
    return {
      min,
      max,
    };
  },
);

export const selectMlsListingStatus = createSelector(
  selectCurrentSearch,
  current => current.get('mlsListingStatus', emptyList),
);

export const selectMortgageLoanCode = createSelector(
  selectCurrentSearch,
  current => current.get('mortgageLoanCode', emptyList),
);

export const selectMlsListingKeyword = createSelector(
  selectCurrentSearch,
  current => current.get('mlsListingKeyword', null),
);

export const selectDocumentTypeCode = createSelector(
  selectCurrentSearch,
  current => current.get('documentTypeCode', null),
);

export const selectForeclosureStatus = createSelector(
  selectCurrentSearch,
  current => current.get('foreclosureStatus', null),
);

export const selectListingAmount = createSelector(
  selectCurrentSearch,
  (current) => {
    const min = current.get('mlsListingAmountMin', '');
    const max = current.get('mlsListingAmountMax', '');
    return {
      min,
      max,
    };
  },
);

export const selectBelowMarketPrice = createSelector(
  selectCurrentSearch,
  current => current.get('belowMarketPrice', null),
);

export const selectEstimatedEquityPercent = createSelector(
  selectCurrentSearch,
  (current) => {
    const min = current.get('estimatedEquityPercentMin', '');
    const max = current.get('estimatedEquityPercentMax', '');
    return {
      min,
      max,
    };
  },
);

export const selectMortgageInterestRate = createSelector(
  selectCurrentSearch,
  (current) => {
    const min = current.get('mortgageInterestRateMin', '');
    const max = current.get('mortgageInterestRateMax', '');
    return {
      min,
      max,
    };
  },
);

export const selectMortgageTerm = createSelector(
  selectCurrentSearch,
  (current) => {
    const min = current.get('mortgageTermMin', '');
    const max = current.get('mortgageTermMax', '');
    return {
      min,
      max,
    };
  },
);

export const selectMortgageMaturityDate = createSelector(
  selectCurrentSearch,
  (current) => {
    const min = current.get('mortgageMaturityDateMin', '');
    const max = current.get('mortgageMaturityDateMax', '');
    return {
      min,
      max,
    };
  },
);

export const selectForSale = createSelector(
  selectCurrentSearch,
  current => current.get('forSale', null),
);

export const selectMortgageCount = createSelector(
  selectCurrentSearch,
  (current) => {
    const min = current.get('mortgageCountMin', '');
    const max = current.get('mortgageCountMax', '');
    return {
      min,
      max,
    };
  },
);

export const selectMortgageAmount = createSelector(
  selectCurrentSearch,
  (current) => {
    const min = current.get('mortgageAmountMin', '');
    const max = current.get('mortgageAmountMax', '');
    return {
      min,
      max,
    };
  },
);

export const selectLoanToValueRatio = createSelector(
  selectCurrentSearch,
  (current) => {
    const min = current.get('loanToValueRatioMin', '');
    const max = current.get('loanToValueRatioMax', '');
    return {
      min,
      max,
    };
  },
);

export const selectEstimatedEquity = createSelector(
  selectCurrentSearch,
  (current) => {
    const min = current.get('estimatedEquityMin', '');
    const max = current.get('estimatedEquityMax', '');
    return {
      min,
      max,
    };
  },
);

export const selectCashBuyer = createSelector(
  selectCurrentSearch,
  current => current.get('cashBuyer', false),
);

export const selectFreeClear = createSelector(
  selectCurrentSearch,
  current => current.get('freeClear', false),
);

export const selectMLSListingDate = createSelector(
  selectCurrentSearch,
  (current) => {
    const min = current.get('mlsListingDateMin', '');
    const max = current.get('mlsListingDateMax', '');
    return {
      min,
      max,
    };
  },
);

export const selectListingDate = createSelector(
  selectCurrentSearch,
  (current) => {
    const min = current.get('listingDateMin', '');
    const max = current.get('listingDateMax', '');
    return {
      min,
      max,
    };
  },
);

export const selectAuctionDate = createSelector(
  selectCurrentSearch,
  (current) => {
    const min = current.get('auctionDateMin', '');
    const max = current.get('auctionDateMax', '');
    return {
      min,
      max,
    };
  },
);

export const selectSearchValidity = createSelector(
  selectCurrentSearch,
  (current) => {
    if (current.get('availableListingTypes').size && (!current.get('listingType') || !current.get('listingType').size)) return false;
    const shapeDefinition = current.get('shapeDefinition');
    if (shapeDefinition) {
      if (shapeDefinition.size > 2) return true;
      if (shapeDefinition.size === 0) return true;
      return false;
    }

    return true;
  },
);

export const selectSearchAllowed = createSelector(
  [selectSearchValidity, selectCurrentSearch],
  (isValid, current) => {
    if (!isValid) return false;
    if (current.get('shapeDefinition')) return true;
    if (current.get('cityId')) return true;
    if (current.get('countyId')) return true;
    if (current.get('zip')) return true;
    if (current.get('cityName') && current.get('stateCode')) return true;
    return false;
  },
);

export const selectHasPositionField = createSelector(
  selectCurrentSearch,
  current => PositionFields.some(field => current.get(field)),
);

export const selectSearchValue = createSelector(
  state => state,
  state => state.getIn(['bing', 'fullData']),
);

export const selectFavoriteSearchParams = createSelector(
  [selectSearchValue, selectCurrentSearch],
  (searchValue, searchParams) => fromJS({ searchValue, searchParams }),
);

export const selectFavoriteSlots = createSelector(
  [selectSearch],
  search => search.getIn(['favourite', 'items'])
    .map((slot, index) => {
      if (slot !== null) {
        return slot.merge({ index, isBusy: true });
      }
      return fromJS({ index, name: 'Empty', isBusy: false });
    }),
);

export const selectSlotsOptions = createSelector(
  selectFavoriteSlots,
  slots => slots.map(
    slot => slot
      .set('value', slot.get('index'))
      .set('label', slot.get('name')),
  ),
);

export const selectSavedSearch = createSelector(
  selectSearch,
  search => search.get('saved'),
);

export const selectSavedSearchIsLoading = createSelector(
  selectSavedSearch,
  saved => saved.get('isLoading'),
);

export const selectSavedSearchError = createSelector(
  selectSavedSearch,
  saved => saved.get('error'),
);

export const selectSavedSearchDisabled = createSelector(
  [selectSavedSearch, selectSource],
  (saved, source) => saved.get('isLoading') || !([SearchSource.LISTING, SearchSource.NONE].includes(source)),
);

export const SAVE_PATH = ['search', 'saved'];
