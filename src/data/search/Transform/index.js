/** @flow */
import ParamTransform from 'data/utils/ParamTransform';
import { Moment } from 'data/utils/ParamTransform/MomentTransform';
import { PositionFields, SearchType } from '../constants';


const SEPARATOR = '/';

const isShape = (value, key) => key === 'shapeDefinition';

const shapeDefinition = {
  ignore: (value, key) => isShape(value, key) && (!value || value.length < 3),
  test: isShape,
  toAPI: (cords: string[]) => cords.concat(cords[0]).join(SEPARATOR),
  toJS: (shapeDefinition: string) => shapeDefinition.split(SEPARATOR).slice(0, -1),
};


const isLocation = (value, key) => key === 'visibleLocation';

const visibleLocation = {
  ignore: (value, key) => isLocation(value, key) && !value,
  test: isLocation,
  toAPI: ({ west, east, north, south }) => [
    [north, west],
    [north, east],
    [south, east],
    [south, west],
    [north, west],
  ].join(SEPARATOR),
  toJS: () => null,
};

const isListingTypes = (value, key) => key === 'listingType';
const isDocumentTypeCode = (value, key) => key === 'documentTypeCode';
const isPropertyTypeCode = (value, key) => key === 'propertyTypeCode';
const isMlsListingStatus = (value, key) => key === 'mlsListingStatus';
const isMortgageLoanCode = (value, key) => key === 'mortgageLoanCode';

const listingType = {
  test: isListingTypes,
  toAPI: (types: string[]) => types.join(''),
  toJS: (types: string) => types.split(''),
};

const documentTypeCode = {
  test: isDocumentTypeCode,
  toAPI: (types: string[]) => types.join(','),
  toJS: (types: string) => types.split(','),
};

const propertyTypeCode = {
  test: isPropertyTypeCode,
  toAPI: (types: string[]) => types.join(','),
  toJS: (types: string) => types.split(','),
};

const mlsListingStatus = {
  test: isMlsListingStatus,
  toAPI: (types: string[]) => types.join(','),
  toJS: (types: string) => types.split(','),
};

const mortgageLoanCode = {
  test: isMortgageLoanCode,
  toAPI: (types: string[]) => types.join(','),
  toJS: (types: string) => types.split(','),
};

const isLienTypeCode = (value, key) => key === 'lienTypeCode';

const lienTypeCodes = {
  test: isLienTypeCode,
  toAPI: (types: string[]) => types.join(','),
  toJS: (types: string) => types.split(','),
};

const isForeclosureStatus = (value, key) => key === 'foreclosureStatus';

const foreclosureStatuses = {
  test: isForeclosureStatus,
  toAPI: (types: string[]) => types.join(','),
  toJS: (types: string) => types.split(','),
};


const extraFieldsRemoving = {
  ignore: [
    /^adminDistrict2$/,
    /^matchCodes$/,
    /^bbox$/,
    /^geocodePoints$/,
    /^countryRegion$/,
    /^confidence$/,
    /^point$/,
    /^formattedAddress$/,
    /^__type$/,
    /^entityType$/,
    /^activeFavourite$/,
    /^lastRunDate$/,
    (value, key) => (key === 'source' && !value),
    (value, key) => (key === 'source' && value === SearchType.PROPERTY),
    (value, key) => (
      key === 'foreclosureStatus' && (
        !value
        || value.length === 0
      )
    ),
  ],
  test: () => false,
  toAPI: v => v,
  toJS: v => v,
};

const keysToPreventVisibleLocationUsage = PositionFields;

const useVisibleLocationIfNeed = (data) => {
  const { visibleLocation, ...rest } = data;
  if (keysToPreventVisibleLocationUsage.some(key => data[key])) return rest;
  return {
    ...rest,
    shapeDefinition: visibleLocation,
  };
};

const transform = new ParamTransform([
  ...Moment,
  shapeDefinition,
  listingType,
  documentTypeCode,
  propertyTypeCode,
  mlsListingStatus,
  mortgageLoanCode,
  visibleLocation,
  extraFieldsRemoving,
  lienTypeCodes,
  foreclosureStatuses,
]);

export const SearchTransformWithoutLocation = {
  toAPI: (params: Object) => {
    const {
      sorting = {},
      id,
      ...searchParams
    } = params;

    return {
      sortAscending: sorting.ascending,
      sortField: sorting.field,
      filterSearchId: id,
      ...transform.toAPI(searchParams),
    };
  },
  toJS: (params: Object, source: any) => {
    const {
      sortAscending,
      sortField,
      ...searchParams
    } = params;
    return {
      sorting: {
        ascending: sortAscending,
        field: sortField,
      },
      ...transform.toJS(searchParams, source),
    };
  },
};

export const SearchTransform = {
  toAPI: (params: Object) => useVisibleLocationIfNeed(SearchTransformWithoutLocation.toAPI(params)),
  toJS: (...rest: *) => SearchTransformWithoutLocation.toJS(...rest),
};

export default SearchTransform;


export const SavedSearchTransform = {
  toAPI: (data: Object) => {
    const {
      name,
      description,
      slot,
      id = null,
      params,
      notificationInterval,
    } = data;

    return {
      ...SearchTransformWithoutLocation.toAPI(params),
      notificationInterval,
      name,
      slot,
      description,
      id,
    };
  },
  toJS: (data: Object, index: *) => {
    const {
      name,
      description,
      totalCount,
      threeMonthCount,
      oneMonthCount,
      oneWeekCount,
      source,
      slot,
      enabled,
      ...rest
    } = data;

    return {
      name,
      slot,
      enabled,
      description,
      id: rest.id || null,
      unifier: `id-${rest.id}` || index,
      info: {
        totalCount,
        threeMonthCount,
        oneMonthCount,
        oneWeekCount,
        source,
      },
      params: SearchTransformWithoutLocation.toJS({ ...rest, source }),
    };
  },
};

export const FavoriteSearchTransform = {
  toAPI: (v: *) => SavedSearchTransform.toAPI(v),
  toJS: (data: Object, index: *) => SavedSearchTransform.toJS(data, index),
};
