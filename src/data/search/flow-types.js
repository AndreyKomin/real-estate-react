/** @flow */
export type SearchType = 'PROPERTY' | 'LISTING' | 'NONE'
