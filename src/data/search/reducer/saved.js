/** @flow */
import { fromJS, Map } from 'immutable';

import { SavedSearchTransform as Transform } from '../Transform';
import * as ActionType from '../actions';


export const defaultState = fromJS({
  isLoading: false,
  error: null,
  items: [],
});


function getSavedSearch(savedSearches: Array<*>) {
  return savedSearches.map(Transform.toJS);
}

export default function reducer(state: Map<string, *> = defaultState, action: ?Object): Map<string, *> {
  if (!action) return state;

  switch (action.type) {
    case ActionType.SAVED_SEARCH_DELETE:
    case ActionType.SAVED_SEARCH_CREATE:
    case ActionType.SAVED_SEARCH_LOAD:
      return defaultState.set('isLoading', true).set('error', null);

    case ActionType.SAVED_SEARCH_CREATE_SUCCESS:
    case ActionType.SAVED_SEARCH_DELETE_SUCCESS:
      return defaultState.merge({
        isLoading: false,
        error: null,
        items: getSavedSearch(action.response),
      });

    case ActionType.SAVED_SEARCH_LOAD_SUCCESS:
      return state.merge({
        isLoading: false,
        error: null,
        items: getSavedSearch(action.response),
      });

    case ActionType.SAVED_SEARCH_CREATE_ERROR:
    case ActionType.SAVED_SEARCH_DELETE_ERROR:
    case ActionType.SAVED_SEARCH_LOAD_ERROR:
      return state.set('isLoading', false).set('error', 'SOME_ERROR');

    default:
      return state;
  }
}
