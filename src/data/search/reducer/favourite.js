/** @flow */
import { fromJS, Map } from 'immutable';

import { FavoriteSearchTransform as Transform } from '../Transform';
import * as ActionType from '../actions';


const MAX_SEARCHES = 8;


function getSlice(search) {
  const res = [];
  for (let i = 0; i < MAX_SEARCHES; i++) {
    res.push(search[i] || null);
  }
  return res;
}

export const defaultState = fromJS({
  isLoading: false,
  error: null,
  items: getSlice([]),
});

function getFavouriteSearch(savedSearches: Array<*>) {
  const searches: Array<Object> = [];
  savedSearches
    .map(Transform.toJS)
    .filter((value: any) => !!value.slot)
    .forEach((search) => {
      searches[search.slot - 1] = search;
    });

  return getSlice(searches);
}

export default function reducer(state: Map<string, *> = defaultState, action: Object): Map<string, *> {
  switch (action.type) {
    case ActionType.FAVOURITE_SEARCH_CREATE: // TODO replace it when API ready
      return state.set('isLoading', true).set('error', null);

    case ActionType.FAVOURITE_SEARCH_DELETE:
    case ActionType.FAVOURITE_SEARCH_LOAD:
      return defaultState.set('isLoading', true).set('error', null);

    case ActionType.FAVOURITE_SEARCH_DELETE_SUCCESS:
      return defaultState.merge({
        isLoading: false,
        error: null,
        items: getFavouriteSearch(action.response),
      });

    case ActionType.FAVOURITE_SEARCH_LOAD_SUCCESS:
      return state.merge({
        isLoading: false,
        error: null,
        items: getFavouriteSearch(action.response),
      });

    case ActionType.FAVOURITE_SEARCH_CREATE_ERROR:
    case ActionType.FAVOURITE_SEARCH_DELETE_ERROR:
    case ActionType.FAVOURITE_SEARCH_LOAD_ERROR:
      return state.set('isLoading', false).set('error', 'SOME_ERROR');

    case ActionType.FAVOURITE_SEARCH_CREATE_SUCCESS: // TODO replace it when API ready
      return state
        .merge({
          isLoading: false,
          error: null,
        })
        .update(
          'items',
          (items: any) => items.set(action.slot, fromJS(action.item).set('id', Math.random())),
        );

    default:
      return state;
  }
}
