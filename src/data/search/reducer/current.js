/** @flow */
import moment from 'moment-timezone';
import { fromJS, Map, List, Set } from 'immutable';

import { getUnixTimestamp } from 'utils/date/formatDate';
import triggerValueInList from 'utils/triggerValueInList';
import { Type as ListingTypes } from 'data/listing/constants';
import { LOAD_LISTINGS } from 'data/listing/actions';
import { AUTHENTICATION_SUCCESS } from 'data/user/actions';

import getSearchSource from '../getSearchSource';

import * as ActionType from '../actions';
import { PositionFields, SearchType } from '../constants';


let availableListingTypes = Set();

const listingTypesValues = Object.values(ListingTypes);
const defaultState = fromJS({
  activeFavourite: null,
  source: getSearchSource(),
  allListingTypesIsChecked: true,
  listingType: listingTypesValues,
  availableListingTypes: [],
  propertyTypeCode: [],
  mlsListingStatus: [],
  lienTypeCode: [],
  foreclosureStatus: [],
  documentTypeCode: [],
  visibleLocation: null,
});


/**
 * Method set Listed Parameter and check for All Listed Parameter
 * @param state {Map}
 * @param action {Object}
 * @returns state {Map}
 */
function changeListingType(state: Map<string, *>, action: Object) {
  return state.updateIn(
    action.name.split('.'),
    (value) => {
      const list = List.isList(value) ? value : List([value]);
      return triggerValueInList(list, fromJS(action.value));
    },
  ).set('activeFavourite', null).delete('id');
}

/**
 * Method sort ranged params
 * @param state {Map}
 * @param name {String}
 * @returns state {Map}
 */
function sortRangeParams(state: Map<string, *>, name: string) {
  if (!name.endsWith('Min') && !name.endsWith('Max')) return state;

  const paramGroupName = name.replace(/Min|Max/, '');
  const minParam = state.get(`${paramGroupName}Min`, null);
  const maxParam = state.get(`${paramGroupName}Max`, null);
  const minParamNumber = (minParam === null || minParam === '') ? NaN : Number(minParam);
  const maxParamNumber = (maxParam === null || maxParam === '') ? NaN : Number(maxParam);

  if (isNaN(minParamNumber) || isNaN(maxParamNumber)) return state;

  if (
    moment.isMoment(minParam) ?
      getUnixTimestamp(minParam) > getUnixTimestamp(maxParam) :
      minParamNumber > maxParamNumber
  ) {
    return state.merge({
      [`${paramGroupName}Min`]: maxParamNumber,
      [`${paramGroupName}Max`]: minParamNumber,
    });
  }

  return state;
}

function reduce(state: Map<string, *>, action: Object): Map<string, *> {
  switch (action.type) {
    case ActionType.CHANGE_SEARCH_TYPE:
      return state.set('source', action.searchType)
        .set('activeFavourite', null).delete('id');

    case ActionType.CHANGE_SEARCH_RESULTS_SORTING:
      return state.updateIn(
        ['sorting'],
        (sorting) => {
          let ascending = true;
          if (sorting) {
            ascending = sorting.get('field') === action.field.fieldName ? !sorting.get('ascending') : true;
          }
          return fromJS({
            ascending,
            field: action.field.fieldName,
          });
        });

    case ActionType.CHANGE_SEARCH_PARAMETER:
      return state.delete('sorting').setIn(action.name.split('.'), fromJS(action.value))
        .set('activeFavourite', null).delete('id');

    case ActionType.SORT_SEARCH_PARAMETER:
      return sortRangeParams(state, action.name);

    case ActionType.CHANGE_ALL_SEARCH_PARAMETERS: {
      return defaultState.merge(action.params).delete('sorting');
    }
    case ActionType.CHANGE_SEARCH_ALL_LISTED_PARAMETER:
      return state.set('listingType', List(state.get('allListingTypesIsChecked') ?
        [] : listingTypesValues)).delete('sorting').set('activeFavourite', null).delete('id');

    case ActionType.CHANGE_SEARCH_LISTED_PARAMETER:
      return changeListingType(state, action).delete('sorting').set('activeFavourite', null).delete('id');

    case ActionType.SET_CURRENT_SEARCH:
      return defaultState.merge(
        state.filter((v, key) => !PositionFields.includes(key)),
        { source: state.get('source') },
        action.currentSearch,
      ).delete('sorting').set('activeFavourite', null).delete('id');

    case ActionType.RESET_CURRENT_SEARCH:
      return defaultState.merge({
        adminDistrict2: state.get('adminDistrict2'),
        bbox: state.get('bbox'),
        cityName: state.get('cityName'),
        confidence: state.get('confidence'),
        countryRegion: state.get('countryRegion'),
        entityType: state.get('entityType'),
        formattedAddress: state.get('formattedAddress'),
        geocodePoints: state.get('geocodePoints'),
        matchCodes: state.get('matchCodes'),
        name: state.get('name'),
        source: state.get('source'),
        stateCode: state.get('stateCode'),
      });

    case ActionType.SET_ACTIVE_FAVOURITE:
      return state.set('activeFavourite', action.id);

    case ActionType.CHANGE_VISIBLE_LOCATION:
      return state.merge({
        visibleLocation: {
          north: action.north,
          west: action.west,
          south: action.south,
          east: action.east,
        },
      });

    case ActionType.CLEAR:
      return defaultState.merge({
        visibleLocation: state.get('visibleLocation'),
        availableListingTypes: state.get('availableListingTypes'),
      });

    case ActionType.APPLY_FAVORITE_SEARCH_PARAMS:
      return defaultState.merge(
        [...PositionFields, ...defaultState.keys()].reduce((res, key) => {
          if (state.has(key)) return { ...res, [key]: state.get(key) };
          return res;
        }, {}),
        action.params,
      );

    case ActionType.REMOVE_POSITION_FIELDS_FROM_SEARCH:
      return PositionFields.reduce(
        (res, key) => res.delete(key),
        state.set('source', defaultState.get('source')),
      );

    case LOAD_LISTINGS:
      return state.update('source', source => source || getSearchSource(SearchType.LISTING));

    case AUTHENTICATION_SUCCESS:
      if (!availableListingTypes.size) {
        availableListingTypes = Set(action.response.availableListingTypes || []);

        return defaultState;
      }
      return state;

    default:
      return state;
  }
}

export default function reducer(state: Map<string, *> = defaultState, action: Object): Map<string, *> {
  const newState: Map<string, any> = reduce(state, action);

  // Ensure that only available listing types are set
  const listingType: List<any> = availableListingTypes.intersect(newState.get('listingType')).toList();

  // Ensure allListingTypesIsChecked is consistent with current selection
  return newState.merge({
    availableListingTypes,
    listingType,
    allListingTypesIsChecked: !!availableListingTypes.size && listingType.size === availableListingTypes.size,
  });
}
