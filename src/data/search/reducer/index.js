/** @flow */
import { combineReducers } from 'redux-immutable';

import current from './current';
import saved from './saved';
import favourite from './favourite';


export default combineReducers({
  current,
  saved,
  favourite,
});
