/** @flow */
import type { Map } from 'immutable';

import { CALL_API, CHAIN_API } from 'store/middleware/api';
import formatRequestQuery from 'utils/formatRequestQuery';
import getRequestParams from 'data/utils/getSearchRequestParams';
import type { SearchType } from '../flow-types';
import SearchTransform, { SavedSearchTransform as Transform } from '../Transform';


function getSearchParams(params: *) {
  return SearchTransform.toAPI(params);
}

export const SAVE_RECENT_SEARCH = Symbol('SAVE_RECENT_SEARCH');

export function saveRecent({ recent }: { recent: Array<Object> }) {
  return {
    type: SAVE_RECENT_SEARCH,
    recent,
  };
}

export const SHOW_RECENT_SEARCH = Symbol('SHOW_RECENT_SEARCH');

export function showRecent() {
  return {
    type: SHOW_RECENT_SEARCH,
  };
}

export const CHANGE_SEARCH_TYPE = Symbol('CHANGE_SEARCH_TYPE');

export function changeSearchType({ searchType }: { searchType: SearchType }) {
  return {
    type: CHANGE_SEARCH_TYPE,
    searchType,
  };
}

export const CHANGE_SEARCH_RESULTS_SORTING = Symbol('CHANGE_SEARCH_RESULTS_SORTING');

export function changeSearchResultsSorting(field: string) {
  return {
    type: CHANGE_SEARCH_RESULTS_SORTING,
    field,
  };
}

export const CHANGE_SEARCH_PARAMETER = Symbol('CHANGE_SEARCH_PARAMETER');

export function changeSearchParams({ name, value }: { name: string, value: * }) {
  return {
    type: CHANGE_SEARCH_PARAMETER,
    name,
    value,
  };
}

export const SORT_SEARCH_PARAMETER = Symbol('SORT_SEARCH_PARAMETER');

export function sortSearchParams(name: string) {
  return {
    type: SORT_SEARCH_PARAMETER,
    name,
  };
}

export const CHANGE_ALL_SEARCH_PARAMETERS = Symbol('CHANGE_ALL_SEARCH_PARAMETERS');

export function changeAllSearchParams(params: { [key: string]: * }) {
  return {
    type: CHANGE_ALL_SEARCH_PARAMETERS,
    params,
  };
}


export const APPLY_FAVORITE_SEARCH_PARAMS = Symbol('APPLY_FAVORITE_SEARCH_PARAMS');

export function applyFavoriteSearchParams(params: { [key: string]: * }) {
  return {
    type: APPLY_FAVORITE_SEARCH_PARAMS,
    params,
  };
}

export const CHANGE_SEARCH_ALL_LISTED_PARAMETER = Symbol('CHANGE_SEARCH_ALL_LISTED_PARAMETER');

export function changeSearchAllListedParams() {
  return {
    type: CHANGE_SEARCH_ALL_LISTED_PARAMETER,
  };
}

export const CHANGE_SEARCH_LISTED_PARAMETER = Symbol('CHANGE_SEARCH_LISTED_PARAMETER');

export function changeSearchListedParams({ name, value }: { name: string, value: * }) {
  return {
    type: CHANGE_SEARCH_LISTED_PARAMETER,
    name,
    value,
  };
}


export const SAVED_SEARCH_LOAD = Symbol('SAVED_SEARCH_LOAD');
export const SAVED_SEARCH_LOAD_SUCCESS = Symbol('SAVED_SEARCH_LOAD_SUCCESS');
export const SAVED_SEARCH_LOAD_ERROR = Symbol('SAVED_SEARCH_LOAD_ERROR');

export function getSavedSearch(): Object {
  const result: any = {};
  result[CALL_API] = {
    method: 'get',
    path: '/resource/auth/ps4/user/searches',
    startType: SAVED_SEARCH_LOAD,
    successType: SAVED_SEARCH_LOAD_SUCCESS,
    errorType: SAVED_SEARCH_LOAD_ERROR,
  };

  return result;
}


export const SET_CURRENT_SEARCH = Symbol('SET_CURRENT_SEARCH');

export function setCurrentSearch(currentSearch: Map<string, *>) {
  return {
    type: SET_CURRENT_SEARCH,
    currentSearch,
  };
}

export const SAVED_SEARCH_DELETE = Symbol('SAVED_SEARCH_DELETE');
export const SAVED_SEARCH_DELETE_SUCCESS = Symbol('SAVED_SEARCH_DELETE_SUCCESS');
export const SAVED_SEARCH_DELETE_ERROR = Symbol('SAVED_SEARCH_DELETE_ERROR');

function deleteSavedSearchOnly(id: number): Object {
  const result: any = {};
  result[CALL_API] = {
    method: 'delete',
    path: '/resource/auth/ps4/user/searches',
    headers: { 'Content-Type': 'application/json' },
    body: [{ id }],
    startType: SAVED_SEARCH_DELETE,
    successType: SAVED_SEARCH_DELETE_SUCCESS,
    errorType: SAVED_SEARCH_DELETE_ERROR,
  };

  return result;
}

export const SAVED_SEARCH_CREATE = Symbol('SAVED_SEARCH_CREATE');
export const SAVED_SEARCH_CREATE_SUCCESS = Symbol('SAVED_SEARCH_CREATE_SUCCESS');
export const SAVED_SEARCH_CREATE_ERROR = Symbol('SAVED_SEARCH_CREATE_ERROR');

export function createSavedSearch(body: Object): Object {
  const result: any = {};
  result[CALL_API] = {
    method: 'put',
    path: '/resource/auth/ps4/user/searches',
    headers: { 'Content-Type': 'application/json' },
    body: Transform.toAPI(body),
    startType: SAVED_SEARCH_CREATE,
    successType: SAVED_SEARCH_CREATE_SUCCESS,
    errorType: SAVED_SEARCH_CREATE_ERROR,
  };

  return result;
}


export const FAVOURITE_SEARCH_LOAD = Symbol('FAVOURITE_SEARCH_LOAD');
export const FAVOURITE_SEARCH_LOAD_SUCCESS = Symbol('FAVOURITE_SEARCH_LOAD_SUCCESS');
export const FAVOURITE_SEARCH_LOAD_ERROR = Symbol('FAVOURITE_SEARCH_LOAD_ERROR');

export function getFavouriteSearch(listingInfo: Object, addressOnly: boolean): Object {
  const requestParams = getRequestParams(listingInfo);
  const params = getSearchParams(requestParams);
  const query: Object = formatRequestQuery(params);
  const filteredQuery: Object = {
    cityName: query.cityName,
    name: query.name,
    stateCode: query.stateCode,
  };

  const result: any = {};
  result[CALL_API] = {
    method: 'get',
    path: '/resource/auth/ps4/user/searches/canned',
    query: addressOnly ? filteredQuery : query,
    unifier: 'fetching canned search',
    dislodging: true,
    startType: FAVOURITE_SEARCH_LOAD,
    successType: FAVOURITE_SEARCH_LOAD_SUCCESS,
    errorType: FAVOURITE_SEARCH_LOAD_ERROR,
  };

  return result;
}


export function deleteSavedSearch(id: number): Object {
  const result: any = {};
  result[CHAIN_API] = [
    () => deleteSavedSearchOnly(id),
    () => getSavedSearch(),
    () => getFavouriteSearch({}, false),
  ];

  return result;
}


export const FAVOURITE_SEARCH_DELETE = Symbol('FAVOURITE_SEARCH_DELETE');
export const FAVOURITE_SEARCH_DELETE_SUCCESS = Symbol('FAVOURITE_SEARCH_DELETE_SUCCESS');
export const FAVOURITE_SEARCH_DELETE_ERROR = Symbol('FAVOURITE_SEARCH_DELETE_ERROR');

export function deleteFavouriteSearch(id: number): Object {
  const result: any = {};
  result[CALL_API] = {
    method: 'delete',
    path: '/resource/auth/ps4/user/searches',
    body: [{ id }],
    headers: { 'Content-Type': 'application/json' },
    startType: FAVOURITE_SEARCH_DELETE,
    successType: FAVOURITE_SEARCH_DELETE_SUCCESS,
    errorType: FAVOURITE_SEARCH_DELETE_ERROR,
  };

  return result;
}

export const FAVOURITE_SEARCH_CREATE = Symbol('FAVOURITE_SEARCH_CREATE');
export const FAVOURITE_SEARCH_CREATE_SUCCESS = Symbol('FAVOURITE_SEARCH_CREATE_SUCCESS');
export const FAVOURITE_SEARCH_CREATE_ERROR = Symbol('FAVOURITE_SEARCH_CREATE_ERROR');

function saveFavoriteSearch({ slot, item }: { slot: string, item: Object}): Object {
  const result: any = {
    item,
    slot,
  };
  result[CALL_API] = {
    method: 'put',
    path: `/resource/auth/ps4/user/searches/slots/${parseInt(slot, 10) + 1}/${item.id}`,
    startType: FAVOURITE_SEARCH_CREATE,
    successType: FAVOURITE_SEARCH_CREATE_SUCCESS,
    errorType: FAVOURITE_SEARCH_CREATE_ERROR,
  };

  return result;
}

export function saveSearchAsFavourite(
  { slot, item, searchQuery, addressOnly }: { slot: string, item: Object, searchQuery: Object, addressOnly: boolean },
): Object {
  const result: any = {};
  result[CHAIN_API] = [
    () => saveFavoriteSearch({ slot, item }),
    () => getFavouriteSearch(searchQuery, addressOnly),
  ];

  return result;
}


export const RESET_CURRENT_SEARCH = Symbol('RESET_CURRENT_SEARCH');

export function resetCurrentSearch() {
  return {
    type: RESET_CURRENT_SEARCH,
  };
}


export const SET_ACTIVE_FAVOURITE = Symbol('SET_ACTIVE_FAVOURITE');

export function setActiveFavouriteSearch(id: Number) {
  return {
    type: SET_ACTIVE_FAVOURITE,
    id,
  };
}

type CreateSearchAsFavoriteArg = {
  slot: string,
  params: Object,
  name: string,
  description: string,
  searchQuery: Object,
};

export function createSearchAsFavourite(
  { slot, params, name, description, searchQuery }: CreateSearchAsFavoriteArg,
): Object {
  const result: any = {};
  result[CHAIN_API] = [
    () => createSavedSearch({ params, name, description }),
    (response) => {
      const item = response.find(search => search.name === name);
      return saveSearchAsFavourite({ slot, item: Transform.toJS(item), searchQuery, addressOnly: true });
    },
  ];

  return result;
}

export const CLEAR = Symbol('CLEAR');

export function clearSearch() {
  return {
    type: CLEAR,
  };
}


export const CHANGE_VISIBLE_LOCATION = Symbol('CHANGE_VISIBLE_LOCATION');

export function changeVisibleLocation({ north, east, west, south }: { [key: string]: number }) {
  return {
    type: CHANGE_VISIBLE_LOCATION,
    north,
    east,
    west,
    south,
  };
}


export const REMOVE_POSITION_FIELDS_FROM_SEARCH = Symbol('REMOVE_POSITION_FIELDS_FROM_SEARCH');

export function removePositionsFromSearch() {
  return {
    type: REMOVE_POSITION_FIELDS_FROM_SEARCH,
  };
}
