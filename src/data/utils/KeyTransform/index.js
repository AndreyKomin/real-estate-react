/** @flow */

type Rule = {| apiName: string, jsName: string |};

const emptyObject = Object.create(null);

function hasKey(obj: Object, key: string) {
  const path = [key];
  const last = path.pop();
  const deepObj = path.reduce((temp = emptyObject, part) => temp[part], obj);
  return (last in deepObj);
}

export default class KeyTransform {
  rules: Rule[];

  constructor(rules: Rule[]) {
    this.rules = rules;
  }

  toJS(params: Object) {
    return Object.keys(params).reduce((result, key) => {
      if (!hasKey(result, key)) return result;

      const nextKey = this.rules.reduce((key, rule) => {
        if (rule.apiName === key) return rule.jsName;
        return key;
      }, key);

      const { [key]: value, ...rest } = result;

      return { ...rest, [nextKey]: value };
    }, params);
  }

  toAPI(params: Object) {
    return Object.keys(params).reduce((result, key) => {
      if (!hasKey(result, key)) return result;

      const nextKey = this.rules.reduce((key, rule) => {
        if (rule.jsName === key) return rule.apiName;
        return key;
      }, key);

      const { [key]: value, ...rest } = result;

      return { ...rest, [nextKey]: value };
    }, params);
  }
}
