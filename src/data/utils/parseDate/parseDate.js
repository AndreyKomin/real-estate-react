/** @flow */
import moment from 'moment-timezone';


export default function parseDate(something: *) {
  const date = moment(something);
  if (date.isValid()) {
    return date;
  }

  return something;
}
