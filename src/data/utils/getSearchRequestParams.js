/** @flow */

export type TSearchParams = {
  cityId?: number,
  saleAmountMax?: number | string,
  saleAmountMin?: number | string,
  bathroomsMax?: number,
  bathroomsMin?: number,
  bedroomsMax?: number,
  bedroomsMin?: number,
  listingType: Array<*>,
  mlsListingStatus?: Array<*>,
  ownerCorporate?: boolean | void,
  ownerOccupied?: boolean,
  squareFeetMin?: number,
  squareFeetMax?: number,
  lotSquareFeetMin?: number,
  lotSquareFeetMax?: number,
  garageAvailable?: boolean,
  yearBuiltMin?: number,
  yearBuiltMax?: number,
  poolAvailable?: boolean,
  listingDateMin?: Date,
  listingDateMax?: Date,
  ownershipLengthMax?: number,
  ownershipLengthMin?: number,
  shapeDefinition: string[],
  vacant?: boolean,

  page?: number,
  limit?: number,
}

export default function getSearchRequestParams(parameters: Object): TSearchParams {
  const { searchValue = {}, searchParams, shapeDefinition, ...rest } = parameters;
  const { address = {} } = searchValue;
  const {
    adminDistrict: stateCode,
    locality: cityName,
    postalCode: zip,
  } = address;

  const result = { ...rest, ...searchParams };
  if (zip) result.zip = zip;
  else if (cityName) result.cityName = cityName;
  if (stateCode) result.stateCode = stateCode;

  return result;
}
