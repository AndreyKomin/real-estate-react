/** @flow */
import SearchTransform from 'data/search/Transform';


function getParams(parameters: Object) {
  const { searchValue, searchParams, statisticParams } = parameters;
  const { address = {} } = searchValue;
  const {
    adminDistrict: stateCode,
    locality: cityName,
    postalCode: zip,
  } = address;
  const {
    cityId,
    countyId,
    countyName,
    stateId,
    shapeDefinition,
    visibleLocation,
    zip: zip2,
  } = searchParams;

  const result = {
    cityId,
    countyId,
    countyName,
    stateId,
    shapeDefinition,
    visibleLocation,
    ...statisticParams,
  };
  if (zip) result.zip = zip;
  if (zip2) result.zip = zip2;
  else if (cityName) result.cityName = cityName;
  if (stateCode) result.stateCode = stateCode;

  return result;
}

export default function getRequestParams(parameters: Object) {
  return SearchTransform.toAPI(getParams(parameters));
}
