/** @flow */
const IGNORE_PARAMS = [
  'ActivationCode',
  'resultOffset',
  'resultLimit',
];

export default function getSearchQueryId(query: Object): string {
  return Object
    .keys(query)
    .filter(parameter => !IGNORE_PARAMS.includes(parameter))
    .sort()
    .map(parameter => `${parameter}=${JSON.stringify(query[parameter])}`)
    .join(' $ ');
}
