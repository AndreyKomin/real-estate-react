/** @flow */
import moment from 'moment-timezone';

import ParamTransform from './ParamTransform';


export const Moment = [
  {
    title: 'MomentTransform',
    description: 'Default date to moment transformation',
    test: [
      /[\b_a-z]Date$/,
      /[\b_a-z]Date[A-Z\b_]/,
      /^date[A-Z\b_]/,
      /^[dD]ate$/,
    ],
    exclude: [
      (value: *) => value === null,
      (value: *) => !moment(value).isValid(),
    ],
    toJS: (value: *) => {
      let timestamp = value;
      if (timestamp && timestamp.toString().length === 10) timestamp *= 1000;
      return moment(timestamp);
    },
    toAPI: (value: *) => ((moment.isMoment(value) || value instanceof Date) ? moment(value).valueOf() : null),
  },
];

export default new ParamTransform(Moment);
