/** @flow */
import ParamTransform from './ParamTransform';


export const Percentage = [
  {
    title: 'PercentageTransform',
    description: 'Percentage rate transformation',
    test: [
      /[a-z]Rate$/,
      /[a-z]Rate[A-Z\b_]/,
      /^rate[A-Z\b_]/,
      /^rate$/,
    ],
    exclude: [
      (value: *) => value === null,
      (value: *) => isNaN(value),
      (value: *, key: *) => key === 'adjustableRate',
    ],
    toJS: (value: *) => Number(value) * 100,
    toAPI: (value: *) => Number(value) / 100,
  },
];

export default new ParamTransform(Percentage);
