import ParamTransform from 'data/utils/ParamTransform'

describe('data::utils::ParamTransform', function(){
  let arg;
  let map;
  let spy;

  beforeEach(function(){
    arg = {};
    map = [];
    spy = sinon.spy();
  });

  describe('static', function(){
    describe('generate()', function(){
      it('throws if called without MapRule[] argument', function(){
        expect(() => ParamTransform.generate()).to.throw(Error);
      });

      it('does not throws on empty rules', function(){
        expect(() => ParamTransform.generate([])).not.to.throw(Error);
      });

      it('returns instance of ParamTransform', function(){
        expect(ParamTransform.generate([])).to.be.instanceOf(ParamTransform);
      });
    });

    describe('checkResult()', function(){
      it('returns true if argument is true', function(){
        expect(ParamTransform.checkResult(true)).to.be.equal(true);
      });

      it('returns false if argument is false', function(){
        expect(ParamTransform.checkResult(false)).to.be.equal(false);
      });

      it('throws if called without argument', function(){
        expect(() => ParamTransform.checkResult()).to.throw(Error);
      });

      [
        null,
        undefined,
        function(){},
        {},
        [],
        ()=>{},
        1,
        0,
        '',
        'str',
        Symbol('a'),
        NaN,
      ].forEach(arg => {
        it(`throws if called with ${String(arg)}`, function(){
          expect(() => ParamTransform.checkResult(arg)).to.throw(Error);
        });
      });
    });

    describe('doesSucceedRule()', function(){
      const value = 'myValue';
      const key = 'myKey';
      const fullData = { anotherKey: 1, [key]: value };
      const succeedsOn = [
        () => true,
        /Key/,
        v => v === value,
        [() => true],
        { test: () => true },
        [() => true, () => false],
        [() => false, () => true],
      ];
      const throwsOn = [
        () => undefined,
        [false, () => true],
        false,
        null,
      ];
      const failsOn = [
        () => false,
        /key/,
        v => v === '',
        { test: () => false },
        [() => false, () => false],
      ];
      it('runs rule if it is function with (value, key, fullData)', function(){
        ParamTransform.doesSucceedRule((...rest) => {spy(...rest); return true;}, value, key, fullData);
        expect(spy).to.have.been.calledWith(value, key, fullData);
      });

      succeedsOn.forEach(rule => it(`returns true for rule ${rule}`, function(){
        expect(ParamTransform.doesSucceedRule(rule, value, key, fullData)).to.be.equal(true);
      }));

      failsOn.forEach(rule => it(`returns false for rule ${rule}`, function(){
        expect(ParamTransform.doesSucceedRule(rule, value, key, fullData)).to.be.equal(false);
      }));

      throwsOn.forEach(rule => it(`throws for rule ${rule}`, function(){
        expect(() => ParamTransform.doesSucceedRule(rule, value, key, fullData)).to.throw(Error);
      }));
    });
  });

  describe('constructor', function(){
    it('throws if called without new', function(){
      expect(() => ParamTransform()).to.throw(Error);
    });

    it('throws if called with new, but without MapRule[] argument', function(){
      expect(() => new ParamTransform()).to.throw(Error);
    });

    it('does not throws on empty rules', function(){
      expect(() => new ParamTransform([])).not.to.throw(Error);
    });
  });

  describe('composition', function(){
    it('properly uses another ParamTransform', function(){
      const VAL_FROM_CHILD = Symbol('VAL_FROM_CHILD');
      const VAL_FROM_PARENT = Symbol('VAL_FROM_PARENT');
      const child = new ParamTransform([
        {
          test: /key/,
          toAPI: () => VAL_FROM_CHILD,
        }
      ]);
      const parent = new ParamTransform([
        {
          test: /^a$/,
          toAPI: () => 2,
        },
        child,
      ]);
      expect(parent.toAPI({ a: 1, key: { b: 3 } })).to.be.deep.equal({ a: 2, key: VAL_FROM_CHILD });
    });
  });
});
