/** @flow */
type SimpleRule<T> = Function | RegExp | { test: Function } | T;
type ArrayedRule<T> = T | SimpleRule<T> | Array<T>;
export type CheckRule<T> = ArrayedRule<T> | T | *;

export type MapRule = {
  title?: string,
  description?: string,
  ignore?: CheckRule<*>, // remove from result
  test?: CheckRule<*>, // transform if not excluded
  exclude?: CheckRule<*>, // does not transform
  toAPI: Function,
  toJS: Function,
}

