import moment from 'moment-timezone';
import MomentTransform from './MomentTransform'

describe('data::utils::MomentTransform', function(){
  let arg;
  let map;
  let spy;

  beforeEach(function(){
    arg = {};
    map = [];
    spy = sinon.spy();
  });

  describe('toJS()', function(){
    it('converts #date = Date.now() to moment object', function(){
      expect(moment.isMoment(MomentTransform.toJS({ date: Date.now() }).date)).to.be.equal(true);
    });

    it('converts #date = null to null', function(){
      expect(MomentTransform.toJS({ date: null })).to.be.deep.equal({ date: null });
    });

    it('converts #date = undefined to moment object', function(){
      expect(moment.isMoment(MomentTransform.toJS({ date: undefined }).date)).to.be.equal(true);
    });

    it('converts #date = "WE" to "WE"', function(){
      expect(MomentTransform.toJS({ date: 'WE' })).to.be.deep.equal({ date: 'WE' });
    });
  });
});
