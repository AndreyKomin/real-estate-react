/** @flow */
import deepFreeze from 'utils/deepFreeze';

import type { MapRule, CheckRule } from './flow-types';


const hasProperty = (obj: Object, key: string): boolean => (key in obj);

const RULE_TYPE_ERROR = 'Rule should be Array<Rule>, function, regExp or Object with #test() method.';

export default class ParamTransform {
  static generate(rules: Array<MapRule>) {
    return new ParamTransform(rules);
  }

  static checkResult(result: *): boolean {
    if (result === true) return true;
    if (result === false) return false;
    throw new Error(`Rule should return boolean value, but has ${result}.`);
  }

  static doesSucceedRule(rule: CheckRule<ParamTransform>, value: *, key: string, fullData: Object): boolean {
    if (Array.isArray(rule)) {
      return rule.some(subRule => ParamTransform.doesSucceedRule(subRule, value, key, fullData));
    } else if (rule instanceof RegExp) {
      return rule.test(key);
    } else if (typeof rule === 'function') {
      return ParamTransform.checkResult(rule(value, key, fullData));
    } else if (hasProperty(rule, 'test')) {
      return ParamTransform.checkResult(rule.test(value, key, fullData));
    }
    throw new Error(RULE_TYPE_ERROR);
  }

  static moveRulesToTop(rules: Array<MapRule>) {
    let tempRules = rules;
    for (let i = 0; i < tempRules.length; i++) {
      const current: any = tempRules[i];
      if (current instanceof ParamTransform) {
        tempRules = tempRules.slice(0, i)
          .concat(((current.rules: any): MapRule))
          .concat(tempRules.slice(i + 1));
        i = 0;
      }
    }
    return tempRules;
  }

  rules: Array<MapRule>;
  constructor(rules: Array<MapRule>) {
    if (!Array.isArray(rules)) throw new Error('Rules should be an array');
    this.rules = deepFreeze(ParamTransform.moveRulesToTop(rules));
    this.ignore = this.ignore.bind(this);
    this.test = this.test.bind(this);
    this.toAPI = this.toAPI.bind(this);
    this.toJS = this.toJS.bind(this);
  }

  /* :: ignore: Function */
  ignore(value: *, key: string, fullData: Object): boolean {
    return this.rules.some((rule) => {
      if (!hasProperty(rule, 'ignore')) return false;
      if (!rule.ignore) throw new Error(RULE_TYPE_ERROR);
      return ParamTransform.doesSucceedRule(rule.ignore, value, key, fullData);
    });
  }

  /* :: test: Function */
  test(value: *, key: string, fullData: Object): boolean {
    return this.rules.some((rule) => {
      if (!hasProperty(rule, 'test')) return false;
      if (!rule.test) throw new Error(RULE_TYPE_ERROR);
      return ParamTransform.doesSucceedRule(rule.test, value, key, fullData);
    });
  }

  getRulesToRun(value: *, key: string, fullData: Object): Array<MapRule> {
    return this.rules.filter((rule) => {
      if (hasProperty(rule, 'test')) {
        if (!rule.test) throw new Error(RULE_TYPE_ERROR);
        const succeedsTest = ParamTransform.doesSucceedRule(rule.test, value, key, fullData);
        if (!succeedsTest) return false;
        if (hasProperty(rule, 'exclude')) {
          if (!rule.exclude) throw new Error(RULE_TYPE_ERROR);
          if (ParamTransform.doesSucceedRule(rule.exclude, value, key, fullData)) return false;
        }
        return true;
      }
      return false;
    });
  }

  /* :: toAPI: Function */
  toAPI(dataToTransform: { [key: string]: * }): { [key: string]: * } {
    return Object.keys(dataToTransform).reduce((result, key) => {
      const value = dataToTransform[key];
      if (this.ignore(value, key, dataToTransform)) return result;

      const nextVal = this.getRulesToRun(value, key, dataToTransform)
        .reduce((value, rule) => rule.toAPI(value, key, dataToTransform, result), value);

      return { ...result, [key]: nextVal };
    }, {});
  }

  /* :: toJS: Function */
  toJS(dataToTransform: { [key: string]: * }): { [key: string]: * } {
    return Object.keys(dataToTransform).reduce((result, key) => {
      const value = dataToTransform[key];
      if (this.ignore(value, key, dataToTransform)) return result;

      const nextVal = this.getRulesToRun(value, key, dataToTransform)
        .reduce((value, rule) => rule.toJS(value, key, dataToTransform, result), value);

      return { ...result, [key]: nextVal };
    }, {});
  }
}

