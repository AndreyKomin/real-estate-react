/** @flow */
import { fromJS, Map, List } from 'immutable';

import * as ActionType from '../actions';
import { MAP_TYPE, MIN_ZOOM, MAX_ZOOM, NO_METRIC } from '../constants';


const defaultState = fromJS({
  zoom: 5,
  canZoomIn: true,
  canZoomOut: true,
  canDisplayPlace: true,
  checkedPushpin: null,
  mapType: MAP_TYPE.CANVAS,
  place: null,
  polygonCords: [],
  property: {},
  propertyIsLoading: false,
  propertyError: null,
  metrics: [],
});

function isUnder(top: number, value: any) {
  return top > (value: number);
}

function isAbove(bottom: number, value: any) {
  return bottom < (value: number);
}

function checkZoom(reducer) {
  return function checkZoomDecorated(state: Map<string, *> = defaultState, action: Object): Map<string, *> {
    const stateToCheck = reducer(state, action).update(
      'zoom',
      (zoom: any) => Math.max(MIN_ZOOM, Math.min(zoom, MAX_ZOOM)),
    );

    return stateToCheck
      .set('canZoomIn', isUnder(MAX_ZOOM, stateToCheck.get('zoom')))
      .set('canZoomOut', isAbove(MIN_ZOOM, stateToCheck.get('zoom')));
  };
}

function triggerPushpin(state, action) {
  return state.set('checkedPushpin', action.value !== state.get('checkedPushpin') ? action.value : null)
    .update('place', place => (
      place && (place: any).get('info') ? null : place
    ));
}

function triggerPlace(state, action) {
  if (state.get('checkedPushpin')) {
    return state.set('checkedPushpin', null)
      .set('place', null);
  }

  if (state.get('place')) {
    return state.merge({ place: null });
  }

  return state.merge({
    place: {
      latitude: action.latitude,
      longitude: action.longitude,
      info: null,
    },
  });
}

function reducer(state: Map<string, *> = defaultState, action: Object): Map<string, *> {
  switch (action.type) {
    case ActionType.SET_ZOOM:
      return state.set('zoom', action.value);

    case ActionType.ZOOM_IN:
      return state.update('zoom', (zoom: any) => zoom + 1);

    case ActionType.ZOOM_OUT:
      return state.update('zoom', (zoom: any) => zoom - 1);

    case ActionType.TRIGGER_PUSHPIN_POPOVER:
      return triggerPushpin(state, action);

    case ActionType.TRIGGER_MAP_TYPE:
      return state.update('mapType', mapType => (mapType === MAP_TYPE.CANVAS ? MAP_TYPE.SATELLITE : MAP_TYPE.CANVAS));

    case ActionType.SET_POLYGON_CORDS:
      return state.merge({ polygonCords: action.cords });

    case ActionType.ADD_POLYGON_CORD:
      return state.update('polygonCords', cords => ((cords: any): List<string>).push(action.cord));

    case ActionType.CLEAR_MAP:
      return defaultState;

    case ActionType.LOAD_PROPERTY:
      return state.merge({
        property: {},
        propertyIsLoading: true,
        propertyError: null,
      });

    case ActionType.LOAD_PROPERTY_SUCCESS:
      return state.merge({
        property: action.response.properties[0],
        propertyIsLoading: false,
        propertyError: null,
      });

    case ActionType.LOAD_PROPERTY_ERROR:
      return state.merge({
        propertyIsLoading: false,
        propertyError: action.error,
      });

    case ActionType.LOAD_METRICS_SUCCESS:
      return state.merge({ metrics: [
        {
          displayName: 'No analytics',
          max: 0,
          maxString: '',
          midString: '',
          min: 0,
          minString: '',
          name: NO_METRIC,
        },
        ...action.response,
      ] });

    case ActionType.TRIGGER_PLACE_ACTIVITY:
      return triggerPlace(state, action).set('canDisplayPlace', true);

    case ActionType.TRIGGER_PLACE_ACTIVITY_SUCCESS:
      return state.update('place', place => (
        place ? (place: any).merge({ info: action.response || null }) : place
      ));

    case ActionType.TRIGGER_PLACE_ACTIVITY_ERROR:
      return state.set('place', null);

    case ActionType.REMOVE_PLACE_ACTIVITY:
      return state.set('place', null).set('canDisplayPlace', false);

    case ActionType.CLEAR_POLYGON:
      return state.set('polygonCords', defaultState.get('polygonCords'));

    default:
      return state;
  }
}

export default checkZoom(reducer);
