/** @flow */
export const MAP_TYPE = {
  CANVAS: 'canvasLight',
  SATELLITE: 'aerial',
};

export const MIN_ZOOM = 1;
export const MAX_ZOOM = 20;

export const NO_METRIC = 'none';
