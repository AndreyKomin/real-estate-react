/** @flow */
import { createSelector } from 'reselect';
import { fromJS, Map } from 'immutable';

import { selectGraph } from 'data/statistic/selectors';
import { Colors } from 'data/property';


const emptyMap = fromJS({});

const checkState = state => state;

export const selectMap = createSelector(
  checkState,
  state => state.getIn(['map']),
);

export const selectIsPropertyLoading = createSelector(
  selectMap,
  map => map.get('propertyIsLoading'),
);

export const selectProperty = createSelector(
  selectMap,
  map => map.get('property', emptyMap),
);

export const selectGraphAndColors = createSelector(
  checkState,
  (graphData) => {
    const graph = selectGraph(graphData);
    const colors = Map(graph.categories.map((category, index) => [category, Colors[index % 10]]));
    return fromJS({ ...graph, colors });
  },
);

export const selectEstimatedValueGraph = createSelector(
  checkState,
  (state) => {
    const estimatedValueGraph = state.getIn(['map', 'property', 'estimatedValueGraph']);
    return estimatedValueGraph ? selectGraphAndColors(estimatedValueGraph) : emptyMap;
  },
);

export const selectMetrics = createSelector(
  selectMap,
  map => map.get('metrics'),
);

export const selectPlacePushpin = createSelector(
  selectMap,
  map => map.get('place'),
);
