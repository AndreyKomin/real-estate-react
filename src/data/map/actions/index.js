/** @flow */
import { CALL_API } from 'store/middleware/api';
import type { List } from 'immutable';


export const CHANGE_SEARCH_VALUE = Symbol('CHANGE_SEARCH_VALUE');

export const ZOOM_IN = Symbol('ZOOM_IN');

export function zoomIn() {
  return {
    type: ZOOM_IN,
  };
}

export const ZOOM_OUT = Symbol('ZOOM_OUT');

export function zoomOut() {
  return {
    type: ZOOM_OUT,
  };
}

export const SET_ZOOM = Symbol('SET_ZOOM');

export function setZoom(value: number) {
  return {
    type: SET_ZOOM,
    value,
  };
}

export const TRIGGER_PUSHPIN_POPOVER = Symbol('TRIGGER_PUSHPIN_POPOVER');

export function triggerPushpinPopover(value: number | string) {
  return {
    type: TRIGGER_PUSHPIN_POPOVER,
    value,
  };
}

export const TRIGGER_PLACE_ACTIVITY = Symbol('TRIGGER_PLACE_ACTIVITY');
export const TRIGGER_PLACE_ACTIVITY_SUCCESS = Symbol('TRIGGER_PLACE_ACTIVITY_SUCCESS');
export const TRIGGER_PLACE_ACTIVITY_ERROR = Symbol('TRIGGER_PLACE_ACTIVITY_ERROR');

export function triggerPlaceActivity({ latitude, longitude }: { latitude: number, longitude: number }) {
  const request: any = { latitude, longitude };

  request[CALL_API] = {
    method: 'get',
    path: `/resource/auth/ps4/map/parcels/${latitude}/${longitude}/property`,
    unifier: 'search property by place',
    dislodging: true,
    startType: TRIGGER_PLACE_ACTIVITY,
    successType: TRIGGER_PLACE_ACTIVITY_SUCCESS,
    errorType: TRIGGER_PLACE_ACTIVITY_ERROR,
  };

  return request;
}

export const REMOVE_PLACE_ACTIVITY = Symbol('REMOVE_PLACE_ACTIVITY');

export function removePlaceActivity() {
  return {
    type: REMOVE_PLACE_ACTIVITY,
  };
}

export const TRIGGER_MAP_TYPE = Symbol('TRIGGER_MAP_TYPE');

export function triggerMapType() {
  return {
    type: TRIGGER_MAP_TYPE,
  };
}

export const SET_POLYGON_CORDS = Symbol('SET_POLYGON_CORDS');

export function setPolygonCords(cords: List<string> | Array<string>) {
  return {
    type: SET_POLYGON_CORDS,
    cords,
  };
}

export const ADD_POLYGON_CORD = Symbol('ADD_POLYGON_CORD');

export function addPolygonCord(cord: string) {
  return {
    type: ADD_POLYGON_CORD,
    cord,
  };
}


export const CLEAR_MAP = Symbol('CLEAR_MAP');

export function clearMap() {
  return {
    type: CLEAR_MAP,
  };
}


export const LOAD_PROPERTY = Symbol('LOAD_PROPERTY');
export const LOAD_PROPERTY_SUCCESS = Symbol('LOAD_PROPERTY_SUCCESS');
export const LOAD_PROPERTY_ERROR = Symbol('LOAD_PROPERTY_ERROR');

export function loadProperty(propertyId: number): Object {
  const request: any = {};

  request[CALL_API] = {
    method: 'get',
    path: `/resource/auth/ps4/property/${propertyId}?loadSurrounding=false`,
    unifier: 'search property',
    dislodging: true,
    startType: LOAD_PROPERTY,
    successType: LOAD_PROPERTY_SUCCESS,
    errorType: LOAD_PROPERTY_ERROR,
  };

  return request;
}


export const LOAD_METRICS = Symbol('LOAD_METRICS');
export const LOAD_METRICS_SUCCESS = Symbol('LOAD_METRICS_SUCCESS');
export const LOAD_METRICS_ERROR = Symbol('LOAD_METRICS_ERROR');

export function loadMetrics() {
  const request: any = {};

  request[CALL_API] = {
    method: 'get',
    path: '/resource/auth/ps4/map/statistics/metrics',
    unifier: 'search property',
    dislodging: true,
    startType: LOAD_METRICS,
    successType: LOAD_METRICS_SUCCESS,
    errorType: LOAD_METRICS_ERROR,
  };

  return request;
}


export const CLEAR_POLYGON = Symbol('CLEAR_POLYGON');

export function clearPolygon() {
  return {
    type: CLEAR_POLYGON,
  };
}
