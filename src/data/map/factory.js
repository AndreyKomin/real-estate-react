/** @flow */
import { fromJS, Map, List } from 'immutable';

import { MAP_TYPE, MIN_ZOOM, MAX_ZOOM, NO_METRIC } from './constants';


const defaultState = fromJS({
  zoom: 5,
  canZoomIn: true,
  canZoomOut: true,
  checkedPushpin: null,
  mapType: MAP_TYPE.CANVAS,
  polygonCords: [],
  metrics: [],
});

function isUnder(top: number, value: any) {
  return top > (value: number);
}

function isAbove(bottom: number, value: any) {
  return bottom < (value: number);
}

function checkZoom(reducer) {
  return function checkZoomDecorated(state: Map<string, *> = defaultState, action: Object): Map<string, *> {
    const stateToCheck = reducer(state, action).update(
      'zoom',
      (zoom: any) => Math.max(MIN_ZOOM, Math.min(zoom, MAX_ZOOM)),
    );

    return stateToCheck
      .set('canZoomIn', isUnder(MAX_ZOOM, stateToCheck.get('zoom')))
      .set('canZoomOut', isAbove(MIN_ZOOM, stateToCheck.get('zoom')));
  };
}


export default function (ActionType: Object, prefix: string = '') {
  function reducer(state: Map<string, *> = defaultState, action: Object): Map<string, *> {
    switch (action.type) {
      case ActionType[`${prefix}SET_ZOOM`]:
        return state.set('zoom', action.value);

      case ActionType[`${prefix}ZOOM_IN`]:
        return state.update('zoom', (zoom: any) => zoom + 1);

      case ActionType[`${prefix}ZOOM_OUT`]:
        return state.update('zoom', (zoom: any) => zoom - 1);

      case ActionType[`${prefix}TRIGGER_PUSHPIN_POPOVER`]:
        return state.set('checkedPushpin', action.value !== state.get('checkedPushpin') ? action.value : null);

      case ActionType[`${prefix}TRIGGER_MAP_TYPE`]:
        return state.update('mapType', mapType => (mapType === MAP_TYPE.CANVAS ? MAP_TYPE.SATELLITE : MAP_TYPE.CANVAS));

      case ActionType[`${prefix}SET_POLYGON_CORDS`]:
        return state.merge({ polygonCords: action.cords });

      case ActionType[`${prefix}ADD_POLYGON_CORD`]:
        return state.update('polygonCords', cords => ((cords: any): List<string>).push(action.cord));

      case ActionType[`${prefix}LOAD_METRICS_SUCCESS`]:
        return state.merge({ metrics: [
          {
            displayName: 'No analytics',
            max: 0,
            maxString: '',
            midString: '',
            min: 0,
            minString: '',
            name: NO_METRIC,
          },
          ...action.response,
        ] });

      default:
        return state;
    }
  }

  return checkZoom(reducer);
}


export const CHANGE_SEARCH_VALUE = Symbol('CHANGE_SEARCH_VALUE');

export const ZOOM_IN = Symbol('ZOOM_IN');

export function zoomIn() {
  return {
    type: ZOOM_IN,
  };
}

export const ZOOM_OUT = Symbol('ZOOM_OUT');

export function zoomOut() {
  return {
    type: ZOOM_OUT,
  };
}

export const SET_ZOOM = Symbol('SET_ZOOM');

export function setZoom(value: number) {
  return {
    type: SET_ZOOM,
    value,
  };
}

export const TRIGGER_PUSHPIN_POPOVER = Symbol('TRIGGER_PUSHPIN_POPOVER');

export function triggerPushpinPopover(value: number | string) {
  return {
    type: TRIGGER_PUSHPIN_POPOVER,
    value,
  };
}

export const TRIGGER_MAP_TYPE = Symbol('TRIGGER_MAP_TYPE');

export function triggerMapType() {
  return {
    type: TRIGGER_MAP_TYPE,
  };
}

export const SET_POLYGON_CORDS = Symbol('SET_POLYGON_CORDS');

export function setPolygonCords(cords: List<string> | Array<string>) {
  return {
    type: SET_POLYGON_CORDS,
    cords,
  };
}

export const ADD_POLYGON_CORD = Symbol('ADD_POLYGON_CORD');

export function addPolygonCord(cord: string) {
  return {
    type: ADD_POLYGON_CORD,
    cord,
  };
}
