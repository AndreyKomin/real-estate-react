import { CALL_API, CHAIN_API } from 'store/middleware/api';
import { joinPath } from 'utils/string';

import { GroupSearchModes } from './constants';


export const SEARCH_GROUP_CONTEXT = Symbol('SEARCH_GROUP_CONTEXT');
export const SEARCH_GROUP_CONTEXT_SUCCESS = Symbol('SEARCH_GROUP_CONTEXT_SUCCESS');
export const SEARCH_GROUP_CONTEXT_ERROR = Symbol('SEARCH_GROUP_CONTEXT_ERROR');
export const EXPORT_GROUP_CONTEXT = Symbol('EXPORT_GROUP_CONTEXT');
export const EXPORT_GROUP_CONTEXT_SUCCESS = Symbol('EXPORT_GROUP_CONTEXT_SUCCESS');
export const EXPORT_GROUP_CONTEXT_ERROR = Symbol('EXPORT_GROUP_CONTEXT_ERROR');
export const DELETE_PROPERTY_GROUP = Symbol('DELETE_PROPERTY_GROUP');
export const DELETE_PROPERTY_GROUP_SUCCESS = Symbol('DELETE_PROPERTY_GROUP_SUCCESS');
export const DELETE_PROPERTY_GROUP_ERROR = Symbol('DELETE_PROPERTY_GROUP_ERROR');
export const UPDATE_PROPERTY_GROUP = Symbol('UPDATE_PROPERTY_GROUP');
export const UPDATE_PROPERTY_GROUP_SUCCESS = Symbol('UPDATE_PROPERTY_GROUP_SUCCESS');
export const UPDATE_PROPERTY_GROUP_ERROR = Symbol('UPDATE_PROPERTY_GROUP_ERROR');
export const DELETE_GROUP_CONTEXT = Symbol('DELETE_GROUP_CONTEXT');
export const DELETE_GROUP_CONTEXT_SUCCESS = Symbol('DELETE_GROUP_CONTEXT_SUCCESS');
export const DELETE_GROUP_CONTEXT_ERROR = Symbol('DELETE_GROUP_CONTEXT_ERROR');
export const SAVE_GROUP_CONTEXT = Symbol('SAVE_GROUP_CONTEXT');
export const SAVE_GROUP_CONTEXT_SUCCESS = Symbol('SAVE_GROUP_CONTEXT_SUCCESS');
export const SAVE_GROUP_CONTEXT_ERROR = Symbol('SAVE_GROUP_CONTEXT_ERROR');
export const LOAD_GROUP_PUSHPINS = Symbol('LOAD_GROUP_PUSHPINS');
export const LOAD_GROUP_PUSHPINS_SUCCESS = Symbol('LOAD_GROUP_PUSHPINS_SUCCESS');
export const LOAD_GROUP_PUSHPINS_ERROR = Symbol('LOAD_GROUP_PUSHPINS_ERROR');
export const SAVE_PROPERTY = Symbol('SAVE_PROPERTY');
export const SAVE_PROPERTY_SUCCESS = Symbol('SAVE_PROPERTY_SUCCESS');
export const SAVE_PROPERTY_ERROR = Symbol('SAVE_PROPERTY_ERROR');
export const SAVE_PROPERTIES = Symbol('SAVE_PROPERTIES');
export const SAVE_PROPERTIES_SUCCESS = Symbol('SAVE_PROPERTIES_SUCCESS');
export const SAVE_PROPERTIES_ERROR = Symbol('SAVE_PROPERTIES_ERROR');
export const GET_PROPERTY = Symbol('GET_PROPERTY');
export const GET_PROPERTY_SUCCESS = Symbol('GET_PROPERTY_SUCCESS');
export const GET_PROPERTY_ERROR = Symbol('GET_PROPERTY_ERROR');
export const GET_SAVED_PROPERTY = Symbol('GET_SAVED_PROPERTY');
export const GET_SAVED_PROPERTY_SUCCESS = Symbol('GET_SAVED_PROPERTY_SUCCESS');
export const GET_SAVED_PROPERTY_ERROR = Symbol('GET_SAVED_PROPERTY_ERROR');
export const SEARCH_SURROUNDING_PROPERTIES = Symbol('SEARCH_SURROUNDING_PROPERTIES');
export const SEARCH_SURROUNDING_PROPERTIES_SUCCESS = Symbol('SEARCH_SURROUNDING_PROPERTIES_SUCCESS');
export const SEARCH_SURROUNDING_PROPERTIES_ERROR = Symbol('SEARCH_SURROUNDING_PROPERTIES_ERROR');
export const SEARCH_COMPARABLES = Symbol('SEARCH_COMPARABLES');
export const SEARCH_COMPARABLES_SUCCESS = Symbol('SEARCH_COMPARABLES_SUCCESS');
export const SEARCH_COMPARABLES_ERROR = Symbol('SEARCH_COMPARABLES_ERROR');
export const DOWNLOAD_COMPARABLES = Symbol('DOWNLOAD_COMPARABLES');
export const DOWNLOAD_COMPARABLES_SUCCESS = Symbol('DOWNLOAD_COMPARABLES_SUCCESS');
export const DOWNLOAD_COMPARABLES_ERROR = Symbol('DOWNLOAD_COMPARABLES_ERROR');
export const LOAD_PROPERTY_DOCUMENTS = Symbol('LOAD_PROPERTY_DOCUMENTS');
export const LOAD_PROPERTY_DOCUMENTS_SUCCESS = Symbol('LOAD_PROPERTY_DOCUMENTS_SUCCESS');
export const LOAD_PROPERTY_DOCUMENTS_ERROR = Symbol('LOAD_PROPERTY_DOCUMENTS_ERROR');
export const EDIT_PROPERTY_DOCUMENT = Symbol('EDIT_PROPERTY_DOCUMENT');
export const EDIT_PROPERTY_DOCUMENT_SUCCESS = Symbol('EDIT_PROPERTY_DOCUMENT_SUCCESS');
export const EDIT_PROPERTY_DOCUMENT_ERROR = Symbol('EDIT_PROPERTY_DOCUMENT_ERROR');
export const UPLOAD_PROPERTY_DOCUMENT = Symbol('UPLOAD_PROPERTY_DOCUMENT');
export const UPLOAD_PROPERTY_DOCUMENT_SUCCESS = Symbol('UPLOAD_PROPERTY_DOCUMENT_SUCCESS');
export const UPLOAD_PROPERTY_DOCUMENT_ERROR = Symbol('UPLOAD_PROPERTY_DOCUMENT_ERROR');
export const DOWNLOAD_PROPERTY_DOCUMENT = Symbol('DOWNLOAD_PROPERTY_DOCUMENT');
export const DOWNLOAD_PROPERTY_DOCUMENT_SUCCESS = Symbol('DOWNLOAD_PROPERTY_DOCUMENT_SUCCESS');
export const DOWNLOAD_PROPERTY_DOCUMENT_ERROR = Symbol('DOWNLOAD_PROPERTY_DOCUMENT_ERROR');
export const DELETE_PROPERTY_DOCUMENT = Symbol('DELETE_PROPERTY_DOCUMENT');
export const DELETE_PROPERTY_DOCUMENT_SUCCESS = Symbol('DELETE_PROPERTY_DOCUMENT_SUCCESS');
export const DELETE_PROPERTY_DOCUMENT_ERROR = Symbol('DELETE_PROPERTY_DOCUMENT_ERROR');
export const PURCHASE_REPORT = Symbol('PURCHASE_REPORT');
export const PURCHASE_REPORT_SUCCESS = Symbol('PURCHASE_REPORT_SUCCESS');
export const PURCHASE_REPORT_ERROR = Symbol('PURCHASE_REPORT_ERROR');
export const DOWNLOAD_PROPERTY_REPORT = Symbol('DOWNLOAD_PROPERTY_REPORT');
export const DOWNLOAD_PROPERTY_REPORT_SUCCESS = Symbol('DOWNLOAD_PROPERTY_REPORT_SUCCESS');
export const DOWNLOAD_PROPERTY_REPORT_ERROR = Symbol('DOWNLOAD_PROPERTY_REPORT_ERROR');
export const GET_STREET_VIEW = Symbol('GET_STREET_VIEW');
export const GET_STREET_VIEW_SUCCESS = Symbol('GET_STREET_VIEW_SUCCESS');
export const GET_STREET_VIEW_ERROR = Symbol('GET_STREET_VIEW_ERROR');

export const SEARCH_GROUP_CONTEXT_CACHED = Symbol('SEARCH_GROUP_CONTEXT_CACHED');
export const LOAD_GROUP_CONTEXT = Symbol('LOAD_GROUP_CONTEXT');
export const UPDATE_GROUP_CONTEXT = Symbol('UPDATE_GROUP_CONTEXT');
export const SELECT_GROUP_CONTEXT = Symbol('SELECT_GROUP_CONTEXT');
export const UPDATE_SURROUNDING_PROPERTY_SEARCH = Symbol('UPDATE_SURROUNDING_PROPERTY_SEARCH');
export const UPDATE_SURROUNDING_PROPERTY_SELECTION = Symbol('UPDATE_SURROUNDING_PROPERTY_SELECTION');
export const UPDATE_SURROUNDING_PROPERTY_TYPE = Symbol('UPDATE_SURROUNDING_PROPERTY_TYPE');
export const SORT_SURROUNDING_PROPERTIES = Symbol('SORT_SURROUNDING_PROPERTIES');
export const ASSIGN_PROPERTIES = Symbol('ASSIGN_PROPERTIES');
export const UNASSIGN_PROPERTIES = Symbol('UNASSIGN_PROPERTIES');


function request(path, method, startType, successType, errorType, body, { afterSuccess, afterError, successParams, download = false, query } = {}) {
  return {
    [CALL_API]: {
      method,
      startType,
      successType,
      errorType,
      body,
      afterSuccess,
      afterError,
      successParams,
      query,
      download,
      path: joinPath('/resource/auth/ps4', path),
      // ...(method === 'post' && !isFormData(body) ? { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } } : {}),
    },
  };
}

const getSearch = context => context.merge({ data: null, info: null, fullSelection: null, name: context.getIn(['info', 'name']), type: context.getIn(['info', 'type']) });

const property = (path, method, startType, successType, errorType, body, params) => request(joinPath('property/new', path), method, startType, successType, errorType, body, params);

const user = (path, method, startType, successType, errorType, body, params) => request(joinPath('user/new/properties', path), method, startType, successType, errorType, body, params);

const group = (path, method, startType, successType, errorType, body, params) => user(joinPath('groups', path), method, startType, successType, errorType, body, params);

export const saveProperty = (id, group, afterSuccess) => user(id, 'post', SAVE_PROPERTY, SAVE_PROPERTY_SUCCESS, SAVE_PROPERTY_ERROR, group || {}, { afterSuccess });

export const saveProperties = (ids, groupName, groupType, listingType, afterSuccess) => user(null, 'post', SAVE_PROPERTIES, SAVE_PROPERTIES_SUCCESS, SAVE_PROPERTIES_ERROR, ids, { afterSuccess, query: { groupName, groupType, listingType } });

export const getSavedProperty = id => user(id, 'get', GET_SAVED_PROPERTY, GET_SAVED_PROPERTY_SUCCESS, GET_SAVED_PROPERTY_ERROR);

export const getProperty = id => property(id, 'get', GET_PROPERTY, GET_PROPERTY_SUCCESS, GET_PROPERTY_ERROR);

export const searchGroupContext = (context) => {
  if (context.getIn(['info', 'data'])) return { type: SEARCH_GROUP_CONTEXT_CACHED, context };
  return group(null, 'post', SEARCH_GROUP_CONTEXT, SEARCH_GROUP_CONTEXT_SUCCESS, SEARCH_GROUP_CONTEXT_ERROR, getSearch(context).set('selection', null), { successParams: { context } });
};

export const loadContextIds = (context, selection) => group(null, 'post', SEARCH_GROUP_CONTEXT, SEARCH_GROUP_CONTEXT_SUCCESS, SEARCH_GROUP_CONTEXT_ERROR, { selection }, { successParams: { context } });

export const exportGroupContext = context => group('export', 'post', EXPORT_GROUP_CONTEXT, EXPORT_GROUP_CONTEXT_SUCCESS, EXPORT_GROUP_CONTEXT_ERROR, getSearch(context), { download: true });

export const deletePropertyGroup = (id, deleteContacts) => request(`user/new2/properties/groups/${id}`, 'delete', DELETE_PROPERTY_GROUP, DELETE_PROPERTY_GROUP_SUCCESS, DELETE_PROPERTY_GROUP_ERROR, null, { query: { deleteContacts } });

export const deleteGroupContext = (context, deleteContacts) => request('user/new2/properties/groups', 'delete', DELETE_GROUP_CONTEXT, DELETE_GROUP_CONTEXT_SUCCESS, DELETE_GROUP_CONTEXT_ERROR, getSearch(context), { query: { deleteContacts } });

export const saveGroupContext = (context, afterSuccess) => request('user/new2/properties/groups', 'put', SAVE_GROUP_CONTEXT, SAVE_GROUP_CONTEXT_SUCCESS, SAVE_GROUP_CONTEXT_ERROR, getSearch(context), { afterSuccess });

export const updatePropertyGroup = group => request('user/new2/properties/groups', 'put', UPDATE_PROPERTY_GROUP, UPDATE_PROPERTY_GROUP_SUCCESS, UPDATE_PROPERTY_GROUP_ERROR, group);

export const loadGroupPushpins = (context, afterSuccess) => group(null, 'post', LOAD_GROUP_PUSHPINS, LOAD_GROUP_PUSHPINS_SUCCESS, LOAD_GROUP_PUSHPINS_ERROR, getSearch(context).set('mode', GroupSearchModes.PUSHPIN), { afterSuccess });

export const loadGroupContext = (name, groupId, defaults) => ({ type: LOAD_GROUP_CONTEXT, name, groupId, defaults });

export const updateGroupContext = context => ({ type: UPDATE_GROUP_CONTEXT, context });

export const updateGroupContextSelection = (context, select, index) => ({ type: SELECT_GROUP_CONTEXT, context, select, index });

export const updateSurroundingPropertySearch = (search, typeIndex) => ({ type: UPDATE_SURROUNDING_PROPERTY_SEARCH, search, typeIndex });

export const searchComparables = (subjectPropertyId, search, afterSuccess) => property('comparables', 'post', SEARCH_COMPARABLES, SEARCH_COMPARABLES_SUCCESS, SEARCH_COMPARABLES_ERROR, search.merge({ subjectPropertyId }).update('shapeDefinition', def => (def ? def.join('/') : null)), { afterSuccess });

export const downloadComparables = (subjectPropertyId, search) => property('comparables/report', 'post', DOWNLOAD_COMPARABLES, DOWNLOAD_COMPARABLES_SUCCESS, DOWNLOAD_COMPARABLES_ERROR, search.merge({ subjectPropertyId }).update('shapeDefinition', def => (def ? def.join('/') : null)), { download: true });

export const searchSurroundingProperties = (subjectPropertyId, listingType, search) => {
  // Sending listingType as query param as hack to get it to reducer. Should be a way to pass params to reducer in all stages.
  if (listingType) return property(`${subjectPropertyId}/surrounding/${listingType}`, 'post', SEARCH_SURROUNDING_PROPERTIES, SEARCH_SURROUNDING_PROPERTIES_SUCCESS, SEARCH_SURROUNDING_PROPERTIES_ERROR, search, { successParams: { listingType }, query: { listingType } });
  return property(`${subjectPropertyId}/surrounding`, 'get', SEARCH_SURROUNDING_PROPERTIES, SEARCH_SURROUNDING_PROPERTIES_SUCCESS, SEARCH_SURROUNDING_PROPERTIES_ERROR);
};

export const sortSurroundingProperties = sort => ({ type: SORT_SURROUNDING_PROPERTIES, sort });

export const updateSurroundingPropertySelection = (index, selected) => ({ type: UPDATE_SURROUNDING_PROPERTY_SELECTION, index, selected });

export const updateSurroundingPropertyType = newType => ({ type: UPDATE_SURROUNDING_PROPERTY_TYPE, newType });

export const loadPropertyDocuments = savedPropertyId => request(`user/properties/${savedPropertyId}/documents`, 'get', LOAD_PROPERTY_DOCUMENTS, LOAD_PROPERTY_DOCUMENTS_SUCCESS, LOAD_PROPERTY_DOCUMENTS_ERROR);

export const editPropertyDocument = document => request('user/documents', 'put', EDIT_PROPERTY_DOCUMENT, EDIT_PROPERTY_DOCUMENT_SUCCESS, EDIT_PROPERTY_DOCUMENT_ERROR, document);

export const uploadPropertyDocument = (savedPropertyId, document) => request(`user/properties/${savedPropertyId}/documents`, 'post', UPLOAD_PROPERTY_DOCUMENT, UPLOAD_PROPERTY_DOCUMENT_SUCCESS, UPLOAD_PROPERTY_DOCUMENT_ERROR, document);

export const downloadPropertyDocument = documentId => request(`user/documents/${documentId}`, 'get', DOWNLOAD_PROPERTY_DOCUMENT, DOWNLOAD_PROPERTY_DOCUMENT_SUCCESS, DOWNLOAD_PROPERTY_DOCUMENT_ERROR, null, { download: true });

export const deletePropertyDocument = (documentId, afterSuccess) => request(`user/documents/${documentId}`, 'delete', DELETE_PROPERTY_DOCUMENT, DELETE_PROPERTY_DOCUMENT_SUCCESS, DELETE_PROPERTY_DOCUMENT_ERROR, null, { afterSuccess });

export const purchaseReport = (savedPropertyId, reportCode, confirmedPrice, propertySaleId) => request(`user/properties/${savedPropertyId}/reports/${reportCode}`, 'post', PURCHASE_REPORT, PURCHASE_REPORT_SUCCESS, PURCHASE_REPORT_ERROR, document, { query: { confirmedPrice, propertySaleId } });

export const downloadReport = (propertyId, report) => request(`property/${propertyId}/reports/${report}`, 'get', DOWNLOAD_PROPERTY_REPORT, DOWNLOAD_PROPERTY_REPORT_SUCCESS, DOWNLOAD_PROPERTY_REPORT_ERROR, null, { download: true });

export const getStreetView = propertyId => request(`map/streetside/${propertyId}`, 'get', GET_STREET_VIEW, GET_STREET_VIEW_SUCCESS, GET_STREET_VIEW_ERROR);

export const purchaseReportAndLoad = (propertyId, reportCode, confirmedPrice, propertySaleId) => ({
  [CHAIN_API]: [
    () => purchaseReport(propertyId, reportCode, confirmedPrice, propertySaleId),
    response => downloadPropertyDocument(response.find(d => d.modified).id),
  ],
});

export const assignContext = () => ({ type: ASSIGN_PROPERTIES });

export const unassignContext = () => ({ type: UNASSIGN_PROPERTIES });
