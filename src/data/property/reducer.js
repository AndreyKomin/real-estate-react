import { Map, List, fromJS } from 'immutable';
import { AUTHENTICATION_SUCCESS } from 'data/user';
import moment from 'moment-timezone';
import {
  loading,
  success,
  error,
  getSortComparator,
  mergeGroups,
  updateGroupContext,
  setGroupContextSelection,
  mergeGroupResults,
  searchGroup,
  loadGroupContext,
  assignContext,
  unassignContext,
} from 'reducers';

import { ListingTypesList, SAVE_LISTINGS, SAVE_LISTINGS_SUCCESS, SAVE_LISTINGS_ERROR } from '../listing';
import { Colors, getBingUrl } from './constants';
import * as ActionType from './actions';


moment.tz.setDefault('Etc/GMT+0');
const now = Date.now();
const date = new Date(now);

const defaultSurroundingPropertySearch = {
  saleDateMin: null,
  saleDateMax: null,
  squareFeetMax: null,
  squareFeetMin: null,
  distanceFromSubjectMax: 1,
  shapeDefinition: null,
  bedroomsMin: null,
  bedroomsMax: null,
  bathroomsMin: null,
  bathroomsMax: null,
  status: null,
};

const defaultSurroundingPropertyType = {
  type: null,
  label: null,
  title: null,
  sort: 'distanceFromSubject',
  properties: [],
  loading: true,
  enabled: false,
  index: null,
  search: { ...defaultSurroundingPropertySearch },
};

const defaultSurroundingPropertyTypes = [
  { type: 'Comparable', label: 'Comps', title: 'Comparable Properties', search: { ...defaultSurroundingPropertySearch, saleDateMin: moment(date.setFullYear(date.getFullYear() - 1)), saleDateMax: moment(now), distanceFromSubjectMax: 0.5 } },
  { type: 'Neighbor', label: 'Neighbors' },
].concat(ListingTypesList.toJS().map(({ value: type, label }) => ({ type, label })))
  .map((t, i) => ({ ...defaultSurroundingPropertyType, ...t, index: i }));

const defaultSurroundingPropertyInfo = {
  surroundingPropertyTypes: defaultSurroundingPropertyTypes,
  surroundingPropertyType: defaultSurroundingPropertyTypes[0],
};

const defaultProperty = {
  address: {},
  estimatedValueGraph: {},
  compSaleAmountGraph: {},
};

const defaultState = fromJS({
  groups: [],
  groupInfo: {},
  property: defaultProperty,
  contexts: {},
  pushpins: [],
  documents: [],
  streetView: null,
  loading: false,
  error: null,
  pushpinLoading: false,
  pushpinError: null,
  comparableLoading: false,
  comparableError: null,
  documentLoading: false,
  documentError: null,
  streetViewLoading: false,
  streetViewError: null,
  ...defaultSurroundingPropertyInfo,
});


const getGraph = (data) => {
  if (!data) return Map();

  const emptyList = List();
  const graph = data.get('series', emptyList).reduce((info, s) => {
    const category = s.get('category', '');
    const points = info.get('points', emptyList);

    return info.merge({
      points: s.get('points').filter(p => !!p.get('value')).map(p => (points.find(p2 => p2.get('label') === p.get('label')) || p).set(category, p.get('value'))),
      categories: info.get('categories').push(category),
    });
  }, Map({ type: data.getIn(['type']), points: emptyList, categories: emptyList }));


  return graph.set('colors', Map(graph.get('categories').map((c, i) => [c, Colors[i % 10]])));
};

const setProperty = (state, property = defaultProperty) => {
  const { latitude, longitude } = property;
  return state.merge({
    ...defaultSurroundingPropertyInfo,
    documents: [],
    streetView: null,
    property: fromJS(property)
      .update('estimatedValueGraph', g => getGraph(g))
      .update('compSaleAmountGraph', g => getGraph(g))
      .set('mapUrl', getBingUrl(latitude, longitude)),
  });
};

export default function reducer(state = defaultState, action, t1, t2, t3) {
  const { response, sort, index, selected } = action;

  console.log('reducer', action, t1, t2, t3);

  const setTypeProperties = (state, index, properties = []) => (
    state.mergeIn(['surroundingPropertyTypes', index], {
      loading: false,
      properties: fromJS(
        properties.map((p) => {
          const { address: { streetAddress, cityName, zip } = {}, lastSale: { saleDate, saleAmount } = {}, lastSaleDate, lastSaleAmount } = p;
          return { ...p, selected: true, streetAddress, cityName, zip, saleDate: saleDate || lastSaleDate, saleAmount: saleAmount || lastSaleAmount, pricePerSquareFoot: saleAmount && p.squareFeet ? saleAmount / p.squareFeet : null };
        }),
      ).sort(getSortComparator(state.getIn(['surroundingPropertyTypes', index, 'sort']))),
    })
  );

  const setType = state => state.set('surroundingPropertyType', state.getIn(['surroundingPropertyTypes', state.getIn(['surroundingPropertyType', 'index'])]));

  switch (action.type) {
    case AUTHENTICATION_SUCCESS: {
      const types = action.response.availableListingTypes || [];
      defaultSurroundingPropertyInfo.surroundingPropertyTypes = defaultSurroundingPropertyTypes.map(t => ({ ...t, enabled: t.type.length > 1 || types.includes(t.type) }));
      return mergeGroups(state, response.propertyGroups).merge({ ...defaultSurroundingPropertyInfo });
    }

    case SAVE_LISTINGS:
    case ActionType.SEARCH_GROUP_CONTEXT:
    case ActionType.EXPORT_GROUP_CONTEXT:
    case ActionType.DELETE_PROPERTY_GROUP:
    case ActionType.DELETE_GROUP_CONTEXT:
    case ActionType.SAVE_GROUP_CONTEXT:
    case ActionType.SAVE_PROPERTY:
    case ActionType.SAVE_PROPERTIES:
    case ActionType.UPDATE_PROPERTY_GROUP:
      return loading(state);

    case ActionType.GET_PROPERTY:
    case ActionType.GET_SAVED_PROPERTY:
      return loading(setProperty(state));

    case ActionType.LOAD_GROUP_PUSHPINS:
      return loading(state, 'pushpin');

    case ActionType.DOWNLOAD_COMPARABLES:
      return loading(state, 'comparable');

    case ActionType.SEARCH_COMPARABLES:
      return loading(state.setIn(['surroundingPropertyTypes', 0, 'loading'], true), 'comparable');

    case ActionType.SEARCH_SURROUNDING_PROPERTIES:
      return setType(action.query.listingType ? state.setIn(['surroundingPropertyTypes', ListingTypesList.findIndex(t => t.get('value') === action.query.listingType) + 2, 'loading'], true) : state);

    case SAVE_LISTINGS_ERROR:
    case ActionType.SEARCH_GROUP_CONTEXT_ERROR:
    case ActionType.EXPORT_GROUP_CONTEXT_ERROR:
    case ActionType.DELETE_PROPERTY_GROUP_ERROR:
    case ActionType.DELETE_GROUP_CONTEXT_ERROR:
    case ActionType.SAVE_GROUP_CONTEXT_ERROR:
    case ActionType.SAVE_PROPERTY_ERROR:
    case ActionType.SAVE_PROPERTIES_ERROR:
    case ActionType.GET_PROPERTY_ERROR:
    case ActionType.GET_SAVED_PROPERTY_ERROR:
    case ActionType.UPDATE_PROPERTY_GROUP_ERROR:
      return error(state, action);

    case ActionType.LOAD_GROUP_PUSHPINS_ERROR:
      return error(state, 'pushpin');

    case ActionType.SEARCH_COMPARABLES_ERROR:
    case ActionType.DOWNLOAD_COMPARABLES_ERROR:
    // case ActionType.SEARCH_SURROUNDING_PROPERTIES_ERROR:
      return error(state, 'comparable');

    case ActionType.SEARCH_GROUP_CONTEXT_SUCCESS:
      return success(mergeGroupResults(state, action.context, response));

    case ActionType.EXPORT_GROUP_CONTEXT_SUCCESS:
      return success(state);

    case ActionType.DELETE_PROPERTY_GROUP_SUCCESS:
    case ActionType.DELETE_GROUP_CONTEXT_SUCCESS:
    case ActionType.SAVE_GROUP_CONTEXT_SUCCESS:
    case ActionType.UPDATE_PROPERTY_GROUP_SUCCESS:
    case ActionType.SAVE_PROPERTIES_SUCCESS:
      return success(mergeGroups(state, response.groups));

    case SAVE_LISTINGS_SUCCESS:
    case ActionType.SAVE_PROPERTY_SUCCESS:
      return success(setProperty(mergeGroups(state, response.groups), response.property));

    case ActionType.SEARCH_GROUP_CONTEXT_CACHED:
      return searchGroup(state, action.context);

    case ActionType.LOAD_GROUP_CONTEXT:
      return loadGroupContext(state, action);

    case ActionType.UPDATE_GROUP_CONTEXT:
      return updateGroupContext(state, action.context);

    case ActionType.SELECT_GROUP_CONTEXT:
      return setGroupContextSelection(state, action);

    case ActionType.LOAD_GROUP_PUSHPINS_SUCCESS:
      return success(state.set('pushpins', fromJS(response.data)), 'pushpin');

    case ActionType.GET_PROPERTY_SUCCESS:
    case ActionType.GET_SAVED_PROPERTY_SUCCESS:
      return success(setProperty(state, response));

    case ActionType.UPDATE_SURROUNDING_PROPERTY_SEARCH: {
      const type = state.get('surroundingPropertyType').set('search', action.search);
      return state.set('surroundingPropertyType', type).setIn(['surroundingPropertyTypes', type.get('index')], type);
    }

    case ActionType.SORT_SURROUNDING_PROPERTIES: {
      const type = state.get('surroundingPropertyType').set('sort', sort).update('properties', p => p.sort(getSortComparator(sort)));
      return state.set('surroundingPropertyType', type).setIn(['surroundingPropertyTypes', type.get('index')], type);
    }

    case ActionType.UPDATE_SURROUNDING_PROPERTY_SELECTION: {
      const type = state.get('surroundingPropertyType').update('properties', c => (index == null ? c.map(c => c.merge({ selected })) : c.mergeIn([index], { selected })));
      return state.set('surroundingPropertyType', type).setIn(['surroundingPropertyTypes', type.get('index')], type);
    }

    case ActionType.UPDATE_SURROUNDING_PROPERTY_TYPE:
      return state.set('surroundingPropertyType', action.newType);

    case ActionType.SEARCH_COMPARABLES_SUCCESS:
      return success(setType(setTypeProperties(state, 0, response)), 'comparable');

    case ActionType.SEARCH_SURROUNDING_PROPERTIES_SUCCESS: {
      console.log('surrounding', action, ListingTypesList.findIndex(t => t.get('value') === action.listingType));
      const types = response.nearbyListings.reduce((m, l) => ({ ...m, [l.type]: (m[l.type] || []).concat([l]) }), {});

      let newState;
      if (action.listingType) newState = setTypeProperties(state, ListingTypesList.findIndex(t => t.get('value') === action.listingType) + 2, types[action.listingType]);
      else {
        newState = setTypeProperties(state, 1, response.neighbors);
        ListingTypesList.forEach((f, i) => { newState = setTypeProperties(newState, i + 2, types[f.get('value')]); });
      }

      return setType(newState);
    }

    case ActionType.DOWNLOAD_COMPARABLES_SUCCESS:
      return success(state, 'comparable');

    case ActionType.LOAD_PROPERTY_DOCUMENTS:
    case ActionType.UPLOAD_PROPERTY_DOCUMENT:
    case ActionType.EDIT_PROPERTY_DOCUMENT:
    case ActionType.PURCHASE_REPORT:
    case ActionType.DOWNLOAD_PROPERTY_DOCUMENT:
    case ActionType.DOWNLOAD_PROPERTY_REPORT:
    case ActionType.DELETE_PROPERTY_DOCUMENT:
      return loading(state, 'document');

    case ActionType.LOAD_PROPERTY_DOCUMENTS_ERROR:
    case ActionType.UPLOAD_PROPERTY_DOCUMENT_ERROR:
    case ActionType.EDIT_PROPERTY_DOCUMENT_ERROR:
    case ActionType.PURCHASE_REPORT_ERROR:
    case ActionType.DOWNLOAD_PROPERTY_DOCUMENT_ERROR:
    case ActionType.DOWNLOAD_PROPERTY_REPORT_ERROR:
    case ActionType.DELETE_PROPERTY_DOCUMENT_ERROR:
      return error(state, 'document');

    case ActionType.LOAD_PROPERTY_DOCUMENTS_SUCCESS:
    case ActionType.UPLOAD_PROPERTY_DOCUMENT_SUCCESS:
    case ActionType.EDIT_PROPERTY_DOCUMENT_SUCCESS:
    case ActionType.PURCHASE_REPORT_SUCCESS:
    case ActionType.DELETE_PROPERTY_DOCUMENT_SUCCESS:
      return success(state.merge({ documents: response.map(d => ({ ...d, editable: d.typeDescription === 'Document' })) }), 'document');

    case ActionType.DOWNLOAD_PROPERTY_DOCUMENT_SUCCESS:
    case ActionType.DOWNLOAD_PROPERTY_REPORT_SUCCESS:
      return success(state, 'document');

    case ActionType.GET_STREET_VIEW:
      return loading(state, 'streetView');

    case ActionType.GET_STREET_VIEW_ERROR:
      return error(state, 'streetView');

    case ActionType.GET_STREET_VIEW_SUCCESS:
      return success(state.merge({ streetView: response || {} }), 'streetView');

    case ActionType.ASSIGN_PROPERTIES:
      return assignContext(state);

    case ActionType.UNASSIGN_PROPERTIES:
      return unassignContext(state);

    default:
      return state;
  }
}
