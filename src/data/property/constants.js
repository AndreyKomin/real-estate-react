import config from 'config';


const { constants: { BING: { API_KEY, MAP_IMAGE_URL } } } = config;

export const PropertyGroupTypes = {
  FAVORITE: 'FAVORITE',
  MARKETING: 'MARKETING',
  ALERT: 'ALERT',
};

export const GroupSearchModes = {
  LIST: 'LIST',
  PUSHPIN: 'PUSHPIN',
};

export const alertIcons = ['iconHome', 'iconPlus', 'iconImportant', 'iconHammer'];

export const Colors = [
  '#ff6909',
  '#ff0909',
  '#ebcb35',
  '#ff6582',
  '#82b045',
  '#2a420b',
  '#3989c0',
  '#27527d',
  '#550885',
  '#a252d0',
];

export const getBingUrl = (lat, lon, zoom = 19, width = 500, height = 400, style = 47) => {
  if (!lat || !lon) return null;
  const ll = `${lat},${lon}`;
  return `${MAP_IMAGE_URL}/${ll}/${zoom}?mapSize=${width},${height}&key=${API_KEY}&pp=${ll};${style}`;
};

export default PropertyGroupTypes;
