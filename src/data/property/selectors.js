import { createSelector } from 'reselect';
import { defaultContextMap, defaultGroupContextMap } from 'reducers';

import PropertyGroupTypes from './constants';


const filterType = (list, type) => list.filter(g => g.get('type') === type);

const checkState = state => state;

export const selectState = createSelector(checkState, state => state.get('property'));

export const selectProp = property => createSelector(selectState, state => state.get(property));

export const selectGroups = createSelector(selectState, state => state.get('groups').filter(g => !!g.get('type')));

export const selectContexts = createSelector(selectState, state => state.get('contexts'));

export const selectPushpins = createSelector(selectState, state => state.get('pushpins'));

export const selectPushpinLoading = createSelector(selectState, state => state.get('pushpinLoading'));

export const selectGroupContext = name => createSelector(selectState, s => s.getIn(['contexts', name], defaultContextMap).get('group') || defaultGroupContextMap);

export const selectFavorites = createSelector(selectGroups, groups => filterType(groups, PropertyGroupTypes.FAVORITE));

export const selectLists = createSelector(selectGroups, groups => filterType(groups, PropertyGroupTypes.MARKETING));

export const selectAlerts = createSelector(selectGroups, groups => filterType(groups, PropertyGroupTypes.ALERT));

export const selectLoading = selectProp('loading');

export const selectProperty = selectProp('property');

export const selectComparableSearch = createSelector(selectState, state => state.getIn(['surroundingPropertyTypes', 0, 'search']));

export const selectComparableLoading = selectProp('comparableLoading');

export const selectSurroundingPropertyType = selectProp('surroundingPropertyType');

export const selectSurroundingPropertyTypes = selectProp('surroundingPropertyTypes');

export const selectDocuments = selectProp('documents');

export const selectDocumentLoading = selectProp('documentLoading');

export const selectStreetView = selectProp('streetView');

export const selectStreetViewLoading = selectProp('streetViewLoading');
