/** @flow */
import promisifyJSONP from 'utils/promisifyJSONP';

import { API_KEY, ZIP_TYPES, PROPERTY_TYPES } from './config';


const ALLOWED_REGIONS = ['United States'];
const CONFIDENCE_ORDER = ['high', 'medium', 'low'];

export function isZipAddress(address: ?Object) {
  if (!address) return false;
  try {
    const { entityType = '', address: { countyId } = {} } = address;
    return ZIP_TYPES.includes(entityType.toLowerCase()) || !!countyId;
  } catch (err) {
    return false;
  }
}

export function isPropertyAddress(address: ?Object) {
  if (!address) return false;
  try {
    const { entityType = '', address: { apn, countyId } = {} } = address;
    return PROPERTY_TYPES.includes(entityType.toLowerCase()) || !!(apn && countyId);
  } catch (err) {
    return false;
  }
}

function sortByConfidence(a, b) {
  const aConf = a.confidence.toLowerCase();
  const bConf = b.confidence.toLowerCase();
  return CONFIDENCE_ORDER.indexOf(aConf) - CONFIDENCE_ORDER.indexOf(bConf);
}

function filterResources(resources) {
  const checkUniqueName = new Set();
  return resources.filter(entity => isPropertyAddress(entity) || isZipAddress(entity))
    .filter(entity => ALLOWED_REGIONS.includes(entity.address.countryRegion))
    .sort(sortByConfidence)
    .filter((entity) => {
      if (checkUniqueName.has(entity.name)) return false;
      checkUniqueName.add(entity.name);
      return true;
    })
    .slice(0, 5);
}

export function find(value: string): Promise<*> {
  const query = [
    'userRegion=US',
    'maxResults=25',
    `key=${String(API_KEY)}`,
  ].join('&');

  const url = `https://dev.virtualearth.net/REST/v1/Locations/${encodeURIComponent(value)}?${query}`;
  return promisifyJSONP(url, 'jsonp')
    .then(result => Promise.resolve(result[0].resourceSets[0]))
    .then(({ resources, ...rest }) => Promise.resolve({
      ...rest,
      result: filterResources(resources),
    }));
}

export function isZip(value: string | Object): Promise<boolean> {
  if (typeof value === 'string') {
    return find(value).then(({ result }) => {
      const hasZipInResults = result.some(isZipAddress);
      return Promise.resolve(hasZipInResults);
    });
  }
  return Promise.resolve(isZipAddress(value));
}

export function getSuggestions(value: string): Promise<Object[]> {
  return find(value)
    .then(({ result }) => {
      const localResult = [...result];
      return Promise.resolve(localResult);
    });
}
