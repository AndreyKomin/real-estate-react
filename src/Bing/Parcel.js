import superagent from 'superagent';

import promisifyJSONP from 'utils/promisifyJSONP';


let mainPromise = null;

export default function init() {
  mainPromise = mainPromise || new Promise((resolve) => {
    superagent.get(`${process.env.API_BASE_URL}/auth/ParcelBoundary?action=getKey`)
      .end((err, res) => {
        if (err) {
          mainPromise = null;
          return;
        }
        const key = res.text;
        const [domain, prefix, id] = key.split('/').filter((_, i) => (i !== 0));

        const pathname = `https://${domain}.parcelstream.com/${prefix}/InitSession.aspx`;
        const query = `sik=${prefix}%2F${id}&obsId=window&obsErrorMethod=DMNLoadError&output=json`;

        promisifyJSONP(`${pathname}?${query}`, 'obsSuccessMethod')
          .then(result => resolve(result[0].Response.Results.Data.Row.Candy));
      });
  });
  return mainPromise;
}

