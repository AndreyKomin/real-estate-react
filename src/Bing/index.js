/** @flow */
import config from 'config';

import * as Map from './Map';

import * as Location from './Location';


export { Map, Location };


const { constants: { BING: { API_KEY, DEFAULT_MAP_TYPE } } } = config;

export const mapPromise = Map.init();

export function drawMap(element: window.HTMLElement, options: Object = {}): Promise<Object> {
  const { mapType = DEFAULT_MAP_TYPE } = options;

  return mapPromise.then((Microsoft) => {
    const {
      [mapType]: mapTypeId = Microsoft.Maps.MapTypeId[DEFAULT_MAP_TYPE],
    } = Microsoft.Maps.MapTypeId;

    const map = new Microsoft.Maps.Map(element, {
      ...options,
      credentials: API_KEY,
      mapTypeId,
      zoom: 10,
      showMapTypeSelector: false,
      showDashboard: false,
      showScalebar: false,
    });

    return Promise.resolve(map);
  });
}

export function drawMapById(id: string, options: Object = {}): Promise<Object> {
  const element = document.getElementById(`${id.replace(/^#/, '')}`);

  return drawMap(element, options);
}

