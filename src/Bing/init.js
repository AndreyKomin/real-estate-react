/** @flow */
import isArray from 'utils/type/isArray';
import { API_KEY } from './config';
import { init } from './Map';


function getDeepModule(Microsoft, resolvePath: string[]) {
  const [...path] = resolvePath;
  path.shift();

  return path.reduce((parent, key) => {
    if (!parent) return null;
    if (!Object.prototype.hasOwnProperty.call(parent, key)) return null;
    return parent[key];
  }, Microsoft);
}

function getResolvePath(resolvePath: string[] | string): string[] {
  if (typeof resolvePath === 'string') {
    return resolvePath.split('.');
  } else if (isArray(resolvePath)) {
    return [...resolvePath];
  }
  throw new Error('Unexpected resolvePath\'s type');
}

function getCallback(externalCallbacks, Microsoft, resolvePath) {
  return new Promise(resolve => resolve(
    getDeepModule(
      Microsoft,
      getResolvePath(resolvePath),
    ),
  )).then(module => externalCallbacks.resolve(module));
}

export function initModule(moduleName: string, resolvePath: string[] | string = []): Promise<*> {
  return init().then((Microsoft) => {
    const prom = new Promise((resolve, reject) => {
      Microsoft.Maps.loadModule(moduleName, {
        callback: getCallback({ resolve, reject }, Microsoft, resolvePath),
        errorCallback: reject,
        credentials: API_KEY,
      });
    });

    return prom;
  });
}

export function getModule(moduleName: string[] | string): Promise<*> {
  if (typeof moduleName === 'string') {
    return initModule(moduleName);
  } else if (isArray(moduleName)) {
    return initModule(moduleName.join('.'));
  }
  return Promise.reject(new Error('Unexpected moduleName\'s type'));
}

