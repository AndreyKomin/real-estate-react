import React, { PureComponent } from 'react';

import { Color, DefaultColor } from 'data/listing/constants';

import MapFrame from '../MapFrame';
import MapType from '../MapType';
import Zoom from '../Zoom';
import MapAnalytics from '../MapAnalytics';
import Legend from '../Legend';
import Draw from '../Draw';
import Satellite from '../Satellite';
import AutoLayout from '../AutoLayout';
import { ParcelLayer } from '../TileLayer';
import { PushpinList } from '../Pushpin';


class MapLayout extends PureComponent<*, *, *> {
  render() {
    const {
      listingType,
      activePushpinId,
      containerClassName,
      containerStyle,
      Microsoft,
      map,
      mapType,
      entities,
      zoom,
      analyticsEnabled,
      children,
    } = this.props;
    const jsEntities = entities.toJS();

    return (
      <div className={containerClassName} style={containerStyle}>
        <PushpinList
          map={map}
          activePushpinId={activePushpinId}
          color={Color[listingType] || Color[DefaultColor]}
          Microsoft={Microsoft}
          locations={jsEntities}
        />
        <Zoom map={map} zoom={zoom} Microsoft={Microsoft} />
        <AutoLayout entities={jsEntities} map={map} Microsoft={Microsoft} />
        <MapFrame {...this.props} />
        <Legend />
        <Draw map={map} Microsoft={Microsoft} showDraw={this.props.showDraw} />
        {!analyticsEnabled ? null : <MapAnalytics map={map} Microsoft={Microsoft} />}
        <Satellite map={map} Microsoft={Microsoft} />
        <AutoLayout entities={jsEntities} map={map} Microsoft={Microsoft} />
        <Zoom map={map} Microsoft={Microsoft} />
        <MapType map={map} Microsoft={Microsoft} mapType={mapType} />
        <ParcelLayer map={map} Microsoft={Microsoft} />
        {children}
      </div>
    );
  }
}

MapLayout.defaultProps = {
  containerStyle: { height: '100%' },
};


export default MapLayout;
