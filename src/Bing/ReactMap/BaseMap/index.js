/** @flow */
import React, { PureComponent, PropTypes } from 'react';
import ImPropTypes from 'react-immutable-proptypes';

import { List } from 'immutable';
import { mapPromise } from 'Bing';
import { DEFAULT_TYPE } from '../MapType/type_enum';

import Layout from './MapLayout';


class BaseMap extends PureComponent<*, *, *> {
  constructor(props: Object) {
    super(props);
    this.state = {
      Microsoft: null,
      map: null,
    };
    this.promise = mapPromise;
    this.onMapRef = this.onMapRef.bind(this);
  }

  state: {
    Microsoft: ?Object,
    map: ?Object,
  };

  componentDidMount() {
    this.promise.then((Microsoft) => {
      this.setState({ Microsoft });
    });
  }

  componentWillUnmount() {
    const { map } = this.state;
    if (map) {
      requestIdleCallback(() => map.dispose());
    }
  }

  /* :: onMapRef: Function */
  onMapRef(map: *) {
    const { Microsoft } = this.state;
    if (!Microsoft) throw new Error('Impossible state in application. Map should be only if Microsoft presents');
    this.setState({ map });
    if (this.props.center) {
      map.setView({
        center: new Microsoft.Maps.Location(this.props.center.latitude, this.props.center.longitude),
      });
    }
    if (this.props.onMap) {
      this.props.onMap({ map, Microsoft });
    }
  }

  promise: Promise<*>;

  render() {
    const { Microsoft, map } = this.state;
    if (!Microsoft) return null;
    return <Layout {...this.props} onMapRef={this.onMapRef} map={map} Microsoft={Microsoft} />;
  }
}


BaseMap.propTypes = {
  entities: ImPropTypes.listOf(
    ImPropTypes.mapContains({
      latitude: PropTypes.number.isRequired,
      longitude: PropTypes.number.isRequired,
    }),
  ),
  mapType: PropTypes.string.isRequired,
  analyticsEnabled: PropTypes.bool.isRequired,
};

BaseMap.defaultProps = {
  entities: List([]),
  mapType: DEFAULT_TYPE,
  analyticsEnabled: true,
};

export default BaseMap;
