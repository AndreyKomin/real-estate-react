import React, { PureComponent } from 'react';
import { findDOMNode } from 'react-dom';
import { Link } from 'react-router';

/**
 * The problem is that react-router/Link can't prevent default in
 * iframe. So click on Link cause page reloading. To allow use
 * Link in iframe use this class. It use original Link behaviour, but
 * prevent default redirecting to another page.
 */
export default class MapLink extends PureComponent {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.handleRef = this.handleRef.bind(this);
  }

  handleClick(event) {
    this.link.handleClick(event);
  }

  handleRef(ref) {
    if (ref) {
      this.link = ref;
      this.node = findDOMNode(ref);
      this.node.addEventListener('click', this.handleClick);
    } else {
      this.node.removeEventListener('click', this.handleClick);
      this.link = null;
      this.node = null;
    }
  }

  render() {
    return <Link {...this.props} ref={this.handleRef} />;
  }
}
