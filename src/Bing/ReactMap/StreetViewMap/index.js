/** @flow */
import React, { PureComponent } from 'react';

import CustomMap from '../CustomMap';
import MapType, { Type } from '../MapType';
import { Headless as Zoom } from '../Zoom';
import Pushpin from '../Pushpin';


const MAP_TYPE = Type.STREETSIDE;

class StreetViewMap extends PureComponent<*, *, *> {
  constructor(props: *) {
    super(props);
    this.handleMap = this.handleMap.bind(this);
    this.state = {
      mapType: MAP_TYPE,
    };
  }

  /* :: handleMap: Function */
  handleMap({ map, Microsoft }: *) {
    const { entity } = this.props;
    const streetsideOptions: any = {
      overviewMapMode: Microsoft.Maps.OverviewMapMode.hidden,
      showCurrentAddress: false,
      showProblemReporting: false,
      showExitButton: false,
      disablePanoramaNavigation: true,
      showHeadingCompass: true,
      showZoomButtons: false,
      onSuccessLoading: () => this.turnToEntity(map, Microsoft),
      onErrorLoading: () => this.setState({ mapType: Type.ROAD }),
    };

    if (entity) {
      streetsideOptions.locationToLookAt = new Microsoft.Maps.Location(entity.latitude, entity.longitude);
    }
    map.setView({ zoom: this.props.zoom });
    map.setOptions({ streetsideOptions });
  }

  turnToEntity(map: *, Microsoft: *) {
    const { entity, center } = this.props;
    if (!entity) return;
    if (entity.latitude !== center.latitude || entity.longitude !== center.longitude) return;

    map.setOptions({
      streetsideOptions: {
        locationToLookAt: new Microsoft.Maps.Location(entity.latitude, entity.longitude),
      },
    });
  }

  render() {
    const { zoom, entity, color, ...rest } = this.props;

    const pin = entity ? <Pushpin color={color} location={entity} /> : null;

    return (
      <CustomMap {...rest} initZoom={zoom} onMap={this.handleMap}>
        <MapType mapType={this.state.mapType} />
        <Zoom zoom={zoom} />
        {pin}
      </CustomMap>
    );
  }
}

export default StreetViewMap;
