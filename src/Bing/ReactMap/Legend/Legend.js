/** @flow */
import React, { PropTypes, PureComponent } from 'react';

import LegendLayout from './LegendLayout';


class Legend extends PureComponent<*, *, *> {
  constructor(props: { isOpen: boolean }) {
    super(props);
    this.state = {
      isOpen: this.props.isOpen,
    };
    this.handleOpen = this.handleOpen.bind(this);
  }

  state: { isOpen: boolean };

  /* :: handleOpen: Function */
  handleOpen() {
    this.setState({ isOpen: !this.state.isOpen });
  }

  render() {
    const { isOpen } = this.state;

    return <LegendLayout {...this.props} onOpen={this.handleOpen} isOpen={isOpen} />;
  }
}

export default Legend;

Legend.propTypes = {
  isOpen: PropTypes.bool.isRequired,
};

Legend.defaultProps = {
  isOpen: false,
};
