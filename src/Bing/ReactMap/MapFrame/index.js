/** @flow */
import React, { PureComponent } from 'react';
import { drawMap } from 'Bing';


class MapFrame extends PureComponent {
  constructor(props: Object) {
    super(props);
    this.onRef = this.onRef.bind(this);
  }

  componentDidMount() {
    drawMap(this.root)
      .then(this.props.onMapRef);
  }

  /* :: onRef: Function */
  onRef(element: ?HTMLElement) {
    this.root = element;
  }

  root: ?HTMLElement;

  render() {
    const { className } = this.props;
    return (<div ref={this.onRef} className={className} />);
  }
}


export default MapFrame;
