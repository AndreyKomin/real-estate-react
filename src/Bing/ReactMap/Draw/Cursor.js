/** @flow */
import { Component, PropTypes } from 'react';
import classNames from 'classnames';


export default class Cursor extends Component<*, *, *> {
  shouldComponentUpdate(oldProps: Object) {
    return this.props.isDraw !== oldProps.isDraw;
  }

  componentDidUpdate() {
    this.updateCursor();
  }

  updateCursor() {
    const { isDraw } = this.props;

    if (isDraw) {
      this.appendClassName();
    } else {
      this.removeClassName();
    }
  }

  appendClassName() {
    const { map, className } = this.props;
    const root = map.getRootElement();

    if (root.className.split(' ').indexOf(className) === -1) {
      root.className = classNames(className, root.className);
    }
  }

  removeClassName() {
    const { map, className } = this.props;
    const root = map.getRootElement();
    const names = root.className.split(' ');

    if (names.indexOf(className) !== -1) {
      root.className = names.filter(cn => cn !== className).join(' ');
    }
  }

  render() {
    return null;
  }
}

Cursor.propTypes = {
  className: PropTypes.string,
  map: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  isDraw: PropTypes.bool.isRequired,
};
