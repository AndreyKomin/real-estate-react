/** @flow */
import Draw from './Draw';
import waitForMap from '../waitForMap';


export default waitForMap(Draw);
