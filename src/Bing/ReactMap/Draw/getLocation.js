/** @flow */
export default function getLocation(Microsoft: Object, cordString: string): * {
  const cord = cordString.split(',').map(Number);
  const [latitude, longitude] = cord;
  return new Microsoft.Maps.Location(latitude, longitude);
}
