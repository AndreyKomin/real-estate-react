/** @flow */
import { PureComponent, PropTypes, cloneElement } from 'react';


export default class TileLayer extends PureComponent<*, *, *> {
  /* :: layer: * */

  componentDidMount() {
    this.drawLayer();
  }

  componentDidUpdate() {
    this.drawLayer();
  }

  componentWillUnmount() {
    this.removeLayer();
  }

  canDraw() {
    const { maxZoom = Infinity, minZoom = -Infinity, map } = this.props;
    const zoom = map.getZoom();
    return maxZoom > zoom && minZoom < zoom;
  }

  drawLayer() {
    const { map, Microsoft, uriConstructor, ...rest } = this.props;
    const mercator = new Microsoft.Maps.TileSource({
      uriConstructor,
      ...rest,
    });
    if (this.layer) this.removeLayer();
    if (!this.canDraw()) return;
    this.layer = new Microsoft.Maps.TileLayer({
      mercator,
    });
    this.layer.setOpacity(0.5);
    map.layers.insert(this.layer);
  }

  removeLayer() {
    const { map } = this.props;
    map.layers.remove(this.layer);
  }


  render() {
    const { Microsoft, map, children, ...rest } = this.props;
    return children ? cloneElement(children, { ...rest }) : null;
  }
}


TileLayer.propTypes = {
  map: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  Microsoft: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  children: PropTypes.element,
  uriConstructor: PropTypes.string.isRequired,
  minZoom: PropTypes.number,
  maxZoom: PropTypes.number,
};

