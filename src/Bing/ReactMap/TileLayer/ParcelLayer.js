/** @flow */
import React, { PureComponent, PropTypes } from 'react';

import Parcel from 'Bing/Parcel';

import TileLayer from './TileLayer';
import EventHandler from '../EventHandler';


const MIN_ZOOM = 15;
const PARCEL_COLOR = 'null';

export default class ParcelLayer extends PureComponent<*, *, *> {
  /* :: layer: * */
  constructor(props: *) {
    super(props);
    this.handleViewChange = this.handleViewChange.bind(this);
    this.state = {
      candy: null,
      zoom: null,
    };
  }

  componentWillMount() {
    const { map } = this.props;
    Parcel().then(candy => this.setState({ candy, zoom: map.getZoom() }));
  }

  getUriConstructor() {
    if (!this.state.candy) return null;
    const { color } = this.props;
    return [
      'https://dc1.parcelstream.com/GetMap.aspx?layers=SS.Base.Parcels/ParcelTiles&tileId={quadkey}',
      `&color=${color}&antiAlias=true&SS_CANDY=${this.state.candy}`,
    ].join('');
  }

  /* :: handleViewChange: Function */
  handleViewChange() {
    this.setState({ zoom: this.props.map.getZoom() });
  }

  render() {
    if (!this.state.candy) return null;

    return (
      <TileLayer
        {...this.props}
        zoom={this.state.zoom}
        minZoom={MIN_ZOOM}
        uriConstructor={this.getUriConstructor()}
      >
        <EventHandler {...this.props} event="viewchangeend" onEvent={this.handleViewChange} />
      </TileLayer>
    );
  }
}


ParcelLayer.propTypes = {
  map: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  Microsoft: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  children: PropTypes.element,
  maxZoom: PropTypes.number,
};

ParcelLayer.defaultProps = {
  color: PARCEL_COLOR,
};
