/** @flow */
import Tile from './TileLayer';
import Parcel from './ParcelLayer';

import waitForMap from '../waitForMap';


const TileLayer = waitForMap(Tile);
const ParcelLayer = waitForMap(Parcel);

export default TileLayer;
export { ParcelLayer, TileLayer };
