/** @flow */
import React, { PureComponent, Children, cloneElement } from 'react';

import MapFrame from '../MapFrame';


class Layout extends PureComponent<*, *, *> {
  render() {
    const {
      containerClassName,
      containerStyle,
      children,
      ...rest
    } = this.props;

    const content = Children.map(children, child => cloneElement(child, rest));

    return (
      <div className={containerClassName} style={containerStyle}>
        <MapFrame {...rest} />
        {content}
      </div>
    );
  }
}

Layout.defaultProps = {
  containerStyle: { height: '100%' },
};

export default Layout;
