/** @flow */
import { Component } from 'react';


export default class Empty extends Component<*, *, *> {
  shouldComponentUpdate() {
    return false;
  }

  render() {
    return null;
  }
}
