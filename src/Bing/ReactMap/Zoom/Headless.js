import React, { PureComponent, PropTypes } from 'react';
import { connect } from 'react-redux';
import _ from 'underscore';

// import { zoomIn, zoomOut, setZoom } from 'data/propertyDetails';

import EventHandler from '../EventHandler';


const VIEW_CHANGE_EVENT = 'viewchangeend';
const DEBOUNCE_TIMEOUT = 200;


class Headless extends PureComponent {
  constructor(props) {
    super(props);
    this.handleViewChange = _.debounce(this.handleViewChange.bind(this), DEBOUNCE_TIMEOUT);
  }

  componentDidMount() {
    const { Microsoft, map } = this.props;
    // setZoom(map.getZoom());
    this.handlerId = Microsoft.Maps.Events.addHandler(
      map,
      VIEW_CHANGE_EVENT,
      this.handleViewChange,
    );
    return this.updateMapZoom();
  }

  componentDidUpdate(oldProps) {
    if (this.props.zoom !== oldProps.zoom) {
      this.updateMapZoom();
    }
  }

  updateMapZoom() {
    const { map, zoom } = this.props;
    map.setView({ zoom });
  }

  /* :: handleViewChange: Function */
  handleViewChange() {
    const { map } = this.props;
    const nextZoom = map.getZoom();
    if (this.props.zoom !== nextZoom) {
      this.zoom = nextZoom;
      // this.props.setZoom(nextZoom);
    }
  }

  render() {
    const { Microsoft, map, children } = this.props;

    return (
      <div>
        <EventHandler map={map} Microsoft={Microsoft} event={VIEW_CHANGE_EVENT} onEvent={this.handleViewChange} />
        {children}
      </div>
    );
  }
}


Headless.propTypes = {
  map: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  Microsoft: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  zoom: PropTypes.number,
};

Headless.defaultProps = {
  disabledZoomIn: false,
  disabledZoomOut: false,
};

function mapStateToProps(state) {
  return {
    disabledZoomIn: !state.getIn(['comparableMap', 'canZoomIn']),
    disabledZoomOut: !state.getIn(['comparableMap', 'canZoomOut']),
    zoom: state.getIn(['comparableMap', 'zoom']),
  };
}

const mapActionToProps = {
  // zoomIn,
  // zoomOut,
  // setZoom,
};

export default connect(mapStateToProps, mapActionToProps)(Headless);
