/** @flow */
import Zoom from './Zoom';
import HeadlessComp from './Headless';

import waitForMap from '../waitForMap';


export default waitForMap(Zoom);
const Headless = waitForMap(HeadlessComp);
export { Headless };
