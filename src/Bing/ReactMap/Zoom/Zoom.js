import React, { PureComponent, PropTypes } from 'react';
import { connect } from 'react-redux';
import _ from 'underscore';
import { zoomIn, zoomOut, setZoom } from 'reducers/tempCompMap';

import SVG from 'components/base/SVG';

import EventHandler from '../EventHandler';

import css from './style.scss';


const VIEW_CHANGE_EVENT = 'viewchangeend';
const DEBOUNCE_TIMEOUT = 200;


class Zoom extends PureComponent {
  constructor(props) {
    super(props);
    this.handleViewChange = _.debounce(this.handleViewChange.bind(this), DEBOUNCE_TIMEOUT);
  }

  componentDidMount() {
    const { Microsoft, map } = this.props;
    // setZoom(map.getZoom());
    this.handlerId = Microsoft.Maps.Events.addHandler(
      map,
      VIEW_CHANGE_EVENT,
      this.handleViewChange,
    );
    return this.updateMapZoom();
  }

  componentDidUpdate(oldProps) {
    if (this.props.zoom !== oldProps.zoom) {
      this.updateMapZoom();
    }
  }

  updateMapZoom() {
    const { map, zoom } = this.props;
    map.setView({ zoom });
  }

  handleViewChange() {
    const { map } = this.props;
    const nextZoom = map.getZoom();
    if (this.props.zoom !== nextZoom) {
      this.zoom = nextZoom;
      this.props.setZoom(nextZoom);
    }
  }

  render() {
    const { Microsoft, map, zoomIn, zoomOut, disabledZoomIn, disabledZoomOut } = this.props;

    return (
      <div>
        <EventHandler map={map} Microsoft={Microsoft} event={VIEW_CHANGE_EVENT} onEvent={this.handleViewChange} />
        <div className={css.zoom}>
          <button
            className={css.buttonZoom}
            disabled={disabledZoomIn}
            type="button"
            onClick={zoomIn}
          >
            <SVG icon="iconZoomIn" className={css.iconZoom} />
          </button>
          <button
            className={css.buttonZoom}
            disabled={disabledZoomOut}
            type="button"
            onClick={zoomOut}
          >
            <SVG icon="iconZoomOut" className={css.iconZoom} />
          </button>
        </div>
      </div>
    );
  }
}


Zoom.propTypes = {
  map: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  Microsoft: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  zoom: PropTypes.number,
  disabledZoomIn: PropTypes.bool.isRequired,
  disabledZoomOut: PropTypes.bool.isRequired,
};

Zoom.defaultProps = {
  disabledZoomIn: false,
  disabledZoomOut: false,
};

function mapStateToProps(state) {
  return {
    disabledZoomIn: !state.getIn(['comparableMap', 'canZoomIn']),
    disabledZoomOut: !state.getIn(['comparableMap', 'canZoomOut']),
    zoom: state.getIn(['comparableMap', 'zoom']),
  };
}

const mapActionToProps = {
  zoomIn,
  zoomOut,
  setZoom,
};

export default connect(mapStateToProps, mapActionToProps)(Zoom);
