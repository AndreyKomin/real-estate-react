export const legendItems = [
  {
    id: 1,
    color: '#92D2D0',
    name: 'Hover State',
  },
  {
    id: 2,
    color: '#61A11B',
    name: 'Hover State',
  },
  {
    id: 3,
    color: '#A7C487',
    name: 'New Property',
  },
  {
    id: 4,
    color: '#4A90E2',
    name: 'Viewed Property',
  },
  {
    id: 5,
    color: '#8CB5E4',
    name: 'Saved Property',
  },
  {
    id: 6,
    color: '#FFAE00',
    name: 'Hover State',
  },
  {
    id: 7,
    color: '#F6CA6B',
    name: 'New Property',
  },
  {
    id: 8,
    color: '#8AB5E8',
    name: 'Viewed Property',
  },
  {
    id: 9,
    color: '#D5A8A8',
    name: 'Saved Property',
  },
];

export const priceItems = [
  {
    id: 1,
    name: 'Listing Price',
    checked: true,
  },
  {
    id: 2,
    name: 'Price Estimates',
    checked: false,
  },
  {
    id: 3,
    name: 'Sale Price',
    checked: false,
  },
  {
    id: 4,
    name: 'Sale Price/Sqft',
    checked: false,
  },
  {
    id: 5,
    name: 'Rental Price',
    checked: false,
  },
];

export const popupData = [
  {
    id: 1,
    image: 'http://lorempixel.com/77/77/',
    buy: 'Pre-Foreclosure',
    address: '44 Muller Drive Apt. 318',
    state: 'Irvine CA',
    zip: '92618',
    district: 'Condo',
    built: 'Built in 2005',
    value: '1,000,000',
    beds: 3,
    baths: 2.5,
    sqft: '1,332',
    sqftPrice: 441,
    unpaidBal: '706,083',
    statHere: 0.2,
    forecastedValue: 0.2,
  },
];

export const paletteItems = ['#e5e5e6', '#daded9', '#cdd1ce', '#aeb4b7', '#9299a3', '#7b848d', '#66707d'];
