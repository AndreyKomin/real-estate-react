/** @flow */
import Map from './BaseMap';
import BirdView from './BirdViewMap';
import StreetView from './StreetViewMap';


export default Map;
export {
  BirdView,
  StreetView,
};
