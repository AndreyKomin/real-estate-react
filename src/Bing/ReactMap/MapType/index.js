/** @flow */
import MapType from './MapType';
import waitForMap from '../waitForMap';


export default waitForMap(MapType);
export { default as Type } from './type_enum';
