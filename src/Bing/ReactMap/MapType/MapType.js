/** @flow */
import React, { PropTypes, PureComponent, cloneElement } from 'react';

import EventHandler from '../EventHandler';

import Type, { DEFAULT_TYPE } from './type_enum';


const mapTypes = Object.keys(Type);

class MapType extends PureComponent<*, *, *> {
  constructor(props: *) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleTypeChange = this.handleTypeChange.bind(this);
  }

  componentDidMount() {
    this.setMapType();
  }

  componentDidUpdate(oldProps: *) {
    if (oldProps.mapType !== this.props.mapType) {
      this.setMapType();
    }
  }

  setMapType() {
    const { map, Microsoft, mapType } = this.props;
    map.setMapType(Microsoft.Maps.MapTypeId[Type[mapType]]);
  }

  setType(nextType: string) {
    const { map, Microsoft } = this.props;
    if (mapTypes.includes(nextType)) {
      map.setMapType(Microsoft.Maps.MapTypeId[Type[nextType]]);
    }
  }

  /* :: handleTypeChange: Function */
  handleTypeChange(event: Object) {
    if (event) {
      const { mapType } = this.props;
      if (mapType !== event.newMapTypeId) {
        if (typeof this.props.onChange === 'function') {
          this.props.onChange(event.newMapTypeId, event);
        }
      }
    }
  }

  /* :: handleChange: Function */
  handleChange(event: string | Event | { value: string }) {
    if (typeof event === 'string') {
      this.setType(event);
    } else if (event instanceof Event && event.target && typeof event.target.value === 'string') {
      this.setType(event.target.value);
    } else if (typeof event === 'object' && typeof event.value === 'string') {
      this.setType(event.value);
    }
  }

  render() {
    const { children, map, Microsoft, ...rest } = this.props;
    const content = children ? cloneElement(children, {
      ...rest,
      mapTypes,
      onChange: this.handleChange,
    }) : null;

    return (
      <div>
        <EventHandler map={map} Microsoft={Microsoft} event="maptypechanged" onEvent={this.handleTypeChange} />
        {content}
      </div>
    );
  }
}

MapType.propTypes = {
  mapType: PropTypes.oneOf(Object.keys(Type)).isRequired,
};

MapType.defaultProps = {
  mapType: DEFAULT_TYPE,
};


export default MapType;
