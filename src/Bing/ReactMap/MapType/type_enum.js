/** @flow */

export default {
  DEFAULT: 'canvasLight',
  aerial: 'aerial',
  AERIAL: 'aerial',
  canvasDark: 'canvasDark',
  CANVAS_DARK: 'canvasDark',
  canvasLight: 'canvasLight',
  CANVAS_LIGHT: 'canvasLight',
  birdseye: 'birdseye',
  BIRDSEYE: 'birdseye',
  grayscale: 'grayscale',
  GRAYSCALE: 'grayscale',
  mercator: 'mercator',
  MERCATOR: 'mercator',
  ordnanceSurvey: 'ordnanceSurvey',
  ORDNANCE_SURVEY: 'ordnanceSurvey',
  road: 'road',
  ROAD: 'road',
  streetside: 'streetside',
  STREETSIDE: 'streetside',
};

export const DEFAULT_TYPE = 'DEFAULT';
