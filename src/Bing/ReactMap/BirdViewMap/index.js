/** @flow */
import React, { PureComponent } from 'react';

import CustomMap from '../CustomMap';
import MapType, { Type } from '../MapType';
import Pushpin from '../Pushpin';


const MAP_TYPE = Type.BIRDSEYE;

class BirdViewMap extends PureComponent<*, *, *> {
  constructor(props: *) {
    super(props);
    this.handleMap = this.handleMap.bind(this);
    this.state = {
      mapType: Type.AERIAL,
      map: null,
    };
  }

  /* :: handleMap: Function */
  handleMap({ map, Microsoft }: *) {
    map.setView({ zoom: this.props.zoom });
    const location = map.getCenter();
    Microsoft.Maps.getIsBirdseyeAvailable(location, Microsoft.Maps.Heading.North, (isAvailable) => {
      if (isAvailable) this.setState({ map, mapType: MAP_TYPE });
      else this.setState({ map });
    });
  }

  render() {
    const { zoom, entity, color, children, ...rest } = this.props;

    const pin = entity ? <Pushpin color={color} location={entity} /> : null;

    return (
      <CustomMap {...rest} initZoom={zoom} onMap={this.handleMap}>
        <MapType mapType={this.state.mapType} />
        {pin}
      </CustomMap>
    );
  }
}

export default BirdViewMap;
