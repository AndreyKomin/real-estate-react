/** @flow */
import React, { PureComponent, PropTypes } from 'react';

import Polyline from './Polyline';


export default class Plain extends PureComponent<*, *, *> {
  /* :: selectLocations: Function */
  getLocations() {
    if (this.props.locations) {
      return this.props.locations.slice(0, 2);
    } else if (this.props.start && this.props.end) {
      return [this.props.start, this.props.end];
    }
    throw new Error('Line need locations or `start + end` properties.');
  }

  render() {
    const { map, Microsoft } = this.props;
    return <Polyline map={map} Microsoft={Microsoft} locations={this.getLocations()} />;
  }
}


Polyline.propTypes = {
  map: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  Microsoft: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  locations: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};

