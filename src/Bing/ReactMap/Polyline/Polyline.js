/** @flow */
import { PureComponent, PropTypes } from 'react';


export default class Polyline extends PureComponent<*, *, *> {
  /* :: polyline: Object */
  componentDidMount() {
    this.init();
    this.update();
  }

  componentDidUpdate(oldProps: Object) {
    if (oldProps.locations !== this.props.locations) {
      this.update();
    }
  }

  componentWillUnmount() {
    this.remove();
  }

  init() {
    const { Microsoft, locations } = this.props;
    if (!this.polyline) {
      this.polyline = new Microsoft.Maps.Polyline(locations, null);
    }
  }

  update() {
    const { map, locations } = this.props;

    this.remove();
    if (locations.length > 1) {
      this.polyline.setLocations(locations);
      map.entities.push(this.polyline);
    }
  }

  remove() {
    const { map } = this.props;
    if (this.polyline) {
      map.entities.remove(this.polyline);
    }
  }

  render() {
    return null;
  }
}


Polyline.propTypes = {
  map: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  Microsoft: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  locations: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};

