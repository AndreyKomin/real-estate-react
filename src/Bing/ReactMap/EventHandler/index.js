/** @flow */
import EventHandler from './EventHandler';
import waitForMap from '../waitForMap';


export default waitForMap(EventHandler);
