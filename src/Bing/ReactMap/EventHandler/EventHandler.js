/** @flow */
import React, { PureComponent, PropTypes } from 'react';


export default class EventHandler extends PureComponent<*, *, *> {
  constructor(props: *) {
    super(props);
    this.handleEvent = this.handleEvent.bind(this);
    this.prevent = this.prevent.bind(this);
  }

  /* :: timer: * */
  /* :: handleEvent: * */
  /* :: prevent: * */
  /* :: handlerId: * */
  componentDidMount() {
    this.addEventHandler();
  }

  componentWillUnmount() {
    this.removeEventHandler();
  }

  addEventHandler() {
    const { map, event, Microsoft } = this.props;

    this.handlerId = Microsoft.Maps.Events.addHandler(
      map,
      event,
      this.handleEvent,
    );
  }

  removeEventHandler() {
    if (this.handlerId) {
      const { Microsoft } = this.props;
      Microsoft.Maps.Events.removeHandler(this.handlerId);
    }
  }

  isClick() {
    return this.props.event === 'click';
  }

  handleEvent(event: *) {
    if (!this.isClick()) {
      this.props.onEvent(event);
      return;
    }
    clearTimeout(this.timer);
    this.timer = setTimeout(() => this.props.onEvent(event), 200);
  }

  prevent() {
    clearTimeout(this.timer);
  }

  render() {
    if (!this.isClick()) return null;
    return <EventHandler {...this.props} event="dblclick" onEvent={this.prevent} />;
  }
}


EventHandler.propTypes = {
  map: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  Microsoft: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  event: PropTypes.string.isRequired,
  onEvent: PropTypes.func.isRequired,
};
