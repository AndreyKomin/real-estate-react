/** @flow */
import { PureComponent, PropTypes } from 'react';


export default class Polygon extends PureComponent<*, *, *> {
  /* :: polygon: Object */
  componentDidMount() {
    this.init();
    this.update();
  }

  componentDidUpdate(oldProps: Object) {
    if (oldProps.locations !== this.props.locations) {
      this.update();
    }
  }

  componentWillUnmount() {
    this.remove();
  }

  init() {
    const { Microsoft, locations } = this.props;
    if (!this.polygon) {
      this.polygon = new Microsoft.Maps.Polygon(locations, null);
    }
  }

  update() {
    const { map, locations } = this.props;

    this.remove();
    if (locations.length > 2) {
      this.polygon.setLocations(locations);
      map.entities.push(this.polygon);
    }
  }

  remove() {
    const { map } = this.props;
    if (this.polygon) {
      map.entities.remove(this.polygon);
    }
  }

  render() {
    return null;
  }
}


Polygon.propTypes = {
  map: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  Microsoft: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  locations: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.shape({})),
  ]).isRequired,
};

Polygon.defaultProps = {
  locations: [],
};
