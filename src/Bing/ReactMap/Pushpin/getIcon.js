/** @flow */
import icon from './pin.raw.svg';


const ColoredIcons = new Map();

const colorRegExp = /{color}/g;

export default function getIcon(color: string): string {
  if (!ColoredIcons.has(color)) {
    ColoredIcons.set(color, icon.replace(colorRegExp, color));
  }
  return (ColoredIcons.get(color): any);
}

export function deleteIcon(color: string) {
  ColoredIcons.delete(color);
}
