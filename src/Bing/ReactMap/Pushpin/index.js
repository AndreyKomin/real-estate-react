/** @flow */
import Pushpin from './Pushpin';
import PushpinList from './PushpinList';
import waitForMap from '../waitForMap';


export default waitForMap(Pushpin);

const List = waitForMap(PushpinList);

export {
  List as PushpinList,
  List,
};
