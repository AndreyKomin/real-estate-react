/** @flow */
import React, { PureComponent, PropTypes } from 'react';

import Pushpin from './Pushpin';


const style = { display: 'none' };
const DEFAULT_KEY_FIELD = 'id';

export default class PushpinList extends PureComponent<*, *, *> {
  /* :: renderPushpins: Function */
  renderPushpins() {
    const { locations, keyField, activePushpinId, ...rest } = this.props;

    return locations.map(location => (
      <Pushpin
        {...location}
        {...rest}
        active={location.id === activePushpinId}
        key={location[keyField]}
        location={location}
      />
    ));
  }

  render() {
    return (
      <div style={style}>
        {this.renderPushpins()}
      </div>
    );
  }
}

PushpinList.propTypes = {
  map: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  Microsoft: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  locations: PropTypes.arrayOf(PropTypes.shape({
    latitude: PropTypes.number.isRequired,
    longitude: PropTypes.number.isRequired,
  })).isRequired,
  keyField: PropTypes.string.isRequired,
};

PushpinList.defaultProps = {
  keyField: DEFAULT_KEY_FIELD,
};

