/** @flow */
import { PureComponent, PropTypes } from 'react';

import { Color } from 'data/listing/constants';

import getIcon from './getIcon';


const FIELD_LATITUDE = 'latitude';
const FIELD_LONGITUDE = 'longitude';
const PUSHPIN_HEIGHT = 30;
const PUSHPIN_HALF_WIDTH = 11;

export default class Pushpin extends PureComponent<*, *, *> {
  componentDidMount() {
    this.draw();
  }

  componentDidUpdate() {
    this.remove();
    this.draw();
  }

  componentWillUnmount() {
    this.remove();
  }

  pushpin: Object;
  handlerId: Object;

  remove() {
    const { Microsoft } = this.props;
    if (this.handlerId) {
      Microsoft.Maps.Events.removeHandler(this.handlerId);
    }
    this.props.map.entities.remove(this.pushpin);
  }

  drawOnMap() {
    const { map } = this.props;
    map.entities.push(this.pushpin);
  }

  draw() {
    const { location, Microsoft, color, active, ...rest } = this.props;
    const latitude = location[FIELD_LATITUDE];
    const longitude = location[FIELD_LONGITUDE];

    const pinLoc = new Microsoft.Maps.Location(latitude, longitude);
    this.pushpin = new Microsoft.Maps.Pushpin(pinLoc, {
      anchor: new Microsoft.Maps.Point(PUSHPIN_HALF_WIDTH, PUSHPIN_HEIGHT),
      ...rest,
      icon: getIcon(active ? Color.ACTIVE : color),
    });

    if (this.props.onClick) {
      this.handlerId = Microsoft.Maps.Events.addHandler(this.pushpin, 'click', () => {
        const { location, Microsoft, map, onClick, ...rest } = this.props;
        onClick({ ...rest });
      });
    }

    this.drawOnMap();
  }


  render() {
    return null;
  }
}


Pushpin.propTypes = {
  color: PropTypes.string,
  cursor: PropTypes.string,
  draggable: PropTypes.bool,
  enableClickedStyle: PropTypes.bool,
  enableHoverStyle: PropTypes.bool,
  icon: PropTypes.string,
  roundClickableArea: PropTypes.bool,
  map: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  Microsoft: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  location: PropTypes.shape({
    latitude: PropTypes.number.isRequired,
    longitude: PropTypes.number.isRequired,
  }).isRequired,
  subTitle: PropTypes.string,
  text: PropTypes.string,
  title: PropTypes.string,
  visible: PropTypes.bool,
};

Pushpin.defaultProps = {
  draggable: false,
  enableClickedStyle: false,
  enableHoverStyle: false,
  roundClickableArea: false,
  visible: true,
  color: 'black',
};
