import React, { PureComponent } from 'react';


export default function waitForMap(Component) {
  return class MapWaiter extends PureComponent {
    render() {
      if (!this.props.map) return null;
      return <Component {...this.props} />;
    }
  };
}
