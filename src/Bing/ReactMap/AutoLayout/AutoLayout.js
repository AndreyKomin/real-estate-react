/** @flow */
import { PureComponent, PropTypes } from 'react';


export default class AutoLayout extends PureComponent<*, *, *> {
  componentDidMount() {
    this.refreshLayout();
  }

  componentDidUpdate(oldProps: *) {
    if (!Object.is(this.props.entities, oldProps.entities)) {
      this.refreshLayout();
    } else if (!Object.is(oldProps.infoboxEntity, this.props.infoboxEntity)) {
      if (this.props.infoboxEntity) this.moveByInfobox();
      else this.refreshLayout();
    }
  }

  refreshLayout() {
    const { map, Microsoft, entities } = this.props;
    if (entities.length === 0) return;

    if (entities.length === 1) {
      const { latitude, longitude } = entities[0];
      map.setView({
        center: new Microsoft.Maps.Location(latitude, longitude),
      });
      return;
    }

    const locations = entities.map(({ latitude, longitude }) => new Microsoft.Maps.Location(latitude, longitude));

    map.setView({
      bounds: Microsoft.Maps.LocationRect.fromLocations(locations),
    });
  }

  moveByInfobox() {
    const { map, infoboxEntity, Microsoft } = this.props;

    const bounds = map.getBounds();
    const center = map.getCenter().latitude;
    const south = bounds.getSouth();

    const latitude = infoboxEntity.latitude + ((center - south) * (2 / 3));
    const longitude = infoboxEntity.longitude;

    map.setView({
      center: new Microsoft.Maps.Location(latitude, longitude),
    });
  }

  render() {
    return null;
  }
}


AutoLayout.propTypes = {
  map: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  Microsoft: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  infoboxEntity: PropTypes.shape({
    latitude: PropTypes.number.isRequired,
    longitude: PropTypes.number.isRequired,
  }),
  entities: PropTypes.arrayOf(PropTypes.shape({
    latitude: PropTypes.number.isRequired,
    longitude: PropTypes.number.isRequired,
  })).isRequired,
};

AutoLayout.defaultProps = {
  entities: [],
};
