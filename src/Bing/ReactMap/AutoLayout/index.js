/** @flow */
import AutoLayout from './AutoLayout';

import waitForMap from '../waitForMap';


export default waitForMap(AutoLayout);
