import { NO_METRIC } from 'data/map/constants';


const METRIC_TYPES = {
  [NO_METRIC]: [0, 1, 2, 3],
  ESTVALUE: [0],
  SQFEET: [1],
  LOTSIZE: [1],
  BEDROOMS: [1],
  ESTVALUE_SQFEET: [0],
  ESTVALUE_LOTSIZE: [0],
  ESTVALUE_BEDROOMS: [0],
  ESTVALUE_1MONTH: [2],
  ESTVALUE_6MONTH: [2],
  ESTVALUE_1YEAR: [2],
  ESTVALUE_3YEAR: [2],
  ESTVALUE_5YEAR: [2],
};

export default function getMetricsByType(type, metrics) {
  return metrics.filter(metric => (METRIC_TYPES[metric.get('name')] || METRIC_TYPES[NO_METRIC]).includes(type));
}
