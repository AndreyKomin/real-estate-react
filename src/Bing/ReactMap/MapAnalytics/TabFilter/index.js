import React, { PropTypes } from 'react';
import { pure } from 'recompose';

import SVG from 'components/base/SVG';

import Tab from './Tab';
import css from './style.scss';


export const TabFilter = ({ activeIndex, onClick }) => (
  <div className={css.buttonsGroup}>
    <Tab index={0} onClick={onClick} activeIndex={activeIndex}>
      <SVG icon="iconDollar" className={css.iconDollar} />
    </Tab>
    <Tab index={1} onClick={onClick} activeIndex={activeIndex}>
      <SVG icon="iconUserGroup" className={css.iconUserGroup} />
    </Tab>
    <Tab index={2} onClick={onClick} activeIndex={activeIndex}>
      <SVG icon="iconWarning" className={css.iconWarning} />
    </Tab>
    <Tab index={3} onClick={onClick} activeIndex={activeIndex}>
      <SVG icon="iconGraduation" className={css.iconGraduation} />
    </Tab>
  </div>
);

TabFilter.propTypes = {
  activeIndex: PropTypes.number.isRequired,
};

TabFilter.defaultProps = {
  activeIndex: 0,
};

export default pure(TabFilter);
