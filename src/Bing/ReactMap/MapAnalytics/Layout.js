import React, { PureComponent, PropTypes } from 'react';
import classNames from 'classnames';

import SVG from 'components/base/SVG';
import { dropdownable } from 'components/base/Dropdown';
import DropdownPrice from './DropdownPrice/index';
import TabFilter from './TabFilter/index';
import HeatOpacity from './HeatOpacity/index';
import Spectrum from './Spectrum/index';
import { paletteItems } from '../mockData';
import css from './style.scss';


class Layout extends PureComponent {
  render() {
    const {
      data,
      isOpen,
      onRef,
      onClick,
      type,
      metric,
      currentMetric,
      onMetricChange,
      onTabChange,
      activeTab,
      disabled,
    } = this.props;
    const dropdownClass = classNames(
      css.dropdown,
      { [css.open]: isOpen },
    );
    const buttonClass = classNames(
      css.dropdownButton,
      { [css.active]: isOpen },
    );

    return (
      <div className={css.dropdownAnalytics}>
        <button className={buttonClass} onClick={onClick} disabled={disabled} type={type}>
          <span className={css.analyticsText}>Analytics</span>
          <SVG icon="iconCaretDown" className={css.iconCaretDown} />
        </button>
        <div ref={onRef} className={dropdownClass}>
          <TabFilter onClick={onTabChange} activeIndex={activeTab} />
          <DropdownPrice currentMetric={currentMetric} onChange={onMetricChange} metric={metric} data={data} />
          <HeatOpacity disabledIn disabledOut name="searchPageOpacity" />
          <Spectrum currentMetric={currentMetric} palette={paletteItems} />
        </div>
      </div>
    );
  }
}

Layout.propTypes = {
  isOpen: PropTypes.bool.isRequired,
};

Layout.defaultProps = {
  isOpen: false,
  disabled: false,
  type: 'button',
};

export default dropdownable(Layout);
