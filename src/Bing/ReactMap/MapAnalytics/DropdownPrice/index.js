import React, { PureComponent, PropTypes } from 'react';
import ImPropTypes from 'react-immutable-proptypes';
import classNames from 'classnames';

import Radio from 'components/base/Radio';
import { dropdownable } from 'components/base/Dropdown';
import css from './style.scss';


class DropdownPrice extends PureComponent {
  render() {
    const { data, isOpen, onClick, onRef, metric, currentMetric, onChange } = this.props;
    const dropdownClass = classNames(
      css.dropdown,
      { [css.open]: isOpen },
    );
    const listPrice = data.map((menu) => {
      const id = `price${menu.get('name')}`;
      const listItemClass = classNames(
        css.listItem,
        { [css.active]: menu.get('name') === metric },
      );
      return (
        <div className={listItemClass} key={menu.get('name')}>
          <Radio
            label={menu.get('displayName')}
            name="price"
            id={id}
            checked={menu.get('name') === metric}
            changeValue={menu.get('name')}
            onChange={onChange}
          />
        </div>
      );
    });

    const metricName = currentMetric && currentMetric.get('displayName');

    return (
      <div className={css.dropdownPrice}>
        <div className={css.dropdownButton} onClick={onClick}>
          <span className={css.iconCaret} />
          <span className={css.priceText}>{metricName}</span>
        </div>
        <div className={dropdownClass} ref={onRef}>
          {listPrice}
        </div>
      </div>
    );
  }
}

DropdownPrice.propTypes = {
  data: ImPropTypes.listOf(
    ImPropTypes.contains({
      name: PropTypes.string.isRequired,
      displayName: PropTypes.string,
    })).isRequired,
};

export default dropdownable(DropdownPrice);
