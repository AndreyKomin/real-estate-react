import React, { PureComponent, PropTypes } from 'react';
import classNames from 'classnames';

import SVG from 'components/base/SVG';
import css from './style.scss';


class HeatOpacity extends PureComponent {
  render() {
    const { name, type, disabledOut, disabledIn } = this.props;
    return (
      <div className={css.opacity}>
        <button className={classNames(css.buttonOpacity, css.out)} name={name} disabled={disabledIn} type={type}>
          <SVG icon="iconCaretRight" className={css.iconOut} />
        </button>
        <button className={classNames(css.buttonOpacity, css.in)} name={name} disabled={disabledOut} type={type}>
          <SVG icon="iconCaretLeft" className={css.iconIn} />
        </button>
      </div>
    );
  }
}

HeatOpacity.propTypes = {
  name: PropTypes.string.isRequired,
  disabledIn: PropTypes.bool.isRequired,
  disabledOut: PropTypes.bool.isRequired,
};

HeatOpacity.defaultProps = {
  disabledIn: false,
  disabledOut: false,
  type: 'button',
};

export default HeatOpacity;
