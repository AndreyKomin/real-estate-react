import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { loadMetrics, selectMetrics, NO_METRIC } from 'data/map';
import { ParcelLayer } from 'Bing/ReactMap/TileLayer';

import Layout from './Layout';
import getMetricsByType from './getMetricsByType';


class MapAnalytics extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      metric: NO_METRIC,
      activeTab: 0,
    };
    this.handleTabChange = this.handleTabChange.bind(this);
    this.handleMetricChange = this.handleMetricChange.bind(this);
  }

  componentWillMount() {
    this.props.loadMetrics();
  }

  handleTabChange(newIndex) {
    this.setState({
      metric: NO_METRIC,
      activeTab: newIndex,
    });
  }

  handleMetricChange(metric) {
    this.setState({ metric });
  }

  renderTile() {
    if (this.state.metric === NO_METRIC) return null;
    return (
      <ParcelLayer {...this.props} />
    );
  }

  render() {
    const { children, metrics, ...rest } = this.props;
    const { activeTab, metric } = this.state;
    if (metrics.size === 0) return null;

    const currentMetric = metrics.find(menu => menu.get('name') === metric);

    return (
      <div>
        <Layout
          metric={metric}
          {...rest}
          onTabChange={this.handleTabChange}
          onMetricChange={this.handleMetricChange}
          activeTab={activeTab}
          currentMetric={currentMetric}
          data={getMetricsByType(activeTab, metrics)}
          skipCatcherCheck
        />
        {this.renderTile()}
        <ParcelLayer {...rest} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    metrics: selectMetrics(state),
    zoom: state.getIn(['comparableMap', 'zoom']),
  };
}

const mapActionToProps = {
  loadMetrics,
};

export default connect(mapStateToProps, mapActionToProps)(MapAnalytics);
