import MapAnalytics from './MapAnalytics';
import waitForMap from '../waitForMap';


export default waitForMap(MapAnalytics);
