/** @flow */
import React, { PureComponent } from 'react';
import ReactDom from 'react-dom';
import { withRouter } from 'react-router';
import { is } from 'immutable';

import withProperty from 'app/components/withProperty';
import { triggerMapType } from 'data/map/actions';
import { saveProperty } from 'data/property';
import { selectPermissions } from 'data/user/selectors';

import InfoboxLayout from './InfoboxLayout';
import { INFOBOX_ID } from '../constants';
import EventHandler from '../EventHandler';


const FIELDS_TO_CHECK = [
  'shouldDisplayPopover',
  'isPropertyLoading',
  'property',
  'info',
  'estimatedValueGraph',
];

class Infobox extends PureComponent<*, *, *> {
  constructor(props) {
    super(props);
    this.closeInfobox = this.closeInfobox.bind(this);
    this.handleSaveDefault = this.handleSaveDefault.bind(this);
  }

  shouldComponentUpdate(nextProps: *) {
    return FIELDS_TO_CHECK.some(field => !is(nextProps[field], this.props[field]));
  }

  componentDidUpdate() {
    this.removeInfobox();
    if (this.props.shouldDisplayPopover) {
      this.drawInfobox();
    }
  }

  componentWillUnmount() {
    this.removeInfobox(true);
  }

  infobox: ?{ setMap: Function };

  createInfobox() {
    if (this.infobox) return;

    const { Microsoft, info } = this.props;
    this.infobox = new Microsoft.Maps.Infobox(
      new Microsoft.Maps.Location(
        info.get('latitude'),
        info.get('longitude'),
      ),
      { htmlContent: `<div id="${INFOBOX_ID}" />` },
    );
  }

  drawInfobox() {
    const { map } = this.props;
    this.createInfobox();
    if (this.infobox) {
      this.infobox.setMap(map);
      ReactDom.unstable_renderSubtreeIntoContainer(
        this,
        <InfoboxLayout {...this.props} saveGroup={this.handleSaveDefault} />,
        document.getElementById(INFOBOX_ID),
      );
    }
  }

  removeInfobox(unmountComponent: ?boolean) {
    const element = document.getElementById(INFOBOX_ID);
    if (element && unmountComponent) {
      ReactDom.unmountComponentAtNode(element);
    }
    if (this.infobox) {
      this.infobox.setMap(null);
      this.infobox = null;
    }
  }

  /* :: closeInfobox: Function */
  closeInfobox() {
    const { activePushpinId } = this.props;
    this.props.triggerPushpinActivity(activePushpinId);
  }

  /* :: handleSaveDefault: Function */
  handleSaveDefault() {
    const selected = [{
      listingId: this.props.info.get('propertyId'),
    }];
    this.props.saveGroup(selected, {
      afterSuccess: this.props.openSavedModal,
    });
  }

  render() {
    const { map, Microsoft } = this.props;
    return <EventHandler map={map} Microsoft={Microsoft} event="click" onEvent={this.closeInfobox} />;
  }
}

function mapStateToProps(state, props) {
  return {
    shouldDisplayPopover: props.activePushpinId !== null,
    activePushpinId: props.activePushpinId,
    info: props.info,
    permissions: selectPermissions(state),
  };
}

const mapActionToProps = {
  triggerMapType,
  saveGroup: saveProperty,
};

export default withRouter(withProperty(Infobox, mapStateToProps, mapActionToProps));
