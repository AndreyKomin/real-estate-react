/** @flow */
import React, { PureComponent } from 'react';

import InfoboxContent from 'app/components/InfoboxContent';


class InfoboxLayout extends PureComponent {
  render() {
    return (
      <InfoboxContent {...this.props} />
    );
  }
}


export default InfoboxLayout;
