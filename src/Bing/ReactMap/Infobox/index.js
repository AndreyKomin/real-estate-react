/** @flow */
import Infobox from './Infobox';
import waitForMap from '../waitForMap';


export default waitForMap(Infobox);
