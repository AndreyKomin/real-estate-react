/** @flow */
import promisifyJSONP from 'utils/promisifyJSONP';

// eslint-disable-next-line import/prefer-default-export
export function init(): Promise<Object> {
  return promisifyJSONP('https://www.bing.com/api/maps/mapcontrol', 'callback')
    .then(() => Promise.resolve(window.Microsoft));
}
