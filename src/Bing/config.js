/** @flow */
import config from 'config';


const { constants: { BING: { API_KEY, DEFAULT_MAP_TYPE } } } = config;

// https://msdn.microsoft.com/en-us/library/dn306801.aspx
export const ZIP_TYPES = [
  'Postcode1',
  'Postcode2',
  'Postcode3',
  'Postcode4',
  'PopulatedPlace',
].map(type => type.toLowerCase());

export const PROPERTY_TYPES = [
  'Address',
  'RoadBlock',
].map(type => type.toLowerCase());

export { API_KEY, DEFAULT_MAP_TYPE };
