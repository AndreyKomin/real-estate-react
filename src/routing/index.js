/** @flow */
import React from 'react';
import Router from 'react-router/lib/Router';

import routes from 'app/routes';


export default function (history: *) {
  return (
    <Router history={history}>
      {routes}
    </Router>
  );
}
