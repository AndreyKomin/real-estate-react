import React, { PureComponent } from 'react';

import Modal from 'components/base/Modal';
import Button from 'components/base/Button';
import { getPopupRegistration, openPopup, closePopup, Priority } from 'app/PopupHolder';
import { SketchPicker } from 'react-color';

import css from './style.scss';


class ColorPicker extends PureComponent {
  constructor(props) {
    super(props);

    this.state = { color: props.color || '#fff' };
  }

  choose(color = null) {
    const { name, onChange, closePopup } = this.props;
    onChange({ name, value: color });
    closePopup();
  }

  render() {
    const { closePopup, transparencyEnabled } = this.props;
    const { color } = this.state;

    return (
      <Modal isOpen uniqId="ColorPicker" caption="Select Color" padding="10px" width="auto">
        <div>
          <div className={css.picker}>
            <SketchPicker disableAlpha color={color || '#ffffff'} onChangeComplete={color => this.setState({ color: color.hex })} />
          </div>
          {!transparencyEnabled ? null : (
            <div className={css.transparent}>
              <Button kind={Button.kind.grayGhost} size={Button.size.large} onClick={() => this.choose()}>Make Transparent</Button>
            </div>
          )}
          <div className={css.buttons}>
            <Button kind={Button.kind.grayGhost} size={Button.size.large} onClick={closePopup}>Cancel</Button>
            <Button kind={Button.kind.blue} size={Button.size.large} onClick={() => this.choose(this.state.color)}>Done</Button>
          </div>
        </div>
      </Modal>
    );
  }
}

ColorPicker.defaultProps = {
  transparencyEnabled: false,
};

ColorPicker.registrationId = getPopupRegistration(ColorPicker);
ColorPicker.open = props => openPopup(ColorPicker.registrationId, { ...props, priority: Priority.HIGHEST });
ColorPicker.close = () => closePopup({ popup: ColorPicker.registrationId });

export default ColorPicker;
