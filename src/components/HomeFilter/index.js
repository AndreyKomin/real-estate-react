import React, { PureComponent } from 'react';
import classNames from 'classnames';

import config from 'config';

import { dropdownable } from 'components/base/Dropdown';
import SVG from 'components/base/SVG';
import FormControlWrapper from 'components/base/FormControlWraper';
import RadioGroup from 'components/base/RadioGroup';

import { withoutPrefix } from 'utils/currency/numberToPrice';
import currencyField from 'components/hoc/currencyField';

import css from './style.scss';


const { constants: { STATE: { BEDROOMS, BATHROOMS } } } = config;

const Input = currencyField('input');

class HomeFilter extends PureComponent {
  /**
   * Method returns Price string
   * @returns {String}
   */
  getPrice() {
    const { defaultAmountMin, defaultAmountMax } = this.props;

    if (defaultAmountMax === '') {
      if (defaultAmountMin === '') {
        return 'Any Price';
      }
      return `${withoutPrefix(defaultAmountMin)}+`;
    } else if (defaultAmountMin === '') {
      return `0–${withoutPrefix(defaultAmountMax)}`;
    }

    return `${withoutPrefix(defaultAmountMin)}–${withoutPrefix(defaultAmountMax)}`;
  }

  /**
   * Method returns Beds string
   * @returns {String}
   */
  getBeds() {
    const { bedroomsMin } = this.props;

    if (bedroomsMin > 0) {
      return `${bedroomsMin}+ Beds`;
    }

    return 'Any Beds';
  }

  /**
   * Method returns Baths string
   * @returns {String}
   */
  getBaths() {
    const { bathroomsMin } = this.props;

    if (bathroomsMin > 0) {
      return `${bathroomsMin}+ Baths`;
    }

    return 'Any Baths';
  }


  render() {
    const {
      isOpen,
      onRef,
      onClick,
      onChange,
      bedroomsMin,
      bathroomsMin,
      defaultAmountMin,
      defaultAmountMax,
    } = this.props;

    const dropdownClass = classNames(
      css.dropdown,
      { [css.open]: isOpen },
    );

    const price = this.getPrice();
    const beds = this.getBeds();
    const baths = this.getBaths();

    return (
      <div className={css.homeFilter} onClick={onClick}>
        <div className={css.filter}>
          <div className={css.value} title={price}>{price} / {beds} / {baths}</div>
          <SVG icon="iconCaretDown" className={css.iconCaretDown} />
        </div>
        <div className={dropdownClass} ref={onRef}>
          <div className={css.rowRange}>
            <label className={css.label} htmlFor="amount-of-default-min">Price</label>
            <div className={css.range}>
              <FormControlWrapper id="defaultAmountMin">
                <Input
                  className={css.input}
                  type="text"
                  id="amount-of-default-min"
                  name="defaultAmountMin"
                  onChange={onChange}
                  value={defaultAmountMin}
                  placeholder="Min"
                />
              </FormControlWrapper>
              <span className={css.divider}> to </span>
              <FormControlWrapper id="defaultAmountMax">
                <Input
                  className={css.input}
                  type="text"
                  name="defaultAmountMax"
                  onChange={onChange}
                  value={defaultAmountMax}
                  placeholder="Max"
                />
              </FormControlWrapper>
            </div>
          </div>
          <div className={css.rowRadio}>
            <label htmlFor="beds" className={css.label}>Beds</label>
            <RadioGroup name="bedroomsMin" onChange={onChange} options={BEDROOMS} checked={bedroomsMin.toString()} />
          </div>
          <div className={css.rowRadio}>
            <label htmlFor="baths" className={css.label}>Baths</label>
            <RadioGroup name="bathroomsMin" onChange={onChange} options={BATHROOMS} checked={bathroomsMin.toString()} />
          </div>
        </div>
      </div>
    );
  }
}

export default dropdownable(HomeFilter);
