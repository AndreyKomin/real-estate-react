/** @flow */
import React, { PureComponent } from 'react';
import classNames from 'classnames';

import Modal from 'components/base/Modal';
import SVG from 'components/base/SVG';

import css from './style.scss';


export default class Layout extends PureComponent<*, *, *> {
  /* :: state: Object */
  /* :: form: any */

  constructor(props: *) {
    super(props);
    this.state = { isLoading: true };
    this.form = null;
    this.refHandler = this.refHandler.bind(this);
    this.loadHandler = this.loadHandler.bind(this);

    // rehabCalculatorButtonCallback in global scope
    if (window.rehabCalculatorButtonCallback) return;
    const { closePopup, router, setBluebook } = this.props;
    window.rehabCalculatorButtonCallback = (search) => {
      const query = JSON.parse(`{"${decodeURI(search).replace('?', '').replace(/"/g, '\\"').replace(/&/g, '","')
        .replace(/=/g, '":"')}"}`);
      setBluebook(query);
      closePopup();
      router.push(`${this.props.detailsRoot}/analysis/purchase`);
    };
  }

  componentDidMount() {
    if (!this.form) return;
    this.form.submit();
  }

  /* :: refHandler: Function */
  refHandler(form: HTMLElement) {
    this.form = form;
  }

  /* :: loadHandler: Function */
  loadHandler(event: any) {
    const { closePopup, router } = this.props;

    this.setState({ isLoading: false });


    if (event.target.contentWindow.location.href) {
      router.push(event.target.contentWindow.location.href);
      closePopup();
    }
  }

  render() {
    const { value } = this.props;
    const iframeClass = classNames(css.iframe, { [css.hidden]: this.state.isLoading });
    const spinnerClass = classNames(css.spinner, { [css.hidden]: !this.state.isLoading });
    const header = (
      <div className={css.header}>
        <div className={css.headerTitle}>Rehab Calculator</div>
      </div>
    );

    return (
      <Modal
        isOpen
        isCloseButton
        background="#F3F5F8"
        uniqId="modalBlueBook"
        caption={header}
        width="1080px"
      >
        <div className={css.modalBody}>
          <div className={spinnerClass}>
            <SVG icon="spinner" />
          </div>
          <iframe
            className={iframeClass}
            name="bluebook-iframe"
            title="bluebook-iframe"
            onLoad={this.loadHandler}
          />
          <form
            action="https://test.bluebook.net/order/directplugin/1832"
            method="POST"
            ref={this.refHandler}
            target="bluebook-iframe"
          >
            <input type="hidden" name="XmlData" value={value} />
          </form>
        </div>
      </Modal>
    );
  }
}
