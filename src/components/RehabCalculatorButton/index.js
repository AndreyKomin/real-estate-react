/** @flow */
import React, { PureComponent, PropTypes } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import ImPropTypes from 'react-immutable-proptypes';

import urlSubscribe from 'app/Property/Detail/urlSubscribe';
import Button from 'components/base/Button';
import SVG from 'components/base/SVG';
import RestrictedContent from 'app/components/RestrictedContent';
import { Permissions } from 'data/user/constants';

import Modal from './Modal';
import xmlData from './data.xml';
import css from './style.scss';


const originUrl = document.location.origin;
const propertyTypes = {
  SFR: 'Single Family Residence',
  CONDO: 'Condo',
  MFR: '',
  OTHER: '',
};

const addNode = (xml, name, text) => {
  const element = xml.createElement(name);
  const content = xml.createTextNode((text && text.toString) ? text.toString() : '');
  element.appendChild(content);
  return element;
};


class RehabCalculatorButton extends PureComponent {
  constructor(props: Object) {
    super(props);
    this.getXmlData = this.getXmlData.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.showPopup = this.showPopup.bind(this);
  }

  /* :: getXmlData: Function */
  getXmlData() {
    const { property, bluebook } = this.props;

    if (!property || !property.size) return '';

    const BluebookOrderChecksum = bluebook.get('BluebookOrderChecksum', '');
    const BluebookOrderId = bluebook.get('BluebookOrderId', '');
    const {
      id,
      address,
      bedrooms,
      bathrooms,
      yearBuilt,
      stories,
      grossSquareFeet,
      propertyTypeCode,
      constructionQuality,
      lotSquareFeet,
    } = property.toJS();

    const sessionId = `${Date.now()}-${Math.ceil(Math.random() * 1000000)}`; // todo need more elegant solution
    const parser = new DOMParser();
    const serializer = new XMLSerializer();
    const xml = parser.parseFromString(xmlData, 'application/xml');
    const xmlSession = xml.getElementsByTagName('Session')[0];
    const xmlEntireHouseCharacteristic = xml.getElementsByTagName('EntireHouseCharacteristic')[0];
    const full = Math.floor(bathrooms);
    const half = (bathrooms - full) ? '1' : '0';
    const sessionCompleteReturnURL = `${originUrl}/bluebook`;

    // Session
    xmlSession.appendChild(addNode(xml, 'SourceReferenceId', id));
    xmlSession.appendChild(addNode(xml, 'PropertyAddress', address.streetAddress));
    xmlSession.appendChild(addNode(xml, 'PropertyAddress2', ''));
    xmlSession.appendChild(addNode(xml, 'PropertyCity', address.cityName));
    xmlSession.appendChild(addNode(xml, 'PropertyState', address.stateCode));
    xmlSession.appendChild(addNode(xml, 'PropertyZip', address.zip));
    xmlSession.appendChild(addNode(xml, 'PropertyType', propertyTypes[propertyTypeCode]));
    xmlSession.appendChild(addNode(xml, 'SessionId', sessionId));
    xmlSession.appendChild(addNode(xml, 'BluebookOrderId', BluebookOrderId));
    xmlSession.appendChild(addNode(xml, 'BluebookOrderCheckSum', BluebookOrderChecksum));
    xmlSession.appendChild(addNode(xml, 'UserEmailAddress', ''));
    xmlSession.appendChild(addNode(xml, 'SessionCompleteReturnURL', sessionCompleteReturnURL));

    // EntireHouseCharacteristic
    xmlEntireHouseCharacteristic.appendChild(addNode(xml, 'TotalBedroomCount', bedrooms));
    xmlEntireHouseCharacteristic.appendChild(addNode(xml, 'TotalFullBathCount', full));
    xmlEntireHouseCharacteristic.appendChild(addNode(xml, 'TotalHalfBathCount', half));
    xmlEntireHouseCharacteristic.appendChild(addNode(xml, 'YearBuilt', yearBuilt));
    xmlEntireHouseCharacteristic.appendChild(addNode(xml, 'Stories', stories));
    xmlEntireHouseCharacteristic.appendChild(addNode(xml, 'GrossLivingArea', grossSquareFeet));
    xmlEntireHouseCharacteristic.appendChild(addNode(xml, 'StructureQuality', constructionQuality));
    xmlEntireHouseCharacteristic.appendChild(addNode(xml, 'LotSize', lotSquareFeet));
    xmlEntireHouseCharacteristic.appendChild(addNode(xml, 'BasementSize', '0'));
    xmlEntireHouseCharacteristic.appendChild(addNode(xml, 'AttachedGarageTotalParkingSpace', 'N/A'));
    xmlEntireHouseCharacteristic.appendChild(addNode(xml, 'BuiltInGarageTotalParkingSpace', ''));
    xmlEntireHouseCharacteristic.appendChild(addNode(xml, 'RoofPitch', ''));

    return serializer.serializeToString(xml);
  }

  /* :: handleClick: Function */
  handleClick() {
    if (this.props.onClick) {
      this.props.onClick(this.showPopup);
      return;
    }

    this.showPopup();
  }

  /* :: showPopup: Function */
  showPopup() {
    const { openPopup, router, property, urls } = this.props;

    openPopup({
      router,
      urls,
      propertyId: property.get('propertyId'),
      value: this.getXmlData(),
    });
  }

  render() {
    return (
      <RestrictedContent permission={Permissions.rehabView}>
        <Button
          type="button"
          kind={Button.kind.grayGhost}
          className={css.buttonRehabCalculator}
          onClick={this.handleClick}
        >
          <SVG icon="iconCalculator" className={css.iconRehabCalculator} /> Rehab Calculator
        </Button>
      </RestrictedContent>
    );
  }
}


RehabCalculatorButton.propTypes = {
  property: ImPropTypes.mapContains({
    id: PropTypes.number,
    address: PropTypes.object,
    bedrooms: PropTypes.number,
    bathrooms: PropTypes.number,
    yearBuilt: PropTypes.number,
    stories: PropTypes.number,
    grossSquareFeet: PropTypes.number,
    landUse: PropTypes.string,
    constructionQuality: PropTypes.string,
    lotSquareFeet: PropTypes.number,
  }),
  bluebook: ImPropTypes.mapContains({
    BluebookOrderChecksum: PropTypes.string,
    BluebookOrderId: PropTypes.string,
  }),
  onClick: PropTypes.func,
};

const mapStateToProps = () => ({});

const mapActionToProps = {
  openPopup: Modal.open,
  closePopup: Modal.close,
};

export default withRouter(connect(mapStateToProps, mapActionToProps)(urlSubscribe(RehabCalculatorButton)));

export { RehabCalculatorButton };
