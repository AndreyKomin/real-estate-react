/** @flow */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { getPopupRegistration, openPopup, closePopup, Priority } from 'app/PopupHolder';
import UrlStorage from 'app/Property/Detail/UrlStorage';
import urlSubscribe from 'app/Property/Detail/urlSubscribe';

import Layout from './Layout';


export class RehabCalculatorModal extends PureComponent<*, *, *> {
  render() {
    return (
      <UrlStorage urls={this.props.urls}>
        <Layout
          {...this.props}
        />
      </UrlStorage>
    );
  }
}

function mapStateToProps() {
  return {};
}

const mapActionToProps = {};

const ConnectedModal = connect(mapStateToProps, mapActionToProps)(RehabCalculatorModal);
const Modal = urlSubscribe(ConnectedModal);
const registrationId = getPopupRegistration(Modal);

(Modal: any).open = (props, ...rest) => openPopup(registrationId, { priority: Priority.HIGHEST, ...props }, ...rest);
(Modal: any).close = () => closePopup({ popup: registrationId });


export default Modal;
