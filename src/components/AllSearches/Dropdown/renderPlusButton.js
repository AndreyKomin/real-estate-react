import React, { PureComponent } from 'react';
import { createSelector } from 'reselect';

import { dropdownable } from 'components/base/Dropdown';
import css from './style.scss';
import tableCss from '../../base/Table/styles.scss';
import AddToHomeScreen from './AddToHomeScreen';


const selectNames = createSelector(
  items => items,
  items => items.map(item => item.get('name')),
);

/* TODO: rewrite with componentDidUpdate */
// todo remove items

class PlusCell extends PureComponent {
  static handleClick(event) {
    event.stopPropagation();
  }

  constructor(props) {
    super(props);
    this.state = {
      positionTop: props.position,
      positionBottom: null,
    };
    this.handleRef = this.handleRef.bind(this);
  }

  componentDidMount() {
    this.props.setActiveRowClassName(tableCss.active);
  }

  componentDidUpdate(oldProps) {
    const { isOpen, indexes, closeDropdown, direction } = this.props;
    if (oldProps.isOpen !== isOpen) {
      if (isOpen) {
        this.props.setRowActive(indexes.row);
        this.props.registerScrollCallback(closeDropdown);
        if (direction === 'bottom') {
          // eslint-disable-next-line react/no-did-update-set-state
          this.setState({ positionTop: this.span.getBoundingClientRect().top, positionBottom: null });
        }
      } else {
        this.props.setRowActive(null);
      }
    } else if (oldProps.direction !== direction) {
      if (direction === 'bottom') {
        // eslint-disable-next-line react/no-did-update-set-state
        this.setState({ positionTop: this.span.getBoundingClientRect().top, positionBottom: null });
      } else {
        // eslint-disable-next-line react/no-did-update-set-state
        this.setState({ positionTop: this.span.getBoundingClientRect().top - 400, positionBottom: 'unset' });
      }
    }
  }

  handleRef(ref) {
    this.span = ref;
  }

  render() {
    const { items, closeDropdown, openDropdown, direction, onRef, isOpen, currentItem, closeParentDropdown } = this.props;
    const { positionTop, positionBottom } = this.state;
    return (
      <div className={css.plus} onClick={PlusCell.handleClick}>
        <span className={tableCss.hidden} onClick={openDropdown} ref={this.handleRef}>+</span>
        <AddToHomeScreen
          isOpen={isOpen}
          onRef={onRef}
          searchNames={selectNames(items)}
          closeDropdown={closeDropdown}
          closeParentDropdown={closeParentDropdown}
          item={currentItem}
          openToTop={direction === 'top'}
          positionTop={positionTop}
          positionBottom={positionBottom}
        />
      </div>
    );
  }
}

PlusCell.defaultProps = {
  positionTop: 0,
};

const DropdownablePlusCell = dropdownable(PlusCell);

export default function renderPlusButton(items, setRowActive, setActiveRowClassName, registerScrollCallback,
  closeParentDropdown, val, indexes, currentItem) {
  return (
    <DropdownablePlusCell
      items={items}
      indexes={indexes}
      setRowActive={setRowActive}
      setActiveRowClassName={setActiveRowClassName}
      registerScrollCallback={registerScrollCallback}
      currentItem={currentItem}
      closeParentDropdown={closeParentDropdown}
    />
  );
}
