import React, { PropTypes } from 'react';
import classNames from 'classnames';
import { List, Map } from 'immutable';

import Button from 'components/base/Button';
import Dropdown from 'components/base/Dropdown';
import css from '../style.scss';


const AddToHomeScreen = ({ isOpen, openToTop, item, onRef, slots, handleSubmit, handleCancel, style }) => {
  if (!isOpen) return null;
  const name = item.get('name');

  return (
    <div className={classNames(css.root, css.addRoot, { [css.toTop]: openToTop })} ref={onRef} style={style}>
      <form name="" onSubmit={handleSubmit}>
        <div className={css.addHeader}>Add to Home Screen</div>
        <div className={css.addBody}>
          <div className={css.fieldNameContainer}>
            <div className={css.caption}>Name:</div>
            <div className={css.fieldName}>{name}</div>
          </div>
          <div className={css.selectContainer}>
            <div className={css.caption}>Select spot to replace</div>
          </div>
          <div>
            <Dropdown name="slot" options={slots.toJS()} />
          </div>
          <div className={css.buttonsContainer}>
            <Button
              name="cancel"
              kind={Button.kind.default}
              size={Button.size.large}
              type="button"
              onClick={handleCancel}
            >Cancel</Button>
            <Button
              name="save"
              kind={Button.kind.blue}
              size={Button.size.large}
              type="submit"
            >Save</Button>
          </div>
        </div>
      </form>
    </div>
  );
};

AddToHomeScreen.propTypes = {
  isOpen: PropTypes.bool,
  slots: PropTypes.instanceOf(List).isRequired,
  item: PropTypes.instanceOf(Map).isRequired,
  handleSubmit: PropTypes.func,
  handleCancel: PropTypes.func,
};

export default AddToHomeScreen;
