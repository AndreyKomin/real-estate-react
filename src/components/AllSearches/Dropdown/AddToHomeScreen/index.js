import React, { PropTypes, PureComponent } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import { selectSlotsOptions } from 'data/search/selectors';
import Confirm from 'app/components/Confirm';
import { saveSearchAsFavourite } from 'data/search';
import { selectSearchQueryParams } from 'app/Search/selectors';

import Layout from './Layout';


export class AddToHomeScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.selectStyle = createSelector([v => v, (_, v) => v], (top, bottom) => ({ top, bottom }));
  }

  handleSubmit(event) {
    event.preventDefault();
    const { item, closeDropdown, closeParentDropdown } = this.props;
    const { target: { slot: { value } = {} } = {} } = event;
    if (value) {
      this.saveToSlot(item, value);
    }
    closeDropdown();
    closeParentDropdown();
  }

  saveToSlot(item, value) {
    const { saveSearchAsFavourite } = this.props;
    const searchQuery = {
      searchParams: {},
      searchValue: {},
    };
    if (this.props.query) {
      const { searchValue, searchParams } = this.props.query.toJS();
      searchQuery.searchValue = searchValue;
      searchQuery.searchParams = searchParams;
    }

    if (this.props.slots.getIn([value, 'isBusy'])) {
      this.props.confirmAction({
        caption: '',
        question: `Do you want to replace "${this.props.slots.getIn([value, 'label'])}" with "${item.get('name')}"?`,
        onOk: () => saveSearchAsFavourite({ slot: value, item: item.toJS(), searchQuery }),
      });
    } else {
      saveSearchAsFavourite({ slot: value, item: item.toJS(), searchQuery });
    }
  }

  render() {
    const { isOpen, closeDropdown, positionTop, positionBottom, ...rest } = this.props;
    if (!isOpen) return null;
    return (
      <Layout
        {...rest}
        isOpen
        style={this.selectStyle(positionTop, positionBottom)}
        handleCancel={closeDropdown}
        handleSubmit={this.handleSubmit}
      />
    );
  }
}


AddToHomeScreen.propTypes = {
  isOpen: PropTypes.bool,
  saveSearchAsFavourite: PropTypes.func.isRequired,
  closeDropdown: PropTypes.func.isRequired,
};


function mapStateToProps(state) {
  return {
    slots: selectSlotsOptions(state),
    query: selectSearchQueryParams(state),
  };
}

const mapActionToProps = {
  saveSearchAsFavourite,
  confirmAction: Confirm.open,
};

export default connect(mapStateToProps, mapActionToProps)(AddToHomeScreen);
