import React from 'react';
import css from './style.scss';


export default function renderDetails(value) {
  return (
    <div className={css.details} title={value}>{value}</div>
  );
}
