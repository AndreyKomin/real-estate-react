import React, { PropTypes } from 'react';
import { List } from 'immutable';
import { createSelector } from 'reselect';

import Table, { Column } from 'components/base/Table';

import css from './style.scss';
import renderPlusButton from './renderPlusButton';
import renderDeleteButton from './renderDeleteButton';
import renderDetails from './renderDetails';


const getButtonRenderer = createSelector(
  [
    props => props.items,
    props => props.setRowActive,
    props => props.setActiveRowClassName,
    props => props.registerScrollCallback,
    props => props.closeDropdown,
  ],
  (items, setRowActive, setActiveRowClassName, registerScrollCallback, closeParentDropdown) =>
    (...rest) => renderPlusButton(
      items,
      setRowActive, setActiveRowClassName, registerScrollCallback, closeParentDropdown, ...rest),
);

const getDeleteRenderer = createSelector(
  onDelete => onDelete,
  onDelete => (...rest) => renderDeleteButton(onDelete, ...rest),
);

const AllSearchesDropdown = ({
  items, activeRow, activeRowClassName, setRowActive, setActiveRowClassName,
  isOpen, onRef, onRowClick, onDelete, onScroll, registerScrollCallback, closeDropdown,
}) => {
  if (!isOpen) return null;

  const plusProps = {
    items,
    setRowActive,
    setActiveRowClassName,
    registerScrollCallback,
    closeDropdown,
  };

  return (
    <div className={css.root} ref={onRef}>
      <div className={css.header}>Here’s a list of all your saved searches</div>
      <div className={css.tableWrapper} onScroll={onScroll}>
        <Table
          data={items}
          activeRow={activeRow}
          activeRowClassName={activeRowClassName}
          isHoverable
          className="type_6"
          keyField="unifier"
          onRowClick={onRowClick}
        >
          <Column field="id" renderCell={getButtonRenderer(plusProps)} />
          <Column field="name">Name</Column>
          <Column field="description" renderCell={renderDetails} />
          <Column field="info.totalCount">Total</Column>
          <Column field="info.oneWeekCount">
            <div className={css.noWrap}>New <span className={css.dot} /> </div>
          </Column>
          <Column field="deleteButtons" renderCell={getDeleteRenderer(onDelete)} />
        </Table>
      </div>
    </div>
  );
};

AllSearchesDropdown.defaultProps = {
  isOpen: false,
};

AllSearchesDropdown.propTypes = {
  isOpen: PropTypes.bool,
  items: PropTypes.instanceOf(List).isRequired,
};

export default AllSearchesDropdown;
