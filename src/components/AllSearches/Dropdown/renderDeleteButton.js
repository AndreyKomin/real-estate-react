import React, { PureComponent } from 'react';
import classNames from 'classnames';
import css from './style.scss';
import tableCss from '../../base/Table/styles.scss';


class DeleteCell extends PureComponent {
  constructor(props) {
    super(props);
    this.onDelete = this.onDelete.bind(this);
  }

  onDelete(event) {
    event.stopPropagation();
    const { item } = this.props;
    this.props.onDelete(item.get('id'));
  }

  render() {
    const { item } = this.props;
    const canDelete = item.get('id');

    return (
      <button
        type="button"
        onClick={this.onDelete}
        className={classNames(css.inlineButton, css.tableBtn, tableCss.hidden)}
        disabled={!canDelete}
      >
        Delete
      </button>
    );
  }
}

export default function renderDeleteButton(onDelete, _, indexes, item) {
  return <DeleteCell onDelete={onDelete} item={item} />;
}
