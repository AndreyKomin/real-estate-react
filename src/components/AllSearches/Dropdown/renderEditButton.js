import React from 'react';
import classNames from 'classnames';
import css from './style.scss';
import tableCss from '../../base/Table/styles.scss';


export default function renderEditButton() {
  return (
    <button
      type="button"
      className={classNames(css.inlineButton, css.tableBtn, tableCss.hidden)}
    >
      Edit
    </button>
  );
}
