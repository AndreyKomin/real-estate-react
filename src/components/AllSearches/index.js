import React, { PureComponent, PropTypes } from 'react';

import SVG from 'components/base/SVG';

import AllSearchesDropdown from './Dropdown';
import css from './style.scss';


class AllSearches extends PureComponent {
  render() {
    const { label, icon, onClick, isLoading, error, disabled, ...rest } = this.props;

    let dropDown = null;
    let displayLabel = label;

    if (isLoading) {
      displayLabel = 'Loading';
    } else if (error) {
      displayLabel = 'Error';
    } else {
      dropDown = <AllSearchesDropdown {...rest} />;
    }

    return (
      <div className={css.allSearches} onClick={onClick} disabled={disabled}>
        <div>
          <SVG icon={icon} className={css.icon} />
          <div className={css.label}>{displayLabel}</div>
        </div>
        {dropDown}
      </div>
    );
  }
}

AllSearches.propTypes = {
  label: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
};

AllSearches.defaultProps = {
  label: 'All Searches',
  icon: 'iconFolder',
};

export default AllSearches;
