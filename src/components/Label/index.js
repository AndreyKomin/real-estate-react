import React, { PureComponent, PropTypes } from 'react';
import classNames from 'classnames';

import css from './style.scss';


class Label extends PureComponent {
  render() {
    const { children, background, size, ...rest } = this.props;
    const labelClass = classNames(css[background], css[size], css.label);
    return (
      <div {...rest} className={labelClass}>{children}</div>
    );
  }
}

Label.propTypes = {
  children: PropTypes.node.isRequired,
  background: PropTypes.string.isRequired,
  size: PropTypes.oneOf(['normal', 'big']).isRequired,
};

Label.defaultProps = {
  size: 'normal',
  background: '#E3EEFF',
};

export default Label;
