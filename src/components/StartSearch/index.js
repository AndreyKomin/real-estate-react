import React, { PureComponent } from 'react';

import Button from 'components/base/Button';

import css from './style.scss';


export default class StartSearch extends PureComponent {
  render() {
    const { onSubmit, disabled, isLoading } = this.props;

    return (
      <form className={css.root} onSubmit={onSubmit}>
        <Button
          kind="border-blue"
          size="large"
          type="submit"
          disabled={disabled}
          isLoading={isLoading}
        >
          Search
        </Button>
      </form>
    );
  }
}

