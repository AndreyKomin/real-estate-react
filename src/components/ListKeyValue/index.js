import React, { PureComponent, PropTypes } from 'react';

import css from './style.scss';


class ListKeyValue extends PureComponent {
  render() {
    const { children } = this.props;
    return (
      <div className={css.listKeyValue}>{children}</div>
    );
  }
}

ListKeyValue.propTypes = {
  children: PropTypes.node,
};

export default ListKeyValue;
