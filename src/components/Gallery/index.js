import React, { PureComponent, PropTypes } from 'react';
import ImageGallery from 'react-image-gallery';
import classNames from 'classnames';

import css from './style.scss';


class Gallery extends PureComponent {
  render() {
    const { images, interval, containerClassName, showThumbnails = images.length > 1, ...rest } = this.props;

    return (
      <div className={classNames(css.gallery, containerClassName)}>
        <ImageGallery
          {...rest}
          items={images}
          slideInterval={interval}
          showThumbnails={showThumbnails}
          defaultImage="http://images.listing.realestate.com/photo_coming_soon.jpg"
        />
      </div>
    );
  }
}

Gallery.propTypes = {
  images: PropTypes.arrayOf(
    PropTypes.shape({
      original: PropTypes.string,
      thumbnail: PropTypes.string,
    })).isRequired,
  interval: PropTypes.number.isRequired,
};

Gallery.defaultProps = {
  interval: 2000,
  showFullscreenButton: false,
  showPlayButton: false,
};

export default Gallery;
