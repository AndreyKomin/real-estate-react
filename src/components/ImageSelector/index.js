import React, { PureComponent } from 'react';

import loadable from 'components/hoc/loadable';
import Button, { ButtonImport } from 'components/base/Button';
import Image from 'components/base/Image';
import { imageFormatList } from 'config/constants/fileFormats';

import css from './style.scss';


class ImageSelector extends PureComponent {
  constructor(props) {
    super(props);

    this.handleRemoveClick = this.handleRemoveClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleRemoveClick() {
    const { name, onChange } = this.props;
    onChange({ name });
  }

  handleChange(ev) {
    const file = ev.target.files && ev.target.files[0];
    if (file) {
      const { name, onChange } = this.props;
      const fileReader = new FileReader();
      fileReader.onload = ev => onChange({ name, value: { file, url: ev.target.result } });
      fileReader.readAsDataURL(file);
    }
  }

  render() {
    const { image } = this.props;

    return (
      <div className={css.fileContainer}>
        {!image ? null : <Image className={css.brandingImage} src={image.url} alt="" hideOnEmpty />}
        <div className={css.controls}>
          <ButtonImport
            kind={Button.kind.grayGhost}
            size={Button.size.small}
            className={css.browseLogo}
            onChange={this.handleChange}
            accept={imageFormatList}
          >Browse</ButtonImport>
          {!image ? null : <Button kind={Button.kind.redLink} size={Button.size.small} onClick={this.handleRemoveClick}>Remove</Button>}
        </div>
        {/* {image ? null : <div className={css.fileName}>No file chosen</div>} */}
      </div>
    );
  }
}

export default loadable(ImageSelector);
