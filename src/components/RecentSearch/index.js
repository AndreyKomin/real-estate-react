import React, { PureComponent, PropTypes } from 'react';
import classNames from 'classnames';

import SVG from 'components/base/SVG';
import recentSearchItems from './mockData';

import css from './style.scss';


console.warn('RecentSearch component is deprecated');

class RecentSearch extends PureComponent {
  render() {
    const { name, type, placeholder, isVisibleDropdown } = this.props;
    const listSearch = recentSearchItems.map(result => <li className={css.item} key={result}>{result}</li>);
    const dropdownClass = classNames(
      css.dropdown,
      { [css.open]: isVisibleDropdown },
    );
    return (
      <div className={css.recentSearch}>
        <SVG icon="iconSearch" className={css.iconSearch} />
        <input type={type} name={name} placeholder={placeholder} className={css.search} />
        <div className={dropdownClass}>
          <div className={css.caption}><SVG icon="iconClock" className={css.iconCaption} /> Recent Searches</div>
          <ul className={css.searchList}>
            {listSearch}
          </ul>
        </div>
      </div>
    );
  }
}

RecentSearch.propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  isVisibleDropdown: PropTypes.bool.isRequired,
};

RecentSearch.defaultProps = {
  placeholder: 'Search',
  name: 'recentSearch',
  type: 'search',
  isVisibleDropdown: false,
};

export default RecentSearch;
