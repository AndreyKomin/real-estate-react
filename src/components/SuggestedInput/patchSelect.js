import React from 'react';
import classNames from 'classnames';

import * as onKeyPress from 'utils/DOM/onKeyPress';

/* eslint-disable no-return-assign, no-param-reassign, no-multi-assign, no-underscore-dangle, prefer-template */

export default function patchSelect(Select) {
  Select.prototype.sar__handleMouseUp = function handleMouseUp(event, ...rest) {
    const selection = window.getSelection();
    if (selection.type.toUpperCase() === 'RANGE') return;
    Select.prototype.handleMouseDown.call(this, event, ...rest);
  };

  const originalDidMount = Select.prototype.componentDidMount || function () {};
  const originalWillUnmount = Select.prototype.componentWillUnmount || function () {};

  Select.prototype.componentDidMount = function componentDidMount(...rest) {
    originalDidMount.call(this, ...rest);
    this.sar__onRemoveText = function () {
      let shouldClean = false;
      if (!this.wrapper) return;
      try {
        shouldClean = this.wrapper.contains(window.getSelection().focusNode);
      } catch (e) {
        return;
      }
      if (shouldClean) this.popValue();
    }.bind(this);

    onKeyPress.addListener('Backspace', this.sar__onRemoveText);
    onKeyPress.addListener('Delete', this.sar__onRemoveText);
  };

  Select.prototype.componentWillUnmount = function componentWillUnmount(...rest) {
    originalWillUnmount.call(this, ...rest);

    onKeyPress.removeListener('Backspace', this.sar__onRemoveText);
    onKeyPress.removeListener('Delete', this.sar__onRemoveText);
  };

  Select.prototype.render = function render() {
    if (!Object.prototype.hasOwnProperty.call(this, 'sar__handleMouseUp')) {
      this.sar__handleMouseUp = this.sar__handleMouseUp.bind(this);
    }
    const valueArray = this.getValueArray(this.props.value);
    const options = this._visibleOptions = this.filterOptions(this.props.multi ? this.getValueArray(this.props.value) : null);
    let isOpen = this.state.isOpen;
    if (this.props.multi && !options.length && valueArray.length && !this.state.inputValue) isOpen = false;
    const focusedOptionIndex = this.getFocusableOptionIndex(valueArray[0]);

    let focusedOption = null;
    if (focusedOptionIndex !== null) {
      focusedOption = this._focusedOption = options[focusedOptionIndex];
    } else {
      focusedOption = this._focusedOption = null;
    }
    const className = classNames('Select', this.props.className, {
      'Select--multi': this.props.multi,
      'Select--single': !this.props.multi,
      'is-clearable': this.props.clearable,
      'is-disabled': this.props.disabled,
      'is-focused': this.state.isFocused,
      'is-loading': this.props.isLoading,
      'is-open': isOpen,
      'is-pseudo-focused': this.state.isPseudoFocused,
      'is-searchable': this.props.searchable,
      'has-value': valueArray.length,
    });

    let removeMessage = null;
    if (this.props.multi &&
      !this.props.disabled &&
      valueArray.length &&
      !this.state.inputValue &&
      this.state.isFocused &&
      this.props.backspaceRemoves) {
      removeMessage = (
        <span id={this._instancePrefix + '-backspace-remove-message'} className="Select-aria-only" aria-live="assertive">
          {this.props.backspaceToRemoveMessage.replace('{label}', valueArray[valueArray.length - 1][this.props.labelKey])}
        </span>
      );
    }

    return (
      <div
        ref={ref => this.wrapper = ref}
        className={className}
        style={this.props.wrapperStyle}
      >
        {this.renderHiddenField(valueArray)}
        <div
          ref={ref => this.control = ref}
          className="Select-control"
          style={this.props.style}
          onKeyDown={this.handleKeyDown}
          onMouseUp={this.sar__handleMouseUp}
          onTouchEnd={this.handleTouchEnd}
          onTouchStart={this.handleTouchStart}
          onTouchMove={this.handleTouchMove}
        >
          <span className="Select-multi-value-wrapper" id={this._instancePrefix + '-value'}>
            {this.renderValue(valueArray, isOpen)}
            {this.renderInput(valueArray, focusedOptionIndex)}
          </span>
          {removeMessage}
          {this.renderLoading()}
          {this.renderClear()}
          {this.renderArrow()}
        </div>
        {isOpen ? this.renderOuter(options, !this.props.multi ? valueArray : null, focusedOption) : null}
      </div>
    );
  };

  return Select;
}

/* eslint-enable no-return-assign, no-param-reassign, no-multi-assign, no-underscore-dangle, prefer-template */
