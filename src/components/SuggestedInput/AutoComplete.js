import React, { PropTypes, PureComponent, Children } from 'react';
import Autosuggest from 'react-autosuggest';
import classNames from 'classnames';
import { List } from 'immutable';
import Portal from 'react-portal';

import uniqueId from 'utils/uniqueId';

import Button from 'components/base/Button';
import SVG from 'components/base/SVG';

import css from './styles.scss';


const noSuggestions = List();
/**
 * General auto suggestion component
 * @param props {Object}
 * @param props.name {string} name of control
 * @param props.label {string} label of control
 * @param props.disabled {bool} to disable or enable input control
 * @param [props.changed=false] {bool} to mark value as changed
 * @param props.showLabel {bool} to show or hide label of control
 * @param props.hasError {bool} to show red border if set to true
 * @param props.className {string} css class of control
 * @param props.onChange {function} to handle text change event
 * @param props.onSuggestionsFetchRequested {function} to recalculate list of users
 * @param props.onSuggestionsClearRequested {function} to clear list of users
 * @param props.getSuggestionValue {function} to get a text value for input control
 * @param props.renderSuggestion {function} to render suggetion list
 * @param props.onSuggestionSelected {function} to handle suggestion selection
 * @param props.id {string} id of control
 * @param props.placeholder {string} place holder text of control
 * @param props.value {string} value of text control
 * @param props.suggestions {Array} array of suggestions
 * @returns {React.Component}
 * @constructor
 */
class AutoComplete extends PureComponent {
  constructor(props) {
    super(props);

    this.id = uniqueId();
    this.handleSuggestionsClearRequested = this.handleSuggestionsClearRequested.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.renderSuggestionsContainer = this.renderSuggestionsContainer.bind(this);
    this.shouldSuggest = this.shouldSuggest.bind(this);
    this.handleSuggestionsSelected = this.handleSuggestionsSelected.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.saveRef = this.saveRef.bind(this);
    this.renderSuggestion = this.renderSuggestion.bind(this);
    this.container = null;
    this.canDisplay = false;
    this.state = {
      value: '',
      loading: false,
      focused: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (
      (!this.props.searchValue.get('formattedAddress') && nextProps.searchValue.get('formattedAddress'))
      || (this.props.searchValue.get('formattedAddress') !== nextProps.searchValue.get('formattedAddress'))
    ) {
      this.setState({ value: nextProps.value });
    }

    if (this.props.loading !== nextProps.loading) {
      this.setState({ loading: nextProps.loading });
    } else if (!('loading' in nextProps)) {
      if (this.props.suggestions !== nextProps.suggestions) {
        this.setState({ loading: false });
      }
    } else {
      this.setState({ loading: this.props.loading });
    }
  }

  shouldSuggest(value) {
    const { suggestIfEmpty = false, minSuggestLength = 0 } = this.props;
    if (suggestIfEmpty) return true;
    if (!this.canDisplay) return false;
    return (value || '').trim().length > minSuggestLength;
  }

  handleSuggestionsSelected(suggests, ...rest) {
    this.canDisplay = false;
    this.props.onSuggestionSelected(suggests, ...rest);
  }

  handleFocus(event) {
    this.canDisplay = true;
    this.setState({ focused: true });
    if (this.props.inputProps && this.props.inputProps.onFocus) {
      this.props.inputProps.onFocus(event);
    }
  }

  handleBlur(event) {
    this.canDisplay = false;
    this.setState({ focused: false });
    if (this.props.inputProps && this.props.inputProps.onBlur) {
      this.props.inputProps.onBlur(event);
    }
  }

  handleInputChange(event) {
    event.preventDefault();
    this.setState({ value: event.target.value || '' });
    this.canDisplay = true;
    if (event.target.value !== undefined) {
      if (typeof event.target.value === 'string') {
        this.props.onUpdateSuggestions({
          text: event.target.value,
          selected: null,
        });
      }
    }
  }

  handleSuggestionsClearRequested() {
    if (this.props.suggestions) {
      if (this.props.suggestions.size > 0) {
        this.props.onUpdateSuggestions({
          suggestions: noSuggestions,
        });
      }
    }
  }

  saveRef(ref) {
    this.container = ref;
    if (!this.props.inputProps) return;
    if (!this.props.inputProps.ref) return;
    this.props.inputProps.ref(ref);
  }

  renderRecentHeader() {
    if (!this.props.loading && this.props.isRecent) {
      return (
        <div className={css.caption}><SVG icon="iconClock" className={css.iconCaption} /> Recent Searches</div>
      );
    }

    return null;
  }

  renderLoader() {
    const { loading } = this.state;
    if (!loading) return null;

    return (
      <ul role="listbox" className="react-autosuggest__suggestions-list">
        <li
          role="option"
          aria-selected="false"
          className="react-autosuggest__suggestion react-autosuggest__suggestion--accent"
        >
          <i className="fa fa-spinner fa-spin" /> Loading...
        </li>
      </ul>
    );
  }

  renderNoData() {
    const { suggestions, value } = this.props;
    const { loading } = this.state;

    if (loading) return null;
    if (!value || value.length === 0) return null;
    if (!suggestions) return null;
    if (suggestions.size > 0) return null;

    return (
      <ul role="listbox" className="react-autosuggest__suggestions-list">
        <li
          role="option"
          aria-selected="false"
          className="react-autosuggest__suggestion react-autosuggest__suggestion--accent"
        >No data</li>
      </ul>
    );
  }

  renderSuggestionsContainer({ children, containerProps }) {
    if (!this.container) return null;
    if (!this.canDisplay) return null;

    const targetElement = this.container.input;
    if (!targetElement) return null;

    const bodyRect = document.documentElement.getBoundingClientRect();
    const targetRect = targetElement.getBoundingClientRect();
    const top = (targetRect.bottom - bodyRect.top) + 5;   // 5px offset
    const left = (targetRect.left - bodyRect.left) - 1;   // 1px border
    const width = Math.max((targetRect.width + 2), 150);  // 2px borders
    const recentHeader = this.renderRecentHeader();
    const noData = this.renderNoData();
    const loader = this.renderLoader();
    const zIndex = 10;

    const style = {
      top,
      left,
      width,
      zIndex,
    };

    const divClass = classNames('autocomplete-div-hide-on-empty', { hidden: !(children || loader || noData) });

    return (
      <Portal isOpened>
        <div {...containerProps} style={style} className={divClass}>
          {loader}
          {recentHeader}
          <div className="react-autosuggest__suggestions-wrapper">
            {loader ? null : children}
          </div>
          {noData}
        </div>
      </Portal>
    );
  }

  renderSuggestion(...rest) {
    return (
      <span className="react-autosuggest__suggestion-wrapper">
        {this.props.renderSuggestion(...rest)}
      </span>
    );
  }

  render() {
    const {
      clearSearch,
      suggestions = noSuggestions,
      inputProps: _inputProps,
      placeholder,
      getSuggestion,
      getSuggestionValue,
      changed = false,
      canEverBeLoading = true,
      extraProps,
      value,
    } = this.props;

    const inputProps = {
      id: this.id,
      ..._inputProps,
      onChange: this.handleInputChange,
      onBlur: this.handleBlur,
      onFocus: this.handleFocus,
      className: classNames(_inputProps.className, { changed }),
      value: this.state.value || value || '',
      placeholder,
    };
    const suggestionsArray = suggestions ? suggestions.toJS() : [];
    const labelClassName = classNames({ hidden: !inputProps.label });

    const wrappedGetSuggestions = (...rest) => {
      this.setState({ loading: canEverBeLoading });
      getSuggestion(...rest);
    };

    return (
      <div className={css.autosuggestWrapper}>
        <Button kind="link-default" name="clear" className={css.btnClear} onClick={clearSearch}>
          <SVG icon="iconClose" className={css.iconClear} />
        </Button>
        <label htmlFor={inputProps.id} className={labelClassName}>{inputProps.label}</label>
        <Autosuggest
          {...extraProps}
          suggestions={suggestionsArray}
          ref={this.saveRef}
          onSuggestionsFetchRequested={wrappedGetSuggestions}
          onSuggestionsClearRequested={this.handleSuggestionsClearRequested}
          getSuggestionValue={getSuggestionValue}
          shouldRenderSuggestions={this.shouldSuggest}
          renderSuggestion={this.renderSuggestion}
          renderSuggestionsContainer={this.renderSuggestionsContainer}
          onSuggestionSelected={this.handleSuggestionsSelected}
          inputProps={inputProps}
          highlightFirstSuggestion
        />
        {this.props.children}
      </div>
    );
  }

}

AutoComplete.defaultProps = {
  inputProps: {},
};

AutoComplete.propTypes = {
  inputProps: PropTypes.shape({}),
  suggestions: PropTypes.oneOfType([PropTypes.instanceOf(List), PropTypes.instanceOf()]).isRequired,
  onUpdateSuggestions: PropTypes.func.isRequired,
  getSuggestion: PropTypes.func.isRequired,
  getSuggestionValue: PropTypes.func.isRequired,
  renderSuggestion: PropTypes.func.isRequired,
  value: PropTypes.string,
  onSuggestionSelected: PropTypes.func.isRequired,
  suggestIfEmpty: PropTypes.bool,
  minSuggestLength: PropTypes.number,
  loading: PropTypes.bool,
};

export default AutoComplete;

/**
 * @link http://screencloud.net/v/333X - usage example
 * @link http://screencloud.net/v/p5I9 - result
 * @param props {Object} inputProps for AutoCompleteComponent
 * @returns {InputProps.props.children}
 * @constructor
 */
// eslint-disable-next-line react/no-multi-comp
export class InputProps extends PureComponent {
  render() {
    const { props } = this;
    const { children, ...inputProps } = props;
    let result = children;
    Children.map(children, (child) => {
      const { inputProps: _inputProps = {}, ...rest } = child.props;
      const inputPropObject = {
        inputProps: {
          ...inputProps,
          ..._inputProps,
        },
      };
      const Component = child.type;
      result = <Component {...inputPropObject} {...rest} />;
    });
    return result;
  }
}
