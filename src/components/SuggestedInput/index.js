/** @flow */
import React, { PureComponent, PropTypes } from 'react';
import { List } from 'immutable';
import Select from './AutoComplete';


export default class SuggestedInput extends PureComponent<*, *, *> {
  /* :: state: Object */
  /* :: optionsSelector: ?Function */
  selectProps() {
    const {
      label,
      options,
      onFetch,
      onChange,
      isLoading,
      changeLabel,
      ...rest
    } = this.props;

    return {
      onUpdateSuggestions: (suggestion) => {
        if (suggestion.text || suggestion.text === '') {
          if (suggestion.text === label) return;

          changeLabel(suggestion.text);
          onFetch(suggestion.text);

          if (suggestion.text === '') {
            this.props.showRecent();
          }
        }
      },
      getSuggestion: () => {},
      getSuggestionValue: () => {},
      onSuggestionSelected: (event, suggestion) => {
        changeLabel(suggestion.suggestion.text);
        onChange(suggestion.suggestion);
      },
      renderSuggestion: data => data.label,
      canEverBeLoading: false,
      suggestIfEmpty: true,
      suggestions: options,
      placeholder: 'Enter City, Zip Code or Address',
      loading: isLoading,
      ...rest,
    };
  }

  render() {
    const props = this.selectProps();

    return (
      <Select
        {...props}
        value={this.props.label}
      />
    );
  }
}

SuggestedInput.propTypes = {
  options: PropTypes.instanceOf(List),
  name: PropTypes.string,
  isLoading: PropTypes.bool,
  onFetch: PropTypes.func.isRequired,
  valueRenderer: PropTypes.func,
  valueKey: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};


SuggestedInput.defaultProps = {
  options: List(),
  valueKey: 'value',
  filterOption: v => v,
};
