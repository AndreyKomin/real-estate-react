/** @flow */
import React, { PureComponent, PropTypes } from 'react';
import { List } from 'immutable';
import { createSelector } from 'reselect';
import Select from 'react-select';

import './styles.scss';
import patchSelect from './patchSelect';


patchSelect(Select);

export default class SuggestedInput extends PureComponent<*, *, *> {
  constructor(props: Object) {
    super(props);
    this.select = null;
    this.optionsSelector = createSelector(
      options => options,
      options => options.toJS(),
    );
  }

  /* :: select: any */
  /* :: optionsSelector: ?Function */
  selectProps() {
    const {
      options,
      onFetch,
      valueRenderer,
      ...rest
    } = this.props;
    const { optionsSelector }: { optionsSelector: Function } = (this: any);

    return {
      ...rest,
      options: optionsSelector(options),
      loadOptions: onFetch,
      valueRenderer,
      placeholder: 'Enter City, Zip Code or Address',
    };
  }

  render() {
    const props = this.selectProps();

    return (
      <Select.Async
        {...props}
        cache={false}
        cacheAsyncResults={false}
        joinValues
        ignoreCase={false}
        backspaceRemoves={false}
      />
    );
  }
}

SuggestedInput.propTypes = {
  options: PropTypes.instanceOf(List),
  name: PropTypes.string,
  isLoading: PropTypes.bool,
  onFetch: PropTypes.func.isRequired,
  valueRenderer: PropTypes.func,
  valueKey: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};


SuggestedInput.defaultProps = {
  options: List(),
  getValue: ({ value }) => value,
  valueKey: 'value',
  value: '',
  filterOption: v => v,
};
