/** @flow */
import React, { PureComponent, PropTypes } from 'react';
import classNames from 'classnames';

import chart1 from 'assets/images/chart-1.png';
import chart2 from 'assets/images/chart-2.png';
import chart3 from 'assets/images/chart-3.png';
import chart4 from 'assets/images/chart-4.png';
import chart5 from 'assets/images/chart-5.png';
import chart6 from 'assets/images/chart-6.png';
import chart7 from 'assets/images/chart-7.png';
import chart8 from 'assets/images/chart-8.png';
import chart9 from 'assets/images/chart-9.png';
import chart10 from 'assets/images/chart-10.png';
import chart11 from 'assets/images/chart-11.png';
import chart12 from 'assets/images/chart-12.png';
import chart13 from 'assets/images/chart-13.png';
import chart14 from 'assets/images/chart-14.png';

import css from './style.scss';

//  TODO: replace it to real Chart component
const sources = {};
sources[1] = chart1;
sources[2] = chart2;
sources[3] = chart3;
sources[4] = chart4;
sources[5] = chart5;
sources[6] = chart6;
sources[7] = chart7;
sources[8] = chart8;
sources[9] = chart9;
sources[10] = chart10;
sources[11] = chart11;
sources[12] = chart12;
sources[13] = chart13;
sources[14] = chart14;

class Chart extends PureComponent {
  render() {
    const { className, caption, type } = this.props;
    return (
      <div className={classNames(className, css.root)}>
        <div className={css.caption}>{caption}</div>
        <div className={css.imgContainer}>
          <img src={sources[type]} alt={caption} className={css.img} />
        </div>
      </div>
    );
  }
}

Chart.propTypes = {
  caption: PropTypes.string.isRequired,
  type: PropTypes.number.isRequired,
};

export default Chart;
