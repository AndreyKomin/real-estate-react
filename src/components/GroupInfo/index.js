import React, { PureComponent, PropTypes } from 'react';

import css from './style.scss';


class GroupInfo extends PureComponent {
  render() {
    const { label, children } = this.props;
    return (
      <div className={css.info}>
        <div className={css.label}>{label}</div>
        <div className={css.value}>{children}</div>
      </div>
    );
  }
}

GroupInfo.propTypes = {
  label: PropTypes.string,
  children: PropTypes.node.isRequired,
};

export default GroupInfo;
