import React, { PureComponent } from 'react';
import classNames from 'classnames';
import Form from 'components/base/Form';

import RestrictedContent from 'app/components/RestrictedContent';
import Button from 'components/base/Button';
import { CheckboxFormed } from 'components/base/Checkbox';
import { RadioFormed } from 'components/base/Radio';
import { dropdownable, DropdownFormed } from 'components/base/Dropdown';
import Info from 'components/base/Info';
import FormControlWraper from 'components/base/FormControlWraper';
import Input from 'components/base/Input';
import TextArea from 'components/base/TextArea';
import ModalSearchSaveInfo from 'app/Search/Results/ModalSearchSaveInfo';
import { Permissions } from 'data/user/constants';

import css from './style.scss';


const Checkbox = CheckboxFormed;
const Dropdown = DropdownFormed;
const Radio = RadioFormed;


class SaveSearch extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: false,
    };
    this.showHomeScreen = this.showHomeScreen.bind(this);
    this.hideHomeScreen = this.hideHomeScreen.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  showHomeScreen() {
    this.setState({
      modalIsOpen: true,
    });
  }
  hideHomeScreen() {
    this.setState({
      modalIsOpen: false,
    });
  }

  handleSubmit(e, ...rest) {
    const { handleSubmit, closeDropdown } = this.props;
    handleSubmit(e, ...rest);
    closeDropdown();
  }

  renderForm() {
    const { isOpen, form, slots, closeDropdown, includeOnHomeScreen, notificationIntervals } = this.props;
    if (!isOpen) return null;

    const intervals = notificationIntervals.map(interval => (
      <Radio
        key={interval.value}
        name="notificationInterval"
        value={interval.value}
        label={interval.label}
      />
    ));

    return (<Form name={form} onSubmit={this.handleSubmit}>
      <div className={css.dropdownTitle}>New Saved Search</div>
      <div className={css.dropdownBody}>
        <FormControlWraper id="newSearchName" label="Name:" hasMarginBottom>
          <Input type="text" name="name" id="newSearchName" />
        </FormControlWraper>
        <FormControlWraper id="newSearchDescription" label="Details:" hasMarginBottom>
          <TextArea name="description" id="newSearchDescription" rows="3" />
        </FormControlWraper>
        <div className={css.flexInclude}>
          <Checkbox
            label="Include on Home Screen and Replace..."
            name="includeOnHomeScreen"
            id="includeOnHomeScreen"
          />
          <Info onClick={this.showHomeScreen} />
        </div>
        <div className={classNames(css.row, { [css.disabled]: !includeOnHomeScreen })}>
          <Dropdown id="slot" name="slot" options={slots.toJS()} disabled={!includeOnHomeScreen} />
        </div>
        <FormControlWraper id="controlsEmail" label="Email when new properties become available:" hasMarginBottom>
          <div className={classNames(css.inlineRadio)}>
            {intervals}
          </div>
        </FormControlWraper>
        <div className={css.buttons}>
          <Button
            kind="border-default"
            size="large"
            name="clear"
            width="95px"
            type="reset"
            onClick={closeDropdown}
          >Cancel</Button>
          <Button
            kind="button-blue"
            size="large"
            name="save"
            width="95px"
            type="submit"
          >Save</Button>
        </div>
      </div>
    </Form>);
  }

  render() {
    const { isLoading, disabled, isOpen, onRef, onClick } = this.props;
    const { modalIsOpen } = this.state;
    const dropdownClass = classNames(css.dropdown, { [css.open]: isOpen });

    const form = this.renderForm();
    return (
      <div className={css.saveSearch}>
        <RestrictedContent permission={Permissions.searchFull}>
          <Button
            kind="border-blue"
            size="large"
            name="saveSearch"
            disabled={disabled}
            onClick={onClick}
            isLoading={isLoading}
          >Save Search</Button>
          <div className={dropdownClass} ref={onRef}>
            {form}
            <ModalSearchSaveInfo onClose={this.hideHomeScreen} isOpen={modalIsOpen} />
          </div>
        </RestrictedContent>
      </div>
    );
  }
}


export default dropdownable(SaveSearch);
