import React, { PureComponent } from 'react';


export default function withFiltering(Component) {
  return class WithFiltering extends PureComponent {
    constructor(props) {
      super(props);

      this.handleFilterChange = this.handleFilterChange.bind(this);
      this.handleToggleFilter = this.handleToggleFilter.bind(this);

      const { filterEnabled: enabled = true } = this.props;
      this.state = { enabled, values: {} };
    }

    handleFilterChange(field, value) {
      this.setState({ values: Object.assign({}, this.state.values, { [field]: value }) });
    }

    handleToggleFilter(event) {
      event.stopPropagation();
      this.setState({ enabled: !this.state.enabled });
    }

    filterData() {
      const { state: { enabled, values }, props: { data } } = this;
      const filters = Object.keys(values).map(name => ({ name, value: values[name].toLowerCase().trim() })).filter(f => f.value !== '');

      return enabled && filters.length ? data.filter(item => !filters.find(filter => !String(item.getIn(filter.name.split('.'))).toLowerCase().includes(filter.value))) : data;
    }

    render() {
      const { handleFilterChange, handleToggleFilter, state, props } = this;
      return (<Component
        {...props}
        data={this.filterData()}
        onFilterChange={handleFilterChange}
        onToggleFilter={handleToggleFilter}
        filter={handleFilterChange}
        filterValues={state.values}
        filterEnabled={state.enabled}
      />);
    }
  };
}
