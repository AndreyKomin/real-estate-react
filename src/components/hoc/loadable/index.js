/**
 * Loadable HOC shows Loader component if isLoading flag is true, ErrorDisplay if isError flag is true,
 * otherwise it shows your component
 *
 * @param isLoading   {Boolean} (optional) flag for loading state
 * @param isHover     {Boolean} (optional) show loader above of component
 * @param isError     {Boolean} (optional) flag for error state
 * @param errorText   {Boolean} (optional) error message
 *
 * @returns {Component}
 *
 *
 * How you can use it
 *
 * const MyComponentLoadable = loadable(MyComponent);
 * <MyComponentLoadable
 *   isLoading={isLoadingState}
 *   isError={isErrorState}
 *   errorText={errorText}
 *   ...any props
 * />
 *
 * Or with children
 *
 * <MyComponentLoadable
 *   isLoading={isLoadingState}
 *   isError={isErrorState}
 *   errorText={errorText}
 *   ...any props
 * >
 *   <ChildrenComponent />
 * </MyComponentLoadable>
 */

import React from 'react';
import { pure } from 'recompose';

import Loader from '../../Loader';
import ErrorDisplay from '../../ErrorDisplay';

import css from '../../Loader/style.scss';


export default WrappedComponent => pure(
  (props) => {
    const { isLoading, loading, isHover, isError, errorText, loaderSize, loaderDirection, ...rest } = props;
    let component;

    if (isError) {
      component = <ErrorDisplay text={errorText} />;
    } else if (isLoading || loading) {
      if (isHover) {
        component = (
          <div className={css.withHoverLoader}>
            <Loader loaderSize={loaderSize} loaderDirection={loaderDirection} isHover />
            <WrappedComponent {...rest} />
          </div>
        );
      } else {
        component = <Loader loaderSize={loaderSize} loaderDirection={loaderDirection} />;
      }
    } else {
      component = <WrappedComponent {...rest} />;
    }

    return component;
  },
);
