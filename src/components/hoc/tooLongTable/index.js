import React, { PureComponent } from 'react';


const INIT_COUNTER = 100;
const STEP = 500;

export default function tooLongTable(Component, { step = STEP, initialSize = INIT_COUNTER } = {}) {
  return class TooLongTable extends PureComponent {
    constructor(props) {
      super(props);
      this.state = {
        filterValues: {},
        counter: initialSize,
      };
      this.handleMoreClick = this.handleMoreClick.bind(this);
    }

    componentWillReceiveProps(nextProps) {
      if (this.props.data !== nextProps.data) {
        this.setState({ counter: initialSize });
      }
    }

    hasMoreData() {
      return !!this.props.data && this.state.counter < this.props.data.size;
    }

    sliceData() {
      const { data } = this.props;
      if (this.state.counter < data.size) {
        return data.slice(0, this.state.counter);
      }
      return data;
    }

    /* :: handleMoreClick: Function */
    handleMoreClick() {
      const { counter } = this.state;
      this.setState({
        counter: counter + step,
      });
    }

    render() {
      const data = this.sliceData();

      return (
        <Component
          {...this.props}
          hasMoreData={this.hasMoreData()}
          onMoreDataClick={this.handleMoreClick}
          data={data}
          realDataSize={this.props.data.size}
        />
      );
    }
  };
}
