import React, { PureComponent, PropTypes } from 'react';
import classNames from 'classnames';

import css from './style.scss';


class CustomTabs extends PureComponent {
  render() {
    const { kind } = this.props;
    const tabsClass = classNames(css.tabs, css[kind]);
    return <div {...this.props} className={tabsClass} />;
  }
}

CustomTabs.defaultProps = {
  kind: 'default',
};

CustomTabs.propTypes = {
  kind: PropTypes.oneOf(['default', 'line']),
};


export default CustomTabs;
