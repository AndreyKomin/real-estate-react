/** @flow */
import React, { PureComponent } from 'react';
import classNames from 'classnames';
import { List } from 'immutable';

import type { PropertyType } from 'data/listing/flow-types';

import * as Listing from 'data/listing';

import SVG from 'components/base/SVG';
import Checkbox from 'components/base/Checkbox';
import { dropdownable } from 'components/base/Dropdown';

import css from './style.scss';


const LISTING_ORDER: Array<PropertyType> = (Object.values(Listing.PropTypeCode): Array<any>);


class HomeType extends PureComponent {
  getLabel(): string {
    const { propertyTypeCode } = this.props;
    const propertyTypeList = List.isList(propertyTypeCode) ? propertyTypeCode : List([propertyTypeCode]);
    if (!propertyTypeList.size || LISTING_ORDER.every(type => propertyTypeCode.includes(type))) {
      return 'All Home Types';
    }
    return propertyTypeList.map(propertyType => Listing.PropName[propertyType])
      .filter(propertyType => propertyType).join(' / ');
  }

  getCheckboxes() {
    const { propertyTypeCode } = this.props;
    return LISTING_ORDER.map(
      (type: PropertyType) => (<Checkbox
        onChange={this.props.onChange}
        key={type}
        label={Listing.PropName[type]}
        name="propertyTypeCode"
        value={Listing.PropTypeCode[type]}
        checked={propertyTypeCode.includes(type)}
      />),
    );
  }

  render() {
    const { size, isOpen, onRef, onClick } = this.props;

    const filterClass = classNames(css.filter, css[size]);
    const dropdownClass = classNames(css.dropdown, { [css.open]: isOpen });

    const label = this.getLabel();
    const checkboxes = this.getCheckboxes();

    return (
      <div className={css.homeType} onClick={onClick}>
        <div className={filterClass}>
          <div className={css.value}>{label}</div>
          <SVG icon="iconCaretDown" className={css.iconCaretDown} />
        </div>
        <div className={dropdownClass} ref={onRef}>
          {checkboxes}
        </div>
      </div>
    );
  }
}


export default dropdownable(HomeType);
