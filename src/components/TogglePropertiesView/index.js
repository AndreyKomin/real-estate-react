import React, { PureComponent, PropTypes } from 'react';
import classNames from 'classnames';
import { Display } from 'data/listing';

import SVG from 'components/base/SVG';
import css from './style.scss';


export default class TogglePropertiesView extends PureComponent {
  render() {
    const { active, onChangeActive } = this.props;
    return (
      <div className={css.togglePropertiesView}>
        <div className={classNames(css.item, { [css.active]: active === Display.PICTURE })} onClick={() => onChangeActive(Display.PICTURE)}>
          <SVG icon="iconPicture" className={classNames([css.icon, css.iconPic])} />
          <div className={css.label}>Pic View</div>
        </div>
        <div className={classNames(css.item, { [css.active]: active === Display.LIST })} onClick={() => onChangeActive(Display.LIST)}>
          <SVG icon="iconList" className={classNames([css.icon, css.iconList])} />
          <div className={css.label}>List View</div>
        </div>
      </div>
    );
  }
}

TogglePropertiesView.propTypes = {
  active: PropTypes.oneOf(Object.values(Display)),
  onChangeActive: PropTypes.func.isRequired,
};

TogglePropertiesView.defaultProps = {
  active: Display.NONE,
};

