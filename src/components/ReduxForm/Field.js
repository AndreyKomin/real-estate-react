/** @flow */
import React from 'react';
import { pure } from 'recompose';
import { Field as ReduxField } from 'redux-form/immutable';


export const Field = (props: *) => <ReduxField {...props} />;

export default pure(Field);
