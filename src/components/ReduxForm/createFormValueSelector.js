/** @flow */
import { formValueSelector } from 'redux-form/immutable';


export default function createFormValueSelector(formName: string, ...rest: *) {
  return formValueSelector(formName, ...rest);
}

