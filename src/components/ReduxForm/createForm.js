/** @flow */
import { reduxForm } from 'redux-form/immutable';


const emptyOptions = Object.freeze({});

export default function createForm(form: string, options: Object = emptyOptions) {
  return reduxForm({ ...options, form });
}
