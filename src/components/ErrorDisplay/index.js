import React, { PropTypes } from 'react';
import { pure } from 'recompose';


const ErrorDisplay = ({ text }) => {
  const errorMessage = text || 'Some error happened on loading.';
  return <div>{ errorMessage }</div>;
};

ErrorDisplay.propTypes = {
  text: PropTypes.string,
};

export default pure(ErrorDisplay);
