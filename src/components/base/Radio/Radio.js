import React, { PureComponent, PropTypes } from 'react';
import classNames from 'classnames';

import SVG from 'components/base/SVG';
import css from './style.scss';


class Radio extends PureComponent {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const { onChange, changeValue } = this.props;
    if (changeValue) {
      onChange(changeValue, event);
    } else if (onChange) {
      onChange(event);
    }
  }

  render() {
    const { id = `radio${Math.random()}`, label, hasArrow, input, meta, changeValue, realValue, ...rest } = this.props;
    const arrow = hasArrow ? <div className={css.arrow} /> : null;
    const disabled = rest.disabled || input.disabled;

    return (
      <div className={classNames(css.radio, { [css.disabled]: disabled })}>
        <label htmlFor={id} className={css.switch}>
          <input {...rest} id={id} className={css.input} type="radio" onChange={this.handleChange} {...input} />
          <SVG icon="iconRadio" className={css.iconRadio} />
          <SVG icon="iconRadioChecked" className={css.iconRadioChecked} />
          <span className={css.label}>{label}</span>
          {arrow}
        </label>
      </div>
    );
  }
}

Radio.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  hasArrow: PropTypes.bool,
};

Radio.defaultProps = {
  input: {},
};

export default Radio;
