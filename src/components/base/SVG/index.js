import React, { PropTypes } from 'react';
import { pure } from 'recompose';
import classNames from 'classnames';


const SVG = (props) => {
  const { styles, icon, className, ...rest } = props;
  const use = icon ? <use xlinkHref={`#${icon}`} /> : null;

  return (
    <svg {...rest} style={styles} className={classNames(`icon-${icon}`, className)}>
      {use}
    </svg>
  );
};

SVG.propTypes = {
  icon: PropTypes.string,
};

export default pure(SVG);
