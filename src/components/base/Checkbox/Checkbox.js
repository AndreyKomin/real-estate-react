import React, { PureComponent, PropTypes } from 'react';
import classNames from 'classnames';

import unidueId from 'utils/uniqueId';
import SVG from 'components/base/SVG';

import css from './style.scss';


class Checkbox extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      checked: props.checked || props.defaultChecked,
    };
    this.id = props.id || unidueId();
    this.handleChange = this.handleChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.checked !== this.props.checked) {
      this.setState({ checked: nextProps.checked });
    }
  }

  handleChange(e) {
    if (this.props.onChange) {
      this.props.onChange(this.props.changeValue || e);
    } else {
      this.setState({ checked: !this.state.checked });
    }
  }

  render() {
    const { id, label, checked, defaultChecked, changeValue, containerClassName, input, meta, ...rest } = this.props;

    return (
      <div className={classNames(css.checkbox, containerClassName)}>
        <label htmlFor={id} className={css.switch}>
          <input
            {...rest}
            id={id}
            className={css.input}
            type="checkbox"
            onChange={this.handleChange}
            checked={this.state.checked}
            {...input}
          />
          <SVG icon="iconCheckbox" className={css.iconCheckbox} />
          <SVG icon="iconCheckboxChecked" className={css.iconCheckboxChecked} />
          <span className={css.label}>{label}</span>
        </label>
      </div>
    );
  }
}

Checkbox.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
};

Checkbox.defaultProps = {
  input: {},
};


export default Checkbox;
