import React, { PureComponent, PropTypes } from 'react';
import classNames from 'classnames';

import Button from './Button';

import css from './style.scss';


class ButtonImport extends PureComponent {
  render() {
    const { children, name = `file${Math.random()}`, accept, className, onChange, ...rest } = this.props;
    const importButtonClass = classNames(css.importButton, className);

    return (
      <Button
        name={name}
        className={importButtonClass}
        {...rest}
      >
        <label htmlFor={name} className={css.importLabel}>&#160;</label>
        <input
          type="file"
          className={css.importInput}
          name={name}
          id={name}
          accept={accept}
          onChange={onChange}
          multiple={this.props.multiple}
        />
        {children}
      </Button>
    );
  }
}

ButtonImport.propTypes = {
  children: PropTypes.node.isRequired,
  kind: PropTypes.string.isRequired,
  name: PropTypes.string,
  accept: PropTypes.string,
};

ButtonImport.kind = Button.kind;
ButtonImport.size = Button.size;

ButtonImport.defaultProps = {
  kind: Button.kind.grayGhost,
  size: Button.size.middle,
};

export default ButtonImport;
