import React, { PureComponent, PropTypes } from 'react';
import classNames from 'classnames';

import Button from './Button';
import { handleFileChange } from '../FileReceiver';

import css from './style.scss';


class ButtonUpload extends PureComponent {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(ev) {
    const { name, document, onChange } = this.props;
    handleFileChange(ev.target.files && ev.target.files[0], name, onChange, document);
  }

  render() {
    const { children, name = `file${Math.random()}`, accept, className, kind, size, loading } = this.props;

    return (
      <Button name={name} kind={kind} size={size} className={classNames(css.importButton, className)} isLoading={loading}>
        <label htmlFor={name} className={css.importLabel}>&#160;</label>
        <input
          type="file"
          className={css.importInput}
          name={name}
          id={name}
          accept={accept}
          onChange={this.handleChange}
        />
        {children}
      </Button>
    );
  }
}

ButtonUpload.propTypes = {
  children: PropTypes.node.isRequired,
  kind: PropTypes.string,
  size: PropTypes.string,
  name: PropTypes.string,
  accept: PropTypes.string,
  document: PropTypes.bool,
  onChange: PropTypes.func,
  loading: PropTypes.bool,
};

ButtonUpload.defaultProps = {
  kind: Button.kind.grayGhost,
  size: Button.size.middle,
  document: true,
  loading: false,
};

export default ButtonUpload;
