import React, { PureComponent, PropTypes } from 'react';
import classNames from 'classnames';
import { Link } from 'react-router';

import SVG from 'components/base/SVG';

import css from './style.scss';


class ButtonSquare extends PureComponent {
  renderSpinner() {
    const { isLoading } = this.props;

    return isLoading ? (
      <SVG icon="spinner" className={css.spinner} />
    ) : null;
  }

  render() {
    const { caption, icon, type, link, isLoading, ...rest } = this.props;
    const button = link ? (
      <Link to={link} className={classNames(css.button, { [css.loading]: isLoading })} role="button">
        {this.renderSpinner()}
        <SVG icon={icon} className={css.icon} />
      </Link>
    ) : (
      <button {...rest} className={classNames(css.button, { [css.loading]: isLoading })} type={type}>
        {this.renderSpinner()}
        <SVG icon={icon} className={css.icon} />
      </button>
    );

    return (
      <div className={css.square}>
        {button}
        <div className={css.caption}>{caption}</div>
      </div>
    );
  }
}

ButtonSquare.propTypes = {
  caption: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
};

ButtonSquare.defaultProps = {
  disabled: false,
  type: 'button',
};

export default ButtonSquare;
