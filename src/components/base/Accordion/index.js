import React, { PureComponent, PropTypes } from 'react';

import classNames from 'classnames';
import SVG from 'components/base/SVG';

import css from './style.scss';


class Accordion extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isOpened: props.isOpened,
    };
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange() {
    this.setState({ isOpened: !this.state.isOpened });
  }

  render() {
    const { caption, children, isLarge, isArrowRight } = this.props;
    const { isOpened } = this.state;
    const rootClass = classNames(css.accordion, { [css.opened]: isOpened, [css.large]: isLarge, [css.right]: isArrowRight });
    return (
      <div className={rootClass}>
        <div className={css.header} onClick={this.handleChange}>
          <SVG icon="iconCaretLeft" className={css.arrow} />
          <span className={css.caption}>{caption}</span>
        </div>
        <div className={css.body}>
          {children}
        </div>
      </div>
    );
  }
}

Accordion.defaultProps = {
  isOpened: false,
  isLarge: false,
  isArrowRight: false,
};

Accordion.propTypes = {
  caption: PropTypes.string,
  isOpened: PropTypes.bool,
  children: PropTypes.node,
  isLarge: PropTypes.bool,
  isArrowRight: PropTypes.bool,
};

export default Accordion;
