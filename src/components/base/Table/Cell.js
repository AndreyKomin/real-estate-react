/** @flow */
import React, { PropTypes } from 'react';

import css from './styles.scss';


type TCellProps = {|
  children: *,
  'data-value': *,
  'data-row-index': number,
  'data-index': number,
|};

export const Cell = ({ children, ...rest }: TCellProps) => (
  <td {...rest} className={css.td}>
    {children}
  </td>
);

Cell.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.string,
  ]),
};


export default Cell;
