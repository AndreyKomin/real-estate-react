/** @flow */
import { PropTypes } from 'react';
import RealColumn from './RealColumn';


const Column = () => null;

Column.propTypes = {
  ...RealColumn.propTypes,
  field: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string,
  ]),
  row: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]).isRequired,
  renderTooltip: PropTypes.func,
};

Column.defaultProps = {
  row: 0,
};

export default Column;
