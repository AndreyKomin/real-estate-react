import React, { Component } from 'react';
import ButtonImport from 'components/base/ButtonImport';


class Uploader extends Component {
  constructor(props) {
    super(props);
    this.handleImageChange = this.handleImageChange.bind(this);
  }

  handleImageChange(event) {
    event.preventDefault();
    const { upload } = this.props;
    const file = event.target.files[0];
    upload(file);
  }

  render() {
    return (
      <div className="previewComponent">
        <ButtonImport
          onChange={this.handleImageChange}
          name={this.props.name}
          width="80px"
          size={ButtonImport.size.small}
          className="browseLogo"
        >Browse</ButtonImport>
        {this.props.children}
      </div>);
  }
}


export default Uploader;
