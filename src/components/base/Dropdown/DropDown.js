/** @flow */
import React, { PureComponent, PropTypes } from 'react';

import SVG from 'components/base/SVG';
import classNames from 'classnames';
import css from './style.scss';
import dropdownable from './decorate';


type TOption = string | { label: string, value: * }

function getOptions(options) {
  return options.map((option: TOption) => {
    if (typeof option !== 'string') {
      const { value, label = value } = option;
      return <option key={`${label}${value}`} value={value}>{label}</option>;
    }
    return <option key={option} value={option}>{option}</option>;
  });
}

class Dropdown extends PureComponent<*, *, *> {
  render() {
    const { className, disabled, name, onChange, groups, options, input, ...rest } = this.props;
    const opts = groups ?
      groups.map((group) => {
        if (!group.options.length) return null;
        if (!group.label) return getOptions(group.options);

        return (
          <optgroup key={group.label} label={group.label}>
            {getOptions(group.options)}
          </optgroup>
        );
      }) :
      getOptions(options);

    const value = 'value' in this.props ? { value: this.props.value == null ? '' : this.props.value } : {};

    // NOTE: input attribute is needed for redux form, used in a couple places.
    return (
      <div className={classNames(className, css.dropdown)}>
        <SVG icon="iconCaretDown" className={css.iconCaretDown} />
        <select
          {...rest}
          name={name}
          className={css.control}
          onChange={onChange}
          disabled={disabled}
          {...value}
          {...input}
        >
          {opts}
        </select>
      </div>
    );
  }
}

Dropdown.propTypes = {
  className: PropTypes.string,
  name: PropTypes.string,
  options: PropTypes.instanceOf(Array),
};

Dropdown.defaultProps = {
  onChange: () => {},
  input: () => {},
};

export { dropdownable };
export default Dropdown;
