import React, { PureComponent } from 'react';

import * as outerClick from 'utils/DOM/onOuterClick';
import isFullyVisible from 'utils/DOM/isFullyVisible';


export default function decorate(Component) {
  const DropDown = class DropDown extends PureComponent {
    constructor(props) {
      super(props);
      this.onRef = this.onRef.bind(this);
      this.onClick = this.onClick.bind(this);
      this.open = this.open.bind(this);
      this.close = this.close.bind(this);
      this.tryClose = this.tryClose.bind(this);
      this.state = {
        isOpen: false,
        direction: 'bottom',
      };
      this.canOpen = true;
    }

    componentDidMount() {
      outerClick.addListener(this.tryClose);
    }

    componentDidUpdate() {
      if (this.state.isOpen) {
        if (this.element instanceof HTMLElement && this.state.direction === 'bottom') {
          requestAnimationFrame(() => {
            if (!isFullyVisible(this.element)) {
              this.setState({ direction: 'top' });
            }
          });
        }
      }
    }

    componentWillUnmount() {
      outerClick.removeListener(this.tryClose);
    }

    onRef(element) {
      this.element = element;

      if (this.props.onRef) {
        this.props.onRef(element);
      }
    }

    onClick(...rest) {
      this.open();
      if (this.props.onClick) {
        return this.props.onClick(...rest);
      }
      return undefined;
    }

    tryClose(event) {
      if (this.state.isOpen === false) return;
      if (outerClick.isOnElement(event, this.element, this.props.skipCatcherCheck)) return;
      this.close();
    }

    open() {
      if (!this.state.isOpen) {
        if (this.canOpen) {
          this.setState({ isOpen: true, direction: 'bottom' });
        }
      }
    }

    close() {
      if (this.state.isOpen) {
        this.canOpen = false;
        this.setState({ isOpen: false }, () => {
          setImmediate(() => {
            this.canOpen = true;
          });
        });
      }
    }

    render() {
      const { isOpen, direction } = this.state;

      return (<Component
        {...this.props}
        openDropdown={this.open}
        closeDropdown={this.close}
        onRef={this.onRef}
        onClick={this.onClick}
        isOpen={isOpen}
        direction={direction}
      />);
    }
  };

  DropDown.defaultProps = {
    skipCatcherCheck: false,
  };

  return DropDown;
}
