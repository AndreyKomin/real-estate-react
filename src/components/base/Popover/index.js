import React, { PureComponent, PropTypes } from 'react';
import classNames from 'classnames';

import css from './style.scss';


class Popover extends PureComponent {
  render() {
    const { children, isOpen, ...inlineStyles } = this.props;
    const popoverClass = classNames(
      css.popover,
      { [css.open]: isOpen },
    );
    return (
      <div
        className={popoverClass}
        style={inlineStyles}
      >{children}</div>
    );
  }
}

Popover.propTypes = {
  children: PropTypes.node.isRequired,
  isOpen: PropTypes.bool.isRequired,
};

Popover.defaultProps = {
  isOpen: true,
};

export default Popover;
