import React, { PureComponent, PropTypes } from 'react';
import { findDOMNode } from 'react-dom';
import DatePicker from 'react-datepicker';
import formatDate from 'utils/date/formatDate';
import * as onOuterClick from 'utils/DOM/onOuterClick';

import SVG from 'components/base/SVG';
import 'react-datepicker/dist/react-datepicker.css';
import css from './style.scss';


const DEFAULT_FORMAT = 'MMM DD, YYYY';


class InputDate extends PureComponent {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleRef = this.handleRef.bind(this);
  }

  componentWillUnmount() {
    if (this.dom) {
      onOuterClick.removeCatcher(this.dom);
    }
  }

  handleClick() {
    if (!this.picker) return;
    if (!this.picker.refs) return;
    if (!this.picker.refs.calendar) return;
    this.dom = findDOMNode(this.picker.refs.calendar);
    onOuterClick.addCatcher(this.dom);
  }

  handleChange(date, event) {
    if (this.props.onChange) {
      this.props.onChange({
        id: this.props.id,
        name: this.props.name,
        value: date,
      }, event);
    }

    // blur not working without setTimeout
    setTimeout(() => {
      this.picker.refs.input.blur();
    }, 0);
  }

  handleRef(picker) {
    this.picker = picker;
  }

  renderIcon() {
    if (this.props.noIcon) return null;

    return <SVG icon="iconCalendar" className={css.icon} />;
  }

  render() {
    const { value, name, onChange, ...rest } = this.props;
    const formatedValue = value ? formatDate(value, '', false, DEFAULT_FORMAT) : null;

    return (
      <div className={css.root} onClick={this.handleClick}>
        <DatePicker
          {...rest}
          ref={this.handleRef}
          onChange={this.handleChange}
          name={name}
          value={formatedValue}
          dateFormat={DEFAULT_FORMAT}
        />
        {this.renderIcon()}
      </div>
    );
  }
}

InputDate.propTypes = {
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  noIcon: PropTypes.bool,
};

export default InputDate;
