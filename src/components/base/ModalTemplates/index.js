import React, { PureComponent } from 'react';

import Modal from 'components/base/Modal';
import Button from 'components/base/Button';
import { getPopupRegistration, openPopup, closePopup } from 'app/PopupHolder';

import Template from './Template';
import css from './style.scss';


class ModalTemplates extends PureComponent {
  constructor(props) {
    super(props);
    this.selectTemplate = this.selectTemplate.bind(this);
  }

  selectTemplate(event) {
    const templateId = Number(event.currentTarget.nextSibling.value);
    this.props.update({ templateId });
    this.props.closePopup();
  }

  render() {
    const { template, postcard, closePopup } = this.props;
    const templatesForSelectedSize = template.filter(x => x.sizeId === Number(postcard.sizeId));
    const mappedTemplates = templatesForSelectedSize.map((templates, i) => (
      <Template
        index={i + 1}
        key={templates.id}
        template={templates}
        postcard={postcard}
        selectTemplate={this.selectTemplate}
      />
    ));

    if (!templatesForSelectedSize.length > 0 || !postcard.templateId) return null;

    return (
      <Modal
        {...this.props}
        isOpen
        uniqId="modalTemplates"
        caption="All Templates"
        width="700px"
        padding="25px 35px"
        onClose={this.props.closeParentPopUp}
      >
        <div className={css.radio}>
          {mappedTemplates}
        </div>
        <div className={css.buttons}>
          <Button
            kind={Button.kind.blue}
            size={Button.size.large}
            name="selectTemplate"
            onClick={closePopup}
            width="100px"
          >Close</Button>
        </div>
      </Modal>
    );
  }
}

ModalTemplates.registrationId = getPopupRegistration(ModalTemplates);
ModalTemplates.open = (...rest) => openPopup(ModalTemplates.registrationId, ...rest);
ModalTemplates.close = () => closePopup({ popup: ModalTemplates.registrationId });

export default ModalTemplates;
