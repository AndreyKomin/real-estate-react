import React, { PureComponent } from 'react';

import classNames from 'classnames';
import css from './style.scss';


class Templates extends PureComponent {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  selectClass() {
    const { template, postcard } = this.props;
    const selected = template.id === postcard.templateId;
    return classNames(css.itemRadio, { [css.selected]: selected });
  }

  handleClick(event) {
    event.stopPropagation();
    this.props.selectTemplate(event);
  }

  render() {
    const { template, index, postcard } = this.props;
    const thumbnailUrl = template ? template.thumbnail : '';
    const templateName = `Template ${index}`;

    if (!template.id || !postcard.templateId) return null;

    return (
      <div className={this.selectClass()}>
        <label htmlFor={template.id} >
          <div className={css.frame} onClick={this.handleClick}>
            <img src={thumbnailUrl} alt={templateName} />
          </div>
          <input
            type="radio"
            value={template.id}
            name="template"
            id={`size${template.id}`}
          />
        </label>
        <div className={css.caption}>{templateName}</div>
      </div>
    );
  }
}


export default Templates;
