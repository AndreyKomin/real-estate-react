/** @flow */
import Input from './Input';
import MinMaxInput from './MinMaxInput';


export default Input;
export {
  Input,
  MinMaxInput,
};
