import React, { Component } from 'react';

import loadable from 'components/hoc/loadable';
import ImageUpload from 'components/base/Uploader';
import SVG from 'components/base/SVG';

import css from './style.scss';


class Uploader extends Component {
  render() {
    return (
      <div>
        <div>
          <SVG icon="iconPicture" className={css.iconPicture} />
        </div>
        <div className={css.selectPhoto}>Select Photo (550 x 250 pixels)</div>
        <ImageUpload name="upload-photo" {...this.props} />
      </div>);
  }
}

export default loadable(Uploader);
