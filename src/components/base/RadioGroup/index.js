import React, { PureComponent, PropTypes } from 'react';

import css from './style.scss';
import RadioButton from './RadioButton';


class RadioGroup extends PureComponent {
  render() {
    const { name, options, checked, onChange, size } = this.props;
    const listRadio = options.map(option => (
      <RadioButton
        key={JSON.stringify(option)}
        name={name}
        checked={checked}
        option={option}
        onChange={onChange}
        size={size}
      />
    ));

    return (
      <div className={css.radioGroup}>
        {listRadio}
      </div>
    );
  }
}

RadioGroup.defaultProps = {
  size: 'large',
};

RadioGroup.propTypes = {
  name: PropTypes.string.isRequired,
  checked: PropTypes.string.isRequired,
  options: PropTypes.instanceOf(Array).isRequired,
  size: PropTypes.oneOf(['large', 'small']),
};

export default RadioGroup;
