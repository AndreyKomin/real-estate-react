export const beds = [
  {
    id: 1,
    name: 'Studio +',
    checked: true,
  },
  {
    id: 2,
    name: '1+',
    checked: false,
  },
  {
    id: 3,
    name: '2+',
    checked: false,
  },
  {
    id: 4,
    name: '3+',
    checked: false,
  },
  {
    id: 5,
    name: '4+',
    checked: false,
  },
];

export const bath = [
  {
    id: 21,
    name: 'Any',
    checked: true,
  },
  {
    id: 22,
    name: '1+',
    checked: false,
  },
  {
    id: 23,
    name: '2+',
    checked: false,
  },
  {
    id: 24,
    name: '3+',
    checked: false,
  },
  {
    id: 25,
    name: '4+',
    checked: false,
  },
  {
    id: 26,
    name: '5+',
    checked: false,
  },
];
