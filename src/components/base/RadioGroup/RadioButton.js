import React, { PureComponent, PropTypes } from 'react';
import classNames from 'classnames';

import css from './style.scss';


class RadioButtom extends PureComponent {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange() {
    if (this.props.onChange) {
      this.props.onChange({ name: this.props.name, value: this.props.option.value });
    }
  }
  render() {
    const { name, option, checked, size } = this.props;
    const checkedOption = option.value === checked;
    const radioClass = classNames([
      css.radioItem,
      css[size],
      { [css.active]: checkedOption },
    ]);

    return (
      <label htmlFor={option.id} className={radioClass} key={option.id}>
        <input
          onChange={this.handleChange}
          type="radio"
          id={option.id}
          value={option.value}
          name={name}
          className={css.radioInput}
          checked={checkedOption}
        />
        {option.label}
      </label>);
  }
}


RadioButtom.propTypes = {
  name: PropTypes.string.isRequired,
  checked: PropTypes.string.isRequired,
};

export default RadioButtom;
