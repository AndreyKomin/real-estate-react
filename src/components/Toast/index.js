import React, { PureComponent, PropTypes } from 'react';
import classNames from 'classnames';

import css from './style.scss';


class Toast extends PureComponent {
  render() {
    const { message, isVisible } = this.props;
    const toastClass = classNames(
      css.toast,
      { [css.visible]: isVisible },
    );

    return (
      <div className={toastClass}>
        <div className={css.center}>
          <div className={css.message}>{message}</div>
        </div>
      </div>
    );
  }
}

Toast.propTypes = {
  message: PropTypes.string.isRequired,
  isVisible: PropTypes.bool.isRequired,
};

Toast.defaultProps = {
  isVisible: false,
};

export default Toast;
