/** @flow */
const images: Array<string> = ['jpg', 'jpeg', 'png', 'gif', 'tif', 'tiff', 'bmp'];
const tables: Array<string> = ['csv', 'xls', 'xlsx'];

export const imageFormatList: string = images.map(format => `.${format}`).join(',');
export const tableFormatList: string = tables.map(format => `.${format}`).join(',');

export default {
  images,
  tables,
};
