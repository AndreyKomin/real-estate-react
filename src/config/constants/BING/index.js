/** @flow */

export default {
  API_KEY: process.env.BING_API_KEY,
  DEFAULT_MAP_TYPE: 'collinsBart',
  MAP_IMAGE_URL: 'http://dev.virtualearth.net/REST/v1/Imagery/Map/AerialWithLabels',
};
