/* eslint-disable no-undef */
const orgId = typeof OPBEAT_ORG_ID !== 'undefined' ? OPBEAT_ORG_ID : '3033556af31b4221b2313a6b2f2b518c';
const appId = typeof OPBEAT_APP_ID !== 'undefined' ? OPBEAT_APP_ID : '9615aa5b9f';
/* eslint-enable no-undef */

export default {
  orgId,
  appId,
};
