export default class ArmRate {
  constructor(effectiveDate, rate, type) {
    this.effectiveDate = effectiveDate;
    this.rate = rate;
    this.type = type;
  }
}
