const AdjustableRateMortgageRateCapTypes = {
  RATE: 'RATE',
  INCREASE: 'INCREASE',
};

export default AdjustableRateMortgageRateCapTypes;
