const AdjustableRateMortgageTypes = {
  STANDARD: 'STANDARD',
  HYBRID: 'HYBRID',
};

export default AdjustableRateMortgageTypes;
