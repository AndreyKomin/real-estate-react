const AdjustableRateMortgageAdjustmentTypes = {
  ABSOLUTE: 'ABSOLUTE',
  RELATIVE: 'RELATIVE',
};

export default AdjustableRateMortgageAdjustmentTypes;
