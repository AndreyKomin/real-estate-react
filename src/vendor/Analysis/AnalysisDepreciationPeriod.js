export default class AnalysisDepreciationPeriod {
  constructor(date, amount) {
    this.date = date;
    this.amount = amount;
  }
}
