const AnalysisRateIncreaseTypes = {
  PERCENTAGE: 'PERCENTAGE',
  DOLLAR_AMOUNT: 'DOLLAR_AMOUNT',
};

export default AnalysisRateIncreaseTypes;
