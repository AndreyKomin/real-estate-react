const AnalysisValueIncreaseTypes = {
  MARKET_VALUE_INCREASE_RATE: 'MARKET_VALUE_INCREASE_RATE',
  CAP_RATE: 'CAP_RATE',
  INFLATION_RATE: 'INFLATION_RATE',
  DOLLAR_AMOUNT: 'DOLLAR_AMOUNT',
};

export default AnalysisValueIncreaseTypes;
