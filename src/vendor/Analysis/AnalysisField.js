export default class AnalysisField {
  constructor(id, name, defaultAmount) {
    this.id = id;
    this.name = name;
    this.defaultAmount = defaultAmount;
  }
}
