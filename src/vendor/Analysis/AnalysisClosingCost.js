export default class AnalysisClosingCost {
  constructor(field, amount, paidBySeller) {
    this.field = field;
    this.amount = amount;
    this.paidBySeller = !!paidBySeller;
  }
}
