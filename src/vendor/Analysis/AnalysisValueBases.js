const AnalysisValueBases = {
  PURCHASE_PRICE: 'PURCHASE_PRICE',
  MARKET_VALUE: 'MARKET_VALUE',
  DOLLAR_AMOUNT: 'DOLLAR_AMOUNT',
};

export default AnalysisValueBases;
