const AnalysisFieldTypes = {
  CLOSING_COST: 'CLOSING_COST',
  IMPROVEMENT: 'IMPROVEMENT',
  INCOME: 'INCOME',
  EXPENSE: 'EXPENSE',
};

export default AnalysisFieldTypes;
