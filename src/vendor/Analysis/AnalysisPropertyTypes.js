const AnalysisPropertyTypes = {
  PERSONAL: 'PERSONAL',
  INVESTMENT: 'INVESTMENT',
};

export default AnalysisPropertyTypes;
