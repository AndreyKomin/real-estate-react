export default class AnalysisImprovement {
  constructor(field, amount) {
    this.field = field;
    this.amount = amount;
  }
}
