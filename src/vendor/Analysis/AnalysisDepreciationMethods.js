const AnalysisDepreciationMethods = {
  STRAIGHT_LINE: 'STRAIGHT_LINE',
  DOUBLE_DECLINING_BALANCE: 'DOUBLE_DECLINING_BALANCE',
};

export default AnalysisDepreciationMethods;
