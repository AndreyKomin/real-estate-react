import * as MortgageRest from './Mortgage';
import * as AnalysisRest from './Analysis';


const Equimine = { ...MortgageRest, ...AnalysisRest };
/* eslint-disable eqeqeq */
/* eslint-disable no-use-before-define */
/* eslint-disable no-unused-vars */

const m = new Equimine.Mortgage();

m.type = Equimine.MortgageTypes.FIXED_INTEREST;
m.compoundFrequency = Equimine.MortgageFrequencies.MONTHLY;
m.amount = 650000;
m.downPaymentAmount = 100000;
m.fixedInterestTerm = 30 * 12;
m.fixedInterestRate = 0.07;

m.process();

const a = new Equimine.Analysis();

function save() {
}


a.propertyType = Equimine.AnalysisPropertyTypes.PERSONAL;
a.foreclosure = false;
a.purchaseAmount = 750000;
a.marketValue = 733577;

a.defaultAmount = 9000;
a.accruedInterestAmount = 1000;
a.otherFeeAmount = 3000;

a.mortgages.push(m);

a.closingCostEntryMode = Equimine.AnalysisEntryModes.DETAILED;

a.closingCostAmount = 6000;
// a.closingCostAmount = 12865;

a.setClosingCostAmount(Equimine.AnalysisClosingCosts.POINTS_ID, 6500);
a.setClosingCostAmount(Equimine.AnalysisClosingCosts.ADMINISTRATION_FEE_ID, 336);
a.setClosingCostAmount(Equimine.AnalysisClosingCosts.APPLICATION_FEE_ID, 205);
a.setClosingCostAmount(Equimine.AnalysisClosingCosts.COMMITMENT_FEE_ID, 498);
a.setClosingCostAmount(Equimine.AnalysisClosingCosts.DOCUMENT_PREPARATION_ID, 194);
a.setClosingCostAmount(Equimine.AnalysisClosingCosts.FUNDING_FEE_ID, 228);
a.setClosingCostAmount(Equimine.AnalysisClosingCosts.MORTGAGE_BROKER_FEE_ID, 839);
a.setClosingCostAmount(Equimine.AnalysisClosingCosts.PROCESSING_ID, 320);
a.setClosingCostAmount(Equimine.AnalysisClosingCosts.TAX_SERVICE_ID, 73);
a.setClosingCostAmount(Equimine.AnalysisClosingCosts.UNDERWRITING_ID, 269);
a.setClosingCostAmount(Equimine.AnalysisClosingCosts.APPRAISAL_ID, 327);
a.setClosingCostAmount(Equimine.AnalysisClosingCosts.ATTORNEY_SETTLEMENT_FEE_ID, 445);
a.setClosingCostAmount(Equimine.AnalysisClosingCosts.CREDIT_REPORT_ID, 29);
a.setClosingCostAmount(Equimine.AnalysisClosingCosts.FLOOD_CERTIFICATION_ID, 17);
a.setClosingCostAmount(Equimine.AnalysisClosingCosts.PEST_OTHER_INSPECTION_ID, 68);
a.setClosingCostAmount(Equimine.AnalysisClosingCosts.SURVEY_ID, 174);
a.setClosingCostAmount(Equimine.AnalysisClosingCosts.TITLE_INSURANCE_ID, 605);
a.setClosingCostAmount(Equimine.AnalysisClosingCosts.TITLE_SEARCH_ID, 200);
a.setClosingCostAmount(Equimine.AnalysisClosingCosts.RECORDING_FEE_ID, 76);
a.setClosingCostAmount(Equimine.AnalysisClosingCosts.TAXES_ID, 1339);
a.setClosingCostAmount(Equimine.AnalysisClosingCosts.OTHER_FEES_COSTS_ID, 123);

// Improvements

a.improvementEntryMode = Equimine.AnalysisEntryModes.DETAILED;

a.improvementAmount = 15000;
// a.improvementAmount = 5950;

a.setImprovementAmount(Equimine.AnalysisImprovements.KITCHEN_ID, 10);
a.setImprovementAmount(Equimine.AnalysisImprovements.EXTERIOR_ID, 20);
a.setImprovementAmount(Equimine.AnalysisImprovements.BATHROOM_ID, 30);
a.setImprovementAmount(Equimine.AnalysisImprovements.WINDOW_ID, 40);
a.setImprovementAmount(Equimine.AnalysisImprovements.DOOR_ID, 50);
a.setImprovementAmount(Equimine.AnalysisImprovements.CARPET_ID, 60);
a.setImprovementAmount(Equimine.AnalysisImprovements.INTERIOR_PAINT_ID, 70);
a.setImprovementAmount(Equimine.AnalysisImprovements.EXTERIOR_PAINT_ID, 80);
a.setImprovementAmount(Equimine.AnalysisImprovements.FLOORING_ID, 90);
a.setImprovementAmount(Equimine.AnalysisImprovements.ROOF_ID, 100);
a.setImprovementAmount(Equimine.AnalysisImprovements.CHIMNEY_ID, 110);
a.setImprovementAmount(Equimine.AnalysisImprovements.GUTTER_ID, 120);
a.setImprovementAmount(Equimine.AnalysisImprovements.CLADDING_ID, 130);
a.setImprovementAmount(Equimine.AnalysisImprovements.DECK_ID, 140);
a.setImprovementAmount(Equimine.AnalysisImprovements.PORCH_ID, 150);
a.setImprovementAmount(Equimine.AnalysisImprovements.HEATING_ID, 160);
a.setImprovementAmount(Equimine.AnalysisImprovements.AC_HEAT_PUMP_ID, 170);
a.setImprovementAmount(Equimine.AnalysisImprovements.PLUMBING_ID, 180);
a.setImprovementAmount(Equimine.AnalysisImprovements.ELECTRICAL_ID, 190);
a.setImprovementAmount(Equimine.AnalysisImprovements.WALLS_ID, 200);
a.setImprovementAmount(Equimine.AnalysisImprovements.CEILING_ID, 210);
a.setImprovementAmount(Equimine.AnalysisImprovements.FIREPLACE_ID, 220);
a.setImprovementAmount(Equimine.AnalysisImprovements.SKYLIGHT_ID, 230);
a.setImprovementAmount(Equimine.AnalysisImprovements.FOUNDATION_ID, 240);
a.setImprovementAmount(Equimine.AnalysisImprovements.BASEMENT_ID, 250);
a.setImprovementAmount(Equimine.AnalysisImprovements.ATTIC_ID, 260);
a.setImprovementAmount(Equimine.AnalysisImprovements.ALARM_ID, 270);
a.setImprovementAmount(Equimine.AnalysisImprovements.DRIVEWAY_ID, 280);
a.setImprovementAmount(Equimine.AnalysisImprovements.GARAGE_ID, 290);
a.setImprovementAmount(Equimine.AnalysisImprovements.IRRIGATION_SPRINKLERS_ID, 300);
a.setImprovementAmount(Equimine.AnalysisImprovements.LANDSCAPING_ID, 310);
a.setImprovementAmount(Equimine.AnalysisImprovements.EQUIPMENT_ID, 320);
a.setImprovementAmount(Equimine.AnalysisImprovements.POOL_SPA_REPAIR_ID, 330);
a.setImprovementAmount(Equimine.AnalysisImprovements.OTHER_ID, 340);

// Income

a.incomeEntryMode = Equimine.AnalysisEntryModes.DETAILED;

a.incomeAmount = 20000;

a.setIncomeAmount(Equimine.AnalysisIncomes.LAUNDRY_ROOM_ID, 200);
a.setIncomeAmount(Equimine.AnalysisIncomes.VENDING_MACHINES_ID, 400);
a.setIncomeAmount(Equimine.AnalysisIncomes.LATE_CHARGES_ID, 500);
a.setIncomeAmount(Equimine.AnalysisIncomes.DEPOSIT_FORFEITURES_ID, 600);
a.setIncomeAmount(Equimine.AnalysisIncomes.SECTION_8_INCOME_ID, 700);
a.setIncomeAmount(Equimine.AnalysisIncomes.INCOME_FROM_INTEREST_ID, 800);
a.setIncomeAmount(Equimine.AnalysisIncomes.OTHER_INCOME_ID, 900);

// a.incomeIncreaseType = Equimine.AnalysisRateIncreaseTypes.DOLLAR_AMOUNT;
// a.incomeIncreaseAmount = 50;

a.incomeIncreaseType = Equimine.AnalysisRateIncreaseTypes.PERCENTAGE;
a.incomeIncreaseRate = 0.02;

a.propertyManagementRate = 0.04;
a.leasingCommissionRate = 0.01;

a.rentalUnits.push(new Equimine.AnalysisRentalUnit('name1', 1000, 1, 0.03, 2, 50));
a.rentalUnits.push(new Equimine.AnalysisRentalUnit('name2', 1400, 0.9, 0.04, 1, 100));
a.rentalUnits.push(new Equimine.AnalysisRentalUnit('name3', 2000, 0.85, 0.045, 4, 80));


// Expenses

a.expenseEntryMode = Equimine.AnalysisEntryModes.DETAILED;

a.expenseAmount = 15000;

a.setExpenseAmount(Equimine.AnalysisExpenses.PROPERTY_TAXES_ID, 7862);
a.setExpenseAmount(Equimine.AnalysisExpenses.ADVERTISING_ID, 100);
a.setExpenseAmount(Equimine.AnalysisExpenses.JANITORIAL_SERVICE_ID, 90);
a.setExpenseAmount(Equimine.AnalysisExpenses.LEGAL_ID, 80);
a.setExpenseAmount(Equimine.AnalysisExpenses.LICENSES_ID, 70);
a.setExpenseAmount(Equimine.AnalysisExpenses.REPAIRS_MAINTENANCE_ID, 60);
a.setExpenseAmount(Equimine.AnalysisExpenses.SUPPLIES_ID, 50);
a.setExpenseAmount(Equimine.AnalysisExpenses.ELECTRICITY_ID, 40);
a.setExpenseAmount(Equimine.AnalysisExpenses.GAS_ID, 30);
a.setExpenseAmount(Equimine.AnalysisExpenses.SEWER_WATER_ID, 20);
a.setExpenseAmount(Equimine.AnalysisExpenses.TELEPHONE_ID, 10);
a.setExpenseAmount(Equimine.AnalysisExpenses.OTHER_UTILITIES_ID, 200);
a.setExpenseAmount(Equimine.AnalysisExpenses.TRASH_ID, 190);
a.setExpenseAmount(Equimine.AnalysisExpenses.ACCOUNTING_ID, 180);
a.setExpenseAmount(Equimine.AnalysisExpenses.ASSOCIATION_FEES_ID, 170);
a.setExpenseAmount(Equimine.AnalysisExpenses.LANDSCAPING_ID, 160);
a.setExpenseAmount(Equimine.AnalysisExpenses.FIRE_INSURANCE_ID, 150);
a.setExpenseAmount(Equimine.AnalysisExpenses.FLOOD_INSURANCE_ID, 140);
a.setExpenseAmount(Equimine.AnalysisExpenses.MORTGAGE_INSURANCE_ID, 130);
a.setExpenseAmount(Equimine.AnalysisExpenses.LIABILITY_INSURANCE_ID, 120);
a.setExpenseAmount(Equimine.AnalysisExpenses.WORKMANS_COMP_INSURANCE_ID, 110);
a.setExpenseAmount(Equimine.AnalysisExpenses.PAYROLL_ID, 300);
a.setExpenseAmount(Equimine.AnalysisExpenses.POOL_SPA_SERVICE_ID, 290);
a.setExpenseAmount(Equimine.AnalysisExpenses.MISCELLANEOUS_ID, 280);

a.expensePerRentalUnit = true;

a.expenseIncreaseType = Equimine.AnalysisRateIncreaseTypes.DOLLAR_AMOUNT;
a.expenseIncreaseAmount = 10;

// a.expenseIncreaseType = Equimine.AnalysisRateIncreaseTypes.PERCENTAGE;
// a.expenseIncreaseRate = 0.02;

// Sale
a.capitalGainTaxRate = 0.032;
a.stateTaxRate = 0.0654;
a.federalTaxRate = 0.023;

a.saleCostIncreaseType = Equimine.AnalysisRateIncreaseTypes.DOLLAR_AMOUNT;
a.saleCostAmount = 5000;

a.firstPaymentDate = new Date(2017, 6, 30);
a.termType = Equimine.AnalysisTermTypes.TEN_YEAR;

// a.termType = Equimine.AnalysisTermTypes.MANUAL;
a.term = 6;
// a.periodTerm = 1;

a.propertyValueBasis = Equimine.AnalysisValueBases.PURCHASE_PRICE;
a.propertyValue = 900000;

a.marketValueIncreaseRate = 0.023;
a.capRate = 0.05;
a.inflationRate = 0.0123;
a.saleAmount = 1000000;
a.propertyValueIncreaseType = Equimine.AnalysisValueIncreaseTypes.MARKET_VALUE_INCREASE_RATE;

a.depreciationType = Equimine.AnalysisRateIncreaseTypes.DOLLAR_AMOUNT;
a.depreciableAmount = 50000;
a.depreciableYears = 27.5;
// a.depreciationMethod = Equimine.AnalysisDepreciationMethods.DOUBLE_DECLINING_BALANCE;

a.process();

// dumpTax(1);
// dumpSale(4);
dumpCashFlow(5);

save();


function dumpTax(pi) {
  Array.from(a.periods).forEach((p) => {
    if (p.seq == pi) {
      print('Seq', p.seq);
      print('Date', p.date);

      print('Gross Operating Income', p.grossOperatingIncomeAmount, true);
      print('Operating Expenses', p.operatingExpenseAmount, true);
      print('Net Operating Income', p.netOperatingIncomeAmount, true);

      print('Building Structure', p.propertyDepreciationAmount);
      print('Capital Improvements', p.improvementDepreciationAmount);
      print('Total Depreciation', p.depreciationTotal, true);

      print('First Mortgage Interest', p.getMortgageInterestAmount(0));
      print('Second Mortgage Interest', p.getMortgageInterestAmount(1));
      print('Total Mortgage Deductions', p.interestAmount, true);

      print('Taxable Income', p.taxableIncomeAmount, true);

      print('State Tax', p.stateTaxAmount);
      print('Federal Tax', p.federalTaxAmount);
      print('Total Tax', p.taxTotal, true);

      print('Up-Front Cash', a.upFrontCashAmount);
      print('Closing', a.upFrontExpenseClosingCostTotal);
      print('Improvements', a.improvementTotal);
      print('Down', a.downPaymentTotal);

      print('Net Operating Income', p.netOperatingIncomeAmount);
      print('Total Deductions', p.taxDeductionTotal);
      print('Taxable Income', p.taxableIncomeAmount);
      print('Total Tax', p.taxTotal);
      print('Cash Before Taxes', p.cashBeforeTaxAmount);
      print('Net Income', p.netIncomeTotal, true);
    }
  });
}

function dumpSale(pi) {
  Array.from(a.periods).forEach((p) => {
    if (p.seq == pi) {
      print('Seq', p.seq);
      print('Date', p.date);

      print('DepreciableAmount', a.depreciableAmount, true);
      print('Adjusted Tax Basis', p.adjustedTaxBasisAmount, true);

      print('Up-Front Cash', a.upFrontCashAmount);
      print('Closing', a.upFrontExpenseClosingCostTotal);
      print('Improvements', a.improvementTotal);
      print('Down', a.downPaymentTotal);
      print('First Mortgage Balance', p.getMortgageBalance(0));
      print('Second Mortgage Balance', p.getMortgageBalance(1));

      print('Property Sale Cost', p.saleCostAmount);
      print('Expense Total', p.saleExpenseAmount);
      print('Exchange Expense Total', p.exchangeExpenseAmount, true);

      print('Taxable Gain', p.taxableGainAmount);
      print('Depreciation Recovery Tax', p.depreciationRecoveryTaxAmount);
      print('Capital Gains Tax', p.capitalGainTaxAmount);
      print('Tax Liability', p.saleTaxTotal, true);

      print('Principal Payments', p.principalCumulativeAmount);
      print('Interest Payments', p.interestCumulativeAmount);
      print('Operating Expenses', p.operatingExpenseCumulativeAmount);
      print('Gross Operating Income', p.grossScheduledIncomeCumulativeAmount);
      print('Income Tax', p.taxCumulativeTotal);
      print('Net Income Profit/Loss', p.saleNetIncomeAmount, true);

      print('Sale Price', p.saleAmount);
      print('Adjusted Tax Basis', p.adjustedTaxBasisAmount);
      print('Taxable Gain', p.taxableGainAmount);
      print('Tax Liability', p.saleTaxTotal);
      print('Expense Total', p.saleExpenseAmount);
      print('Net Sale Profit/Loss', p.saleNetGainAmount);
      print('Net Income Profit/Loss', p.saleNetIncomeAmount);
      print('Net Profit/Loss', p.saleNetProfitAmount);
    }
  });
}

function dumpCashFlow(pi) {
  Array.from(a.periods).forEach((p) => {
    if (p.seq == pi) {
      print('Seq', p.seq);
      print('Date', p.date);

      print('Down', a.downPaymentTotal);
      print('Closing', a.closingCostTotal);
      print('Up-Front', a.additionalExpenseTotal);
      print('Improvements', a.improvementTotal);

      print('Rental Income', p.grossRentAmount);
      print('Section 8', p.getIncomeAmount(Equimine.AnalysisIncomes.SECTION_8_INCOME_ID));
      print('Leasing Commission', p.leasingCommissionAmount);
      print('Total Rental Income', p.rentalIncomeTotal, true);

      print('Vacancy Loss', p.vacancyLossAmount, true);

      print('Depoist Forfeitures', p.getIncomeAmount(Equimine.AnalysisIncomes.DEPOSIT_FORFEITURES_ID));
      print('Income From Interest', p.getIncomeAmount(Equimine.AnalysisIncomes.INCOME_FROM_INTEREST_ID));
      print('Late Charges', p.getIncomeAmount(Equimine.AnalysisIncomes.LATE_CHARGES_ID));
      print('Laundry Room', p.getIncomeAmount(Equimine.AnalysisIncomes.LAUNDRY_ROOM_ID));
      print('Vending Machines', p.getIncomeAmount(Equimine.AnalysisIncomes.VENDING_MACHINES_ID));
      print('Other Income', p.getIncomeAmount(Equimine.AnalysisIncomes.OTHER_INCOME_ID));
      print('Total Other Revenue', p.miscellaneousIncomeTotal, true);

      print('Advertising', p.getExpenseAmount(Equimine.AnalysisExpenses.ADVERTISING_ID));
      print('Legal', p.getExpenseAmount(Equimine.AnalysisExpenses.LEGAL_ID));
      print('Accounting', p.getExpenseAmount(Equimine.AnalysisExpenses.ACCOUNTING_ID));
      print('Licenses', p.getExpenseAmount(Equimine.AnalysisExpenses.LICENSES_ID));
      print('Payroll', p.getExpenseAmount(Equimine.AnalysisExpenses.PAYROLL_ID));
      print('Supplies', p.getExpenseAmount(Equimine.AnalysisExpenses.SUPPLIES_ID));
      print('Total Administrative Expense', p.administrativeExpenseTotal, true);

      print('Management Fees', p.propertyManagementAmount);
      print('Association Fees', p.getExpenseAmount(Equimine.AnalysisExpenses.ASSOCIATION_FEES_ID));
      print('Total Management Expense', p.propertyExpenseTotal, true);

      print('Electricity', p.getExpenseAmount(Equimine.AnalysisExpenses.ELECTRICITY_ID));
      print('Gas', p.getExpenseAmount(Equimine.AnalysisExpenses.GAS_ID));
      print('Sewer and Water', p.getExpenseAmount(Equimine.AnalysisExpenses.SEWER_WATER_ID));
      print('Other Utilities', p.getExpenseAmount(Equimine.AnalysisExpenses.OTHER_UTILITIES_ID));
      print('Total Utility Expense', p.utilityExpenseTotal, true);

      print('Fire', p.getExpenseAmount(Equimine.AnalysisExpenses.FIRE_INSURANCE_ID));
      print('Flood', p.getExpenseAmount(Equimine.AnalysisExpenses.FLOOD_INSURANCE_ID));
      print('Mortgage', p.getExpenseAmount(Equimine.AnalysisExpenses.MORTGAGE_INSURANCE_ID));
      print('Liability', p.getExpenseAmount(Equimine.AnalysisExpenses.LIABILITY_INSURANCE_ID));
      print("Workmen's Comp", p.getExpenseAmount(Equimine.AnalysisExpenses.WORKMANS_COMP_INSURANCE_ID));
      print('Total Insurance Expense', p.insuranceExpenseTotal, true);

      print('Telephone', p.getExpenseAmount(Equimine.AnalysisExpenses.TELEPHONE_ID));
      print('Janitorial', p.getExpenseAmount(Equimine.AnalysisExpenses.JANITORIAL_SERVICE_ID));
      print('Landscaping', p.getExpenseAmount(Equimine.AnalysisExpenses.LANDSCAPING_ID));
      print('Pool & Spa', p.getExpenseAmount(Equimine.AnalysisExpenses.POOL_SPA_SERVICE_ID));
      print('Property Taxes', p.getExpenseAmount(Equimine.AnalysisExpenses.PROPERTY_TAXES_ID));
      print('Repairs', p.getExpenseAmount(Equimine.AnalysisExpenses.REPAIRS_MAINTENANCE_ID));
      print('Trash', p.getExpenseAmount(Equimine.AnalysisExpenses.TRASH_ID));
      print('Misc', p.getExpenseAmount(Equimine.AnalysisExpenses.MISCELLANEOUS_ID));
      print('Total Maintenance Expense', p.maintenanceExpenseTotal, true);

      print('Gross Scheduled Income', p.grossScheduledIncomeAmount);
      print('Vacancy & Leasing Commission', p.vacancyLeasingCommissionTotal);
      print('Gross Operating Income', p.grossOperatingIncomeAmount);
      print('Operating Expenses', p.operatingExpenseAmount);
      print('Net Operating Income', p.netOperatingIncomeAmount);
      print('Cap Rate', p.capRate);
      print('Principal Payments', p.principalAmount);
      print('Interest Payments', p.interestAmount);
      print('Closing Costs & Additional Expenses', a.upFrontExpenseClosingCostTotal);

      print('Cash Flow Before Taxes', p.cashBeforeTaxAmount);
      print('Cash Flow Before Taxes (Cumulative)', p.cashBeforeTaxCumulativeAmount);
      print('Cash on Cash Return', p.cashOnCashReturnRate);
      print('Depreciation', p.depreciationTotal);
      print('Taxable Income / Loss', p.taxableIncomeAmount);
      print('Total Tax Cost / Credit', p.taxTotal);
      print('Cash Flow After Taxes', p.netIncomeTotal);
      print('Cash Flow After Taxes (Cumulative)', p.netIncomeCumulativeTotal);
    }
  });
}

function print(label, val, addBreak) {
  console.log(`${label}: ${val}${addBreak ? '\n' : ''}`);
}

/* eslint-enable no-use-before-define eqeqeq no-unused-vars */
