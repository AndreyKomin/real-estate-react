/** @flow */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { deferExecutor } from 'app/DeferredOnLocation';
import { getBrandCode } from 'utils/brand';

import PopupHolder from './PopupHolder';
import ErrorDisplay from './components/ErrorDisplay';

import AppLayout from './AppLayout';


class App extends Component {
  componentDidMount() {
    if (document.body) document.body.classList.add(`theme-${getBrandCode()}`);
  }

  shouldComponentUpdate(nextProps) {
    const { location: loc } = this.props;
    const { location: nextLoc } = nextProps;
    return loc.pathname !== nextLoc.pathname || loc.query !== nextLoc.query;
  }

  render() {
    return (
      <div>
        <PopupHolder />
        <ErrorDisplay />
        <AppLayout {...this.props} />
      </div>
    );
  }
}

export default connect()(deferExecutor(App));
