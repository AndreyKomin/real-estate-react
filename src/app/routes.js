import React from 'react';
import Route from 'react-router/lib/Route';
import IndexRedirect from 'react-router/lib/IndexRedirect';
import IndexRoute from 'react-router/lib/IndexRoute';

import App from './App';
import AuthGuard from './AuthGuard';
import DeferredOnLocation from './DeferredOnLocation';
import SvgHost from './SvgHost';
import SiteMap from './SiteMap';
import UrlStorage from './Property/Detail/UrlStorage';

import Login from './routes/Login';
import Sso from './routes/Sso';
import Account from './Account/Account';
import Search from './Search/Search';
import PropertyGroup from './Property/Group';
import PropertyGroupMap from './Property/Group/Map';
import PropertyDetail from './Property/Detail';
import PropertyGallery from './Property/Gallery';
import Analysis from './Property/Analysis';
import Contacts from './Contacts/Contacts';
import ContactsList from './Contacts/ContactsList';
import ContactEditor from './Contacts/ContactEditor';
import BirdEye from './components/BirdEye';
import StreetView from './components/StreetView';
import Loadable from './components/Loadable';
import Components from './components/Components';
import Image from './components/Image';
import FormExample from './components/FormExample';
import NoCampaigns from './Campaigns/NoCampaigns';
import Campaigns from './Campaigns';
import CampaignEditor from './Campaigns/CampaignEditor';
import Dashboard from './Campaigns/Dashboard';
import OnlineAd from './Campaigns/OnlineAd';
import AddActivity from './Campaigns/Dashboard/AddActivity';
import CampaignCreated from './Campaigns/Dashboard/CampaignCreated';
import Email from './Campaigns/Email';
import EmailSettings from './Campaigns/Email/Settings';
import Postcard from './Campaigns/Postcard';
import Voicemail from './Campaigns/Voicemail';
import Website from './Campaigns/Website';
import WebsiteSettings from './Campaigns/Website/Settings';


const getId = obj => (isNaN(obj) ? obj.get('id') : obj);
const getPath = (root, path) => `${root}${path ? `/${path}` : ''}`;
export const propertyGroupPath = path => getPath('/property/group', path);
export const contactGroupPath = path => getPath('/contact', path);
export const campaignPath = path => getPath('/campaign', path);
export const campaignWebsitePath = (campaign, path) => campaignPath(`${getId(campaign)}/website${path ? `/${path}` : ''}`);
export const campaignEmailPath = (campaign, path) => campaignPath(`${getId(campaign)}/email${path ? `/${path}` : ''}`);

const analysisRoutes = (
  <Route path="analysis" component={Analysis}>
    <Route path="purchase" />
    <Route path="mortgage" />
    <Route path="income" />
    <Route path="expenses" />
    <Route path="sale" />
    <Route path="finish" />
    <IndexRedirect to="purchase" />
  </Route>
);

const propertyRoutes = (
  <Route path=":id" component={UrlStorage}>
    <Route component={PropertyDetail}>
      <Route path="gallery" component={PropertyGallery} />
      {analysisRoutes}
      <IndexRoute />
    </Route>
  </Route>
);

const routes = (
  <Route path="/" component={SvgHost}>
    <Route component={DeferredOnLocation}>
      <Route path="login" component={Login} />
      <Route path="sso" component={Sso} />

      <Route component={AuthGuard}>
        <Route component={App}>

          <Route path="account" component={Account} />

          <Route path="search" component={UrlStorage}>
            <Route component={Search}>
              {propertyRoutes}
              <IndexRoute />
            </Route>
          </Route>

          <Route path="property" component={UrlStorage}>
            <Route path="group" component={PropertyGroup}>
              <Route path=":groupId">
                <Route path="map" component={PropertyGroupMap}>
                  {propertyRoutes}
                </Route>
                {propertyRoutes}
              </Route>
            </Route>
            <IndexRedirect to="group" />
          </Route>

          <Route path="campaign" component={Campaigns}>
            <Route path="new" component={CampaignEditor} />
            <Route path=":campaignId">
              <Route path="postcard" component={Postcard}>
                <Route path=":postcardId" />
              </Route>
              <Route path="advertisement" component={OnlineAd}>
                <Route path=":onlineAdId" />
              </Route>
              <Route path="email" component={Email}>
                <Route path="settings" component={EmailSettings} />
                <Route path=":emailId">
                  <Route path="settings" component={EmailSettings} />
                  <IndexRoute />
                </Route>
                <IndexRoute />
              </Route>
              <Route path="voicemail" component={Voicemail}>
                <Route path=":voicemailCampaignId" />
              </Route>
              <Route path="website" component={Website}>
                <Route path="settings" component={WebsiteSettings} />
                <IndexRoute />
              </Route>
              <Route component={Dashboard}>
                <Route path="created" component={CampaignCreated} />
                <Route path="activity" component={AddActivity} />
                <IndexRoute />
              </Route>
            </Route>
            <IndexRoute component={NoCampaigns} />
          </Route>

          <Route path="contact" component={Contacts}>
            <Route path=":groupId">
              <Route path="new" component={ContactEditor} />
              <Route path=":contactId" component={ContactEditor} />
              <IndexRoute component={ContactsList} />
            </Route>
            <IndexRedirect to="0" />
          </Route>

          <Route path="components">
            <Route path="image" component={Image} />
            <Route path="loadable" component={Loadable} />
            <Route path="birdeye" component={BirdEye} />
            <Route path="streetview" component={StreetView} />
            <Route path="form" component={FormExample} />
            <IndexRoute component={Components} />
          </Route>

          <Route path="site-map" component={SiteMap} />

          <IndexRedirect to="search" />
        </Route>
      </Route>
    </Route>
  </Route>
);

export default routes;
