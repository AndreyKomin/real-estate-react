/** @flow */
import { combineReducers } from 'redux-immutable';
import { reducer as popup } from './PopupHolder';


export default combineReducers({
  popup,
});
