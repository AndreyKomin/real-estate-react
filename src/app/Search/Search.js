/** @flow */
import React, { Component } from 'react';
import { shallowEqual } from 'recompose';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { is } from 'immutable';
import _ from 'underscore';

import { deferExecutor, deferQueue, deferRegistrator } from 'app/DeferredOnLocation';
import {
  clearSearch,
  selectSearchValidity,
  selectAvailableListingTypes,
  selectSearchAllowed,
  selectHasPositionField,
  changeSearchType,
  selectCurrentSearchPosition,
  getSearchQuery,
} from 'data/search';
import { selectCurrentSearch } from 'data/search/selectors';
import { clear as clearListing, triggerSelect, hideOverlay, changeOverlayType, loadListings, clearProperties, loadProperties } from 'data/listing';
import { selectAreaStatistics, selectStatisticQueryParams, selectStatisticTitle } from 'data/statistic/selectors';
import { clearStatistic, loadStatistics } from 'data/statistic';
import { clearMap, triggerPushpinPopover, removePlaceActivity } from 'data/map';

import {
  selectSearchParams,
  selectPropertiesParams,
  selectListingResultParams,
  selectOverlayParams,
  selectCityName,
  selectStateCode,
  selectSearchQueryParams,
  selectCurrentPage,
  selectLimit,
  selectRangeTo,
  selectRangeFrom,
  selectRangeEnabled,
} from './selectors';
import SearchLayout from './SearchLayout';


const KEYS_TO_EQUAL_CHECK = [
  'overlayParams',
  'searchParams',
  'propertiesParams',
  'listingResultParam',
  'query',
  'statisticQuery',
  'checkedPushpin',
  'page',
  'limit',
  'isValidSearch',
  'isAllowedSearch',
  'areaStatistics',
];
const KEYS_TO_SHALLOW_CHECK = [
  'params',
];
const KEYS_TO_DEEP_CHECK = [
  'location',
];


const getQuery = (plainQuery, hasPositionField) => {
  if (!plainQuery) return plainQuery;
  if (!hasPositionField) return plainQuery;
  return plainQuery.deleteIn(['searchParams', 'visibleLocation']);
};


class Search extends Component {
  constructor(props: *) {
    super(props);
    this.handleToggleClick = this.handleToggleClick.bind(this);
    this.loadStatistic = _.debounce(this.loadStatistic.bind(this), 10);
  }

  shouldComponentUpdate(nextProps: *) {
    return KEYS_TO_EQUAL_CHECK.some(key => nextProps[key] !== this.props[key])
      || KEYS_TO_SHALLOW_CHECK.some(key => !shallowEqual(nextProps[key], this.props[key]))
      || KEYS_TO_DEEP_CHECK.some(key => !Object.is(nextProps[key], this.props[key]));
  }

  componentDidUpdate(oldProps: {
    query: ?Object,
    hasPositionField: boolean,
    page: number,
    currentSearchPosition: ?Object,
  }) {
    if (!is(oldProps.currentSearchPosition, this.props.currentSearchPosition)) {
      const shapeDefinition = this.props.currentSearchPosition.get('shapeDefinition');
      if (shapeDefinition) {
        if (shapeDefinition.size) {
          const { searchValue, searchParams } = getSearchQuery(this.props.currentSearchPosition);

          this.props.loadListings({
            searchValue,
            searchParams,
            page: this.props.page,
            limit: this.props.limit,
          });
        } else {
          this.props.clearSearch();
        }
      }
    }

    if (this.props.isAllowedSearch) {
      const { hasPositionField, rangeFrom, rangeTo, rangeEnabled, page, limit, statisticQuery, loadListings } = this.props;
      const oldQuery = getQuery(oldProps.query, hasPositionField);
      const query = getQuery(this.props.query, hasPositionField);

      const rangeChanged = oldProps.rangeFrom !== rangeFrom || oldProps.rangeTo !== rangeTo;
      if (!is(oldQuery, query)) {
        this.trySearch();
      } else if (oldProps.page !== page || rangeChanged) {
        const { searchValue, searchParams } = this.props.query.toJS();

        loadListings({
          searchValue,
          searchParams,
          rangeFrom,
          rangeTo,
          rangeEnabled,
          page: rangeChanged ? 1 : page,
          limit,
        });
      } else if (oldProps.statisticQuery !== statisticQuery) {
        this.loadStatistic();
      }
    }
  }

  componentWillUnmount() {
    this.props.defer(() => {
      deferQueue([
        this.props.clearSearch,
        this.props.clearMap,
        this.props.clearListing,
        this.props.clearStatistic,
        // this.props.clearProperties, ** Causes race condition when loading property in new window, immediately clearing the property data after it loads.
        this.props.removePlaceActivity,
      ]);
    });
  }

  trySearch() {
    if (this.props.isAllowedSearch) {
      if (this.props.query) {
        const { searchValue, searchParams } = this.props.query.toJS();

        if (this.props.searchParams.isProperty) {
          this.loadStatistic();
          this.props.removePlaceActivity();
          this.props.loadProperties(searchParams);
        } else if (this.props.searchParams.isListing && (!this.props.availableListingTypes.size || searchParams.listingType.length > 0)) {
          this.loadStatistic();
          this.props.removePlaceActivity();
          this.props.loadListings({
            searchValue,
            searchParams,
            page: 1,
            limit: this.props.limit,
            rangeFrom: null,
            rangeTo: null,
            rangeEnabled: false,
          });
        }
      }
    }
  }

  /* :: loadStatistic: Function */
  loadStatistic() {
    if (!this.props.query) return;
    const { searchValue, searchParams } = this.props.query.toJS();
    const statisticParams = this.props.statisticQuery.toJS();

    this.props.loadStatistics({
      searchValue,
      searchParams,
      statisticParams,
    });
  }

  /* :: handleToggleClick: Function */
  handleToggleClick() {
    if (this.props.overlayParams.isTable) {
      this.props.hideOverlay();
    }
  }

  render() {
    const { children = null, ...rest } = this.props;
    return (
      <div>
        <Helmet title="Search" />
        <SearchLayout {...rest} onToggleClick={this.handleToggleClick} />
        {children}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    availableListingTypes: selectAvailableListingTypes(state),
    overlayParams: selectOverlayParams(state),
    searchParams: selectSearchParams(state),
    propertiesParams: selectPropertiesParams(state),
    listingResultParam: selectListingResultParams(state),
    query: selectSearchQueryParams(state),
    currentSearchPosition: selectCurrentSearchPosition(state),
    statisticQuery: selectStatisticQueryParams(state),
    checkedPushpin: state.getIn(['app', 'map', 'checkedPushpin'], null),
    page: selectCurrentPage(state),
    rangeFrom: selectRangeFrom(state),
    rangeTo: selectRangeTo(state),
    rangeEnabled: selectRangeEnabled(state),
    limit: selectLimit(state),
    isValidSearch: selectSearchValidity(state),
    isAllowedSearch: selectSearchAllowed(state),
    hasPositionField: selectHasPositionField(state),
    areaStatistics: selectAreaStatistics(state),
    cityName: selectCityName(state),
    stateCode: selectStateCode(state),
    statisticTitle: selectStatisticTitle(state),
    currentSearch: selectCurrentSearch(state),
  };
}

const mapActionToProps = {
  searchResultSelect: triggerSelect,
  changeOverlay: changeOverlayType,
  hideOverlay,
  loadListings,
  loadStatistics,
  loadProperties,
  changeSearchType,
  triggerPushpinPopover,
  tableTrigger: (event, data) => triggerPushpinPopover(data.get('id')),
  clearSearch,
  clearMap,
  clearListing,
  clearStatistic,
  clearProperties,
  removePlaceActivity,
};

export { Search };
export default connect(mapStateToProps, mapActionToProps)(deferExecutor(deferRegistrator(Search)));
