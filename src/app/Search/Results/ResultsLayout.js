/** @flow */
import React, { PureComponent } from 'react';
import Paginator from 'components/Paginator';
import loadable from 'components/hoc/loadable';

import Header from './Header';

import css from './style.scss';


class SearchResults extends PureComponent {
  getContent() {
    const { availableListingTypes: { size }, totalCount } = this.props;
    if (!totalCount) {
      return (
        <div className={css.container}>
          <h3>No Matching Results.</h3>
          <h4>{size ? 'Please try changing or expanding your search criteria.' : 'To search listings you must upgrade to the full version.'}</h4>
        </div>
      );
    }

    return this.props.children;
  }

  render() {
    const {
      selectedCount,
      allSelectedState,
      loadAllListings,
      totalCount,
      currentPage,
      totalPages,
      onChangePage,
      saveDefault,
      canSaveDefault,
      isPropertiesOrGroupLoading,
      addToList,
      selectAll,
      properties,
      allListingsSelected,
      currentSearch,
      onChangeRange,
      rangeFrom,
      rangeTo,
      rangeEnabled,
    } = this.props;

    const content = this.getContent();

    return (
      <div className={css.wrapper}>
        <Header
          selectedCount={selectedCount}
          allSelectedState={allSelectedState}
          loadAllListings={loadAllListings}
          propertyCount={totalCount}
          save={saveDefault}
          addToList={addToList}
          canSaveDefault={canSaveDefault}
          isLoading={isPropertiesOrGroupLoading}
          selectAll={selectAll}
          properties={properties}
          allSelected={allListingsSelected}
          currentSearch={currentSearch}
          onChangeRange={onChangeRange}
          rangeFrom={rangeFrom}
          rangeTo={rangeTo}
          rangeEnabled={rangeEnabled}
        />
        <div className={css.view}>
          {content}
        </div>
        <Paginator current={currentPage} total={totalPages} onChangePage={onChangePage} />
      </div>
    );
  }
}

SearchResults.propTypes = {};


export default loadable(SearchResults);
