import React, { PureComponent } from 'react';

import Modal from 'components/base/Modal';
import map from './map-modal.jpg';
import css from './style.scss';


class ModalSearchSaveInfo extends PureComponent {
  render() {
    const header = (
      <div className={css.header}>
        <div className={css.headerSearch}>
          <div className={css.headerTitle}>Home Screen</div>
          <div className={css.tinyCaption}>Add a saved search to the home screen for quick and easy access!</div>
        </div>
      </div>);
    return (
      <Modal {...this.props} isCloseButton padding="50px" uniqId="modalSearchSaveInfo" caption={header} width="815px">
        <img className={css.map} src={map} alt="" />
      </Modal>
    );
  }
}

export default ModalSearchSaveInfo;
