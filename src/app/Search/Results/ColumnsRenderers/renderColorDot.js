/** @flow */
import React from 'react';
import { Map } from 'immutable';

import type { ListingType } from 'data/listing/constants';
import { Color, Fields } from 'data/listing/constants';

import ColorDot from 'components/ColorDot';
import css from './style.scss';


export default function renderColorDot(
  counter: *,
  indexes: { row: number, column: number },
  data: Map<string, *>,
  _: Function,
  extraProps: Object,
) {
  const type = ((data.get(Fields.TYPE): any): ListingType);
  const colorDot = extraProps.colorDot ? <ColorDot color={Color[type]} isBig={false}>{counter}</ColorDot> : null;

  return (
    <div className={css.flex}>
      {colorDot}
    </div>
  );
}
