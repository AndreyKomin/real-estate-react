/** @flow */
import { Map } from 'immutable';


export default function renderUnitCell(address: Map<string, *>) {
  return address.get('unitNumber', '');
}
