/** @flow */
import formatDate from 'utils/date/formatDate';


export default function renderDateCell(date: string | Date | number | void) {
  return formatDate(date, '-');
}
