/** @flow */
export default function renderDefault(value: *) {
  if (value === null) return '-';
  if (value === undefined) return '-';
  return String(value);
}
