/** @flow */
import numberToPrice from 'utils/currency/numberToPrice';


export default function renderCurrencyCell(value: *) {
  return numberToPrice(value, '-');
}
