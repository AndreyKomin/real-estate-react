/** @flow */
import { Name as ListingType } from 'data/listing/constants';


export default function renderTypeCell(value: *) {
  return ListingType[value];
}
