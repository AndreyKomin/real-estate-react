/** @flow */
import React, { PureComponent } from 'react';
import { Map } from 'immutable';

import type { ListingType } from 'data/listing/constants';
import { Color, Fields, Active } from 'data/listing/constants';

import ColorDot from 'components/ColorDot';
import Checkbox from 'components/base/Checkbox';
import css from './style.scss';


class CheckboxCell extends PureComponent {
  render() {
    const { checked, counter, onSelect, changeValue } = this.props;

    return (
      <Checkbox
        changeValue={changeValue}
        onChange={onSelect}
        name={`SearchResultsItem-${counter}`}
        id={`SearchResultsItem-${counter}`}
        checked={checked}
      />
    );
  }
}

export default function renderCheckboxCell(
  counter: *,
  indexes: { row: number, column: number },
  data: Map<string, *>,
  _: Function,
  extraProps: Object,
) {
  const selectId = data.get('id');
  const checked = data.get('selected', false);

  const { checkedPushpin } = extraProps;
  const type = checkedPushpin === selectId ? Active : ((data.get(Fields.TYPE): any): ListingType);
  const colorDot = extraProps.colorDot ? <ColorDot color={Color[type]} isBig={false}>{counter}</ColorDot> : null;

  return (
    <div className={css.flex}>
      <CheckboxCell
        {...extraProps}
        counter={counter}
        checked={checked}
        changeValue={selectId}
      />
      {colorDot}
    </div>
  );
}
