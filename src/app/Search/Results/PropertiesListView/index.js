/** @flow */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { changePropertySorting } from 'data/listing';
import { selectSorting } from 'data/listing/selectors';

import Layout from './Layout';


class PropertiesListView extends PureComponent<*, *, *> {
  constructor(props) {
    super(props);
    this.sortTable = this.sortTable.bind(this);
  }

  /* :: sortTable: Function */
  sortTable(data) {
    this.props.changePropertySorting(data);
  }

  render() {
    return (
      <Layout
        {...this.props}
        onColumnSort={this.sortTable}
        sorting={this.props.sorting.get('properties')}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    sorting: selectSorting(state),
  };
}

const mapActionToProps = {
  changePropertySorting,
};


export default connect(mapStateToProps, mapActionToProps)(PropertiesListView);
