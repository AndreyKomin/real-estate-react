/** @flow */
import React, { PureComponent, PropTypes } from 'react';
import { createSelector } from 'reselect';
import { List } from 'immutable';

import { Tables } from 'data/listing/constants';

import SVG from 'components/base/SVG';
import Table from 'components/base/Table';

import ColumnsRenderers from '../ColumnsRenderers';

import css from '../style.scss';


const selectExtraProps = createSelector(
  func => func,
  () => ({ colorDot: true }),
);

const selectCheckedPushpin = createSelector(
  pin => pin,
  checkedPushpin => ({ checkedPushpin }),
);

class PropertiesListView extends PureComponent<*, *, *> {
  componentDidMount() {
    this.props.onColumnSort({ fieldName: 'unitNumber', tableName: 'properties', ascending: true });
  }

  render() {
    const { checkedPushpin, data, onRowClick, sorting, onColumnSort } = this.props;
    const tableType = 'R';
    const sortFields = sorting ? sorting.get('fields') : null;
    const caretIconName = sorting && sorting.get('ascending') ? 'iconCaretUp' : 'iconCaretDown';
    const caretIcon = (<SVG icon={caretIconName} />);
    const counterColumn = ColumnsRenderers.COUNTER(selectExtraProps());
    const columns = Tables[tableType].map(column =>
      ColumnsRenderers[column](selectCheckedPushpin(checkedPushpin), caretIcon),
    );

    return (
      <div className={css.content}>
        <Table
          tableName={'properties'}
          data={data}
          keyField="id"
          className="type_11"
          sortFields={sortFields}
          onColumnSort={onColumnSort}
          isSortable
          onRowClick={onRowClick}
          isHoverable
        >
          {counterColumn}
          {columns}
        </Table>
      </div>
    );
  }
}

PropertiesListView.propTypes = {
  data: PropTypes.instanceOf(List).isRequired,
  onRowClick: PropTypes.func,
};

export default PropertiesListView;
