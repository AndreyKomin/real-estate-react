/** @flow */
import React, { PureComponent, PropTypes } from 'react';
import { List } from 'immutable';
import { createSelector } from 'reselect';

import { Tables, Type, TypeCustom, Default } from 'data/listing/constants';
import SVG from 'components/base/SVG';

import Table from 'components/base/Table';

import ColumnsRenderers from '../ColumnsRenderers';
import css from '../style.scss';


const selectExtraProps = createSelector(
  func => func,
  onSelect => ({ onSelect, colorDot: true }),
);

const selectCheckedPushpin = createSelector(
  pin => pin,
  checkedPushpin => ({ checkedPushpin }),
);

class ListView extends PureComponent<*, *, *> {
  render() {
    const { checkedPushpin, data, onRowClick, onSelect, type, onColumnSort, sorting } = this.props;
    const sortFields = sorting ? sorting.fields : '';
    const caretIconName = sorting && sorting.ascending ? 'iconCaretUp' : 'iconCaretDown';
    const caretIcon = (<SVG icon={caretIconName} />);
    const tableType = Type[type] || TypeCustom;
    const table = Tables[tableType] || Tables[Default];
    const columns = table.map(
      column => ColumnsRenderers[column](selectCheckedPushpin(checkedPushpin), caretIcon),
    );
    const checkboxColumn = ColumnsRenderers.CHECKBOX(selectExtraProps(onSelect));

    return (
      <div className={css.content}>
        <Table
          data={data}
          keyField="id"
          className="type_11"
          onRowClick={onRowClick}
          onColumnSort={onColumnSort}
          isHoverable
          isSortable
          sortFields={sortFields}
        >
          {checkboxColumn}
          {columns}
        </Table>
      </div>
    );
  }
}


ListView.propTypes = {
  data: PropTypes.instanceOf(List).isRequired,
  onSelect: PropTypes.func.isRequired,
  onRowClick: PropTypes.func,
};

ListView.defaultProps = {
  type: TypeCustom,
};

export default ListView;
