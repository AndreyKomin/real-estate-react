/** @flow */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { changePage, changeRange, markAllListingsAsSelected, clearSelectedListings, loadAllListings } from 'data/listing';
import { saveProperty } from 'data/property';
import { selectCurrentSearch, selectAvailableListingTypes } from 'data/search/selectors';

import Confirm from 'app/components/Confirm';

import {
  selectCurrentPage,
  selectTotalPageCount,
  selectTotalCount,
  selectSelectedCount,
  selectAllListingsSelected,
  selectRangeFrom,
  selectRangeTo,
  selectRangeEnabled,
  selectIsLoading,
  selectIsPropertiesOrGroupLoading,
  selectIsError,
  selectCanSaveDefault,
  selectAllSelectedState,
  selectSearchQueryParams,
} from '../selectors';

import ResultsLayout from './ResultsLayout';


class Results extends PureComponent<*, *, *> {
  constructor(props: Object) {
    super(props);
    this.handleChangePage = this.handleChangePage.bind(this);
    this.handleChangeRange = this.handleChangeRange.bind(this);
    this.handleSelectAll = this.handleSelectAll.bind(this);
    this.loadPropertiesBeforeSave = this.loadPropertiesBeforeSave.bind(this);
  }

  /* :: handleChangePage: Function */
  handleChangePage(page: number) {
    this.props.changePage(page);
  }

  /* :: handleChangeRange: Function */
  handleChangeRange(event: Object) {
    const target = event.target || event;
    this.props.changeRange({ [target.name]: Number(target.value) || null });
  }

  /* :: loadPropertiesBeforeSave: Function */
  loadPropertiesBeforeSave(limit: number, callbacks: Object) {
    const { query, loadAllListings, rangeFrom, selectedCount } = this.props;
    const { searchValue, searchParams } = query.toJS();
    loadAllListings({
      searchValue,
      searchParams,
    }, rangeFrom, Math.min(limit, selectedCount), callbacks);
  }

  /* :: handleSelectAll: Function */
  handleSelectAll() {
    const { allListingsSelected, markAllListingsAsSelected, clearSelectedListings } = this.props;
    if (allListingsSelected) {
      clearSelectedListings();
      return;
    }

    markAllListingsAsSelected();
  }

  render() {
    const { children = null, ...rest } = this.props;
    return (
      <ResultsLayout
        {...rest}
        onChangePage={this.handleChangePage}
        onChangeRange={this.handleChangeRange}
        selectAll={this.handleSelectAll}
        loadAllListings={this.loadPropertiesBeforeSave}
      >
        {children}
      </ResultsLayout>
    );
  }
}

function mapStateToProps(state) {
  return {
    availableListingTypes: selectAvailableListingTypes(state),
    selectedCount: selectSelectedCount(state),
    currentPage: selectCurrentPage(state),
    totalCount: selectTotalCount(state),
    totalPages: selectTotalPageCount(state),
    rangeFrom: selectRangeFrom(state),
    rangeTo: selectRangeTo(state),
    rangeEnabled: selectRangeEnabled(state),
    isLoading: selectIsLoading(state),
    isError: selectIsError(state),
    canSaveDefault: selectCanSaveDefault(state),
    isPropertiesOrGroupLoading: selectIsPropertiesOrGroupLoading(state),
    allListingsSelected: selectAllListingsSelected(state),
    allSelectedState: selectAllSelectedState(state),
    query: selectSearchQueryParams(state),
    currentSearch: selectCurrentSearch(state),
  };
}

const mapActionToProps = {
  changePage,
  changeRange,
  saveGroup: saveProperty,
  markAllListingsAsSelected,
  clearSelectedListings,
  loadAllListings,
  confirmAction: Confirm.open,
};

export { Results };
export default connect(mapStateToProps, mapActionToProps)(Results);
