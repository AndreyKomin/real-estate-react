import React, { PureComponent, PropTypes } from 'react';
import { List } from 'immutable';
import { createSelector } from 'reselect';

import SVG from 'components/base/SVG';
import Table from 'components/base/Table';

import ColumnsRenderers from '../ColumnsRenderers';


const selectCheckedPushpin = createSelector(
  pin => pin,
  checkedPushpin => ({ checkedPushpin, colorDot: true }),
);


class DetailTable extends PureComponent {
  render() {
    const { data, fields, tableName, checkedPushpin, sorting, onColumnSort } = this.props;
    const sortFields = sorting ? sorting.get('fields') : null;
    const caretIconName = sorting && sorting.get('ascending') ? 'iconCaretUp' : 'iconCaretDown';
    const caretIcon = (<SVG icon={caretIconName} />);
    const columns = fields.map(field => (
      ColumnsRenderers[field](selectCheckedPushpin(checkedPushpin), caretIcon)
    ));
    const newData = data.map((property, i) => (property.set('resultIndex', i + 1)));

    return (
      <div>
        <Table
          tableName={tableName}
          data={newData}
          keyField="id"
          className="type_11"
          sortFields={sortFields}
          onColumnSort={onColumnSort}
          isSortable
          isHoverable
        >
          {columns}
        </Table>
      </div>
    );
  }
}

DetailTable.propTypes = {
  data: PropTypes.instanceOf(List).isRequired,
  fields: PropTypes.instanceOf(List).isRequired,
};

export default DetailTable;

