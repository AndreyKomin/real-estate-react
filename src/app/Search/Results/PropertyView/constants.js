import { fromJS } from 'immutable';


export default fromJS({
  neighbors: [
    'COUNTER',
    'ADDRESS',
    'BEDROOMS',
    'BATHROOMS',
    'ESTIMATE_VALUE',
    'SALE_PRICE',
    'SALE_DATE',
  ],
  nearbyMlsListings: [
    'COUNTER',
    'ADDRESS',
    'BEDROOMS',
    'BATHROOMS',
    'LISTING_PRICE',
    'LISTING_DATE',
    'STATUS',
  ],
  nearbyPreForeclosures: [
    'COUNTER',
    'DOCUMENT_TYPE',
    'ADDRESS',
    'BEDROOMS',
    'BATHROOMS',
    'LISTING_AMOUNT',
    'UPDATE_DATE',
  ],
  nearbyForeclosures: [
    'COUNTER',
    'DOCUMENT_TYPE',
    'ADDRESS',
    'BEDROOMS',
    'BATHROOMS',
    'LISTING_AMOUNT',
    'UPDATE_DATE',
  ],
});
