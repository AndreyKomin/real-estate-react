import React, { PureComponent, PropTypes } from 'react';
import { Map, List } from 'immutable';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import CustomTabs from 'components/CustomTabs';

import DetailTable from './DetailTable';
import fields from './constants';
import css from './style.scss';


const emptyList = List();
const tabNames = [
  'neighbors',
  'nearbyMlsListings',
  'nearbyPreForeclosures',
  'nearbyForeclosures',
];


class DetailTabs extends PureComponent {
  constructor(props) {
    super(props);
    this.handleSelect = this.handleSelect.bind(this);
  }

  handleSelect(index) {
    this.props.setActiveTab(index, tabNames[index]);
  }

  render() {
    const { data, onColumnSort, sorting } = this.props;
    const neighbors = data.get('neighbors', emptyList);
    const nearbyMlsListings = data.get('nearbyMlsListings', emptyList);
    const nearbyPreForeclosures = data.get('nearbyPreForeclosures', emptyList);
    const nearbyForeclosures = data.get('nearbyForeclosures', emptyList);

    return (
      <div>
        <div className={css.wrapTitle}>
          <h3 className={css.title}>Nearby Listings</h3>
        </div>
        <div className={css.scrollTable}>
          <CustomTabs kind="line">
            <Tabs selectedIndex={this.props.activeTabIndex} onSelect={this.handleSelect}>
              <TabList>
                <Tab>Neighbors ({neighbors.size})</Tab>
                <Tab>MLS ({nearbyMlsListings.size})</Tab>
                <Tab>Pre-Foreclosure ({nearbyPreForeclosures.size})</Tab>
                <Tab>Foreclosure ({nearbyForeclosures.size})</Tab>
              </TabList>
              <TabPanel>
                <DetailTable
                  tableName="neighbors"
                  data={neighbors}
                  fields={fields.get('neighbors')}
                  sorting={sorting.get('neighbors')}
                  onColumnSort={onColumnSort}
                />
              </TabPanel>
              <TabPanel>
                <DetailTable
                  tableName="nearbyMlsListings"
                  data={nearbyMlsListings}
                  fields={fields.get('nearbyMlsListings')}
                  sorting={sorting.get('nearbyMlsListings')}
                  onColumnSort={onColumnSort}
                />
              </TabPanel>
              <TabPanel>
                <DetailTable
                  tableName="nearbyPreForeclosures"
                  data={nearbyPreForeclosures}
                  fields={fields.get('nearbyPreForeclosures')}
                  sorting={sorting.get('nearbyPreForeclosures')}
                  onColumnSort={onColumnSort}
                />
              </TabPanel>
              <TabPanel>
                <DetailTable
                  tableName="nearbyForeclosures"
                  data={nearbyForeclosures}
                  fields={fields.get('nearbyForeclosures')}
                  sorting={sorting.get('nearbyForeclosures')}
                  onColumnSort={onColumnSort}
                />
              </TabPanel>
            </Tabs>
          </CustomTabs>
        </div>
      </div>
    );
  }
}

DetailTabs.propTypes = {
  data: PropTypes.instanceOf(Map).isRequired,
  setActiveTab: PropTypes.func.isRequired,
};


export default DetailTabs;

