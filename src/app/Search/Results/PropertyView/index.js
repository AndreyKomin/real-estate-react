import React, { PureComponent, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Map } from 'immutable';

import { changePropertySorting, setActiveTab } from 'data/listing';
import { selectSorting, selectIsLoading, selectActiveTabIndex, selectSearchResultBingMapImageUrl } from 'data/listing/selectors';
import { selectPermissions } from 'data/user/selectors';
import { selectLoading, saveProperty, selectFavorites } from 'data/property';

import loadable from 'components/hoc/loadable';
import Confirm from 'app/components/Confirm';
import AddToGroup from 'app/components/AddToGroup';

import Info from './Info';
import DetailTabs from './DetailTabs';

import css from './style.scss';


class PropertyView extends PureComponent {
  constructor(props) {
    super(props);
    this.sortTable = this.sortTable.bind(this);
    this.saveProperty = this.saveProperty.bind(this);

    this.state = { savedPropertyId: null };
  }

  sortTable(data) {
    this.props.changePropertySorting(data);
  }

  saveProperty() {
    const { saveProperty, openAddToGroup, favorites } = this.props;

    const data = this.props.results.get('data');
    const properties = data.getIn(['properties', 0]);
    const propertyId = properties.get('id');
    const obj = this;

    openAddToGroup({
      selectLoading,
      groups: favorites,
      size: 1,
      save: onComplete => saveProperty(propertyId, null, ({ response: { quantity, property: { savedPropertyId } } }) => { onComplete(quantity); obj.setState({ savedPropertyId }); }),
      add: (name, onComplete) => saveProperty(propertyId, { name }, ({ response: { quantity } }) => onComplete(quantity)),
    });
  }

  render() {
    const data = this.props.results.get('data');
    const properties = data.getIn(['properties', 0]);

    if (!properties) return null;

    return (
      <div>
        <Info
          properties={properties}
          bingMapImageUrl={this.props.bingMapImageUrl}
          savedPropertyId={properties.get('savedPropertyId') || this.state.savedPropertyId}
          isLoading={this.props.isLoading}
          saveProperty={this.saveProperty}
        />
        <hr className={css.divider} />
        <DetailTabs
          data={data}
          onSelect={this.props.onSelect}
          onColumnSort={this.sortTable}
          sorting={this.props.sorting}
          setActiveTab={this.props.setActiveTab}
          activeTabIndex={this.props.activeTabIndex}
        />
      </div>
    );
  }
}


PropertyView.propTypes = {
  results: PropTypes.instanceOf(Map).isRequired,
  onSelect: PropTypes.func,
};


function mapStateToProps(state, ownProps) {
  const propertyId = ownProps.results.getIn(['data', 'properties', 0, 'id']);
  const isLoading = selectIsLoading(state) || selectLoading(state);

  return {
    sorting: selectSorting(state),
    propertyId,
    activeTabIndex: selectActiveTabIndex(state),
    bingMapImageUrl: selectSearchResultBingMapImageUrl(state, 0),
    isLoading,
    favorites: selectFavorites(state),
    permissions: selectPermissions(state),
  };
}

const mapActionToProps = {
  changePropertySorting,
  setActiveTab,
  saveProperty,
  confirmSaveProperty: Confirm.open,
  openAddToGroup: AddToGroup.open,
};

export default connect(mapStateToProps, mapActionToProps)(loadable(PropertyView));
