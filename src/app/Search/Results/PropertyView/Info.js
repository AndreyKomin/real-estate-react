import React, { PureComponent, PropTypes } from 'react';
import classNames from 'classnames';
import { Map } from 'immutable';
import { Link } from 'react-router';

import numberToPrice from 'utils/currency/numberToPrice';
import AnalysisButton from 'app/components/AnalysisButton';
import Image from 'components/base/Image';
import Button, { ButtonLink } from 'components/base/Button';
import Label from 'components/Label';
import Row from './Row';

import css from './style.scss';


const landUseClassName = classNames(css.apartmentValue, css.spacedValue);
const wideButtonStyles = css.wideButton;


class Info extends PureComponent {
  render() {
    const { properties, bingMapImageUrl, savedPropertyId, loading, saveProperty } = this.props;

    const {
      id,
      address,
      imageUrls,
      imageUrl = (imageUrls || '').split(',')[0],
      yearBuilt,
      estimatedValue,
      estimatedEquity,
      compSaleAmount,
      rentAmount,
      ownership,
      bathrooms,
      bedrooms,
      squareFeet,
      occupancy,
      distressed,
      involuntaryLienAmount,
      onMarket,
      landUse,
    } = properties.toJS();

    const {
      streetAddress,
      cityName,
      stateCode,
      zip,
    } = address;

    const estimatedEquityString = numberToPrice(estimatedEquity, '-');
    const compSaleAmountString = numberToPrice(compSaleAmount, '-');
    const rentAmountString = numberToPrice(rentAmount, '-');
    const estimatedValueString = numberToPrice(estimatedValue, '-');
    const lienAmountString = numberToPrice(involuntaryLienAmount || 0, '-');

    const linkToDetails = `/search/${id}`;
    const estimatedEquityGraph = estimatedEquity ? (<div className={css.tile}>
      <div className={css.wrapper}>
        <span className={css.value}>{estimatedEquityString}</span>
        <span className={css.title}>Est. Equity</span>
      </div>
    </div>) : null;
    const compSaleAmountGraph = compSaleAmount ? (<div className={css.tile}>
      <div className={css.wrapper}>
        <span className={css.value}>{compSaleAmountString}</span>
        <span className={css.title}>Avg. Comps</span>
      </div>
    </div>) : null;
    const rentAmountGraph = rentAmount ? (<div className={css.tile}>
      <div className={css.wrapper}>
        <span className={css.value}>{rentAmountString}</span>
        <span className={css.title}>Est. Rent</span>
      </div>
    </div>) : null;

    return (
      <div className={css.wrap}>
        <h3 className={css.title}>
          <Link className={css.title} to={linkToDetails}>{`${streetAddress}, ${cityName} ${stateCode} ${zip}`}</Link>
        </h3>
        <div className={css.widgetInfo}>
          <div className={css.left}>
            <div className={css.imagePreview}>
              <Link to={linkToDetails}>
                <Image src={imageUrl} alt={streetAddress} placeholder={bingMapImageUrl} />
              </Link>
            </div>
          </div>
          <div className={css.right}>
            <Row toCheck={[estimatedValue]}>
              <div className={css.rowInfo}>
                <div className={css.label}>Estimated Value:</div>
                <div className={css.value}>{estimatedValueString}</div>
              </div>
            </Row>
            <Row>
              <div className={css.rowInfo}>
                <div className={css.label}>Status:</div>
                <div className={css.value}>{`${onMarket ? 'On Market' : 'Off Market'}`}</div>
              </div>
            </Row>
            <Row>
              <div className={css.rowInfo}>
                <div className={css.label}>Distressed:</div>
                <div className={css.value}>{distressed ? 'Yes' : 'No'}</div>
              </div>
            </Row>
            <Row>
              <div className={css.rowInfo}>
                <div className={css.label}>Liens:</div>
                <div className={css.value}>{lienAmountString}</div>
              </div>
            </Row>
            <Row toCheck={[ownership]}>
              <div className={css.rowInfo}>
                <div className={css.label}>Owner:</div>
                <div className={css.value}>
                  <Label background="blue" size="normal">{`${ownership}`}</Label>
                </div>
              </div>
            </Row>
            <Row toCheck={[occupancy]}>
              <div className={css.rowInfo}>
                <div className={css.label}>Occupancy:</div>
                <div className={css.value}>
                  <Label background="blue" size="normal">{`${occupancy}`}</Label>
                </div>
              </div>
            </Row>
            <Row toCheck={[landUse]}>
              <div className={css.apartmentItem}>
                <div className={css.centered}>
                  <div className={landUseClassName}>{landUse}</div>
                </div>
              </div>
            </Row>
            <div className={css.apartment}>
              <Row toCheck={[bedrooms]}>
                <div className={css.apartmentItem}>
                  <div className={css.centered}>
                    <div className={css.apartmentValue}>{bedrooms}</div>
                    <div className={css.apartmentCaption}>Beds</div>
                  </div>
                </div>
              </Row>
              <Row toCheck={[bathrooms]}>
                <div className={css.apartmentItem}>
                  <div className={css.centered}>
                    <div className={css.apartmentValue}>{bathrooms}</div>
                    <div className={css.apartmentCaption}>Baths</div>
                  </div>
                </div>
              </Row>
              <Row toCheck={[squareFeet]}>
                <div className={css.apartmentItem}>
                  <div className={css.centered}>
                    <div className={css.apartmentValue}>{squareFeet}</div>
                    <div className={css.apartmentCaption}>Sq. Ft</div>
                  </div>
                </div>
              </Row>
              <Row toCheck={[yearBuilt]}>
                <div className={css.apartmentItem}>
                  <div className={css.centered}>
                    <div className={css.apartmentValue}>{yearBuilt}</div>
                    <div className={css.apartmentCaption}>Yr. Built</div>
                  </div>
                </div>
              </Row>
            </div>
          </div>
        </div>
        <div className={css.tiles}>
          {estimatedEquityGraph}
          {compSaleAmountGraph}
          {rentAmountGraph}
        </div>
        <div className={css.buttons}>
          <ButtonLink to={linkToDetails} className={wideButtonStyles}>Details</ButtonLink>
          <AnalysisButton propertyId={id} width="115px" isLoading={loading} />
          <Button kind={Button.kind.ghost} size={Button.size.middle} width="115px" disabled={!!savedPropertyId} onClick={saveProperty} isLoading={loading}>{savedPropertyId ? 'Saved' : 'Save'}</Button>
        </div>
      </div>
    );
  }
}

Info.propTypes = {
  properties: PropTypes.instanceOf(Map).isRequired,
};

export default Info;

