import { PureComponent, PropTypes } from 'react';
import { List } from 'immutable';


class Row extends PureComponent {
  render() {
    const { children, toCheck } = this.props;

    if (toCheck && !toCheck.every(object => (!!object))) {
      return null;
    }

    return children;
  }
}


Row.propTypes = {
  toCheck: PropTypes.oneOfType([PropTypes.instanceOf(List), PropTypes.array]),
};

export default Row;
