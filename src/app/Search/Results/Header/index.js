import React, { PropTypes, PureComponent } from 'react';
import { connect } from 'react-redux';

import Button from 'components/base/Button';
import Checkbox from 'components/base/Checkbox';
import { MinMaxInput } from 'components/base/Input';
import AddToGroup from 'app/components/AddToGroup';
import { PropertyGroupTypes, selectFavorites, selectLists, selectLoading } from 'data/property';
import { saveListings } from 'data/listing';
import { selectProfile } from 'data/user';
import css from './style.scss';
import { selectRangeEnabled, selectRangeFrom, selectRangeTo, selectSearchQueryParams, selectSelectedIds } from '../../selectors';


class Header extends PureComponent {
  constructor(props) {
    super(props);

    this.handleSaveGroup = this.handleSaveGroup.bind(this);
    this.handleSaveList = this.handleSaveList.bind(this);
  }

  handleSaveGroup() {
    const { favorites, openAddToGroup, selectedCount, profile } = this.props;
    openAddToGroup({
      selectLoading,
      groups: favorites,
      size: selectedCount,
      limit: profile.get('favoritePropertyAddLimit'),
      save: onComplete => this.save(PropertyGroupTypes.FAVORITE, '', onComplete),
      add: (name, onComplete) => this.save(PropertyGroupTypes.FAVORITE, name, onComplete),
    });
  }

  handleSaveList() {
    const { lists, openAddToGroup, selectedCount, profile } = this.props;
    openAddToGroup({
      selectLoading,
      groups: lists,
      size: selectedCount,
      limit: profile.get('marketingListAddLimit'),
      groupType: 'Marketing List',
      add: (name, onComplete) => this.save(PropertyGroupTypes.MARKETING, name, onComplete),
    });
  }

  save(type, name, onComplete) {
    const { query, saveListings, rangeFrom, rangeTo, allSelected, selection } = this.props;
    const { searchValue, searchParams } = query.toJS();
    saveListings({ searchValue, searchParams }, rangeFrom, rangeTo, allSelected, selection, type, name, ({ response: { quantity } }) => onComplete(quantity));
  }

  render() {
    const {
      propertyCount,
      selectedCount,
      isLoading,
      selectAll,
      onChangeRange,
      allSelected,
      rangeFrom,
      rangeTo,
      rangeEnabled,
    } = this.props;

    const btn = { size: Button.size.small, kind: Button.kind.blueGhost, isLoading };

    return (
      <div className={css.container}>
        <div className={css.left}>
          <Checkbox name="SearchResultsAll" id="SearchResultsAll" checked={allSelected} onChange={selectAll} />
          {rangeEnabled ? null : <div className={css.caption}>Properties{rangeEnabled ? '' : ` (${propertyCount})`}</div>}
          {!rangeEnabled ? <div className={css.selected}>{selectedCount ? `${selectedCount} Selected` : null}</div> : (
            <div className={css.range}>
              <div>Show</div>
              <MinMaxInput name="rangeFrom" placeholder="1" value={rangeFrom} min={1} max={rangeTo || propertyCount} onChange={onChangeRange} />
              <div>-</div>
              <MinMaxInput name="rangeTo" placeholder={propertyCount} value={rangeTo} min={rangeFrom || 1} max={propertyCount} onChange={onChangeRange} />
              <div>of {propertyCount} Properties</div>
            </div>
          )}
        </div>
        {!rangeEnabled ? null : <div className={css.selectedBottom}>{selectedCount ? `${selectedCount} Selected` : null}</div>}
        <div className={css.right}>
          {rangeEnabled ? null : <Button {...btn} onClick={() => onChangeRange({ name: 'rangeEnabled', value: true })}>Filter</Button>}
          <Button {...btn} onClick={this.handleSaveList} disabled={!selectedCount}>Add to List</Button>
          <Button {...btn} onClick={this.handleSaveGroup} disabled={!selectedCount}>Save</Button>
        </div>
      </div>
    );
  }
}
/*
            properties={properties}
            isLoading={isLoading}
            disabled={!canSaveDefault}
            selectedCount={selectedCount}
            allSelectedState={allSelectedState}
            loadAllListings={loadAllListings}

    selectedCount={selectedCount}
    allSelectedState={allSelectedState}
    loadAllListings={loadAllListings}
    size={Button.size.small}
    name="addToList"
    properties={properties}
    disabled={disabled}
    isLoading={isLoading}
    isGroup
    preloadGroups
    saveToDefaultFirst
 */
Header.propTypes = {
  propertyCount: PropTypes.number.isRequired,
  selectedCount: PropTypes.number.isRequired,
};


export default connect(state => ({
  lists: selectLists(state),
  favorites: selectFavorites(state),
  rangeFrom: selectRangeFrom(state),
  rangeTo: selectRangeTo(state),
  rangeEnabled: selectRangeEnabled(state),
  query: selectSearchQueryParams(state),
  selection: selectSelectedIds(state),
  profile: selectProfile(state),
}), { openAddToGroup: AddToGroup.open, saveListings })(Header);
