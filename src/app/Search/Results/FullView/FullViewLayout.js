/** @flow */
import React, { PureComponent, PropTypes } from 'react';
import { List } from 'immutable';
import { createSelector } from 'reselect';

import { Tables, FullTable, LiensTable } from 'data/listing/constants';
import SVG from 'components/base/SVG';

import Table from 'components/base/Table';

import ColumnsRenderers from '../ColumnsRenderers';
import css from '../style.scss';


const selectExtraProps = createSelector(
  func => func,
  onSelect => ({ onSelect, colorDot: true }),
);

const selectCheckedPushpin = createSelector(
  pin => pin,
  checkedPushpin => ({ checkedPushpin }),
);


class FullViewLayout extends PureComponent {
  render() {
    const { checkedPushpin, data, onSelect, onRowClick, onColumnSort, sorting, currentSearch } = this.props;
    const listingType = currentSearch.get('listingType');
    const liensOnly = listingType.size === 1 && listingType.get(0) === 'L';
    const sortFields = sorting ? sorting.fields : '';
    const caretIconName = sorting && sorting.ascending ? 'iconCaretUp' : 'iconCaretDown';
    const caretIcon = (<SVG icon={caretIconName} />);
    const columns = Tables[liensOnly ? LiensTable : FullTable].map(
      column => ColumnsRenderers[column](selectCheckedPushpin(checkedPushpin), caretIcon),
    );
    const checkboxColumn = ColumnsRenderers.CHECKBOX(selectExtraProps(onSelect));

    return (
      <div className={css.content}>
        <Table
          data={data}
          keyField="id"
          onRowClick={onRowClick}
          onColumnSort={onColumnSort}
          className="type_11"
          isHoverable
          isSortable
          sortFields={sortFields}
        >
          {checkboxColumn}
          {columns}
        </Table>
      </div>
    );
  }
}


FullViewLayout.propTypes = {
  data: PropTypes.instanceOf(List).isRequired,
  onSelect: PropTypes.func.isRequired,
  onRowClick: PropTypes.func,
  onColumnSort: PropTypes.func,
};

export default FullViewLayout;
