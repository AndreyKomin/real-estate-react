/** @flow */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { changeSearchResultsSorting, selectSorting } from 'data/search';

import FullViewLayout from './FullViewLayout';


class FullView extends PureComponent {
  constructor(props) {
    super(props);
    this.sortTable = this.sortTable.bind(this);
  }

  /* :: sortTable: Function */
  sortTable(column) {
    this.props.changeSearchResultsSorting(column);
  }

  render() {
    return (
      <FullViewLayout
        {...this.props}
        onColumnSort={this.sortTable}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    sorting: selectSorting(state),
  };
}

const mapActionToProps = {
  changeSearchResultsSorting,
};


export default connect(mapStateToProps, mapActionToProps)(FullView);
