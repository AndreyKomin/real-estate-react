/** @flow */
import React, { PureComponent, PropTypes } from 'react';
import { Link } from 'react-router';
import classNames from 'classnames';

import type { ListingType } from 'data/listing/constants';
import {
  Name as ListingTypeName,
  TypeCustom,
  Color,
  Active,
  Fields,
  PicViewFields,
  DataType,
  Name,
} from 'data/listing/constants';
import numberToPrice from 'utils/currency/numberToPrice';
import formatDate from 'utils/date/formatDate';

import Image from 'components/base/Image';
import Checkbox from 'components/base/Checkbox';
import ColorDot from 'components/ColorDot';
import DEFAULT_IMAGE_URL from 'assets/favicon.png';

import css from './style.scss';


const BIG_COUNTER = (10 ** 3) - 10;

class Item extends PureComponent<*, *, *> {
  constructor(props: Object) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  /* :: handleChange: Function */
  handleChange() {
    this.props.onSelect(this.props.selectId);
  }

  /* :: handleClick: Function */
  handleClick() {
    this.props.onClick(this.props.selectId);
  }

  /* :: renderFooterItem: Function */
  renderFooterItem(data: Object, type: ListingType) {
    const valueName = Fields[data.value];
    const value = this.props.item.get(valueName);

    if (!value && type !== 'L') return null;

    let formattedValue = value;

    if (value && data.type === DataType.DATE) {
      formattedValue = formatDate(value);
    } else if (value && data.type === DataType.PRICE) {
      formattedValue = numberToPrice(value);
    }

    formattedValue = formattedValue || 'N/A';

    return (
      <div className={css.item} key={`${this.props.selectId}${data.value}`}>
        <span className={css.title}>{data.label}:</span>
        <span className={css.value}>{formattedValue}</span>
      </div>
    );
  }

  /* :: renderFooter: Function */
  renderFooter() {
    const fields = PicViewFields[this.props.item.get('type')] || PicViewFields[TypeCustom];
    const items = fields.map(data => this.renderFooterItem(data, this.props.item.get('type')));

    return (<div className={css.footer}>
      {items}
    </div>);
  }

  render() {
    const {
      propertyId,
      propertyType,
      imageUrl = DEFAULT_IMAGE_URL,
      address,
      [Fields.TYPE]: type,
      bedrooms,
      bathrooms,
      squareFeet = 0,
      resultIndex,
      selected: checked,
    } = this.props.item.toJS();

    const name = address.streetAddress;
    const counter = resultIndex;
    const rootClasses = classNames(css.root, { [css.rootChecked]: checked, [css.bigCounter]: counter >= BIG_COUNTER });
    const linkToDetailsPopup = `/search/${propertyId}`;

    return (
      <div className={rootClasses}>
        <div className={css.checkboxContainer} onChange={this.handleChange}>
          <Checkbox name={`SearchResultsItem-${counter}`} id={`SearchResultsItem-${counter}`} checked={checked} />
        </div>
        <span className={css.index} onClick={this.handleClick}>{counter}</span>
        <div className={css.mask} onClick={this.handleClick}>
          <Link to={linkToDetailsPopup}>
            <Image src={imageUrl} alt={name} placeholder="http://images.listing.realestate.com/photo_coming_soon_sm8.jpg" />
          </Link>
          <ColorDot color={Color[this.props.active ? Active : type]} className={css.dot} title={Name[type]} />
        </div>
        <div className={css.info} onClick={this.handleClick}>
          <div className={css.header}>
            <div className={css.left}>
              <Link to={linkToDetailsPopup} className={css.name}>{name}</Link>
              <div className={css.address}>{address.cityName}</div>
              <div className={css.address}>{address.stateCode} {address.zip}</div>
            </div>
            <div className={css.right}>
              <div className={css.type}>{ListingTypeName[type]}</div>
              <div className={css.type}>{propertyType}</div>
              <div>
                <span className={css.option}>
                  <span className={css.value}>{bedrooms}</span> Bd.
                </span>
                <span className={css.option}>
                  <span className={css.value}>{bathrooms}</span> Ba.
                </span>
                <span className={css.option}>
                  <span className={css.value}>{squareFeet}</span> sq. ft.
                </span>
              </div>
            </div>
          </div>
          {this.renderFooter()}
        </div>
      </div>
    );
  }
}

Item.propTypes = {
  item: PropTypes.shape({
    img: PropTypes.string,
    name: PropTypes.string,
    address: PropTypes.string,
    type: PropTypes.string,
    bedrooms: PropTypes.number,
    bathrooms: PropTypes.number,
    square: PropTypes.number,
    estValue: PropTypes.number,
    amtDefault: PropTypes.number,
    lastUpdated: PropTypes.string,
  }).isRequired,
  selectId: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
};


export default Item;
