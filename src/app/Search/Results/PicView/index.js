/** @flow */
import React, { PureComponent, PropTypes } from 'react';
import { List } from 'immutable';

import Item from './Item';
import css from '../style.scss';


class PicView extends PureComponent {
  render() {
    const { checkedPushpin, onRowClick, data, onSelect } = this.props;

    const content = data.map(item => (
      <Item
        active={item.get('id') === checkedPushpin}
        onClick={onRowClick}
        item={item}
        selectId={item.get('id')}
        key={item.get('id')}
        onSelect={onSelect}
      />
    ));

    return (
      <div className={css.content}>
        {content}
      </div>
    );
  }
}


PicView.propTypes = {
  data: PropTypes.oneOfType([
    PropTypes.arrayOf(
      PropTypes.shape({
        img: PropTypes.string,
        name: PropTypes.string,
        address: PropTypes.string,
        type: PropTypes.string,
        bedrooms: PropTypes.number,
        bathrooms: PropTypes.number,
        square: PropTypes.number,
        estValue: PropTypes.number,
        amtDefault: PropTypes.number,
        lastUpdated: PropTypes.string,
      })),
    PropTypes.instanceOf(List),
  ]).isRequired,
  onSelect: PropTypes.func.isRequired,
};

export default PicView;
