import React, { PureComponent } from 'react';
import SVG from 'components/base/SVG';
import classNames from 'classnames';
import formatPercent from 'utils/percent/formatPercent';

import css from './style.scss';


const arrowClassName = (base, direction = 'up') => (
  classNames(base, { [css.arrowDown]: direction === 'down' })
);

class PriceRentChart extends PureComponent {
  render() {
    const { className, price, rent } = this.props;

    const priceItem = price ? (
      <div className={css.statItemValue}>
        <div className={css.icon}>
          <SVG icon="iconArrowUp" className={arrowClassName(css.arrow, price.direction)} />
        </div>
        <span className={css.value}>{formatPercent(price.percents, '', { maximumFractionDigits: 2 })}</span>
      </div>
    ) : <span className={css.value}>&#8212;</span>;

    const rentItem = rent ? (
      <div className={css.statItemValue}>
        <div className={css.icon}>
          <SVG icon="iconArrowUp" className={arrowClassName(css.arrow, rent.direction)} />
        </div>
        <span className={css.value}>{formatPercent(rent.percents, '', { maximumFractionDigits: 2 })}</span>
      </div>
    ) : <span className={css.value}>&#8212;</span>;

    return (
      <div className={classNames(className, css.chartInfo)}>
        <div className={css.statWraper}>
          <div className={css.statItem}>
            {priceItem}
            <span className={css.label}>Last 30 Days Price Change</span>
          </div>
          <hr className={css.hr} />
          <div className={css.statItem}>
            {rentItem}
            <span className={css.label}>Last 30 Days Rent Change</span>
          </div>
        </div>
      </div>
    );
  }
}

export default PriceRentChart;

PriceRentChart.defaultProps = {
  className: null,
};

