import React, { PureComponent } from 'react';
import StatUnit from 'components/StatUnit';
import Dropdown from 'components/base/Dropdown';
import classNames from 'classnames';

import css from './style.scss';


class Trends extends PureComponent {
  render() {
    const { className, items, periods, title, onPeriodChange, currentPeriod, periodName } = this.props;

    const units = items.map(item => (
      <StatUnit key={item.name} className={css.item} stat={item} />
    ));

    return (
      <div className={classNames(className, css.container)}>
        <div className={css.header}>
          <span>{title}</span>
          <Dropdown
            className={css.dropdown}
            name={periodName}
            options={periods}
            onChange={onPeriodChange}
            value={currentPeriod}
          />
        </div>
        <div className={css.items}>
          {units}
        </div>
      </div>
    );
  }
}

export default Trends;
