/** @flow */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { changeStatisticParams, exportStatistics, Periods, Colors } from 'data/statistic';
import { selectStatisticQueryParams, selectIsLoading } from 'data/statistic/selectors';

import Layout from './Layout';


class StatisticCharts extends PureComponent {

  constructor(props: *) {
    super(props);
    this.onParamsChange = this.onParamsChange.bind(this);
    this.exportAreaStatistics = this.exportAreaStatistics.bind(this);
  }

  /* :: onParamsChange: Function */
  onParamsChange(event: Object) {
    const { name, value } = event.target || event;
    return this.props.changeStatisticParams({ name, value });
  }

  /* :: exportAreaStatistics: Function */
  exportAreaStatistics() {
    const { searchValue, searchParams } = this.props.query.toJS();
    const statisticParams = this.props.statisticQuery.toJS();
    this.props.exportStatistics({
      searchValue,
      searchParams,
      statisticParams,
    });
  }

  render() {
    return (
      <Layout
        {...this.props}
        periods={Periods}
        onParamsChange={this.onParamsChange}
        colors={Colors}
        exportAreaStatistics={this.exportAreaStatistics}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    currentStatisticQuery: selectStatisticQueryParams(state),
    isLoading: selectIsLoading(state),
  };
}

const mapDispatchToProps = {
  changeStatisticParams,
  exportStatistics,
};

export default connect(mapStateToProps, mapDispatchToProps)(StatisticCharts);
