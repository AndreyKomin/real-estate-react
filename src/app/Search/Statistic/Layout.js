/** @flow */
import React, { PureComponent } from 'react';
import { Map } from 'immutable';
import Chart from 'components/Chart';
import SVG from 'components/base/SVG';
import Button from 'components/base/Button';
import classNames from 'classnames';

import PriceRentChart from './PriceRentChart';
import Trends from './Trends';
import css from './style.scss';


class StatisticCharts extends PureComponent<*, *, *> {
  render() {
    const {
      areaStatistics,
      currentStatisticQuery,
      periods,
      onParamsChange,
      colors,
      exportAreaStatistics,
      isLoading,
      children,
    } = this.props;
    const currentStatisticQueryObject = currentStatisticQuery.toJS();
    const {
      oneMonthPriceChange,
      oneMonthRentChange,
      marketTrends,
      listingTrends,
      salesTrends,
      pricePerSquareFootGraph = {},
      averageDaysOnMarketGraph = {},
      averageMonthlyRentGraph = {},
      newPreForeclosuresGraph = {},
      listPriceVsSalePriceGraph = {},
      daysOnMarketVsInventoryGraph = {},
    } = areaStatistics;

    return (
      <section className={css.container}>
        <div className={css.rowWrapper}>
          {children}
          <Button
            kind={Button.kind.link}
            name="print"
            className={css.exportButton}
            onClick={exportAreaStatistics}
            isLoading={isLoading}
          >
            <SVG icon="iconPrint" className={css.iconExport} />
          </Button>
        </div>
        <div className={css.rowWrapper}>
          <PriceRentChart className={css.item} price={oneMonthPriceChange} rent={oneMonthRentChange} />
          <Chart
            className={css.item}
            caption="$/SqFt"
            data={pricePerSquareFootGraph.points}
            type={pricePerSquareFootGraph.type}
            categories={pricePerSquareFootGraph.categories}
            yTickFormat={Chart.yTickFormat.currency}
          />
          <Chart
            className={css.item}
            caption="Average Days on Market"
            xTickFormat={Chart.xTickFormat.bedroom}
            data={averageDaysOnMarketGraph.points}
            type={averageDaysOnMarketGraph.type}
            chartStyle={Chart.chartStyle.bedroom}
            categories={averageDaysOnMarketGraph.categories}
          />
        </div>
        <div className={css.rowWrapper}>
          <Trends
            title="Market Trend"
            className={css.item}
            items={marketTrends}
            periods={periods}
            onPeriodChange={onParamsChange}
            currentPeriod={currentStatisticQueryObject.marketTrendGrowthPeriod}
            periodName="marketTrendGrowthPeriod"
          />
          <Chart
            className={css.item}
            caption="New Pre-Foreclosures"
            data={newPreForeclosuresGraph.points}
            type={newPreForeclosuresGraph.type}
            categories={newPreForeclosuresGraph.categories}
          />
          <Chart
            className={css.item}
            caption="Average Monthly Rent"
            xTickFormat={Chart.xTickFormat.bedroom}
            data={averageMonthlyRentGraph.points}
            type={averageMonthlyRentGraph.type}
            chartStyle={Chart.chartStyle.bedroom}
            categories={averageMonthlyRentGraph.categories}
            yTickFormat={Chart.yTickFormat.currency}
          />
        </div>
        <div className={css.rowWrapper}>
          <Trends
            title="Listing Trend"
            className={classNames(css.item, css.longTrend)}
            items={listingTrends}
            periods={periods}
            onPeriodChange={onParamsChange}
            currentPeriod={currentStatisticQueryObject.listingTrendGrowthPeriod}
            periodName="listingTrendGrowthPeriod"
          />
          <Chart
            className={css.item}
            caption="List Price vs. Sale Price"
            data={listPriceVsSalePriceGraph.points}
            type={listPriceVsSalePriceGraph.type}
            categories={listPriceVsSalePriceGraph.categories}
            chartStyle={Chart.chartStyle.type4}
            colors={colors}
            yTickFormat={Chart.yTickFormat.currency}
          />
        </div>
        <div className={css.rowWrapper}>
          <Trends
            title="Sales Trend"
            className={classNames(css.item, css.longTrend)}
            items={salesTrends}
            periods={periods}
            onPeriodChange={onParamsChange}
            currentPeriod={currentStatisticQueryObject.salesTrendGrowthPeriod}
            periodName="salesTrendGrowthPeriod"
          />
          <Chart
            className={css.item}
            caption="Days on Market vs. Inventory"
            data={daysOnMarketVsInventoryGraph.points}
            type={daysOnMarketVsInventoryGraph.type}
            categories={daysOnMarketVsInventoryGraph.categories}
            chartStyle={Chart.chartStyle.type4}
            colors={colors}
          />
        </div>
      </section>
    );
  }
}

StatisticCharts.defaultProps = {
  areaStatistics: {},
  currentStatisticQuery: Map(),
};


export default StatisticCharts;
