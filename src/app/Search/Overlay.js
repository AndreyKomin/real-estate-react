/** @flow */
import React, { PureComponent } from 'react';
import { createSelector } from 'reselect';

import SVG from 'components/base/SVG';


class Overlay extends PureComponent {
  selectOnClick(props: Object) {
    const self: any = this;
    if (!self) {
      throw new Error('`this` equals to false');
    } else if (!self.realSelectOnClick) {
      self.realSelectOnClick = createSelector(
        props => props.overlayType,
        overlayType => (
          () => this.props.triggerOverlay(overlayType)
        ),
      );
    }

    return self.realSelectOnClick(props);
  }

  render() {
    const { children, className, hideButton, buttonClassName, icon, iconClassName } = this.props;
    const button = hideButton ? null : (<button className={buttonClassName} onClick={this.selectOnClick(this.props)}>
      <SVG icon={icon} className={iconClassName} />
    </button>);

    return (
      <div className={className}>
        {button}
        {children}
      </div>
    );
  }
}

export default Overlay;
