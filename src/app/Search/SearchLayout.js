/** @flow */
import React, { PureComponent } from 'react';

import { Overlay } from 'data/listing';
import Header from 'app/components/Header';
import classNames from 'classnames';

import StatisticCharts from './Statistic';

import Map from './Map';
import SearchResults, { FullView, ListView, PicView, PropertyView, PropertiesListView } from './Results';
import OverlayView from './Overlay';

import css from './style.scss';


class SearchLayout extends PureComponent {
  componentDidUpdate(oldProps: Object) {
    const isOldProperty = oldProps.searchParams.isProperty;
    const { isProperty } = this.props.searchParams;

    if (isProperty && isProperty !== isOldProperty) {
      this.props.changeOverlay(Overlay.NONE);
    }
  }

  renderListingResults() {
    const {
      searchResultSelect,
      listingResultParam,
      overlayParams,
      checkedPushpin,
      triggerPushpinPopover,
      tableTrigger,
      currentSearch,
    } = this.props;
    const { isTable: isOverlay = false } = overlayParams;
    const {
      isAny,
      isList,
      isPicture,
      results,
      listingType,
    } = listingResultParam;

    if (!isAny) {
      return null;
    } else if (isOverlay) {
      return (
        <SearchResults>
          <FullView
            currentSearch={currentSearch}
            checkedPushpin={checkedPushpin}
            onSelect={searchResultSelect}
            data={results}
            onRowClick={tableTrigger}
          />
        </SearchResults>
      );
    } else if (isList) {
      return (
        <SearchResults>
          <ListView
            checkedPushpin={checkedPushpin}
            onSelect={searchResultSelect}
            data={results}
            type={listingType}
            onRowClick={tableTrigger}
          />
        </SearchResults>
      );
    } else if (isPicture) {
      return (
        <SearchResults>
          <PicView
            checkedPushpin={checkedPushpin}
            onSelect={searchResultSelect}
            data={results}
            onRowClick={triggerPushpinPopover}
          />
        </SearchResults>
      );
    }

    throw new Error('Unexpected listType value');
  }

  renderSearchResults() {
    const {
      searchParams,
      propertiesParams,
      changeOverlay,
      overlayParams,
      searchResultSelect,
      checkedPushpin,
      listingResultParam,
      tableTrigger,
    } = this.props;
    const { isAny, isListing, isProperty } = searchParams;
    const { listingType } = listingResultParam;
    const properties = propertiesParams.results.getIn(['data', 'properties']);

    if (!isAny) return null;

    if (isListing) {
      const {
        isTable: shouldOverlay = false,
        tableType: tableOverlayType,
      } = overlayParams;

      const renderedListingResults = this.renderListingResults();
      const containerClassName = classNames(css.rightSide, { [css.rightSideExpanded]: shouldOverlay });

      return (
        <OverlayView
          className={containerClassName}
          buttonClassName={css.rightExpand}
          icon="iconCaretDown"
          iconClassName={css.iconCaret}
          overlayType={tableOverlayType}
          triggerOverlay={changeOverlay}
        >
          {renderedListingResults}
        </OverlayView>
      );
    }

    if (isProperty) {
      const {
        isTable: shouldOverlay = false,
        tableType: tableOverlayType,
      } = overlayParams;
      const containerClassName = classNames(css.rightSide, { [css.rightSideExpanded]: shouldOverlay });

      if (properties.size > 1) {
        return (
          <OverlayView
            className={containerClassName}
            hideButton
            overlayType={tableOverlayType}
            triggerOverlay={changeOverlay}
          >
            <PropertiesListView
              checkedPushpin={checkedPushpin}
              onSelect={searchResultSelect}
              data={properties}
              type={listingType}
              onRowClick={tableTrigger}
            />
          </OverlayView>
        );
      }
      return (
        <OverlayView
          className={containerClassName}
          hideButton
          overlayType={tableOverlayType}
          triggerOverlay={changeOverlay}
        >
          <PropertyView
            {...propertiesParams}
            onSelect={searchResultSelect}
            checkedPushpin={checkedPushpin}
            history={this.props.history}
          />
        </OverlayView>
      );
    }

    throw new Error('Unexpected searchType value');
  }

  renderCharts() {
    const {
      searchParams,
      changeOverlay,
      overlayParams,
      areaStatistics,
      statisticQuery,
      query,
      statisticTitle,
    } = this.props;
    const { isAny } = searchParams;
    if (!isAny) return null;

    const {
      isChart: shouldOverlay = false,
      chartType: chartOverlayType,
    } = overlayParams;
    const className = classNames(css.statistics, { [css.statisticsExpanded]: shouldOverlay });

    return (
      <OverlayView
        className={className}
        buttonClassName={css.bottomExpand}
        icon="iconCaretDown"
        iconClassName={css.iconCaret}
        overlayType={chartOverlayType}
        triggerOverlay={changeOverlay}
      >
        <StatisticCharts areaStatistics={areaStatistics} statisticQuery={statisticQuery} query={query} >
          <div className={css.statisticTitle}>{statisticTitle}</div>
        </StatisticCharts>
      </OverlayView>
    );
  }

  renderMap() {
    const { searchParams } = this.props;
    const { isAny } = searchParams;
    const className = classNames(css.map, { [css.mapFullSize]: !isAny });

    return (
      <div className={className}>
        <Map {...this.props} />
      </div>
    );
  }

  render() {
    const contentClassName = classNames(css.content, { [css.contentOverlayed]: this.props.overlayParams.isAny });
    const leftSideClassName = classNames(css.leftSide, { [css.leftSideFullSize]: !this.props.searchParams.isAny });
    const map = this.renderMap();
    const charts = this.renderCharts();
    const searchResults = this.renderSearchResults();

    return (
      <div className={css.wrapper}>
        <Header onToggleClick={this.props.onToggleClick} />
        <section className={contentClassName}>
          <div className={leftSideClassName}>
            {map}
            {charts}
          </div>
          {searchResults}
        </section>
      </div>
    );
  }
}

export default SearchLayout;
