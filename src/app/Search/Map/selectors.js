/** @flow */
import { createSelector } from 'reselect';
import { Map } from 'immutable';

import { selectCurrentSearch } from 'data/search/selectors';
import { selectPlacePushpin } from 'data/map/selectors';
import { getBingUrl } from 'data/property';

import { selectListResult } from '../selectors';


const emptyMap = Map();

export const selectPushpins = createSelector(
  [selectListResult, selectPlacePushpin],
  (list, place) => {
    if (!place || !place.get('info')) return list;
    if (list.some(ent => ent.get('propertyId') === place.getIn(['info', 'id']))) return list;
    return list.push(place.get('info').set('__onlyPlace', true));
  },
);

export const selectActivePushpinId = createSelector(
  [state => state, selectPlacePushpin],
  (state, place) => {
    if (place && place.get('info')) {
      const checked = place.getIn(['info', 'id']);
      const selectedPushpin = selectPushpins(state).find(pin => pin.get('propertyId') === checked);
      if (selectedPushpin) return selectedPushpin.get('id');
    }
    const checked = state.getIn(['map', 'checkedPushpin'], null);
    if (selectPushpins(state).some(pin => pin.get('id') === checked)) {
      return checked;
    }
    return null;
  },
);


export const selectActivePushpin = createSelector(
  [selectPushpins, selectActivePushpinId, selectPlacePushpin],
  (pushpins, activePushpinId, place) => (
    (place && place.get('info')) || pushpins.find(pin => pin.get('id') === activePushpinId)
  ),
);

export const selectActivePushpinBingMapImageUrl = createSelector(
  selectActivePushpin,
  (activePushpin = emptyMap) => {
    if (!activePushpin.has('latitude')) return null;
    if (!activePushpin.has('longitude')) return null;
    return getBingUrl(activePushpin.get('latitude'), activePushpin.get('longitude'));
  },
);

export const selectActiveParcel = createSelector(
  selectActivePushpin,
  pin => pin && pin.get('parcelBoundaryDefinition'),
);

export const selectCanDisplayActiveParcel = createSelector(
  state => state.get('map'),
  map => map.get('canDisplayPlace'),
);

export const selectSearchPolygonCords = createSelector(
  state => state.getIn(['search', 'current', 'shapeDefinition'], []),
  (shapeDefinition) => {
    const temp = shapeDefinition
      .map(cord => cord.split(','))
      .reduce((res, cord) => {
        res.latitude += Number(cord[0]) || 0;
        res.longitude += Number(cord[1]) || 0;
        res.count++;
        return res;
      }, { latitude: 0, longitude: 0, count: 0 });
    if (temp.count === 0) return emptyMap;

    return Map({
      latitude: temp.latitude / temp.count,
      longitude: temp.longitude / temp.count,
    });
  },
);

export const selectPolygonLatitudeCord = createSelector(
  [selectSearchPolygonCords],
  cords => cords.get('latitude'),
);

export const selectPolygonLongitudeCord = createSelector(
  [selectSearchPolygonCords],
  cords => cords.get('longitude'),
);

const selectSearchPoint = createSelector(
  selectCurrentSearch,
  search => search.get('point', emptyMap),
);

const selectEntitiesCord = createSelector(
  state => state.getIn(['listing', 'list', 'items']),
  (items) => {
    const temp = items
      .map(cord => cord.toJS())
      .reduce((res, cord) => {
        res.latitude += Number(cord.latitude) || 0;
        res.longitude += Number(cord.longitude) || 0;
        res.count++;
        return res;
      }, { latitude: 0, longitude: 0, count: 0 });
    if (temp.count === 0) return emptyMap;

    return Map({
      latitude: temp.latitude / temp.count,
      longitude: temp.longitude / temp.count,
    });
  },
);

export const selectCenter = createSelector(
  [selectSearchPoint, selectSearchPolygonCords, selectEntitiesCord],
  (...points) => points.find(point => point.has('latitude') && point.has('longitude')) || emptyMap,
);
