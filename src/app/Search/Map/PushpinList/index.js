/** @flow */
import PushpinList from './PushpinList';
import waitForMap from '../waitForMap';


export default waitForMap(PushpinList);
