/** @flow */
import { Component } from 'react';

import { Color, Fields, Active, DefaultColor } from 'data/listing/constants';
import getIcon from 'Bing/ReactMap/Pushpin/getIcon';


const FIELD_ID = 'id';
const FIELD_LATITUDE = 'latitude';
const FIELD_LONGITUDE = 'longitude';
const FIELD_INNER_TEXT = 'resultIndex';
const PUSHPIN_HEIGHT = 30;
const PUSHPIN_HALF_WIDTH = 11;


class Pushpin extends Component<*, *, *> {
  static getId(props) {
    return props.pushpin.get(FIELD_ID);
  }

  componentDidMount() {
    this.draw();
  }

  shouldComponentUpdate(nextProps: Object) {
    if (this.props.pushpin !== nextProps.pushpin) {
      return true;
    } else if (!nextProps.pushpin) {
      return true;
    } else if (nextProps.active !== this.props.active) {
      if (Pushpin.getId(this.props) === nextProps.active) {
        return true;
      } else if (Pushpin.getId(this.props) === this.props.active) {
        return true;
      }
    }
    return false;
  }

  componentDidUpdate() {
    this.remove();
    this.draw();
  }

  componentWillUnmount() {
    this.remove();
  }

  pushpin: Object;

  remove() {
    this.props.map.entities.remove(this.pushpin);
  }

  drawOnMap() {
    const { map } = this.props;
    map.entities.push(this.pushpin);
  }

  draw() {
    const { pushpin, active, Microsoft } = this.props;

    const latitude = pushpin.get(FIELD_LATITUDE);
    const longitude = pushpin.get(FIELD_LONGITUDE);
    const location = new Microsoft.Maps.Location(latitude, longitude);

    const isActive = pushpin.get(FIELD_ID) === active;
    const drawType = isActive ? Active : (pushpin.get(Fields.TYPE) || pushpin.get(Fields.LISTING_TYPE));
    const color = Color[drawType] || Color[DefaultColor];
    const resultIndex = pushpin.get(FIELD_INNER_TEXT);

    this.pushpin = new Microsoft.Maps.Pushpin(location, {
      icon: getIcon(color),
      text: resultIndex ? `${resultIndex}` : '',
      anchor: new Microsoft.Maps.Point(PUSHPIN_HALF_WIDTH, PUSHPIN_HEIGHT),
      color,
    });

    Microsoft.Maps.Events.addHandler(this.pushpin, 'click', () => {
      this.props.triggerPushpinActivity(pushpin.get(FIELD_ID));
    });

    this.drawOnMap();
  }


  render() {
    return null;
  }
}


export default Pushpin;

