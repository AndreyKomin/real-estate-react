/** @flow */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { triggerPlaceActivity, selectPlacePushpin } from 'data/map';

import Pushpin from './Pushpin';
import EventHandler from '../EventHandler';


const style = { display: 'none' };
const KEY_FIELD = 'id';

class PushpinList extends PureComponent<*, *, *> {
  constructor(props: *) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  /* :: handleClick: Function */
  handleClick(event: Object) {
    const { latitude, longitude }: { latitude: number, longitude: number } = event.location;
    this.props.triggerPlaceActivity({ latitude, longitude });
  }

  renderPushpins() {
    const { pushpins, ...rest } = this.props;
    return pushpins.map(pushpin => (
      <Pushpin {...rest} key={pushpin.get(KEY_FIELD)} pushpin={pushpin} />
    ));
  }

  render() {
    const { map, Microsoft, disabledActiveParcel } = this.props;
    const parcelHandler = disabledActiveParcel ?
      null : <EventHandler map={map} Microsoft={Microsoft} event="click" onEvent={this.handleClick} />;

    return (
      <div style={style}>
        {parcelHandler}
        {this.renderPushpins()}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    place: selectPlacePushpin(state),
  };
}

const mapActionToProps = { triggerPlaceActivity };

export default connect(mapStateToProps, mapActionToProps)(PushpinList);

