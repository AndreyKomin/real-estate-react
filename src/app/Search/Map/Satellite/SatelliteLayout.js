import React, { PureComponent, PropTypes } from 'react';

import SVG from 'components/base/SVG';

import css from './style.scss';


class SatelliteLayout extends PureComponent {
  render() {
    const { mapType, disabled, triggerMapType } = this.props;

    const typeName = mapType === 'aerial' ? 'Aerial' : 'Road';

    return (
      <button className={css.buttonSatellite} disabled={disabled} type="button" onClick={triggerMapType}>
        <SVG icon="iconSatellite" className={css.iconSatellite} />
        <span className={css.satelliteText}>{typeName}</span>
      </button>
    );
  }
}

SatelliteLayout.propTypes = {
  triggerMapType: PropTypes.func.isRequired,
};

SatelliteLayout.defaultProps = {
  disabled: false,
};


export default SatelliteLayout;
