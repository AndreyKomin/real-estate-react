import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { triggerMapType } from 'data/map';

import SatelliteLayout from './SatelliteLayout';


class Satellite extends PureComponent {
  componentDidUpdate(oldProps) {
    if (oldProps.mapType !== this.props.mapType) {
      const { map, Microsoft, mapType } = this.props;
      map.setMapType(Microsoft.Maps.MapTypeId[mapType]);
    }
  }

  render() {
    return <SatelliteLayout {...this.props} />;
  }
}

function mapStateToProps(state) {
  const mapType = state.getIn(['map', 'mapType']);
  return {
    mapType,
  };
}

const mapActionToProps = {
  triggerMapType,
};


export default connect(mapStateToProps, mapActionToProps)(Satellite);
