// @flow
const METRIC_NAME = {};
METRIC_NAME[0] = 'Value';
METRIC_NAME[1] = 'MLS Info';
METRIC_NAME[2] = 'Growth';
METRIC_NAME[3] = 'Rent';

export default function getDefaultMetricName(activeTab: number) {
  return METRIC_NAME[activeTab];
}
