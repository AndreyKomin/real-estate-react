import { NO_METRIC } from 'data/map/constants';
import getDefaultMetricName from './getDefaultMetricName';


export default function usePrettyNaming(type, metrics) {
  return metrics
    .map(
      metric => (metric.get('name') === NO_METRIC ?
        metric.set('displayName', getDefaultMetricName(type)) :
        metric),
    );
}

export function getName(type, metric) {
  return metric && metric.get('name') === NO_METRIC ?
    getDefaultMetricName(type) : metric.get('displayName');
}
