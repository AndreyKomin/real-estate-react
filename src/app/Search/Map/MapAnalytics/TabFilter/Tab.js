import React, { PureComponent, PropTypes } from 'react';
import classNames from 'classnames';

import css from './style.scss';


class Tab extends PureComponent {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.props.onClick(this.props.index);
  }

  render() {
    const { children, index, activeIndex } = this.props;
    return (
      <button onClick={this.handleClick} className={classNames(css.buttonGroup, { [css.active]: activeIndex === index })}>
        {children}
      </button>
    );
  }
}

Tab.propTypes = {
  index: PropTypes.number.isRequired,
  activeIndex: PropTypes.number.isRequired,
};

Tab.defaultProps = {
  activeIndex: 0,
};

export default Tab;
