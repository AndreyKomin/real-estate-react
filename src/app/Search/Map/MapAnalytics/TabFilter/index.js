import React, { PropTypes } from 'react';
import { pure } from 'recompose';

import SVG from 'components/base/SVG';

import Tab from './Tab';
import css from './style.scss';


export const TabFilter = ({ activeIndex, onClick }) => (
  <div className={css.buttonsGroup}>
    <Tab index={0} onClick={onClick} activeIndex={activeIndex}>
      <SVG icon="iconDollar" className={css.iconDollar} />
      <div className={css.label}>Est. Values</div>
    </Tab>
    <Tab index={1} onClick={onClick} activeIndex={activeIndex}>
      <SVG icon="iconSmallChart" className={css.iconSmallChart} />
      <div className={css.label}>MLS Stats</div>
    </Tab>
    <Tab index={2} onClick={onClick} activeIndex={activeIndex}>
      <SVG icon="iconGrowth" className={css.iconGrowth} />
      <div className={css.label}>Price Growth</div>
    </Tab>
    <Tab index={3} onClick={onClick} activeIndex={activeIndex}>
      <SVG icon="iconHouse" className={css.iconHouse} />
      <div className={css.label}>Rental Values</div>
    </Tab>
  </div>
);

TabFilter.propTypes = {
  activeIndex: PropTypes.number.isRequired,
};

TabFilter.defaultProps = {
  activeIndex: 0,
};

export default pure(TabFilter);
