import React, { PureComponent, PropTypes } from 'react';

import { NO_METRIC } from 'data/map/constants';

import css from './style.scss';


class Spectrum extends PureComponent {
  render() {
    const { palette, currentMetric } = this.props;
    const listPalette = palette.map(result => (
      <div
        key={result}
        className={css.paletteItem}
        style={{ background: result }}
      />
    ));

    const noMetric = currentMetric.get('name') === NO_METRIC;

    const min = noMetric ? <span>&#160;</span> : `<${currentMetric.get('minString')}`;
    const max = noMetric ? <span>&#160;</span> : `${currentMetric.get('maxString')}+`;

    return (
      <div className={css.palette}>
        <div className={css.paletteRow}>{listPalette}</div>
        <div className={css.labels}>
          <span>{min}</span>
          <span>{max}</span>
        </div>
      </div>
    );
  }
}

Spectrum.propTypes = {
  palette: PropTypes.instanceOf(Array).isRequired,
};

Spectrum.defaultProps = {
  palette: 0,
};

export default Spectrum;
