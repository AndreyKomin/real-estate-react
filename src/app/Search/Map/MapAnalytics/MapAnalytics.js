import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { loadMetrics, selectMetrics, NO_METRIC } from 'data/map';
import TileLayer, { ParcelLayer } from 'Bing/ReactMap/TileLayer';

import config from 'config';
import { getCode } from 'store/middleware/api/activate';

import Layout from './Layout';
import getMetricsByType from './getMetricsByType';


const { constants: { API: { BASE_URL } } } = config;

class MapAnalytics extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      metric: NO_METRIC,
      activeTab: 0,
    };
    this.handleTabChange = this.handleTabChange.bind(this);
    this.handleMetricChange = this.handleMetricChange.bind(this);
  }

  componentWillMount() {
    this.props.loadMetrics();
  }

  getUriConstructor() {
    const { metric } = this.state;
    const code = getCode();
    return `${BASE_URL}/resource/auth/ps4/map/tiles/{quadkey}/statistics/${metric}?ActivationCode=${code}`;
  }

  handleTabChange(newIndex) {
    this.setState({
      metric: NO_METRIC,
      activeTab: newIndex,
    });
  }

  handleMetricChange(metric) {
    this.setState({ metric });
  }

  renderTile() {
    if (this.state.metric === NO_METRIC) return null;
    return (
      <TileLayer
        {...this.props}
        uriConstructor={this.getUriConstructor()}
      />
    );
  }

  render() {
    const { children, metrics, ...rest } = this.props;
    const { activeTab, metric } = this.state;
    if (metrics.size === 0) return null;

    const currentMetric = metrics.find(menu => menu.get('name') === metric);

    return (
      <div>
        <Layout
          metric={metric}
          {...rest}
          onTabChange={this.handleTabChange}
          onMetricChange={this.handleMetricChange}
          activeTab={activeTab}
          currentMetric={currentMetric}
          data={getMetricsByType(activeTab, metrics)}
        />
        {this.renderTile()}
        <ParcelLayer {...rest} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    metrics: selectMetrics(state),
    zoom: state.getIn(['map', 'zoom']),
  };
}

const mapActionToProps = {
  loadMetrics,
};

export default connect(mapStateToProps, mapActionToProps)(MapAnalytics);
