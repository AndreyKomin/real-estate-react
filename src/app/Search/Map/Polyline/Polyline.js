/** @flow */
import { PureComponent, PropTypes } from 'react';


export default class Polyline extends PureComponent<*, *, *> {
  /* :: polyline: Object */
  componentDidMount() {
    this.initPolyline();
    this.updatePolyline();
  }

  componentDidUpdate(oldProps: Object) {
    if (oldProps.locations !== this.props.locations) {
      this.updatePolyline();
    }
  }

  componentWillUnmount() {
    this.removePolyline();
  }

  initPolyline() {
    const { Microsoft, locations } = this.props;
    if (!this.polyline) {
      this.polyline = new Microsoft.Maps.Polyline(locations, null);
    }
  }

  updatePolyline() {
    const { map, locations } = this.props;

    this.removePolyline();
    if (locations.length > 1) {
      this.polyline.setLocations(locations);
      map.entities.push(this.polyline);
    }
  }

  removePolyline() {
    const { map } = this.props;
    if (this.polyline) {
      map.entities.remove(this.polyline);
    }
  }

  render() {
    return null;
  }
}


Polyline.propTypes = {
  map: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  Microsoft: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  locations: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};

