/** @flow */
import React, { PureComponent, PropTypes } from 'react';
import { createSelector } from 'reselect';

import Polyline from './Polyline';


export default class Plain extends PureComponent<*, *, *> {
  /* :: selectLocations: Function */
  constructor(props: Object) {
    super(props);
    this.selectLocations = createSelector(
      [props => props.start, props => props.end],
      (start, end) => [start, end],
    );
  }
  render() {
    const { map, Microsoft } = this.props;
    return <Polyline map={map} Microsoft={Microsoft} locations={this.selectLocations(this.props)} />;
  }
}


Polyline.propTypes = {
  map: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  Microsoft: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  locations: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};

