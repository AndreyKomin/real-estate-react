/** @flow */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import getLocation from './getLocation';
import Polygon from '../Polygon';


class DrawPolygon extends PureComponent<*, *, *> {
  getLocations() {
    const { Microsoft, cords } = this.props;
    return Array.from(cords.map(cordString => getLocation(Microsoft, cordString)));
  }

  render() {
    const { map, Microsoft } = this.props;
    const locations = this.getLocations();

    return <Polygon map={map} Microsoft={Microsoft} locations={locations} />;
  }
}

function mapStateToProps(state) {
  return {
    cords: state.getIn(['map', 'polygonCords']),
  };
}

export default connect(mapStateToProps)(DrawPolygon);
