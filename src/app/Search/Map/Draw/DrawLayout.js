/** @flow */
import React, { PureComponent, PropTypes } from 'react';
import classNames from 'classnames';

import Button from 'components/base/Button';
import SVG from 'components/base/SVG';

import css from './style.scss';


class DrawLayout extends PureComponent<*, *, *> {
  render() {
    const { type, disabled, isOpen, onTrigger, onCancel, onOk } = this.props;

    const searchClass = classNames(
      css.search,
      { [css.open]: isOpen },
    );
    const buttonClass = classNames(
      css.buttonDraw,
      { [css.active]: isOpen },
    );
    return (
      <div>
        <button className={buttonClass} disabled={disabled} type={type} onClick={onTrigger}>
          <SVG icon="iconMapDraw" className={css.iconMapDraw} />
        </button>
        <div className={searchClass}>
          <div className={css.searchInner}>
            <div>
              <div className={css.text}>Draw a shape on the map in the area you would like to search.</div>
              <div className={css.text}>Right Click or press Enter to Search.</div>
            </div>
            <div>
              <Button kind="link-default" size="middle" name="cancel" onClick={onCancel}>Cancel</Button>
              <Button kind="border-grey" size="middle" name="search" onClick={onOk}>Search</Button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

DrawLayout.propTypes = {
  isOpen: PropTypes.bool.isRequired,
};

DrawLayout.defaultProps = {
  disabled: false,
  type: 'button',
  isOpen: false,
};

export default DrawLayout;
