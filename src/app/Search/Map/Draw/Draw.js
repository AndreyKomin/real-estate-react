/** @flow */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import * as onKeyPress from 'utils/DOM/onKeyPress';
import { changeSearchParams } from 'data/search';
import { SearchSource } from 'data/search/constants';
import { setPolygonCords, addPolygonCord } from 'data/map';

import EventHandler from '../EventHandler';
import Cursor from './Cursor';
import DrawPolygon from './DrawPolygon';
import SearchPolygon from './SearchPolygon';
import DrawLayout from './DrawLayout';
import LinesToCursor from './LinesToCursor';
import css from './style.scss';


const POLYGON_CORD_NAME_VALUE = 'shapeDefinition';
const EMPTY_CORDS = [];

class Draw extends PureComponent<*, *, *> {
  /* :: clickHandlerId: * */
  constructor(props) {
    super(props);
    this.state = {
      isOpen: this.props.isOpen,
    };
    this.handleMapClick = this.handleMapClick.bind(this);
    this.handleMapRightClick = this.handleMapRightClick.bind(this);
    this.handleTrigger = this.handleTrigger.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleOk = this.handleOk.bind(this);
  }

  componentDidMount() {
    onKeyPress.addListener('Escape', this.handleCancel);
    onKeyPress.addListener('Enter', this.handleOk);
  }

  componentDidUpdate(oldProps: *, oldState: ?{ isOpen: boolean }) {
    if (oldState && oldState.isOpen !== this.state.isOpen) {
      if (typeof this.props.onDrawTrigger === 'function') {
        this.props.onDrawTrigger(this.state.isOpen);
      }
    }
  }

  componentWillUnmount() {
    onKeyPress.removeListener('Escape', this.handleCancel);
    onKeyPress.removeListener('Enter', this.handleOk);
  }

  /* :: handleMapClick: Function */
  handleMapClick(event) {
    if (!this.state.isOpen) return;
    const { location: { latitude, longitude } } = event;
    this.props.addPolygonCord(`${latitude},${longitude}`);
  }

  /* :: handleMapRightClick: Function */
  handleMapRightClick() {
    if (!this.state.isOpen) return;
    this.handleOk();
  }

  /* :: handleTrigger: Function */
  handleTrigger() {
    if (this.state.isOpen) {
      if (this.props.searchCords) {
        this.props.setPolygonCords(this.props.searchCords);
      } else {
        this.props.setPolygonCords(EMPTY_CORDS);
      }
    }
    this.setState({ isOpen: !this.state.isOpen });
  }

  /* :: handleCancel: Function */
  handleCancel() {
    if (this.state.isOpen) {
      this.props.setPolygonCords(EMPTY_CORDS);
      this.props.changeSearchParams({
        name: POLYGON_CORD_NAME_VALUE,
        value: EMPTY_CORDS,
      });
      this.setState({ isOpen: false });
    }
  }

  /* :: handleOk: Function */
  handleOk() {
    if (this.state.isOpen) {
      this.setState({ isOpen: false });
      this.props.changeSearchParams({
        name: POLYGON_CORD_NAME_VALUE,
        value: this.props.cords,
      });
      this.props.changeSearchParams({
        name: 'source',
        value: SearchSource.LISTING,
      });
    }
  }

  render() {
    const { isOpen } = this.state;
    const { map, Microsoft } = this.props;

    return (
      <div>
        <Cursor isDraw={isOpen} className={css.crosshair} map={map} />
        <EventHandler event="click" map={map} Microsoft={Microsoft} onEvent={this.handleMapClick} />
        <EventHandler event="rightclick" map={map} Microsoft={Microsoft} onEvent={this.handleMapRightClick} />
        {
          isOpen ?
            <DrawPolygon map={map} Microsoft={Microsoft} />
            :
            <SearchPolygon map={map} Microsoft={Microsoft} />
        }
        <LinesToCursor isDisplayed={isOpen} map={map} Microsoft={Microsoft} cords={this.props.cords} />
        <DrawLayout
          {...this.props}
          name="drawer"
          isOpen={isOpen}
          onTrigger={this.handleTrigger}
          onCancel={this.handleCancel}
          onOk={this.handleOk}
        />;
      </div>
    );
  }
}


Draw.defaultProps = {
  disabled: false,
  type: 'button',
  isOpen: false,
};

function mapStateToProps(state) {
  return {
    cords: state.getIn(['map', 'polygonCords']),
    searchCords: state.getIn(['search', 'current', POLYGON_CORD_NAME_VALUE], null),
  };
}

const mapActionToProps = {
  setPolygonCords,
  addPolygonCord,
  changeSearchParams,
};

export default connect(mapStateToProps, mapActionToProps)(Draw);
