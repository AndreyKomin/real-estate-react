/** @flow */
import React, { PureComponent } from 'react';
import { createSelector } from 'reselect';

import getLocation from './getLocation';
import EventHandler from '../EventHandler';
import { Plain } from '../Polyline';


export default class LinesToCursor extends PureComponent<*, *, *> {
  /* :: selectLines: Function */
  /* :: getLines: Function */
  constructor(props: Object) {
    super(props);
    this.state = {
      mouseLocation: null,
    };
    this.handleMapMouseMove = this.handleMapMouseMove.bind(this);
    this.getLines = createSelector(
      [props => props.cords, props => props.Microsoft],
      (cords, Microsoft) => cords.map(cord => getLocation(Microsoft, cord)).map(this.getLine, this),
    );
  }

  getLine(location: Object, index: number, locations: *) {
    const { map, Microsoft } = this.props;
    const key = `${location.latitude},${location.longitude}`;
    const start = location;
    const end = locations.get(index + 1, locations.get(0));
    return <Plain key={key} map={map} Microsoft={Microsoft} start={start} end={end} />;
  }

  getStartLine() {
    const { mouseLocation } = this.state;
    if (!mouseLocation) return null;
    const { map, Microsoft, cords } = this.props;
    if (cords.size < 1) return null;
    const start = getLocation(Microsoft, cords.first());
    return <Plain map={map} Microsoft={Microsoft} start={start} end={mouseLocation} />;
  }

  getEndLine() {
    const { mouseLocation } = this.state;
    if (!mouseLocation) return null;
    const { map, Microsoft, cords } = this.props;
    if (cords.size < 2) return null;
    const start = getLocation(Microsoft, cords.last());
    return <Plain map={map} Microsoft={Microsoft} start={start} end={mouseLocation} />;
  }

  updateMouseLocation(event: Object) {
    this.setState({ mouseLocation: event.location });
  }

  /* :: handleMapMouseMove: Function */
  handleMapMouseMove(event: Object) {
    this.updateMouseLocation(event);
  }

  render() {
    const { map, Microsoft, cords, isDisplayed } = this.props;

    if (!isDisplayed) return null;
    if (!cords) return null;

    const lines = this.getLines(this.props);

    return (
      <div>
        <EventHandler map={map} Microsoft={Microsoft} event="mousemove" onEvent={this.handleMapMouseMove} />
        {lines}
        {this.getStartLine()}
        {this.getEndLine()}
      </div>
    );
  }
}

