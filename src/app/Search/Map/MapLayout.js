/** @flow */
import React, { PureComponent } from 'react';

import PushpinList from './PushpinList';
import Infobox from './Infobox';
import Legend from './Legend';
import Draw from './Draw';
import MapFrame from './MapFrame';
import Satellite from './Satellite';
import Zoom from './Zoom';
import MapAnalytics from './MapAnalytics';
import ActiveParcel from './ActiveParcel';

import css from './style.scss';


class Map extends PureComponent {
  render() {
    const {
      Microsoft,
      map,
      pushpins,
      triggerPushpinActivity,
      activePushpin,
      activePushpinId,
      latitude,
      longitude,
      isPropertyLoading,
      loadProperty,
      property,
      estimatedValueGraph,
      onDrawTrigger,
      disabledActiveParcel,
      activePushpinBingMapImageUrl,
      params,
    } = this.props;

    return (
      <div className={css.mapWrap}>
        <ActiveParcel activeId={activePushpinId} map={map} Microsoft={Microsoft} />
        <PushpinList
          map={map}
          Microsoft={Microsoft}
          pushpins={pushpins}
          active={activePushpinId}
          triggerPushpinActivity={triggerPushpinActivity}
          disabledActiveParcel={disabledActiveParcel}
        />
        <MapFrame {...this.props} />
        <Legend />
        <Satellite map={map} Microsoft={Microsoft} />
        <Zoom
          latitude={latitude}
          longitude={longitude}
          activeEntity={activePushpin}
          autoScaleEntities={pushpins}
          map={map}
          Microsoft={Microsoft}
        />
        <MapAnalytics map={map} Microsoft={Microsoft} />
        <Draw map={map} Microsoft={Microsoft} onDrawTrigger={onDrawTrigger} />
        <Infobox
          params={params}
          location={location}
          Microsoft={Microsoft}
          map={map}
          activePushpinId={activePushpinId}
          info={activePushpin}
          bingMapImageUrl={activePushpinBingMapImageUrl}
          triggerPushpinActivity={triggerPushpinActivity}
          isPropertyLoading={isPropertyLoading}
          loadProperty={loadProperty}
          property={property}
          estimatedValueGraph={estimatedValueGraph}
        />
      </div>
    );
  }
}

export default Map;
