/** @flow */
import ActiveParcel from './ActiveParcel';
import waitForMap from '../waitForMap';


export default waitForMap(ActiveParcel);
