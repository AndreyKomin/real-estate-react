/** @flow */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import Polyline from 'Bing/ReactMap/Polyline';

import { selectActiveParcel, selectCanDisplayActiveParcel } from '../selectors';


const EMPTY_ARR = [];

class ActiveParcel extends PureComponent<*, *, *> {
  constructor(props) {
    super(props);
    this.state = {
      parcel: EMPTY_ARR,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.canDisplay) {
      this.setState({ parcel: EMPTY_ARR });
      return;
    }
    if (!nextProps.activeId) return;
    if (nextProps.activeId !== this.props.activeId) {
      const parcel = (nextProps.parcel || '')
        .split('/')
        .map(location => location.split(',').map(Number))
        .map(([latitude, longitude]) => ({ latitude, longitude }));

      this.setState({ parcel });
    }
  }

  shouldComponentUpdate(nextProps: *, nextState: *) {
    return nextState.parcel !== this.state.parcel;
  }

  render() {
    const { map, Microsoft } = this.props;
    const { parcel: locations } = this.state;

    return (<Polyline map={map} Microsoft={Microsoft} color="007700" locations={locations} />);
  }
}

const mapStateToProps = state => ({
  parcel: selectActiveParcel(state),
  canDisplay: selectCanDisplayActiveParcel(state),
});

export default connect(mapStateToProps)(ActiveParcel);
