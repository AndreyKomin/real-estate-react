/** @flow */
import { PureComponent, PropTypes } from 'react';


export default class Polygon extends PureComponent<*, *, *> {
  /* :: polygon: Object */
  componentDidMount() {
    this.initPolygon();
    this.updatePolygon();
  }

  componentDidUpdate(oldProps: Object) {
    if (oldProps.locations !== this.props.locations) {
      this.updatePolygon();
    }
  }

  componentWillUnmount() {
    this.removePolygon();
  }

  initPolygon() {
    const { Microsoft, locations } = this.props;
    if (!this.polygon) {
      this.polygon = new Microsoft.Maps.Polygon(locations, null);
    }
  }

  updatePolygon() {
    const { map, locations } = this.props;

    this.removePolygon();
    if (locations.length > 2) {
      this.polygon.setLocations(locations);
      map.entities.push(this.polygon);
    }
  }

  removePolygon() {
    const { map } = this.props;
    if (this.polygon) {
      map.entities.remove(this.polygon);
    }
  }

  render() {
    return null;
  }
}


Polygon.propTypes = {
  map: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  Microsoft: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  locations: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.shape({})),
  ]).isRequired,
};

Polygon.defaultProps = {
  locations: [],
};
