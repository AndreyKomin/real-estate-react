/** @flow */
import React, { PureComponent } from 'react';
import classNames from 'classnames';

import type { ListingType } from 'data/listing/constants';
import { Name, Color, Active, Type } from 'data/listing/constants';
import ColorDot from 'components/ColorDot';
import SVG from 'components/base/SVG';

import css from './style.scss';


class LegendLayout extends PureComponent {
  render() {
    const { disabled, isOpen } = this.props;

    const dropdownClass = classNames(css.dropdown, { [css.open]: isOpen });
    const buttonClass = classNames(css.dropdownButton, { [css.active]: isOpen });

    const listLegend = Object.keys(Type)
      .map(typeKey => Type[((typeKey: any): ListingType)])
      .map(type => (
        <div className={css.legendItem} key={type}>
          <ColorDot color={Color[type]} className={css.dot} isBig />
          <span>{Name[type]}</span>
        </div>
        ),
      );

    return (
      <div className={css.dropdownLegend}>
        <button className={buttonClass} onClick={this.props.onOpen} disabled={disabled} type="button">
          <SVG icon="iconLegend" className={css.iconLegend} />
          <span className={css.legendText}>Legend</span>
          <SVG icon="iconCaretUp" className={css.iconCaretUp} />
        </button>
        <div className={dropdownClass}>
          <div className={css.legendItem}>
            <ColorDot color={Color[Active]} className={css.dot} isBig />
            <span>Selected Property</span>
          </div>
          {listLegend}
        </div>
      </div>
    );
  }
}

export default LegendLayout;
