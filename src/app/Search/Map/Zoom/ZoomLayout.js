/** @flow */
import React, { PureComponent, PropTypes } from 'react';

import SVG from 'components/base/SVG';

import css from './style.scss';


class ZoomLayout extends PureComponent<*, *, *> {
  render() {
    const { disabledZoomOut, disabledZoomIn, zoomIn, zoomOut } = this.props;
    return (
      <div className={css.zoom}>
        <button
          className={css.buttonZoom}
          disabled={disabledZoomIn}
          type="button"
          onClick={zoomIn}
        >
          <SVG icon="iconZoomIn" className={css.iconZoom} />
        </button>
        <button
          className={css.buttonZoom}
          disabled={disabledZoomOut}
          type="button"
          onClick={zoomOut}
        >
          <SVG icon="iconZoomOut" className={css.iconZoom} />
        </button>
      </div>
    );
  }
}

ZoomLayout.propTypes = {
  disabledZoomIn: PropTypes.bool.isRequired,
  disabledZoomOut: PropTypes.bool.isRequired,
};

ZoomLayout.defaultProps = {
  disabledZoomIn: false,
  disabledZoomOut: false,
};

export default ZoomLayout;
