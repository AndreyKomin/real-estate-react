/** @flow */
import Zoom from './Zoom';

import waitForMap from '../waitForMap';


export default waitForMap(Zoom);
