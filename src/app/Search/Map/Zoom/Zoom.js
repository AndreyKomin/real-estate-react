/** @flow */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { List, is } from 'immutable';
import _ from 'underscore';

import { zoomIn, zoomOut, setZoom } from 'data/map';
import { changeVisibleLocation } from 'data/search';

import ZoomLayout from './ZoomLayout';

import { selectCenter } from '../selectors';


const emptyList = List();

const VIEW_CHANGE_EVENT = 'viewchange';
const DEBOUNCE_TIMEOUT = 200;


class Zoom extends Component<*, *, *> {
  /* :: is__mounted: bool */
  static getDiffs(centerLatitude: any, centerLongitude: any, cords) {
    let latitude = 0;
    let longitude = 0;

    cords.forEach((cord) => {
      latitude = Math.max(latitude, Math.abs(centerLatitude - cord.get('latitude')));
      longitude = Math.max(longitude, Math.abs(centerLongitude - cord.get('longitude')));
    });

    return { latitude, longitude };
  }

  static getMiddle(cords, type) {
    return cords.reduce((summ, cord) => cord.get(type, 0) + summ, 0) / cords.size;
  }

  static getEntitiesIdentity(entities) {
    return entities
      .filter(entity => !entity.get('__onlyPlace'))
      .map(
        entity => entity.get(
          'id',
          `${entity.get('latitude')}-${entity.get('longitude')}`),
      )
      .join('|');
  }

  constructor(props) {
    super(props);
    this.handleViewChange = _.debounce(this.handleViewChange.bind(this), DEBOUNCE_TIMEOUT);
    this.handleLocationChange = _.debounce(this.handleLocationChange.bind(this), DEBOUNCE_TIMEOUT, true);
  }

  componentDidMount() {
    const { Microsoft, map, setZoom } = this.props;
    setZoom(map.getZoom());
    this.handlerId = Microsoft.Maps.Events.addHandler(
      map,
      VIEW_CHANGE_EVENT,
      this.handleViewChange,
    );
    this.moveCenter();
  }

  shouldComponentUpdate(nextProps) {
    return nextProps.zoom !== this.props.zoom || nextProps.center !== this.props.center ||
      nextProps.autoScaleEntities !== this.props.autoScaleEntities ||
      nextProps.activeEntity !== this.props.activeEntity;
  }

  componentDidUpdate(oldProps) {
    if (oldProps.center !== this.props.center) {
      this.moveCenter();
    }

    if (this.props.zoom !== oldProps.zoom) {
      this.updateZoom();
    }

    if (!is(oldProps.activeEntity, this.props.activeEntity)) {
      if (this.props.activeEntity) this.moveByInfobox();
    }

    const currentIdentity = Zoom.getEntitiesIdentity(this.props.autoScaleEntities);
    const previousIdentity = Zoom.getEntitiesIdentity(oldProps.autoScaleEntities);
    if (currentIdentity !== previousIdentity) {
      if (this.props.autoScaleEntities !== emptyList) {
        this.rescaleMap();
      }
    }
  }

  componentWillUnmount() {
    const { Microsoft } = this.props;
    this.is__mounted = false;
    Microsoft.Maps.Events.removeHandler(this.handlerId);
  }

  getLocationsToBound() {
    const { longitude, latitude, autoScaleEntities, Microsoft } = this.props;
    if (!longitude || !latitude) return [];
    const { latitude: latitudeDiff, longitude: longitudeDiff } = Zoom.getDiffs(latitude, longitude, autoScaleEntities);

    return [
      new Microsoft.Maps.Location(latitude - latitudeDiff, longitude - longitudeDiff),
      new Microsoft.Maps.Location(latitude + latitudeDiff, longitude - longitudeDiff),
      new Microsoft.Maps.Location(latitude - latitudeDiff, longitude + longitudeDiff),
      new Microsoft.Maps.Location(latitude + latitudeDiff, longitude + longitudeDiff),
    ];
  }

  handlerId: *;

  rescaleMap() {
    const { map, Microsoft } = this.props;
    const locationsToBound = this.getLocationsToBound();
    map.setView({
      bounds: Microsoft.Maps.LocationRect.fromLocations(locationsToBound),
    });
    this.handleLocationChange();
  }

  updateZoom() {
    const { map, zoom } = this.props;
    if (map.getZoom() !== zoom) {
      map.setView({ zoom });
    }
    this.handleLocationChange();
  }

  moveCenter() {
    const { map, longitude, latitude, Microsoft } = this.props;

    this.handleLocationChange();

    if (!longitude || !latitude) return;
    map.setView({
      center: new Microsoft.Maps.Location(latitude, longitude),
    });
  }

  moveByInfobox() {
    const { map, activeEntity, Microsoft } = this.props;

    const bounds = map.getBounds();
    const center = map.getCenter().latitude;
    const south = bounds.getSouth();

    const latitude = activeEntity.get('latitude') + ((center - south) * (2 / 3));
    const longitude = activeEntity.get('longitude');

    map.setView({
      center: new Microsoft.Maps.Location(latitude, longitude),
    });
  }

  /* :: handleViewChange: Function */
  handleViewChange() {
    if (!this.is__mounted) return;
    const { map } = this.props;
    const nextZoom = map.getZoom();
    if (this.props.zoom !== nextZoom) {
      this.props.setZoom(nextZoom);
    }
    this.handleLocationChange();
  }

  /* :: handleLocationChange: Function */
  handleLocationChange() {
    if (!this.is__mounted) return;
    const { map } = this.props;
    const locationRect = map.getBounds();

    if (locationRect) {
      const north = locationRect.getNorth();
      const west = locationRect.getWest();
      const east = locationRect.getEast();
      const south = locationRect.getSouth();
      this.props.onLocationRectChange({ north, west, east, south });
    }
  }

  render() {
    return <ZoomLayout {...this.props} />;
  }
}

function mapStateToProps(state) {
  const center = selectCenter(state);
  return {
    disabledZoomIn: !state.getIn(['map', 'canZoomIn']),
    disabledZoomOut: !state.getIn(['map', 'canZoomOut']),
    zoom: state.getIn(['map', 'zoom']),
    center,
    latitude: center.get('latitude'),
    longitude: center.get('longitude'),
  };
}

const mapActionToProps = {
  zoomIn,
  zoomOut,
  setZoom,
  onLocationRectChange: changeVisibleLocation,
};

Zoom.defaultProps = {
  autoScaleEntities: emptyList,
};

export default connect(mapStateToProps, mapActionToProps)(Zoom);
