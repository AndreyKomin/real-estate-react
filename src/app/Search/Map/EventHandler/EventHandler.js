/** @flow */
import { PureComponent, PropTypes } from 'react';


export default class EventHandler extends PureComponent<*, *, *> {
  /* :: handlerId: * */
  componentDidMount() {
    this.addEventHandler();
  }

  componentWillUnmount() {
    this.removeEventHandler();
  }

  addEventHandler() {
    const { map, event, onEvent, Microsoft } = this.props;

    this.handlerId = Microsoft.Maps.Events.addHandler(
      map,
      event,
      onEvent,
    );
  }

  removeEventHandler() {
    if (this.handlerId) {
      const { Microsoft } = this.props;
      Microsoft.Maps.Events.removeHandler(this.handlerId);
    }
  }

  render() {
    return null;
  }
}


EventHandler.propTypes = {
  map: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  Microsoft: PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
  event: PropTypes.string.isRequired,
  onEvent: PropTypes.func.isRequired,
};
