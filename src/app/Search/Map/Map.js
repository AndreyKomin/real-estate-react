/** @flow */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { mapPromise } from 'Bing';

import { triggerPushpinPopover, loadProperty } from 'data/map/actions';
import { selectIsPropertyLoading, selectProperty, selectEstimatedValueGraph } from 'data/map/selectors';

import {
  selectActivePushpinId,
  selectActivePushpin,
  selectPushpins,
  selectActivePushpinBingMapImageUrl,
} from './selectors';
import MapLayout from './MapLayout';


class Map extends PureComponent<*, *, *> {
  constructor(props: Object) {
    super(props);
    this.state = {
      Microsoft: null,
      map: null,
      disabledActiveParcel: false,
    };
    this.promise = mapPromise;
    this.onMapRef = this.onMapRef.bind(this);
    this.handleDrawTrigger = this.handleDrawTrigger.bind(this);
  }

  state: {
    Microsoft: ?Object,
    map: ?Object,
    disabledActiveParcel: boolean,
  };

  componentDidMount() {
    this.promise.then((Microsoft) => {
      this.setState({ Microsoft });
    });
  }

  componentWillUnmount() {
    const { map } = this.state;
    if (map) {
      requestIdleCallback(() => map.dispose());
    }
  }

  /* :: onMapRef: Function */
  onMapRef(map: *) {
    this.setState({ map });
  }

  promise: Promise<*>;

  /* :: handleDrawTrigger: Function */
  handleDrawTrigger(isDrawing: boolean) {
    this.setState({ disabledActiveParcel: isDrawing });
  }

  render() {
    const { Microsoft, map, disabledActiveParcel } = this.state;
    if (!Microsoft) return null;
    return (
      <MapLayout
        {...this.props}
        onMapRef={this.onMapRef}
        map={map}
        Microsoft={Microsoft}
        onDrawTrigger={this.handleDrawTrigger}
        disabledActiveParcel={disabledActiveParcel}
      />
    );
  }
}

function mapStateToProps(state) {
  const entities = selectPushpins(state);

  return {
    activePushpinId: selectActivePushpinId(state),
    activePushpin: selectActivePushpin(state),
    activePushpinBingMapImageUrl: selectActivePushpinBingMapImageUrl(state),
    pushpins: entities,
    mapType: state.getIn(['map', 'mapType']),
    isPropertyLoading: selectIsPropertyLoading(state),
    property: selectProperty(state),
    estimatedValueGraph: selectEstimatedValueGraph(state),
  };
}

const mapActionToProps = {
  triggerPushpinActivity: triggerPushpinPopover,
  loadProperty,
};

export default connect(mapStateToProps, mapActionToProps)(Map);
