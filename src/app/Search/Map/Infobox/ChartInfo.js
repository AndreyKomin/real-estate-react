/** @flow */
import React, { PureComponent, PropTypes } from 'react';
import ImPropTypes from 'react-immutable-proptypes';

import loadable from 'components/hoc/loadable';
import Chart from 'components/Chart';
import numberToPrice from 'utils/currency/numberToPrice';

import css from './style.scss';


class ChartInfo extends PureComponent<*, *, *> {
  renderChart() {
    const { estimatedValueGraph } = this.props;
    if (estimatedValueGraph) {
      return (
        <Chart
          data={estimatedValueGraph.points}
          type={estimatedValueGraph.type}
          categories={estimatedValueGraph.categories}
          colors={estimatedValueGraph.colors.toJS()}
          yTickFormat={Chart.yTickFormat.bigNumber}
          caption="Estimated Value"
        />
      );
    }

    return null;
  }

  renderTiles() {
    const { property } = this.props;
    const { estimatedEquity, compSaleAmount } = property.toJS();

    if (!estimatedEquity && !compSaleAmount) return null;

    return (
      <div className={css.tiles}>
        <div className={css.tile}>
          <div className={css.wrapper}>
            <span className={css.value}>{numberToPrice(estimatedEquity, '-')}</span>
            <span className={css.title}>Est. Equity</span>
          </div>
        </div>
        <div className={css.tile}>
          <div className={css.wrapper}>
            <span className={css.value}>{numberToPrice(compSaleAmount, '-')}</span>
            <span className={css.title}>Avg. Comp</span>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const { estimatedValueGraph, property } = this.props;
    const { estimatedEquity, compSaleAmount } = property.toJS();

    if (!estimatedValueGraph && !estimatedEquity && !compSaleAmount) return null;

    return (
      <div className={css.chartInfo}>
        {this.renderChart()}
        {this.renderTiles()}
      </div>
    );
  }
}

ChartInfo.propTypes = {
  property: ImPropTypes.mapContains({
    estimatedEquity: PropTypes.number,
    compSaleAmount: PropTypes.number,
  }).isRequired,
  estimatedValueGraph: PropTypes.shape({
    points: ImPropTypes.list,
    type: PropTypes.string,
    categories: ImPropTypes.list,
    colors: ImPropTypes.map,
  }),
};


export default loadable(ChartInfo);
