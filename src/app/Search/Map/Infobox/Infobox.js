/** @flow */
import React, { PureComponent } from 'react';
import { withRouter } from 'react-router';
import ReactDom from 'react-dom';

import { triggerMapType } from 'data/map';
import { selectPermissions } from 'data/user/selectors';
import withProperty from 'app/components/withProperty';

import InfoboxLayout from './InfoboxLayout';
import { INFOBOX_ID } from '../constants';
import EventHandler from '../EventHandler';


// const FIELDS_TO_CHECK = [
//   'shouldDisplayPopover',
//   'info',
//   'estimatedValueGraph',
// ];


class Infobox extends PureComponent<*, *, *> {
  constructor(props) {
    super(props);
    this.closeInfobox = this.closeInfobox.bind(this);
  }

  // shouldComponentUpdate(nextProps: *) {
  //   return FIELDS_TO_CHECK.some(field => !is(nextProps[field], this.props[field]));
  // }

  componentDidUpdate() {
    this.removeInfobox();
    if (this.props.shouldDisplayPopover) {
      this.drawInfobox();
    }
  }

  componentWillUnmount() {
    this.removeInfobox(true);
  }

  infobox: ?{ setMap: Function };

  createInfobox() {
    if (this.infobox) return;

    const { Microsoft, info } = this.props;

    this.infobox = new Microsoft.Maps.Infobox(
      new Microsoft.Maps.Location(
        info.get('latitude'),
        info.get('longitude'),
      ),
      { htmlContent: `<div id="${INFOBOX_ID}" />` },
    );
  }

  drawInfobox() {
    const { map } = this.props;
    this.createInfobox();
    if (this.infobox) {
      this.infobox.setMap(map);
      ReactDom.unstable_renderSubtreeIntoContainer(
        this,
        <InfoboxLayout {...this.props} />,
        document.getElementById(INFOBOX_ID),
      );
    }
  }

  removeInfobox(unmountComponent: ?boolean) {
    const element = document.getElementById(INFOBOX_ID);
    if (element && unmountComponent) {
      ReactDom.unmountComponentAtNode(element);
    }
    if (this.infobox) {
      this.infobox.setMap(null);
      this.infobox = null;
    }
  }

  /* :: closeInfobox: Function */
  closeInfobox() {
    const { activePushpinId } = this.props;
    this.props.triggerPushpinActivity(activePushpinId);
  }

  render() {
    const { map, Microsoft } = this.props;
    return <EventHandler map={map} Microsoft={Microsoft} event="click" onEvent={this.closeInfobox} />;
  }
}

export default withRouter(withProperty(Infobox, (state, props) => ({
  shouldDisplayPopover: props.activePushpinId !== null,
  activePushpinId: props.activePushpinId,
  info: props.info,
  propertyId: props.info && (props.info.get('propertyId') || props.info.get('id')),
  permissions: selectPermissions(state),
}), { triggerMapType }));
