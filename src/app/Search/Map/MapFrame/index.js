/** @flow */
import React, { Component } from 'react';
import { drawMap } from 'Bing';

import css from '../style.scss';


class MapFrame extends Component {
  constructor(props: Object) {
    super(props);
    this.onRef = this.onRef.bind(this);
  }

  componentDidMount() {
    drawMap(this.root)
      .then(this.props.onMapRef);
  }

  shouldComponentUpdate() {
    return false;
  }

  /* :: onRef: Function */
  onRef(element: ?HTMLElement) {
    this.root = element;
  }

  root: ?HTMLElement;

  render() {
    return (<div ref={this.onRef} className={css.map} />);
  }
}


export default MapFrame;
