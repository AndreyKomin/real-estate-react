/** @flow */
import { fromJS } from 'immutable';
import { createSelector } from 'reselect';

import { Display, Overlay, Neighbors } from 'data/listing';
import { selectType, selectSearchAllowed } from 'data/search';
import { SearchType } from 'data/search/constants';
import { selectLoading } from 'data/property';


const emptyList = fromJS([]);
const checkState = state => state;

const selectListing = createSelector(
  checkState,
  state => state.getIn(['listing', 'list']),
);

const selectRawProperties = createSelector(
  checkState,
  state => state.getIn(['listing', 'details']),
);

const selectProperties = createSelector(
  selectRawProperties,
  properties => properties.update(
    'data',
    data => data.map(
      (entities, type) => {
        if (type === 'neighbors') {
          return entities.map(
            (entity, index) => entity.set('resultIndex', index + 1).set('type', Neighbors),
          );
        }
        return entities.map(
          (entity, index) => entity.set('resultIndex', index + 1),
        );
      },
    ),
  ),
);

export const selectAllSelectedState = createSelector(
  selectListing,
  listing => listing.get('allSelectedState'),
);

export const selectListResult = createSelector(
  checkState,
  selectListing,
  selectProperties,
  (state, listing, properties) => {
    const searchType = selectType(state);
    const allSelectedState = selectAllSelectedState(state);
    const selectedIds = listing.get('selected').map(list => list.get('id'));

    if (searchType === SearchType.PROPERTY) {
      let tabProperties = properties.getIn(['data', properties.get('activeTab')]) || emptyList;
      if (properties.get('activeTab') === 'neighbors') {
        tabProperties = tabProperties.map(p => p.set('type', Neighbors));
      }
      return properties.getIn(['data', 'properties'])
        .concat(tabProperties);
    } else if (searchType === SearchType.LISTING) {
      return listing.get('items').map(
        value => value.set('selected', allSelectedState || selectedIds.includes(value.get('id'))),
      );
    }

    return emptyList;
  },
);

export const selectLimit = createSelector(
  selectListing,
  listing => listing.getIn(['limit'], 1),
);

export const selectSelectedIds = createSelector(
  selectListing,
  listing => listing.get('selected').map(list => list.get('id')),
);

export const selectPropertyCount = createSelector(
  selectListing,
  listing => listing.getIn(['items', '0', 'resultCount'], 0),
);

export const selectTotalCount = createSelector(
  selectListing,
  listing => listing.getIn(['totalCount'], 0),
);

export const selectRangeFrom = createSelector(
  selectListing,
  listing => listing.get('rangeFrom'),
);

export const selectRangeTo = createSelector(
  selectListing,
  listing => listing.get('rangeTo'),
);

export const selectRangeEnabled = createSelector(
  selectListing,
  listing => listing.get('rangeEnabled'),
);

export const selectSelectedCount = createSelector(
  [selectListing, selectAllSelectedState, selectTotalCount, selectRangeFrom, selectRangeTo],
  (listing, allSelectedState, totalCount, rangeFrom, rangeTo) => (allSelectedState ? (rangeTo || totalCount) - ((rangeFrom || 1) - 1) : listing.getIn(['selected']).size),
);

export const selectCanSaveDefault = createSelector(
  selectSelectedCount,
  count => count > 0,
);

export const selectAllListingsSelected = createSelector(
  [selectAllSelectedState, selectSelectedCount, selectTotalCount],
  (allSelectedState, selectedCount, totalCount) => allSelectedState || selectedCount === totalCount,
);

export const selectOverlay = createSelector(
  selectListing,
  listing => listing.getIn(['overlay']),
);

export const selectActiveDisplay = createSelector(
  selectListing,
  listing => listing.getIn(['display']),
);

export const selectActiveTab = createSelector(
  checkState,
  (state) => {
    const searchType = selectType(state);
    if (searchType !== SearchType.LISTING) return Display.NONE;

    const overlay = selectOverlay(state);
    if (overlay === Overlay.TABLE) return Display.LIST;

    return selectActiveDisplay(state);
  },
);

export const selectOverlayParams = createSelector(
  selectOverlay,
  overlay => ({
    isAny: overlay !== Overlay.NONE,
    isChart: overlay === Overlay.CHARTS,
    isTable: overlay === Overlay.TABLE,
    chartType: Overlay.CHARTS,
    tableType: Overlay.TABLE,
  }),
);

export const selectSearchParams = createSelector(
  selectType,
  searchType => ({
    isAny: searchType !== SearchType.NONE,
    isListing: searchType === SearchType.LISTING,
    isProperty: searchType === SearchType.PROPERTY,
  }),
);

export const selectListingResultDisplayParams = createSelector(
  selectActiveTab,
  activeTab => ({
    isAny: activeTab !== Display.NONE,
    isPicture: activeTab === Display.PICTURE,
    isList: activeTab === Display.LIST,
  }),
);

export const selectListingType = createSelector(
  state => state.getIn(['search', 'current']),
  current => current.get('listingType', emptyList).toJS().join(''),
);

export const selectListingResultParams = createSelector(
  [selectListing, selectListResult, selectListingResultDisplayParams, selectSelectedCount, selectPropertyCount, selectListingType],
  (isLoading, results, listingParams, selectedCount, propertyCount, listingType) => ({
    ...listingParams,
    results,
    selectedCount,
    propertyCount,
    isLoading,
    listingType,
  }),
);

export const selectPropertiesLoading = createSelector(
  selectProperties,
  properties => properties.get('propertiesLoading', false),
);

export const selectPropertiesParams = createSelector(
  [selectProperties, selectPropertiesLoading],
  (results, isLoading) => ({
    results,
    isLoading,
  }),
);

export const selectSearchValue = createSelector(
  checkState,
  state => state.getIn(['bing', 'fullData']),
);

export const selectSearchQueryExtraParams = createSelector(
  checkState,
  state => state.getIn(['search', 'current']),
);

export const selectCityName = createSelector(
  checkState,
  state => state.getIn(['search', 'current', 'cityName']),
);

export const selectStateCode = createSelector(
  checkState,
  state => state.getIn(['search', 'current', 'stateCode']),
);

export const selectSearchQuery = createSelector(
  [selectSearchAllowed, selectSearchValue, selectSearchQueryExtraParams, selectLimit, selectRangeFrom],
  (isAllowed, searchValue, searchParams) => {
    if (!searchParams) return null;
    if (!isAllowed) return null;
    return fromJS({ searchValue, searchParams });
  },
);

export const selectSearchQueryParams = createSelector(
  checkState,
  state => selectSearchQuery(state),
);

export const selectCurrentPage = createSelector(
  selectListing,
  listing => listing.getIn(['page'], 1),
);

export const selectTotalPageCount = createSelector(
  [selectTotalCount, selectLimit, selectRangeFrom, selectRangeTo],
  (total, limit, rangeFrom, rangeTo) => Math.ceil(((rangeTo || total) - ((rangeFrom || 1) - 1)) / limit),
);

export const selectIsLoading = createSelector(
  selectListing,
  listing => listing.get('isLoading'),
);

export const selectIsPropertiesOrGroupLoading = createSelector(
  [selectListing, selectLoading],
  (listing, isGroupLoading) => listing.get('isAllPropertiesLoading') || isGroupLoading,
);

export const selectIsError = createSelector(
  selectListing,
  listing => !!listing.get('error'),
);
