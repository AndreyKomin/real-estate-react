/** @flow */
import { Component } from 'react';
import { connect } from 'react-redux';

import loadable from 'components/hoc/loadable';
import { setBackUrl, checkAuth, selectIsAuthed, selectIsLoading } from 'data/user';


export class AuthGuard extends Component<*, *, *> {
  componentWillMount() {
    this.props.checkAuth({
      afterError: () => {
        this.props.setBackUrl(this.props.location);
        this.props.history.replace('/login');
      },
    });
  }

  render() {
    const { isLoading, isAuthed, children } = this.props;
    if (!isAuthed) return null;
    if (isLoading) return null;
    return children;
  }
}

const mapStateToProps = state => ({
  isLoading: selectIsLoading(state),
  isAuthed: selectIsAuthed(state),
});

const mapActionToProps = {
  checkAuth,
  setBackUrl,
};

export default connect(mapStateToProps, mapActionToProps)(loadable(AuthGuard));
