/** @flow */
import React, { PropTypes, PureComponent } from 'react';
import Helmet from 'react-helmet';
import { getBrandName, getBrandIconUrl } from 'utils/brand';

import Sidebar from './components/Sidebar';
import css from './style.scss';


const meta = [];
const htmlAttributes = { lang: 'en' };

class AppLayout extends PureComponent {
  render() {
    const { children, location } = this.props;
    const title = getBrandName();

    return (
      <div>

        <Helmet
          defaultTitle={title}
          titleTemplate={`%s - ${title}`}
          meta={meta}
          htmlAttributes={htmlAttributes}
        >
          <link rel="icon" type="image/png" href={getBrandIconUrl()} />
        </Helmet>

        <div className={css.wrapper}>
          <Sidebar location={`${location.pathname}${location.search}`} />
          <div>
            {children}
          </div>
        </div>
      </div>
    );
  }
}

AppLayout.propTypes = {
  children: PropTypes.node,
};

export default AppLayout;
