/** @flow */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';

import { deferExecutor } from 'app/DeferredOnLocation';
import { authenticate, selectBackUrl } from 'data/user';

import Layout from './Layout';


class Sso extends PureComponent {

  componentDidMount() {
    const { authenticate, history, location: { query: { c: activationCode } } } = this.props;

    authenticate({
      activationCode,
      afterSuccess: () => history.replace('/'),
      // eslint-disable-next-line no-alert
      afterError: data => alert(JSON.parse(data.response.text).message),
    });
  }

  render() {
    return (
      <div>
        <Helmet title="Loading..." />
        <Layout {...this.props} />
      </div>
    );
  }
}

export default connect(state => ({ backUrl: selectBackUrl(state) }), { authenticate })(deferExecutor(Sso));
