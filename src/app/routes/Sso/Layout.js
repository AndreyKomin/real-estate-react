import React, { PureComponent } from 'react';

import SVGsprite from 'components/SVGsprite';
import SVG from 'components/base/SVG';
import css from './style.scss';


class SsoPageLayout extends PureComponent {
  render() {
    return (
      <div>
        <SVGsprite />
        <div className={css.root}>
          <div className={css.inner}>
            <SVG icon="spinner" />
          </div>
        </div>
      </div>
    );
  }
}


export default SsoPageLayout;
