import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { getPopupRegistration, openPopup, closePopup } from 'app/PopupHolder';

import Modal from 'components/base/Modal';
import Button from 'components/base/Button';
import css from './style.scss';


class ModalError extends PureComponent {
  render() {
    return (
      <Modal
        isOpen
        isCloseButton
        uniqId="modalPublish"
        caption="Invalid activation code"
        width="400px"
      >
        <div className={css.caption}>
          The Activation Code You Entered Is Incorrect. Please Re-enter your Activation Code
        </div>
        <div className={css.buttons}>
          <Button
            role="button"
            kind={Button.kind.blue}
            size={Button.size.large}
            onClick={this.props.closePopup}
          >Close</Button>
        </div>
      </Modal>
    );
  }
}


function mapStateToProps() {
  return {};
}
const connectedModalPublish = connect(mapStateToProps, {})(ModalError);

ModalError.registrationId = getPopupRegistration(connectedModalPublish);
ModalError.open = (obj = {}, ...rest) => openPopup(ModalError.registrationId, { ...obj }, ...rest);
ModalError.close = () => closePopup({ popup: ModalError.registrationId });


export default ModalError;
