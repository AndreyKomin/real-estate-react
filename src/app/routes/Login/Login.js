/** @flow */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';

import { deferExecutor } from 'app/DeferredOnLocation';
import PopupHolder from 'app/PopupHolder';
import ErrorDisplay from 'app/components/ErrorDisplay';
import { authenticate, selectIsLoading, selectBackUrl } from 'data/user';
import getBrandUrl from 'utils/brand';

import Layout from './Layout';
import ModalError from './ModalError';


class Login extends PureComponent {
  /* :: loginRedirect: string */

  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);

    this.loginRedirect = process.env.NODE_ENV !== 'dev-server' ? getBrandUrl('www', 'login') : '';
  }

  componentWillMount() {
    if (this.loginRedirect) window.location.href = this.loginRedirect;
  }

  /* :: handleSubmit: Function */
  handleSubmit(event: *) {
    event.preventDefault();
    if (this.props.isLoading) return;
    const { target: form } = event;
    const activationCode = form.activationCode.value;
    this.props.authenticate({
      activationCode,
      afterSuccess: () => {
        this.props.history.replace(this.props.backUrl);
      },
      afterError: (data) => {
        const errorMessage = JSON.parse(data.response.text);
        const errorText = errorMessage.message;
        if (errorText === 'Invalid activation code.') this.props.openModal();
      },
    });
  }

  render() {
    return this.loginRedirect !== '' ? null : (
      <div>
        <Helmet title="Login" />
        <PopupHolder />
        <ErrorDisplay />
        <Layout {...this.props} onSubmit={this.handleSubmit} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoading: selectIsLoading(state),
    backUrl: selectBackUrl(state),
  };
}

const mapActionToProps = {
  authenticate,
  selectIsLoading,
  openModal: ModalError.open,
};

export default connect(mapStateToProps, mapActionToProps)(deferExecutor(Login));
