import React, { PureComponent } from 'react';

import SVGsprite from 'components/SVGsprite';
import Image from 'components/base/Image';
import Button from 'components/base/Button';
import { getBrandLogoUrl, getBrandLogoAltUrl } from 'utils/brand';
import css from './style.scss';


const year = new Date().getFullYear();

class LoginPageLayout extends PureComponent {
  render() {
    const { onSubmit, isLoading } = this.props;
    return (
      <div>
        <SVGsprite />

        <div className={css.root}>
          <div className={css.inner}>
            <Image src={getBrandLogoUrl()} placeholder={getBrandLogoAltUrl()} className={css.logo} />
            <div className={css.formContainer}>
              <form onSubmit={onSubmit}>
                <label htmlFor="activation-code" className={css.label}>Activation Code</label>
                <input type="text" name="activationCode" id="activation-code" className={css.input} />
                <Button
                  type="submit"
                  disabled={isLoading}
                  kind={Button.kind.blue}
                  size={Button.size.large}
                  className={css.btn}
                  isLoading={isLoading}
                  spinnerColor="white"
                >
                  Login
                </Button>
              </form>
            </div>
          </div>
          <div className={css.copyright}>Copyright © {year}. All Rights Reserved</div>
        </div>
      </div>
    );
  }
}


export default LoginPageLayout;
