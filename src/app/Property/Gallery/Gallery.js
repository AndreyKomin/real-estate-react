import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { List } from 'immutable';
import classNames from 'classnames';
import { Active, Color } from 'data/listing/constants';
import StreetViewMap from 'Bing/ReactMap/StreetViewMap';
import BirdViewMap from 'Bing/ReactMap/BirdViewMap';
import Modal from 'components/base/Modal';
import Gallery from 'components/Gallery';
import Button from 'components/base/Button';
import { formatFullAddressObject } from 'utils/address/formatAddress';
import { getPopupRegistration, openPopup, closePopup, Priority } from 'app/PopupHolder';
import { selectLoading, selectProperty, selectStreetView, selectStreetViewLoading, getStreetView } from 'data/property';

import css from './style.scss';
import UrlStorage from '../Detail/UrlStorage';


const Modes = {
  STREET: 'Street View',
  BIRD: 'Bird\'s Eye View',
  GALLERY: 'Gallery',
};

export class PropertyGallery extends PureComponent {
  componentWillMount() {
    this.load(this.props);
  }

  componentWillReceiveProps(props) {
    this.load(props);
  }

  load(props) {
    const { params: { id }, getStreetView, streetView, loading, property } = props;
    if (!this.state && !loading) {
      if (!streetView) getStreetView(id);
      else {
        const { latitude, longitude, targetLatitude, targetLongitude } = streetView.toJS();
        this.modes = [Modes.BIRD];
        this.images = property.has('imageUrls') ? property.get('imageUrls').split(',').map(url => ({ original: url, thumbnail: url })) : null;
        this.center = { latitude: property.get('latitude'), longitude: property.get('longitude') };

        if (this.images) this.modes.unshift(Modes.GALLERY);
        if (targetLatitude) {
          this.modes.push(Modes.STREET);
          this.streetCenter = { latitude, longitude };
          this.streetTarget = { latitude: targetLatitude, longitude: targetLongitude };
        }

        this.setState({ mode: this.modes[0] });
      }
    }
  }

  render() {
    const { urls, closePopup, loading, property } = this.props;
    const { center, streetCenter, streetTarget, modes, images, state } = this;
    const { mode } = state || {};
    const btn = { kind: Button.kind.blueGhost, size: Button.size.large, isLoading: loading };
    const map = { zoom: 18, color: Color[Active], entities: List(property) };

    return (
      <UrlStorage urls={urls}>
        <Modal
          isLoading={loading}
          onClose={closePopup}
          isOpen
          isCloseButton
          uniqId="PropertyGallery"
          width="1300px"
          caption={(
            <div className={css.header}>
              <div className={css.headerTitle}>{formatFullAddressObject(property.get('address'), true)}</div>
            </div>
          )}
        >
          {!mode ? null : (
            <div className={css.modalWrapper}>
              <div className={classNames(css.container, { [css.galleryContainer]: mode === Modes.GALLERY })}>
                {mode !== Modes.STREET ? null : <StreetViewMap {...map} center={streetCenter} entity={streetTarget} />}
                {mode !== Modes.BIRD ? null : <BirdViewMap {...map} center={center} entity={center} />}
                {mode !== Modes.GALLERY ? null : <Gallery containerClassName={css.gallery} images={images} />}
              </div>
              <div className={css.buttons}>
                {modes.filter(m => m !== mode).map(mode => <Button key={mode} {...btn} onClick={() => this.setState({ mode })}>{mode}</Button>)}
              </div>
            </div>
          )}
        </Modal>
      </UrlStorage>
    );
  }
}

const PropertyGalleryPopup = connect(state => ({
  loading: selectLoading(state) || selectStreetViewLoading(state),
  property: selectProperty(state),
  streetView: selectStreetView(state),
}), { getStreetView })(PropertyGallery);

const registrationId = getPopupRegistration(PropertyGalleryPopup);
(PropertyGalleryPopup).open = props => openPopup(registrationId, { priority: Priority.HIGH, ...props });
(PropertyGalleryPopup).close = () => closePopup({ popup: registrationId });

export default PropertyGalleryPopup;
