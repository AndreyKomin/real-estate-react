import React, { PureComponent, PropTypes } from 'react';
import SVG from 'components/base/SVG';
import ColorDot from 'components/ColorDot';

import css from './style.scss';


const icon = (rate) => {
  switch (rate) {
    case 'positive':
      return <SVG icon="iconArrowUp" className={css.iconArrowUp} />;
    case 'negative':
      return <SVG icon="iconArrowDown" className={css.iconArrowDown} />;
    case 'neutral':
      return <SVG icon="iconNeutral" className={css.iconNeutral} />;
    default:
      return '';
  }
};

class FlowView extends PureComponent {
  render() {
    const { data } = this.props;
    const flow = data.map(result =>
      (<div className={css.item} key={result.id}>
        <div className={css.rate}>{icon(result.rate)}</div>
        <div className={css.year}>{result.year}</div>
      </div>),
    );

    return (
      <div>
        <div className={css.flowView}>
          {flow}
        </div>
        <div className={css.dots}>
          <div className={css.dotItem}>
            <ColorDot color="#D0021B" isBig={false} />
            <div className={css.dotCaption}>Negative</div>
          </div>
          <div className={css.dotItem}>
            <ColorDot color="#109E2F" isBig={false} />
            <div className={css.dotCaption}>Positive</div>
          </div>
          <div className={css.dotItem}>
            <ColorDot color="#5394F5" isBig={false} />
            <div className={css.dotCaption}>Neutral</div>
          </div>
        </div>
      </div>
    );
  }
}

FlowView.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      year: PropTypes.number,
      rate: PropTypes.string,
    })).isRequired,
};

export default FlowView;
