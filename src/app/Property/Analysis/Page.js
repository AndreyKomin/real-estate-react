/** @flow */
import { PureComponent } from 'react';
import { connect } from 'react-redux';

// TODO: Needed? Analysis should be able to run independently of a property.
import urlSubscribe from 'app/Property/Detail/urlSubscribe';

import Modal from './Modal/index';


export class Page extends PureComponent<*, *, *> {
  componentDidMount() {
    const { params, openPopup, history, routes, urls, location } = this.props;
    openPopup({
      onClose: () => history.push(urls.getState().detailsRoot),
      urls,
      params: { ...params },
      history,
      location,
      route: routes[routes.length - 1] || {},
    });
  }

  componentWillUnmount() {
    this.props.closePopup();
  }

  render() {
    return null;
  }
}

Page.defaultProps = {
  params: {},
  history: {},
};

const mapStateToProps = () => ({});

const mapActionToProps = {
  openPopup: Modal.open,
  closePopup: Modal.close,
};

export default connect(mapStateToProps, mapActionToProps)(urlSubscribe(Page));
