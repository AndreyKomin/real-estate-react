import React, { PureComponent } from 'react';
import { withRouter, Link } from 'react-router';
import classNames from 'classnames';

import { Permissions } from 'data/user/constants';
import RestrictedContent from 'app/components/RestrictedContent';
import Button from 'components/base/Button';

import buttonCss from 'components/base/Button/style.scss';
import css from './style.scss';


class ExpandedRow extends PureComponent {
  render() {
    const { data, save } = this.props;
    const propertyId = data.get('propertyId', data.get('id'));
    const path = `/search/${propertyId}`;

    const btn = { kind: Button.kind.grayGhost, className: classNames(css.buttonExpanded, css.button) };
    const link = { ...btn, target: '_blank', className: classNames(buttonCss['border-grey'], css.buttonExpanded, css.button), style: { display: 'inline-block', width: '100px' } };

    /*
    listingId: property.get('id'),
    listingType: property.get('type'),
    */

    return (
      <div className={css.buttonsExpanded}>
        <Link {...link} to={path}>Details</Link>
        <RestrictedContent permission={Permissions.analysisView}>
          <Link {...link} to={`${path}/analysis`}>Analysis</Link>
        </RestrictedContent>
        <Button {...btn} onClick={() => save(propertyId)}>Save</Button>
      </div>
    );
  }
}

export default withRouter(ExpandedRow);
