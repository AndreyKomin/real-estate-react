import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { is, List, Map } from 'immutable';
import { bind, unbind } from 'utils/DOM/scrollSpy';
import InputDate from 'components/base/InputDate';
import Button from 'components/base/Button';
import FormControlWraper from 'components/base/FormControlWraper';
import SearchItem from 'components/SearchItem';
import ReactMap from 'Bing/ReactMap';
import Confirm from 'app/components/Confirm';
import Dropdown from 'components/base/Dropdown';
import { Neighbors, MlsListingStatusNoneList } from 'data/listing';
import RestrictedContent from 'app/components/RestrictedContent';
import LoadablePanel from 'components/hoc/loadable/Panel';
import AddToGroup from 'app/components/AddToGroup';
import { selectProfile } from 'data/user';
import numberToPrice from 'utils/currency/numberToPrice';
import { getOptionsValueLabel } from 'utils/normalizers/dropdown';

import {
  selectComparableLoading,
  updateSurroundingPropertySearch,
  searchComparables,
  searchSurroundingProperties,
  downloadComparables,
  sortSurroundingProperties,
  updateSurroundingPropertySelection,
  updateSurroundingPropertyType,
  selectSurroundingPropertyType,
  selectSurroundingPropertyTypes,
  PropertyGroupTypes,
  selectLists,
  selectFavorites,
  selectLoading,
  saveProperties,
} from 'data/property';

import ExpandedRow from './ExpandedRow';
import Panel from '../Panel';
import { typeColumns, writeTable } from '../Table';

import css from './style.scss';


const defaultPosition = { position: 'relative' };

class PropertyComparables extends PureComponent {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.handleDownloadClick = this.handleDownloadClick.bind(this);
    this.handleSort = this.handleSort.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
    this.handleTypeChange = this.handleTypeChange.bind(this);
    this.handleScrollModal = this.handleScrollModal.bind(this);
    this.setContentRef = this.setContentRef.bind(this);
    this.handleSaveGroup = this.handleSaveGroup.bind(this);
    this.handleSaveList = this.handleSaveList.bind(this);

    this.timer = null;
    this.contentRef = null;

    this.state = { loaded: false, offset: 10, position: defaultPosition };
  }

  componentWillMount() {
    this.load(this.props);
  }

  componentDidMount() {
    bind('modal', this.handleScrollModal);
  }

  componentWillReceiveProps(props) {
    this.load(props);
  }

  componentWillUnmount() {
    unbind('modal', this.handleScrollModal);
  }

  setContentRef(ref) {
    this.contentRef = ref;
  }

  getSelection() {
    return this.props.type.get('properties').filter(c => c.get('selected'));
  }

  getSelectionSize() {
    return this.getSelection().size;
  }

  load() {
    const { propertyId } = this.props;
    if (!this.state.loaded && propertyId) this.setState({ loaded: true }, () => this.search());
  }

  search(search = this.props.type.get('search')) {
    const { searchSurroundingProperties, searchComparables, type, propertyId } = this.props;
    if (type.get('type') === 'Comparable') searchComparables(propertyId, search, this.props.types.getIn([1, 'loading']) ? () => searchSurroundingProperties(propertyId) : null);
    else searchSurroundingProperties(propertyId, type.get('type'), search);
  }

  killTimer() {
    if (this.timer) clearTimeout(this.timer);
    this.timer = null;
  }

  handleScrollModal() {
    const { top } = this.contentRef.getBoundingClientRect();
    const { offset } = this.state;

    this.setState({ position: top < offset ? { position: 'absolute', top: offset - top, width: '100%' } : defaultPosition });
  }

  handleDownloadClick() {
    const { downloadComparables, search, propertyId, confirm, savedPropertyId, type } = this.props;
    const propertyIds = type.get('properties').filter(c => c.get('selected')).map(c => c.get('id'));

    if (!propertyIds.size) confirm({ cancelLabel: null, question: 'You must select at least one comp.' });
    else downloadComparables(propertyId, search.merge({ savedPropertyId, propertyIds }));
  }

  handleSort({ fieldName: sort }) {
    const { sortSurroundingProperties, type } = this.props;
    sortSurroundingProperties(sort && sort === type.get('sort') ? `!${sort}` : sort);
  }

  handleSelect(index, selected) {
    this.props.updateSurroundingPropertySelection(index, selected);
  }

  handleChange(ev) {
    const { name, value, type } = ev.target || ev;
    const { updateSurroundingPropertySearch } = this.props;
    const search = this.props.type.get('search');

    let val = value;
    if (typeof value === 'string') {
      if (value.trim() === '') val = null;
      else if (name !== 'mlsListingStatus') {
        // Only allow one decimal
        val = value.replace(/[^\d.]/g, '');
        const idx = val.indexOf('.');
        if (idx >= 0) val = `${val.substr(0, idx + 1)}${val.substr(idx + 1).replace(/\D/g, '')}`;
      }
    }

    if (val !== search.get(name)) {
      const newSearch = search.set(name, val);
      updateSurroundingPropertySearch(newSearch);

      this.killTimer();
      if (type !== 'text') this.search(newSearch);
      else {
        this.timer = setTimeout(() => {
          this.killTimer();
          this.search(newSearch);
        }, 1000);
      }
    }
  }

  handleSaveGroup() {
    const { favorites, openAddToGroup, profile } = this.props;
    openAddToGroup({
      selectLoading,
      groups: favorites,
      size: this.getSelectionSize(),
      limit: profile.get('favoritePropertyAddLimit'),
      save: onComplete => this.save(PropertyGroupTypes.FAVORITE, '', onComplete),
      add: (name, onComplete) => this.save(PropertyGroupTypes.FAVORITE, name, onComplete),
    });
  }

  handleSaveList() {
    const { lists, openAddToGroup, profile } = this.props;
    openAddToGroup({
      selectLoading,
      groups: lists,
      size: this.getSelectionSize(),
      limit: profile.get('marketingListAddLimit'),
      groupType: 'Marketing List',
      add: (name, onComplete) => this.save(PropertyGroupTypes.MARKETING, name, onComplete),
    });
  }

  save(groupType, groupName, onComplete) {
    const { type, saveProperties } = this.props;
    const compMode = type.get('type') === 'Comparable';
    const ids = this.getSelection().map(p => (compMode ? [p.get('id')] : [p.get('propertyId'), p.get('id')])).toJS();

    console.log('selection', this.getSelection().toJS(), this.props.type.toJS(), groupType, groupName, ids);
    saveProperties(ids, groupName, groupType, compMode ? null : type.get('type'), ({ response: { quantity } }) => onComplete(quantity));
  }

  handleTypeChange(type) {
    const { updateSurroundingPropertyType, types } = this.props;
    this.killTimer();
    updateSurroundingPropertyType(types.find(t => t.get('type') === type));
  }

  render() {
    // const { leftColumnTop } = this.state;style={leftColumnTop}
    const { loading, saveProperty, property, type, types } = this.props;
    const properties = type.get('properties');
    const activeType = type.get('type');
    const search = type.get('search');
    const compMode = activeType === 'Comparable';

    const btn = { kind: Button.kind.grayGhost, className: css.button, isLoading: loading };
    const textProps = { type: 'text', onChange: this.handleChange };
    const dateProps = { className: css.dateInput, onChange: this.handleChange, startDate: search.get('saleDateMin'), endDate: search.get('saleDateMax'), noIcon: true };
    const selection = this.getSelection();
    const selected = selection.size;

    let compStats = '';
    if (compMode) {
      console.log('sales', selection.toJS(), selection.map(p => p.get('lastSale'), Map()).toJS(), selection.map(p => p.get('lastSale')).toJS());
      const sales = selection.map(p => p.get('lastSale'), Map()).filter(s => !!s.get('saleAmount'));
      const days = selection.filter(p => !!p.get('compDaysOnMarket'));
      const saleAmount = sales.size ? Math.round(sales.reduce((amount, s) => amount + s.get('saleAmount'), 0) / sales.size) : null;
      const daysOnMarket = days.size ? Math.floor(days.reduce((days, p) => days + p.get('compDaysOnMarket'), 0) / days.size) : null;

      compStats = `, Avg. Sale Price: ${saleAmount ? numberToPrice(saleAmount) : '--'}, Days on Market: ${daysOnMarket || '--'}`;
    }

    console.log(type, type.toJS());

    return (
      <Panel padding="15px 10px">
        <div className={css.searchItems}>
          {types.map(t => (
            <RestrictedContent permission={t.get('enabled')} key={t.get('type')}>
              <SearchItem
                label={t.get('label')}
                value={t.get('properties').size}
                tileName={t.get('type')}
                loading={t.get('loading')}
                isActive={is(t, type)}
                onClick={this.handleTypeChange}
              />
            </RestrictedContent>
          ))}
        </div>
        <div className={css.content} ref={this.setContentRef}>
          <div className={css.left}>
            <div style={this.state.position}>
              {activeType === 'Neighbor' ? null : (
                <div>
                  <div className={css.title}>{type.get('label').replace(/s$/, '')} Search</div>
                  <div className={css.content}>
                    <div className={css.leftSearch}>
                      <div className={css.range}>
                        <FormControlWraper label="Sale Date Range" hasMarginBottom>
                          <InputDate {...dateProps} selectsStart name="saleDateMin" placeholderText="Sale Date Min" selected={dateProps.startDate} />
                        </FormControlWraper>
                        <span className={css.divider}> to </span>
                        <FormControlWraper id="saleDateMax" showEmptyLabel hasMarginBottom>
                          <InputDate {...dateProps} selectsEnd name="saleDateMax" placeholderText="Sale Date Max" selected={dateProps.endDate} />
                        </FormControlWraper>
                      </div>
                      <div className={css.range}>
                        <FormControlWraper id="bedroomsMin" label="Bedrooms" hasMarginBottom>
                          <input {...textProps} name="bedroomsMin" value={search.get('bedroomsMin') || ''} placeholder="Min" />
                        </FormControlWraper>
                        <span className={css.divider}> to </span>
                        <FormControlWraper id="bedroomsMax" showEmptyLabel hasMarginBottom>
                          <input {...textProps} name="bedroomsMax" value={search.get('bedroomsMax') || ''} placeholder="Max" />
                        </FormControlWraper>
                      </div>
                    </div>
                    <div className={css.rightSearch}>
                      <div className={css.flexRow}>
                        <div className={css.range}>
                          <FormControlWraper id="squareFeetMin" label="Square Feet" hasMarginBottom>
                            <input {...textProps} name="squareFeetMin" value={search.get('squareFeetMin') || ''} placeholder="Min" />
                          </FormControlWraper>
                          <span className={css.divider}> to </span>
                          <FormControlWraper id="squareFeetMax" showEmptyLabel hasMarginBottom>
                            <input {...textProps} name="squareFeetMax" value={search.get('squareFeetMax') || ''} placeholder="Max" />
                          </FormControlWraper>
                        </div>
                        <FormControlWraper id="distance" label="Distance (Miles)" hasMarginBottom>
                          <input {...textProps} name="distanceFromSubjectMax" value={search.get('distanceFromSubjectMax') || ''} />
                        </FormControlWraper>
                      </div>
                      <div className={css.flexRow}>
                        <div className={css.range}>
                          <FormControlWraper id="bathroomsMin" label="Bathrooms" hasMarginBottom>
                            <input {...textProps} name="bathroomsMin" value={search.get('bathroomsMin') || ''} placeholder="Min" />
                          </FormControlWraper>
                          <span className={css.divider}> to </span>
                          <FormControlWraper id="bathroomsMax" showEmptyLabel hasMarginBottom>
                            <input {...textProps} name="bathroomsMax" value={search.get('bathroomsMax') || ''} placeholder="Max" />
                          </FormControlWraper>
                        </div>
                        <FormControlWraper id="status" label="MLS Status" hasMarginBottom>
                          <Dropdown name="mlsListingStatus" value={search.get('mlsListingStatus') || ''} onChange={this.handleChange} options={getOptionsValueLabel(MlsListingStatusNoneList, false, 'Any')} />
                        </FormControlWraper>
                      </div>
                    </div>
                  </div>
                </div>
              )}
              <div className={css.mapWrap}>
                <ReactMap
                  activePushpinId={property.get('id')}
                  listingType={activeType.length > 1 ? Neighbors : activeType}
                  showDraw={compMode}
                  analyticsEnabled={false}
                  entities={List().push(property).concat(properties.map((c, i) => c.set('text', `${i + 1}.`))).filter(c => !!c.get('latitude'))}
                />
              </div>
            </div>
          </div>
          <div className={css.right}>
            <LoadablePanel loading={type.get('loading')}>
              <div className={css.justifyContent}>
                <div className={css.titleContainer}>
                  <div className={css.title}>{type.get('title') || type.get('label')} ({properties.size})</div>
                  {!selected ? null : <div className={css.selected}>{selected} Selected{compStats}</div>}
                </div>
                <div>
                  {!compMode ? null : <Button {...btn} onClick={this.handleDownloadClick} disabled={loading || !properties.size}>View Report</Button>}
                  <Button {...btn} onClick={this.handleSaveList} disabled={loading || !selected}>Add to List</Button>
                  <Button {...btn} onClick={this.handleSaveGroup} disabled={loading || !selected}>Save</Button>
                </div>
              </div>
              <div className={css.tableComparableProperties}>
                {writeTable(properties, typeColumns[activeType], 'type_11', { sort: type.get('sort'), onSort: this.handleSort, onSelect: this.handleSelect, expandComponent: ExpandedRow, save: saveProperty })}
              </div>
            </LoadablePanel>
          </div>
        </div>
      </Panel>
    );
  }
}

export default connect(state => ({
  loading: selectLoading(state) || selectComparableLoading(state),
  comparableLoading: selectComparableLoading(state),
  type: selectSurroundingPropertyType(state),
  types: selectSurroundingPropertyTypes(state),
  profile: selectProfile(state),
  lists: selectLists(state),
  favorites: selectFavorites(state),
}), {
  saveProperties,
  updateSurroundingPropertySearch,
  searchComparables,
  searchSurroundingProperties,
  downloadComparables,
  sortSurroundingProperties,
  updateSurroundingPropertyType,
  updateSurroundingPropertySelection,
  confirm: Confirm.open,
  openAddToGroup: AddToGroup.open,
})(PropertyComparables);
