import { PureComponent } from 'react';
import { connect } from 'react-redux';

import urlSubscribe from './urlSubscribe';

import Modal from './Detail';


export class PropertyDetailLoader extends PureComponent {
  componentDidMount() {
    const { openPopup, urls, history } = this.props;
    const { root: parentRoute, detailsRoot: currentRoute } = urls.getState();

    openPopup({
      ...this.props,
      onClose: () => history.push(parentRoute),
      parentRoute,
      currentRoute,
    });
  }

  componentWillUnmount() {
    this.props.closePopup();
  }

  render() {
    return this.props.children;
  }
}

export default connect(null, { openPopup: Modal.open, closePopup: Modal.close })(urlSubscribe(PropertyDetailLoader));
