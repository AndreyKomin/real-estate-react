import React, { PureComponent } from 'react';
import { Link } from 'react-router';
import { pure } from 'recompose';
import classNames from 'classnames';

import Modal from 'components/base/Modal';
import Button from 'components/base/Button';
import SVG from 'components/base/SVG';
import Image from 'components/base/Image/index';
import AnalysisButton from 'app/components/AnalysisButton';
import Chart from 'components/Chart';
import MyBluebookButton from 'app/components/MyBluebookButton';
import { formatFullAddressObject } from 'utils/address/formatAddress';
import numberToPrice from 'utils/currency/numberToPrice';
import formatDate from 'utils/date/formatDate';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import CustomTabs from 'components/CustomTabs';

import PropertyComparables from './Comparables';
import PropertyDocuments from './Documents';
import Panel from './Panel';
import ValueCell from './valueCell';
import css from './style.scss';
import { TAX_COLUMNS, MORTGAGE_COLUMNS, TRANSACTION_COLUMNS, BOTTOM_FIELDS, RIGHT_FIELDS, COMP_FIELDS, OPPORTUNITY_FILELDS, writeValues, writeSections, writeFields, writeTable } from './Table';


export class PropertyDetailLayout extends PureComponent {
  render() {
    const {
      closePopup,
      onPrint,
      documentLoading,
      loading,
      saveProperty,
      params,
      location,
      currentRoute,
      property,
      propertySaved,
      propertyId,
      savedPropertyId,
      checkSavedProperty,
      selectedTabIndex,
      onTabSelect,
      tabs,
      taxPage,
    } = this.props;
    const btn = { kind: Button.kind.ghost, size: Button.size.middle, isLoading: loading };
    const imageUrls = property.has('imageUrls') ? property.get('imageUrls').split(',') : [property.get('mapUrl')];
    const lastSaleDate = property.getIn(['lastSale', 'saleDate']);
    const tabProps = { property, propertyId, savedPropertyId, checkSavedProperty, saveProperty };

    return (
      <Modal
        isOpen
        onClose={closePopup}
        isCloseButton
        background="#F3F5F8"
        uniqId="modalPropertiesDetails"
        width="1340px"
        caption={(
          <div className={css.header}>
            <div className={css.headerTitle}>{formatFullAddressObject(property.get('address'), true)}</div>
            <div className={css.buttons}>
              <Button {...btn} isLoading={documentLoading} onClick={onPrint}>Print</Button>
              <AnalysisButton propertyId={propertyId} width="115px" params={params} location={location} />
              <Button {...btn} onClick={saveProperty} disabled={propertySaved}>{propertySaved ? 'Saved' : 'Save'}</Button>
              <Link to={currentRoute} kind={Button.link} className={css.buttonExport} target="_blank">
                <SVG icon="iconExport" className={css.iconExport} />
              </Link>
            </div>
          </div>
        )}
      >
        <div className={css.modalBody}>
          <div className={css.rowPanels}>
            <Panel width="500px" loading={loading}>
              <div className={css.propertyInfo}>
                <div className={css.left}>
                  <div className={css.image}>
                    <Link to={`${currentRoute}/gallery`}>
                      <Image src={imageUrls[0]} placeholder="http://images.listing.realestate.com/no_photo.jpg" />
                      {imageUrls.length === 1 ? null : <div className={css.circle}>+{imageUrls.length - 1}</div>}
                    </Link>
                  </div>
                  <div className={css.items}>{writeFields(property, BOTTOM_FIELDS)}</div>
                </div>
                <div className={classNames(css.right, css.items)}>{writeFields(property, RIGHT_FIELDS)}</div>
              </div>
            </Panel>
            <Panel width="260px" loading={loading}>
              <div className={css.valueSection}>
                <div className={css.label}>Value:</div>
                <div className={css.values}>
                  <ValueCell name="Estimated Value" value={numberToPrice(property.get('estimatedValue'))} />
                  <ValueCell name="Last Sale" value={numberToPrice(property.getIn(['lastSale', 'saleAmount']))}>
                    {!lastSaleDate ? null : <div className={css.date}>({formatDate(lastSaleDate)})</div>}
                  </ValueCell>
                </div>
                <div className={css.graph}>
                  <Chart caption="Estimated Value" yTickFormat={Chart.yTickFormat.currency} chartWidth={210} graph={property.get('estimatedValueGraph')} />
                </div>
              </div>
            </Panel>
            <Panel width="315px" loading={loading}>
              <div className={css.valueSection}>
                {writeValues('Comparables', property, COMP_FIELDS)}
                <div className={css.graph}>
                  <Chart caption="Average Comps" yTickFormat={Chart.yTickFormat.currency} chartWidth={270} graph={property.get('compSaleAmountGraph')} />
                </div>
              </div>
            </Panel>
            <Panel width="225px" loading={loading}>
              <div className={css.valueSection}>
                {writeValues('Opportunity', property, OPPORTUNITY_FILELDS)}
                <MyBluebookButton savedPropertyId={savedPropertyId} checkSavedProperty={checkSavedProperty} />
              </div>
            </Panel>
          </div>
          <div className={css.tabs}>
            <CustomTabs kind="line">
              <Tabs selectedIndex={selectedTabIndex} onSelect={onTabSelect}>
                <TabList>
                  {tabs.map(({ name }) => (<Tab key={name}>{name}</Tab>))}
                  <Tab>Comparables & Nearby Listings</Tab>
                  <Tab>Tax Information</Tab>
                  <Tab>Mortgage Information</Tab>
                  <Tab>Transaction History</Tab>
                  <Tab>Documents & Reports</Tab>
                </TabList>
                {tabs.map(({ name, sections }) => (
                  <TabPanel key={name}>
                    {writeSections(sections)}
                  </TabPanel>
                ))}
                <TabPanel>
                  <PropertyComparables {...tabProps} />
                </TabPanel>
                <TabPanel className={css.tax}>
                  <Panel>
                    {writeSections(taxPage.sections)}
                    {writeTable(property.get('taxes'), TAX_COLUMNS, 'type_7')}
                  </Panel>
                </TabPanel>
                <TabPanel>
                  <Panel>
                    {writeTable(property.get('mortgages'), MORTGAGE_COLUMNS)}
                  </Panel>
                </TabPanel>
                <TabPanel>
                  <Panel>
                    {writeTable(property.get('sales'), TRANSACTION_COLUMNS)}
                  </Panel>
                </TabPanel>
                <TabPanel>
                  <PropertyDocuments {...tabProps} />
                </TabPanel>
              </Tabs>
            </CustomTabs>
          </div>
        </div>
      </Modal>
    );
  }
}

export default pure(PropertyDetailLayout);
