import React, { PureComponent } from 'react';
import { Reports } from 'data/user/constants';

import { getPopupRegistration, openPopup, closePopup } from 'app/PopupHolder';
import withProperty from 'app/components/withProperty';
import { downloadReport, selectDocumentLoading } from 'data/property';
import { selectPages } from 'data/user';
import numberToPrice from 'utils/currency/numberToPrice';
import formatDate from 'utils/date/formatDate';
import numberFormat from 'utils/number/format';
import percentFormat from 'utils/percent/formatPercent';
import formatFullAddressObject from 'utils/address/formatAddress';

import UrlStorage from './UrlStorage';
import Layout from './Layout';


const taxPageFields = {
  sections: [{
    code: 'TOP',
    fields: [
      { name: 'Parcel Number', path: 'apn' },
      { name: 'Situs County', path: 'countyName' },
      { name: 'Primary Owner Full Name', path: 'owner1FullName' },
      { name: 'Tax Bill Mailing Address', path: 'mailAddress', mask: '{address}' },
    ],
  }, {
    code: 'BOTTOM',
    fields: [
      { name: 'Tax Year', path: 'assessmentYear' },
      { name: 'Tax Amount', path: 'taxAmount', mask: '{currency}' },
      { name: 'Total Assessed Value', path: 'assessedValue', mask: '{currency}' },
      { name: 'Land Value', path: 'assessedLandValue', mask: '{currency}' },
      { name: 'Improvement Value', path: 'assessedImprovementValue', mask: '{currency}' },
    ] },
  ],
};

const pageFields = 'PROPERTY_SALE:cashSale,MLS:mlsListing,MLS2:listhubMlsListing,FORECLOSURE,LIEN,DIVORCE,BANKRUPTCY'
  .split(',').map(f => f.split(':')).reduce((m, f) => ({ ...m, [f[0]]: f[1] || f[0].toLowerCase() }), {});

let pageMap;

const getPage = (page, target) => {
  const { name, sections, code } = page;
  const tab = {
    code,
    initialized: false,
    name: code === 'FORECLOSURE' && target && target.get('type') === 'P' ? 'Pre-Foreclosure Details' : name,
    sections: [],
  };

  if (target) {
    tab.sections = sections.map(({ name, code, fields }) => ({
      name,
      code,
      fields: fields.map(({ name, path, mask }) => {
        let tab;
        let value = target.getIn(path.split('.'), null);
        if (value != null && mask) {
          if (mask.startsWith('[')) tab = getPage(pageMap[mask.substr(1, mask.length - 2)], value);
          else if (mask.includes('{string}')) value = mask.replace('{string}', value);
          else if (mask.includes('{currency}')) value = mask.replace('{currency}', numberToPrice(value));
          else if (mask.includes('{date}')) value = mask.replace('{date}', formatDate(value));
          else if (mask.includes('{yesno}')) value = mask.replace('{yesno}', value ? 'Yes' : 'No');
          else if (mask.includes('{number}')) value = mask.replace('{number}', numberFormat(value));
          else if (mask.includes('{percent}')) value = mask.replace('{percent}', percentFormat(value * 100));
          else if (mask.includes('{address}')) value = mask.replace('{address}', formatFullAddressObject(value));
        }

        return { name, value, tab };
      }).filter(({ value, tab }) => (!tab && value != null) || (tab && tab.sections.length)),
    })).filter(section => section.fields.length);
  }

  return tab;
};

export class PropertyDetail extends PureComponent {
  constructor(props) {
    super(props);

    this.handlePrint = this.handlePrint.bind(this);
    this.handleTabSelect = this.handleTabSelect.bind(this);

    if (!pageMap && props.pages.size) pageMap = props.pages.toJS().reduce((m, p) => ({ ...m, [p.code]: p }), {});

    this.state = { selectedTabIndex: 0, currentPropertyId: null, tabs: [], taxPage: { sections: [] } };
  }

  componentWillMount() {
    const { loadProperty, params: { id }, property } = this.props;

    if (Number(id) !== property.get('id')) loadProperty(id);
    this.loadProperty(this.props);
  }

  componentWillReceiveProps(props) {
    this.loadProperty(props);
  }

  loadProperty(props) {
    const { property, pages } = props;
    const currentPropertyId = property.get('id');

    if (!currentPropertyId) this.setState({ currentPropertyId: null, tabs: [{ name: 'Property Details', sections: [] }] });
    else if (pages.size && currentPropertyId !== this.state.currentPropertyId) {
      const taxPage = getPage(taxPageFields, property);
      if (!taxPage.sections.length) taxPage.sections.push({ fields: [] });
      taxPage.sections[0].name = 'Tax Details';

      this.setState({
        currentPropertyId,
        taxPage,
        tabs: pages.map(p => p.get('code')).map(code => getPage(pageMap[code], pageFields[code] ? property.get(pageFields[code]) : property)).filter(tab => tab.sections.length),
      });
    }
  }

  handlePrint() {
    const { downloadReport, propertyId } = this.props;
    downloadReport(propertyId, Reports.comparativeMarketAnalysis);
  }

  handleTabSelect(selectedTabIndex) {
    this.setState({ selectedTabIndex }, () => {
      // Perform tab initialization on first load. (Will need to run this somewhere else if first tab ever needs to init, but so far it's only MLS.)
      const tab = this.state.tabs.get(selectedTabIndex);
      if (tab && !tab.initialized && tab.code === 'MLS2') {
        tab.initialized = true;

        const listingKey = this.props.property.getIn(['listhubMlsListing', 'listingKey']);
        if (listingKey) window.listHub('submit', 'DETAIL_PAGE_VIEWED', { lkey: listingKey });
      }
    });
  }

  render() {
    return (
      <UrlStorage urls={this.props.urls}>
        <Layout
          {...this.props}
          {...this.state}
          onPrint={this.handlePrint}
          onTabSelect={this.handleTabSelect}
        />
      </UrlStorage>
    );
  }
}

const PropertyDetailPopup = withProperty(PropertyDetail, state => ({
  documentLoading: selectDocumentLoading(state),
  pages: selectPages(state),
}), { downloadReport });

const registrationId = getPopupRegistration(PropertyDetailPopup);

PropertyDetailPopup.open = props => openPopup(registrationId, props);
PropertyDetailPopup.close = () => closePopup({ popup: registrationId });

export default PropertyDetailPopup;
