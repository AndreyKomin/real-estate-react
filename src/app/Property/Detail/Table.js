import React from 'react';
import classNames from 'classnames';
import { fromJS } from 'immutable';
import Label from 'components/Label/index';
import Checkbox from 'components/base/Checkbox';
import SVG from 'components/base/SVG';
import percentFormat from 'utils/percent/formatPercent';
import GroupInfo from 'components/GroupInfo';
import Table, { Column } from 'components/base/Table';
import numberToPrice from 'utils/currency/numberToPrice';
import { formatDateShort } from 'utils/date/formatDate';
import numberFormat from 'utils/number/format';
import { joinWith } from 'utils/string';
import { MlsListingStatusNames } from 'data/listing';

import Panel from './Panel';
import ValueCell from './valueCell';
import css from './style.scss';


export const currencyRenderer = val => (!Number(val) ? '' : numberToPrice(val));
const currencyRenderer2 = val => (!Number(val) ? '-' : numberToPrice(val));
const numberRenderer = val => (!Number(val) ? '-' : numberFormat(val));
const dateRenderer = val => (!Number(val) ? '' : formatDateShort(val));
const statusRenderer = val => (MlsListingStatusNames[val] || '');
const changeRenderer = val => (!Number(val) ? '--' : <span className={css[val < 0 ? 'minus' : 'plus']}>{percentFormat(val * 100)}</span>);
const addressRenderer = (streetAddress, coords, data) => (
  <div style={{ whiteSpace: 'pre', maxWidth: '180px', overflow: 'hidden', textOverflow: 'ellipsis' }}>
    {joinWith('\n', streetAddress, joinWith(' ', data.get('cityName'), data.get('zip')))}
  </div>
);

export const TAX_COLUMNS = [
  { field: 'year', title: 'YEAR' },
  { field: 'amount', title: 'PROPERTY TAXES', renderer: currencyRenderer },
  { field: 'amountChange', title: 'CHANGE', renderer: changeRenderer },
  { field: 'assessedValue', title: 'TAX ASSESSMENT', renderer: currencyRenderer },
  { field: 'assessedValueChange', title: 'CHANGE', renderer: changeRenderer },
];

export const MORTGAGE_COLUMNS = [
  { field: 'recordingDate', title: 'Recording Date', renderer: dateRenderer },
  { field: 'granteeName', title: 'Grantee' },
  { field: 'lenderName', title: 'Lender Name' },
  { field: 'description', title: 'Loan Description' },
  { field: 'amount', title: 'Loan Amount', renderer: currencyRenderer },
];

export const TRANSACTION_COLUMNS = [
  { field: 'recordingDate', title: 'Recording Date', renderer: dateRenderer },
  { field: 'sellerNames', title: 'Grantor' },
  { field: 'ownerNames', title: 'Grantee' },
  { field: 'documentType', title: 'Document Type' },
  { field: 'documentNumber', title: 'Document #' },
  { field: 'saleAmount', title: 'Sale Amount', renderer: currencyRenderer },
];

export const BOTTOM_FIELDS = [
  { label: 'Beds', value: 'bedrooms' },
  { label: 'Baths', value: 'bathrooms' },
  { label: 'Sqft', value: 'squareFeet' },
  { label: 'Yr. Built', value: 'yearBuilt' },
];

export const RIGHT_FIELDS = [
  { label: 'Estimated Value:', value: 'estimatedValue', format: numberToPrice },
  { label: 'Status:', value: 'marketStatus' },
  { label: 'Distressed:', value: 'distressed', format: v => (v ? 'Yes' : 'No') },
  { label: 'Liens:', value: 'involuntaryLienAmount', format: numberToPrice },
  { label: 'Owner:', value: 'ownership', withLabel: true },
  { label: 'Occupancy:', value: 'occupancy', withLabel: true },
  { label: 'Property Type:', value: 'propertyType', withLabel: true },
];

export const COMP_FIELDS = [
  { name: 'Properties', field: 'comps' },
  { name: 'Avg. Sale Price', field: 'compSaleAmount', format: numberToPrice },
  { name: 'Days on Market', field: 'compDaysOnMarket' },
];

export const OPPORTUNITY_FILELDS = [
  { name: 'Est. Equity', field: 'estimatedEquity', format: numberToPrice },
  { name: 'Monthly Rent', field: 'rentAmount', format: numberToPrice },
  { name: 'Mortgage Balance', field: 'mortgageBalance', format: numberToPrice },
  { name: 'Liens', field: 'involuntaryLienAmount', format: numberToPrice },
];

const address = { field: 'streetAddress', title: 'Address', renderer: addressRenderer };
const bedrooms = { field: 'bedrooms', title: 'Beds' };
const bathrooms = { field: 'bathrooms', title: 'Baths' };
const squareFeet = { field: 'squareFeet', title: 'SqFt', renderer: numberRenderer };
const yearBuilt = { field: 'yearBuilt', title: 'Year Built' };
const saleDate = { field: 'saleDate', title: 'Sale Date', renderer: dateRenderer };
const saleAmount = { field: 'saleAmount', title: 'Sale Amount', renderer: currencyRenderer2 };
const distanceFromSubject = { field: 'distanceFromSubject', title: 'Distance' };
const pricePerSquareFoot = { field: 'pricePerSquareFoot', title: '$/SqFt', renderer: currencyRenderer2 };
const estimatedEquity = { field: 'estimatedEquity', title: 'Estimated Equity', renderer: currencyRenderer2 };
const estimatedValue = { field: 'estimatedValue', title: 'Estimated Value', renderer: currencyRenderer2 };
const listingDate = { field: 'listingDate', title: 'List Date', renderer: dateRenderer };
const listingAmount = { field: 'listingAmount', title: 'List Price', renderer: currencyRenderer2 };
const status = { field: 'status', title: 'Status', renderer: statusRenderer };
const lienDocumentType = { field: 'lienDocumentType', title: 'Lien Type' };
const documentType = { field: 'documentType', title: 'Document Type' };
const defaultAmount = { ...listingAmount, title: 'Default Amount' };
const lastUpdated = { ...listingDate, title: 'Last Updated' };

export const typeColumns = {
  Comparable: [address, bedrooms, bathrooms, squareFeet, yearBuilt, saleDate, saleAmount, distanceFromSubject, pricePerSquareFoot],
  Neighbor: [address, bedrooms, bathrooms, saleDate, saleAmount, estimatedEquity],
  M: [address, bedrooms, bathrooms, squareFeet, yearBuilt, listingAmount, listingDate, status],
  P: [documentType, address, bedrooms, bathrooms, squareFeet, yearBuilt, defaultAmount, lastUpdated, status],
  L: [lienDocumentType, address, bedrooms, bathrooms, squareFeet, yearBuilt, { ...listingAmount, title: 'Lien Amount' }, { ...listingDate, title: 'Lien Date' }, status],
  C: [address, bedrooms, bathrooms, squareFeet, yearBuilt, estimatedValue, saleDate, saleAmount, status],
  E: [address, bedrooms, bathrooms, squareFeet, yearBuilt, estimatedValue, estimatedEquity, saleDate, status],
  A: [address, bedrooms, bathrooms, squareFeet, yearBuilt, estimatedValue, defaultAmount, lastUpdated, status],
};

typeColumns.F = typeColumns.P;
typeColumns.V = typeColumns.E;
typeColumns.B = typeColumns.E;
typeColumns.D = typeColumns.E;
typeColumns.W = typeColumns.E;
typeColumns.R = typeColumns.C;

export const writeValues = (label, property, fields) => (
  <div>
    <div className={css.label}>{label}:</div>
    <div className={css.values}>
      {fields.map(f => <ValueCell {...{ ...f, value: (f.format || (v => v))(property.get(f.field)) }} key={f.field} />)}
    </div>
  </div>
);

export const writeFields = (property, fields) => (
  fields.map(f => ({ ...f, value: property.has(f.value) ? (f.format || (v => v))(property.get(f.value)) : null })).filter(f => f.value != null).map(({ label, value, withLabel }) => (
    <div className={css.item} key={label}>
      <div className={css.label}>{label}</div>
      <div className={css.value}>
        {withLabel ? <Label title={value} background="blue" size="normal">{value}</Label> : value}
      </div>
    </div>
  ))
);

export const writeSections = sections => (
  sections.map(({ name, code, fields }) => (
    <div className={css.page} key={`${code}`}>
      <Panel padding="20px">
        {!name ? null : <div className={css.heading}>{name}</div>}
        <div className={css.row}>
          {fields.map(({ name, value, tab }) => (
            tab ? writeSections(tab.sections) : (
              <div className={classNames(css.cell, { [css.single]: fields.length === 1 })} key={`${name}}`}>
                <GroupInfo label={fields.length > 1 ? name : null}>
                  <div>{value}</div>
                </GroupInfo>
              </div>
            )))}
        </div>
      </Panel>
    </div>
  ))
);

const selectRenderer = onSelect => (value, { row }) => <Checkbox checked={value} onClick={ev => onSelect(row, ev.target.checked)} />;
const indexRenderer = (value, { row }) => `${row + 1}.`;

export const writeTable = (data, columns, style = 'type_8', { onRowClick, onSelect, onSort, sort, expandComponent, save } = {}) => {
  let caretIcon;
  let sortFields;
  let sortableFields;

  if (onSort) {
    sortableFields = columns.map(c => c.field);
    if (sort) {
      const desc = sort.startsWith('!');
      caretIcon = <SVG icon={desc ? 'iconCaretDown' : 'iconCaretUp'} />;
      sortFields = fromJS([desc ? sort.substr(1) : sort]);
    }
  }

  return (
    <div className={css.trendTable}>
      <Table
        data={data}
        keyField="id"
        className={style}
        sortFields={sortFields}
        sortableFields={sortableFields}
        onColumnSort={onSort}
        onRowClick={onRowClick}
        isSortable={!!sortableFields}
        expandComponent={expandComponent}
        expandableRow={() => !!expandComponent}
        save={save}
        isHoverable
      >
        {!onSelect ? null : (
          <Column key="selected" field="selected" renderCell={selectRenderer(onSelect)}>
            <Checkbox checked={data.size && !data.find(d => !d.get('selected'))} onClick={ev => onSelect(null, ev.target.checked)} />
          </Column>
        )}
        {style !== 'type_11' ? null : <Column key="index" field="index" renderCell={indexRenderer}>#</Column>}
        {columns.map(col => <Column key={col.field} field={col.field} renderCell={col.renderer} sortIcon={caretIcon}>{col.title}</Column>)}
      </Table>
    </div>
  );
};
