import React from 'react';
import Label from 'components/Label';

import css from './style.scss';


const valueCell = ({ name, value, children }) => (
  <div className={css.value}>
    <Label background="green" size="big">{value || 'N/A'}{children}</Label>
    <div className={css.name}>{name}</div>
  </div>
);

export default valueCell;
