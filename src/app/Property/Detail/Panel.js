import React, { PureComponent } from 'react';
import LoadablePanel from 'components/hoc/loadable/Panel';

import css from './style.scss';


class Panel extends PureComponent {
  render() {
    const { children, width, loading } = this.props;

    return (
      <div className={css.panel} style={{ width }}>
        <LoadablePanel loading={loading}>
          {children}
        </LoadablePanel>
      </div>
    );
  }
}


export default Panel;
