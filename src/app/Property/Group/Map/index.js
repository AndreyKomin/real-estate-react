import { PureComponent } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { propertyGroupPath } from 'app/routes';

import Modal from './map';


export class PropertyGroupMap extends PureComponent {
  componentDidMount() {
    const { openPopup, router, params: { groupId } } = this.props;

    openPopup({
      ...this.props,
      onClose: () => router.push(propertyGroupPath(groupId)),
    });
  }

  componentWillUnmount() {
    this.props.closePopup();
  }

  render() {
    return this.props.children;
  }
}

export default withRouter(connect(null, {
  openPopup: Modal.open,
  closePopup: Modal.close,
})(PropertyGroupMap));
