import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { fromJS } from 'immutable';

import { triggerPushpinPopover, loadProperty } from 'data/map/actions';
import { selectIsPropertyLoading, selectProperty, selectEstimatedValueGraph } from 'data/map/selectors';
import { getPopupRegistration, openPopup, closePopup, Priority } from 'app/PopupHolder';
import { selectPushpinLoading, selectPushpins, selectGroupContext, loadGroupPushpins, getBingUrl } from 'data/property';
import { Color, DefaultColor } from 'data/listing';
import { MAIN_CONTEXT } from 'reducers';

import Modal from 'components/base/Modal';
import CustomMap from 'Bing/ReactMap/CustomMap';
import MapType, { Type } from 'Bing/ReactMap/MapType';
import AutoLayout from 'Bing/ReactMap/AutoLayout';
import Legend from 'Bing/ReactMap/Legend';
import Pushpin from 'Bing/ReactMap/Pushpin';
import { ParcelLayer } from 'Bing/ReactMap/TileLayer';
import Infobox from 'Bing/ReactMap/Infobox';

import css from './style.scss';


class PropertyGroupMap extends PureComponent {
  constructor(props) {
    super(props);

    this.handlePushpinClick = this.handlePushpinClick.bind(this);

    this.state = { activePin: null, pins: [], pinMap: {} };
  }

  componentWillMount() {
    const { context, loadGroupPushpins } = this.props;
    loadGroupPushpins(context, () => {
      const pins = this.props.pushpins.toJS().map((p, i) => ({ ...p, index: String(i + 1) })).filter(p => p.latitude);
      this.setState({ pins, pinMap: pins.reduce((m, p) => ({ ...m, [p.id]: p }), {}) });
    });
  }

  handlePushpinClick(activePin) {
    this.setState({ activePin });
  }

  render() {
    const { context, loading, property, propertyLoading, loadProperty, estimatedValueGraph, params, location } = this.props;
    const { pins, pinMap } = this.state;
    let { activePin } = this.state;
    const activeId = activePin && activePin.id;
    if (activePin) {
      const { latitude, longitude } = pinMap[activePin.propertyId];
      activePin = fromJS({ ...activePin, latitude, longitude });
    }


    return (
      <Modal
        isOpen
        isCloseButton
        uniqId="modal-properties-map"
        caption={`${context.getIn(['info', 'name'])} Map`}
        width="1300px"
      >
        <div className={css.modalBody}>
          <CustomMap>
            <MapType mapType={Type.ROAD} />
            <AutoLayout entities={pins} />
            {pins.map(pin => (
              <Pushpin
                key={pin.id}
                propertyId={pin.id}
                color={Color[pin.listingType || DefaultColor]}
                text={pin.index}
                location={pin}
                onClick={this.handlePushpinClick}
                active={activeId === pin.id}
              />
            ))}
            <Infobox
              params={params}
              location={location}
              triggerPushpinActivity={triggerPushpinPopover}
              isPropertyLoading={propertyLoading}
              loadProperty={loadProperty}
              property={property}
              estimatedValueGraph={estimatedValueGraph}
              isLoading={loading}
              activePushpinId={activeId}
              isOpen={!!activePin}
              center={activePin}
              info={activePin}
              bingMapImageUrl={activePin ? getBingUrl(activePin.latitude, activePin.longitude) : null}
            />
            <Legend />
            <ParcelLayer />
          </CustomMap>
        </div>
      </Modal>
    );
  }
}

const PropertyGroupMapPopup = connect(state => ({
  loading: selectPushpinLoading(state),
  pushpins: selectPushpins(state),
  context: selectGroupContext(MAIN_CONTEXT)(state),
  propertyLoading: selectIsPropertyLoading(state),
  property: selectProperty(state),
  estimatedValueGraph: selectEstimatedValueGraph(state),
}), {
  triggerPushpinPopover,
  loadProperty,
  loadGroupPushpins,
})(PropertyGroupMap);

const registrationId = getPopupRegistration(PropertyGroupMapPopup);

PropertyGroupMapPopup.open = props => openPopup(registrationId, { priority: Priority.LOW, ...props });
PropertyGroupMapPopup.close = () => closePopup({ popup: registrationId });

export default PropertyGroupMapPopup;
