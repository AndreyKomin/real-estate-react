import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router';
import { fromJS } from 'immutable';

import { deferExecutor } from 'app/DeferredOnLocation';
import { createConfirmable, type } from 'app/components/Confirm';
import { propertyGroupPath } from 'app/routes';
import Button from 'components/base/Button';
import ToggleList from 'app/components/ToggleList';
import RightPart from 'app/components/RightPart';
import LeftPart from 'app/components/LeftPart';
import AddToGroup from 'app/components/AddToGroup';
import Prompt from 'app/components/Prompt';

import PropertySearchTable from 'app/components/SearchTable/PropertySearchTable';
import SVG from 'components/base/SVG';
import CreateGroupPopup from 'app/components/CreateGroup';
import { MAIN_CONTEXT } from 'reducers';
import {
  selectGroupContext,
  selectGroups,
  selectFavorites,
  selectLists,
  selectAlerts,
  selectLoading,
  loadGroupContext,
  searchGroupContext,
  exportGroupContext,
  deletePropertyGroup,
  deleteGroupContext,
  saveGroupContext,
  alertIcons,
  PropertyGroupTypes,
  updatePropertyGroup,
} from 'data/property';

import ContractManagerPopup from 'app/components/ContractManager';

import css from './style.scss';


const groupTypeName = context => (context.getIn(['info', 'type']) === PropertyGroupTypes.FAVORITE ? 'Group' : 'Marketing List');

let i = 0;
class PropertyGroup extends Component {
  constructor(props) {
    super(props);

    this.handleOpenContractManager = this.handleOpenContractManager.bind(this);
    this.handleAddGroup = this.handleAddGroup.bind(this);
    this.handleAddList = this.handleAddList.bind(this);
    this.handleRemove = this.handleRemove.bind(this);
    this.handleAddToGroup = this.handleAddToGroup.bind(this);
    this.handleAddToList = this.handleAddToList.bind(this);
    this.handleExport = this.handleExport.bind(this);
    this.handleDeleteGroup = this.handleDeleteGroup.bind(this);
    this.handleRename = this.handleRename.bind(this);

    this.state = { loadId: null };
  }

  componentWillMount() {
    i = 0;
    this.loadGroup(this.props);
  }

  componentWillReceiveProps(props) {
    this.loadGroup(props);
  }

  loadGroup(props = this.props) {
    const { router, loadGroupContext, searchGroupContext, groups, context, params: { groupId } } = props;
    const loadId = Number(groupId) || 0;

    if (groups.size && i++ < 1000) {
      const group = groups.find(g => g.get('id') === loadId);

      if (!group) router.push(propertyGroupPath(0));
      else if (!context || context.get('id') !== loadId) loadGroupContext(MAIN_CONTEXT, loadId, { filterEnabled: true });
      else if (this.state.loadId !== loadId && context.get('size') == null) this.setState({ loadId }, () => searchGroupContext(context));
      else if (context.get('size') != null) this.setState({ loadId: null });
    }
  }

  confirmContactDeletion(quantity, callback) {
    this.props.confirmable({
      caption: 'Confirm Contact Deletion',
      okLabel: 'Yes',
      cancelLabel: 'No',
      type: type.default,
      question: `Do you want to delete any contacts associated with ${quantity === 1 ? 'this property' : 'these properties'}?`,
      onOk: () => callback(true),
      onCancel: () => callback(false),
    })();
  }

  handleAddGroup() {
    this.addGroup(PropertyGroupTypes.FAVORITE);
  }

  handleAddList() {
    this.addGroup(PropertyGroupTypes.MARKETING, 'Marketing List');
  }

  handleOpenContractManager() {
    this.props.openContractManager();
  }

  handleRemove() {
    const { confirmable, context, deleteGroupContext } = this.props;
    const qty = context.get('selected');
    confirmable({
      caption: 'Confirm Property Removal',
      okLabel: 'Remove',
      type: type.danger,
      question: `Are you sure you want to remove the selected ${qty === 1 ? 'property' : `${qty} properties`} from ${context.get('id') < 100 ? 'all assigned groups and marketing lists' : `this ${groupTypeName(context).toLowerCase()}`}?`,
      onOk: () => this.confirmContactDeletion(qty, deleteContacts => deleteGroupContext(context, deleteContacts)),
    })();
  }

  handleDeleteGroup() {
    const { confirmable, context, deletePropertyGroup } = this.props;
    const qty = context.getIn(['info', 'size']);
    const type = groupTypeName(context);
    const doDelete = (deleteContacts = false) => deletePropertyGroup(context.get('id'), deleteContacts);
    confirmable({
      caption: `Confirm ${type} Deletion`,
      okLabel: 'Delete',
      type: type.danger,
      question: `Are you sure you want to remove the selected ${type.toLowerCase()}?${qty ? ` The ${qty === 1 ? 'property' : `${qty} properties`} under this ${type.toLowerCase()} will be removed.` : ''}`,
      onOk: () => (!qty ? doDelete() : this.confirmContactDeletion(qty, doDelete)),
    })();
  }

  handleAddToGroup() {
    this.addToGroup(PropertyGroupTypes.FAVORITE, 'Group', 100);
  }

  handleAddToList() {
    this.addToGroup(PropertyGroupTypes.MARKETING, 'Marketing List', 1000);
  }

  handleExport() {
    const { context, exportGroupContext } = this.props;
    exportGroupContext(context);
  }

  handleRename() {
    const { context, prompt, updatePropertyGroup } = this.props;

    prompt({
      text: 'Please enter a new name for this group.',
      title: 'Rename Group',
      value: context.getIn(['info', 'name']),
      onSubmit: name => updatePropertyGroup({ id: context.get('id'), name }),
    });
  }

  addToGroup(type, groupType, limit) {
    const { context, groups, openAddToGroup } = this.props;
    const size = context.get('selected');

    openAddToGroup({
      groupType,
      limit,
      selectLoading,
      size,
      itemType: 'Property',
      groups: groups.filter(g => g.get('id') !== context.get('id') && g.get('type') === type),
      add: (name, afterSuccess) => this.saveGroup(context.mergeIn(['info'], { name, type }), () => afterSuccess(Math.min(size, limit))),
    });
  }

  addGroup(groupType, type) {
    this.props.openCreateGroupPopup({ type, createGroup: (name, afterSuccess) => this.saveGroup(fromJS({ info: { type: groupType, name } }), afterSuccess) });
  }

  saveGroup(context, afterSuccess) {
    const { saveGroupContext, router } = this.props;
    saveGroupContext(context, ({ response }) => {
      afterSuccess();
      router.push(propertyGroupPath(response.find(g => g.modified).id));
    });
  }

  render() {
    const { children, context, favorites, lists, alerts, loading } = this.props;

    const size = context.get('size');
    const id = context.get('id');
    const retrieved = context.get('data').size;
    const selected = context.get('selected');
    const btnProps = { kind: Button.kind.grayGhost, className: css.btn, isLoading: loading };
    const pbtnProps = { ...btnProps, disabled: !selected };
    const toggleProps = { location: propertyGroupPath(), sizeProperty: 'size', selectedId: id };
    const customGroup = id >= 10;

    return (
      <div className={css.wrapper}>
        <div className={css.leftSide}>
          <LeftPart caption="My Properties">
            <ToggleList {...toggleProps} data={favorites} onPlusClick={this.handleAddGroup} caption="Groups" />
            <ToggleList {...toggleProps} data={lists} onPlusClick={this.handleAddList} caption="Marketing Lists" />
            <div className={css.footer}>
              <Button {...btnProps} onClick={this.handleOpenContractManager}>Manage Contracts</Button>
            </div>
          </LeftPart>
        </div>
        <RightPart>
          <header className={css.alerts}>
            {alerts.map(alert => (
              <Link key={alert.get('id')} className={css.col} activeClassName={css.colActive} to={propertyGroupPath(alert.get('id'))}>
                <SVG className={css.iconHome} icon={alertIcons[alert.get('id') - 1]} />
                <div className={css.text}>
                  <div className={css.value}>{alert.get('size')}</div>
                  <div className={css.name}>{alert.get('name')}</div>
                </div>
              </Link>
            ))}
          </header>
          <section className={css.dashboard}>
            <div className={css.properties}>
              <div className={css.header}>
                <div className={css.headerCaption}>
                  <h1 className={css.title}>{context.getIn(['info', 'name'])}{size == null ? '' : ` (${retrieved}${retrieved < size ? ` of ${size}` : ''})`}</h1>
                  <div className={css.selected}>{selected ? `${selected} Selected` : null}</div>
                </div>
                <div className={css.headerControls}>
                  <Link to={`${propertyGroupPath(context.get('id'))}/map`} className={css.viewMap}>
                    <SVG icon="iconMap" className={css.viewMapIcon} />
                    <span>View Map</span>
                  </Link>
                  <Button {...pbtnProps} onClick={this.handleRemove}>Remove</Button>
                  <Button {...pbtnProps} onClick={this.handleAddToGroup}>Add to Group</Button>
                  <Button {...pbtnProps} onClick={this.handleAddToList}>Add to List</Button>
                  <Button {...pbtnProps} onClick={this.handleExport}>Export</Button>
                  {!customGroup || context.getIn(['info', 'defaultGroup']) ? null : <Button {...btnProps} onClick={this.handleRename}>Rename</Button>}
                  {!customGroup ? null : <Button {...btnProps} kind={Button.kind.redGhost} onClick={this.handleDeleteGroup}>Delete {groupTypeName(context)}</Button>}
                </div>
              </div>
              <div className={css.body}>
                <PropertySearchTable context={context} loading={loading} fullMode />
              </div>
            </div>
          </section>
        </RightPart>
        {children}
      </div>
    );
  }
}

export default createConfirmable(withRouter(connect(state => ({
  loading: selectLoading(state),
  context: selectGroupContext(MAIN_CONTEXT)(state),
  groups: selectGroups(state),
  favorites: selectFavorites(state),
  lists: selectLists(state),
  alerts: selectAlerts(state),
}), {
  loadGroupContext,
  searchGroupContext,
  exportGroupContext,
  deletePropertyGroup,
  deleteGroupContext,
  saveGroupContext,
  updatePropertyGroup,
  prompt: Prompt.open,
  openContractManager: ContractManagerPopup.open,
  openCreateGroupPopup: CreateGroupPopup.open,
  openAddToGroup: AddToGroup.open,
})(deferExecutor(PropertyGroup))));
