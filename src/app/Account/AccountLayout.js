import React, { PureComponent } from 'react';

import AccountForm from './AccountForm';
import css from './style.scss';


class AccounLayout extends PureComponent {
  render() {
    return (
      <div className={css.root}>
        <div className={css.inner}>
          <AccountForm {...this.props} />
        </div>
      </div>
    );
  }
}

export default AccounLayout;
