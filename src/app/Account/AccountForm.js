import React, { PureComponent } from 'react';
import { findDOMNode } from 'react-dom';
import MaskedInput from 'react-maskedinput';
import classNames from 'classnames';

import config from 'config';
import { Countries } from 'data/user/constants';
import {
  validate, verify, verifyRequired, isValidEmail, isValidCardNumber, isValidCardCode, isValidCardExpiration,
} from 'utils/validation';

import Dropdown from 'components/base/Dropdown';
import Button from 'components/base/Button';
import ButtonImport from 'components/base/ButtonImport';
import Image from 'components/base/Image';

import css from './style.scss';


const { constants: { API: { BASE_URL } } } = config;

// TODO: remove auto-complete in card's part of this form

const monthOptions = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map(month => ({ label: month, value: month }));
const countryOptions = Countries.map(country => ({ label: country.name, value: country.code }));
const inputMediumClass = classNames(css.input, css.inputMedium);
const inputStreetAddressClass = classNames(css.input, css.inputStreetAddress);
const countryMap = Countries.reduce((map, country) => {
  const c = Object.assign({}, country);
  const m = map;

  if (!c.postalCodeLabel) c.postalCodeLabel = country.code === 'US' ? 'Zip Code' : 'Postal Code';
  if (!c.stateLabel) c.stateLabel = 'State';

  m[c.code] = c;
  return m;
}, {});


class AccountForm extends PureComponent {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleLogoRemove = this.handleLogoRemove.bind(this);
    this.handleLogoChange = this.handleLogoChange.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.firstNameRef = null;
    this.lastNameRef = null;
    this.usernameRef = null;
    this.passwordRef = null;
    this.cardNumberRef = null;
    this.cardNameRef = null;
    this.cardCodeRef = null;
    this.cardExpMonthRef = null;
    this.cardExpYearRef = null;
    this.billingStreetAddressRef = null;
    this.billingCityRef = null;
    this.billingStateRef = null;
    this.billingZipRef = null;
    this.origProfile = null;
  }

  componentWillMount() {
    this.origProfile = Object.assign({}, this.props.profile);
  }

  handleChange(event) {
    const stateChange = {};
    stateChange[event.target.name] = event.target.value;
    if (event.target.name === 'cardNumber') stateChange[event.target.name] = event.target.value.replace(/\D/g, '');
    if (event.target.name === 'billingCountry') stateChange.billingState = '';
    this.props.updateState(stateChange);
  }

  handleLogoRemove() {
    this.props.updateState({
      logo: null,
      logoUrl: `${BASE_URL}/resource/auth/ps4/user/defaultLogo`,
    });
  }

  handleLogoChange(event) {
    if (!event.target.files || !event.target.files[0]) return;

    const file = event.target.files[0];
    const fileReader = new FileReader();
    fileReader.onload = (event) => {
      this.props.updateState({
        logo: file,
        logoUrl: event.target.result,
      });
    };
    fileReader.readAsDataURL(file);
  }

  handleSave() {
    const p = this.origProfile;
    const {
      firstNameRef, lastNameRef, usernameRef, passwordRef, cardNumberRef, cardCodeRef, cardNameRef, cardExpMonthRef,
      cardExpYearRef, billingStreetAddressRef, billingCityRef, billingStateRef, billingZipRef,
    } = this;
    const {
      firstName, lastName, username, password, cardNumber, cardCode, cardName, cardExpMonth, cardExpYear,
      billingStreetAddress, billingCity, billingState, billingZip, billingCountry, paymentMethodId,
    } = this.props.profile;

    validate(() => {
      verifyRequired(firstNameRef, firstName, 'First Name is required.');
      verifyRequired(lastNameRef, lastName, 'Last Name is required.');
      verifyRequired(usernameRef, username, 'Email is required.');
      verify(usernameRef, isValidEmail(username), 'Email is invalid.');

      // Ignore password if blank or all asterisks; existing password will be preserved.
      if (password !== '' && !/^\*+$/.test(password)) {
        verify(passwordRef, password.length >= 8, 'Password must be at least 8 characters.');
        verify(passwordRef, !/\s/.test(password), 'Password cannot contain any white space.');
        verify(passwordRef, /[A-Z]/.test(password), 'Password must contain at least one capital letter.');
        verify(passwordRef, /[^a-zA-Z0-9]/.test(password), 'Password must contain at least one symbol.');
      }

      const billingChanged = cardNumber !== p.cardNumber || cardName !== p.cardName || cardExpMonth !== p.cardExpMonth
        || cardExpYear !== p.cardExpYear || billingStreetAddress !== p.billingStreetAddress
        || billingCity !== p.billingCity || billingState !== p.billingState || billingZip !== p.billingZip
        || billingCountry !== p.billingCountry;
      const hasBillingInfo = [
        `${cardNumber}${cardCode}${cardName}${cardExpMonth}${cardExpYear}`,
        `${billingStreetAddress}${billingCity}${billingState}${billingZip}`,
      ].join('').trim() !== '';

      // Only need to validate if billing info has changed. Do not allow and existing payment method to be removed.
      // Not checking if card code changed since that's only needed when a new CC number is entered, in which case
      // that will have been changed, triggering the checks. This will prevent checks when only code is changed.
      if (billingChanged && (hasBillingInfo || paymentMethodId)) {
        verifyRequired(cardNumberRef, cardNumber, 'Credit Card Number is required.');

        // Only validate card number and code if the card number has been changed or there was no previous payment
        // method set.
        if (!paymentMethodId || cardNumber !== p.cardNumber) {
          verify(cardNumberRef, isValidCardNumber(cardNumber), 'Credit Card Number is invalid.');
          verifyRequired(cardCodeRef, cardCode, 'Credit Card CVV is required.');
          verify(cardCodeRef, isValidCardCode(cardCode, cardNumber), 'Credit Card CVV is invalid.');
        }

        verifyRequired(cardNameRef, cardName, 'Name on Card is required.');
        verifyRequired(cardExpMonthRef, cardExpMonth, 'Expiration Month is required.');
        verifyRequired(cardExpYearRef, cardExpYear, 'Expiration Year is required.');

        const country = countryMap[billingCountry];
        verify(cardExpMonthRef, isValidCardExpiration(cardExpYear, cardExpMonth), 'Credit card is expired.');
        verifyRequired(billingStreetAddressRef, billingStreetAddress, 'Billing Street Address is required.');
        verifyRequired(billingCityRef, billingCity, 'Billing City is required.');
        verifyRequired(billingStateRef, billingState, `Billing ${country.stateLabel} is required.`);
        verifyRequired(billingZipRef, billingZip, `Billing ${country.postalCodeLabel} is required.`);
        verify(billingZipRef, !country.postalCodePattern || new RegExp(country.postalCodePattern, 'i')
          .test(billingZip), `${country.postalCodeLabel} is invalid.`);
      }

      this.props.saveProfile();
    });
  }

  render() {
    const { profile, onSubmit } = this.props;
    const {
      firstName,
      lastName,
      logoUrl,
      cardExpMonth,
      cardExpYear,
      cardName,
      cardNumber,
      cardNumberMasked,
      username,
      billingStreetAddress,
      billingCity,
      billingState,
      billingZip,
      billingCountry,
    } = profile;

    // Get year options for next 15 years. If user has an older card then add the older year.
    // (Validation wll fail on save.)
    const year = new Date().getFullYear();
    const yearOptions = [];
    if (cardExpYear && cardExpYear < year) yearOptions.push({ label: cardExpYear, value: cardExpYear });
    for (let i = 0; i < 15; i++) yearOptions.push({ label: year + i, value: year + i });

    // Get country state list. If no country is selected, default to US.
    const country = countryMap[billingCountry] || countryMap.US;
    const stateOptions = (country.states || []).map(state => ({ label: state.name, value: state.code }));
    if (stateOptions.length) stateOptions.unshift({ value: '' });

    // Show appropriate patterns for Amex vs other cards
    const { ccNumPattern, ccCodePattern } = cardNumber.startsWith('34') || cardNumber.startsWith('37') ? { ccNumPattern: '1111 111111 11111', ccCodePattern: '1111' } : { ccNumPattern: '1111 1111 1111 1111', ccCodePattern: '111' };

    // Countries that have state options will have a drop down. Free type otherwise.
    const stateInput = stateOptions.length ? (
      <Dropdown
        name="billingState"
        ref={el => (this.billingStateRef = findDOMNode(el))}
        className={classNames(css.dropdown, css.dropdownState)}
        options={stateOptions}
        onChange={this.handleChange}
        value={billingState}
      />
    ) : (
      <input
        type="text"
        className={classNames(css.input, css.inputState)}
        ref={el => (this.billingStateRef = el)}
        name="billingState"
        value={billingState}
        onChange={this.handleChange}
      />
    );

    const fullName = `${firstName} ${lastName}`;

    const removeLogo = !logoUrl || logoUrl.endsWith('/defaultLogo') ? null : (
      <div className={css.avatarInner}>
        <div className={css.control} onClick={this.handleLogoRemove}>remove</div>
      </div>
    );

    const logo = !logoUrl ? null : (
      <div>
        <div className={css.avatarImg}>
          <Image src={`${logoUrl}`} alt={fullName} />
        </div>
        {removeLogo}
      </div>
    );

    return (
      <form onSubmit={onSubmit}>
        <div className={css.header}>
          <div className={css.caption}>My Account</div>
          <div className={css.buttons}>
            <Button
              kind={Button.kind.grayGhost}
              size={Button.size.large}
              name="cancel"
              className={css.button}
              onClick={this.props.getBack}
            >Cancel</Button>
            <Button
              kind={Button.kind.blue}
              size={Button.size.large}
              name="save"
              className={css.button}
              onClick={this.handleSave}
            >Save</Button>
          </div>
        </div>
        <div className={css.section}>
          <div className={css.sectionInner}>
            <div className={css.sectionPart}>
              <div className={css.fieldBlock}>
                <label className={css.sectionCaption} htmlFor="firstName">First Name*</label>
                <input
                  type="text"
                  className={css.input}
                  name="firstName"
                  ref={el => (this.firstNameRef = el)}
                  value={firstName}
                  onChange={this.handleChange}
                />
              </div>
              <div className={css.fieldBlock}>
                <label className={css.sectionCaption} htmlFor="username">Email*</label>
                <input
                  type="text"
                  className={css.input}
                  ref={el => (this.usernameRef = el)}
                  name="username"
                  value={username}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className={css.sectionPart}>
              <div className={css.fieldBlock}>
                <label className={css.sectionCaption} htmlFor="lastName">Last Name*</label>
                <input
                  type="text"
                  className={css.input}
                  name="lastName"
                  ref={el => (this.lastNameRef = el)}
                  value={lastName}
                  onChange={this.handleChange}
                />
              </div>
              <div className={css.fieldBlock}>
                <label className={css.sectionCaption} htmlFor="password">Password*</label>
                <input
                  type="password"
                  className={css.input}
                  ref={el => (this.passwordRef = el)}
                  name="password"
                  placeholder="********"
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className={css.sectionPart}>
              <label className={css.sectionCaption} htmlFor="avatar">Profile Picture*</label>
              <div className={css.avatar}>
                {logo}
                <ButtonImport
                  name="avatar"
                  kind={Button.kind.ghost}
                  size={Button.size.middle}
                  className={css.import}
                  onChange={this.handleLogoChange}
                >Browse</ButtonImport>
              </div>
            </div>
          </div>
        </div>
        <div className={css.section}>
          <div className={css.sectionLeft}>
            <div className={css.sectionInner}>
              <div className={css.sectionPart}>
                <label className={css.sectionCaption} htmlFor="card">Credit Card Number*</label>
                <MaskedInput
                  {...(cardNumberMasked && cardNumberMasked.length ? { placeholder: cardNumberMasked } : {})}
                  mask={ccNumPattern}
                  name="cardNumber"
                  ref={el => (this.cardNumberRef = findDOMNode(el))}
                  onChange={this.handleChange}
                  className={css.input}
                  value={cardNumber}
                />
              </div>
              <div className={css.sectionPart}>
                <label className={css.sectionCaption} htmlFor="cardCode">CVV*</label>
                <MaskedInput
                  mask={ccCodePattern}
                  className={inputMediumClass}
                  name="cardCode"
                  ref={el => (this.cardCodeRef = el)}
                  id="cardCode"
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className={css.sectionInner}>
              <div className={css.sectionPart}>
                <label className={css.sectionCaption} htmlFor="credit-card-name">Name on Card*</label>
                <input
                  type="text"
                  className={css.input}
                  name="cardName"
                  ref={el => (this.cardNameRef = el)}
                  onChange={this.handleChange}
                  value={cardName}
                />
              </div>
              <div className={css.sectionPart}>
                <label className={css.sectionCaption} htmlFor="expiryMonth">Expiration Date*</label>
                <Dropdown
                  name="cardExpMonth"
                  ref={el => (this.cardExpMonthRef = el)}
                  className={classNames(css.dropdown, css.dropdownCardExpiration)}
                  options={monthOptions}
                  onChange={this.handleChange}
                  value={cardExpMonth}
                />
                <Dropdown
                  name="cardExpYear"
                  ref={el => (this.cardExpYearRef = el)}
                  className={classNames(css.dropdown, css.dropdownCardExpiration)}
                  options={yearOptions}
                  onChange={this.handleChange}
                  value={cardExpYear}
                />
              </div>
            </div>
          </div>
          <div className={css.sectionRight}>
            <div className={css.fieldBlock}>
              <label className={css.sectionCaption} htmlFor="billingStreetAddress">Street Address*</label>
              <input
                type="text"
                className={inputStreetAddressClass}
                name="billingStreetAddress"
                ref={el => (this.billingStreetAddressRef = el)}
                value={billingStreetAddress}
                onChange={this.handleChange}
              />
            </div>
            <div className={css.fieldBlock}>
              <div className={css.sectionInner}>
                <div className={css.sectionPart}>
                  <label className={css.sectionCaption} htmlFor="billingCity">City*</label>
                  <input
                    type="text"
                    className={classNames(css.input, css.inputCity)}
                    name="billingCity"
                    ref={el => (this.billingCityRef = el)}
                    value={billingCity}
                    onChange={this.handleChange}
                  />
                </div>
                <div className={css.sectionPart}>
                  <label className={css.sectionCaption} htmlFor="billingState">{country.stateLabel}*</label>
                  {stateInput}
                </div>
                <div className={css.sectionPart}>
                  <label className={css.sectionCaption} htmlFor="billingZip">{country.postalCodeLabel}*</label>
                  <input
                    type="text"
                    className={inputMediumClass}
                    name="billingZip"
                    ref={el => (this.billingZipRef = el)}
                    value={billingZip}
                    onChange={this.handleChange}
                  />
                </div>
              </div>
            </div>
            <div className={css.fieldBlock}>
              <label className={css.sectionCaption} htmlFor="billingCountry">Country*</label>
              <Dropdown
                name="billingCountry"
                className={classNames(css.dropdown, css.dropdownCountry)}
                options={countryOptions}
                onChange={this.handleChange}
                value={country.code}
              />
            </div>
          </div>
        </div>
      </form>
    );
  }
}

export default AccountForm;
