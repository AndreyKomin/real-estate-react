/** @flow */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';

import {
  selectProfile,
  selectIsLoading,
} from 'data/user/selectors';
import {
  saveProfile,
  removeError,
} from 'data/user';

import { deferExecutor } from 'app/DeferredOnLocation';

import AccountLayout from './AccountLayout';
import ModalError from './ModalError';


class Account extends PureComponent<*, *, *> {
  /* :: state: Object */
  /* :: logo: Object | null */
  /* :: setState: Function */

  constructor(props: Object) {
    super(props);
    this.state = { ...props.profile.toJS() };
    this.logo = null;
    this.setState = this.setState.bind(this);
    this.getBack = this.getBack.bind(this);
    this.saveProfile = this.saveProfile.bind(this);
    this.handleLogoChange = this.handleLogoChange.bind(this);
  }

  componentDidUpdate() {
    const message = this.props.profile.get('message');

    if (this.props.profile.get('success') === false && message) {
      this.props.removeError();
      this.props.openModalError({ message });
    }
  }

  /* :: getBack: Function */
  getBack() {
    this.props.history.goBack();
  }

  /* :: handleLogoChange: Function */
  handleLogoChange(logo) {
    this.logo = logo;
  }

  /* :: saveProfile: Function */
  saveProfile() {
    const formData: Object = new FormData();

    Object.keys(this.state).forEach((property) => {
      formData.append(property, this.state[property]);
    });

    this.props.saveProfile(formData);
  }

  render() {
    return (
      <div>
        <Helmet title="My Account" />

        <div>
          <AccountLayout
            {...this.props}
            profile={this.state}
            getBack={this.getBack}
            updateState={this.setState}
            saveProfile={this.saveProfile}
            onLogoChange={this.handleLogoChange}
          />
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    profile: selectProfile(state),
    isLoading: selectIsLoading(state),
  };
}

const mapActionToProps = {
  saveProfile,
  removeError,
  openModalError: ModalError.open,
};

export default connect(mapStateToProps, mapActionToProps)(deferExecutor(Account));
