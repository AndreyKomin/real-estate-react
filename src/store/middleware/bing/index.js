/** @flow */
/** @module middleware.Bing */
import type { Store, Middleware } from 'redux';

import Promise from 'bluebird';
import * as Bing from 'Bing';
import isFunction from 'utils/type/isFunction';
import createTaskQueuePool, { AbortingError } from './queue';


const requestsQueue = createTaskQueuePool();

export const BING_ACTION = Symbol('BING_ACTION');

const globalAfterErrors = [];
const middlewareErrorTypes = [];

class ApiError {
  name: string;
  message: string;
  stack: string;
  // static prototype: Object

  constructor(error: Error) {
    this.name = this.constructor.name;
    this.message = error.message;
    this.stack = error.stack;
  }
}

const a: any = ApiError;
a.prototype = Object.create(Error.prototype);


export default ({ dispatch, getState }: Store<*, Object>) => (next: Middleware<*, Object>) => (action: Object) => {
  if (!action) return next(action);
  if (!action[BING_ACTION]) {
    return next(action);
  }

  let resolve;
  const deferred = new Promise((res) => {
    [resolve] = [res];
  });

  // eslint-disable-next-line no-use-before-define
  const promiseCreators = [createRequestPromise(() => action, next, getState, dispatch)];

  const overall = promiseCreators.reduce((promise, creator) => promise.then(creator), Promise.resolve());

  overall.finally(() => {
    resolve();
  }).catch((error) => {
    if (error instanceof ApiError) return;
    if (error instanceof AbortingError) return;
    middlewareErrorTypes.map(type => dispatch(actionWith(action, { // eslint-disable-line no-use-before-define
      error,
      type,
    })));
  });

  return deferred;
};


function actionWith(action, toMerge) {
  const ret = Object.assign({}, action, toMerge);
  delete ret[BING_ACTION];
  return ret;
}

function performPreStart({ dispatch, params, apiAction, getState }) {
  if (params.startType) {
    dispatch(actionWith(apiAction, {
      type: params.startType,
      args: params.args,
      query: params.query,
    }));
  }

  if (isFunction(params.beforeStart)) {
    params.beforeStart({ dispatch, getState });
  }
}

function getRequestObject({ params }) {
  return new Promise((resolve) => {
    const path = params.method.split('.');
    const [module, method] = path;
    return resolve(
      Bing[module][method](params.query, ...params.args),
    );
  });
}

function performError({ dispatch, error, params, apiAction, getState, reject }) {
  const tempErr = new ApiError(error);
  globalAfterErrors.map(cb => cb({ dispatch, getState, error }));

  if (params.errorType) {
    dispatch(actionWith(apiAction, {
      type: params.errorType,
      error: tempErr,
    }));
  }

  if (isFunction(params.afterError)) {
    params.afterError({ dispatch, getState, error });
  }
  reject(error);
}

function performSuccess({ dispatch, params, apiAction, getState, resolve, response }) {
  if (params.successType) {
    const action = actionWith(apiAction, {
      ...params.successParams,
      type: params.successType,
      response,
    });
    dispatch(action);
  }

  if (isFunction(params.afterSuccess)) {
    params.afterSuccess({ getState, dispatch, response });
  }
  resolve(response);
}

function createRequestPromise(apiActionCreator, next, getState, dispatch) {
  return () => {
    const apiAction = apiActionCreator();
    let resolveRequest;
    let rejectRequest;
    const deferred = new Promise((res, rej) => {
      [resolveRequest, rejectRequest] = [res, rej];
    });
    // eslint-disable-next-line no-use-before-define
    const params = extractParams(apiAction[BING_ACTION]);

    requestsQueue.push(params.unifier, params.maxCount, (error) => {
      if (error) return rejectRequest(error);
      return new Promise((resolve, reject) => {
        const baseOptions = { dispatch, params, apiAction, getState };
        performPreStart({ ...baseOptions });

        return getRequestObject({ params })
          .then(response => performSuccess({ ...baseOptions, response, resolve }))
          .catch(error => performError({ ...baseOptions, error, reject }));
      })
        .then(resolveRequest)
        .catch(rejectRequest);
    });

    return deferred;
  };
}

function extractParams(callApi) {
  const {
    method,
    query,
    args = [],
    startType,
    beforeStart,
    successType,
    errorType,
    afterSuccess,
    afterError,
    maxCount,
    ...changeableParams
  } = callApi;
  let {
    // eslint-disable-next-line no-use-before-define
    unifier = defaultUnifier(callApi),
  } = changeableParams;

  if (unifier instanceof Function) unifier = unifier(callApi);

  return {
    ...changeableParams,
    method,
    query,
    args,
    startType,
    beforeStart,
    successType,
    errorType,
    afterSuccess,
    afterError,
    unifier,
    maxCount,
  };
}

const defaultUnifier: Function = (function () {
  return function getDefaultUnifier(params): string {
    if (params.method && !(/get/.test(params.method.toLowerCase()))) {
      return `${params.method} ${params.query} ${JSON.stringify(params.args)}`;
    }
    return `${params.method} ${JSON.stringify(params.args)}`;
  };
}());

