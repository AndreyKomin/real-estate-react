// @flow
export default class AbortAttempt {
  constructor(unifier: string, allQueue: boolean) {
    this.unifier = unifier;
    this.allQueue = allQueue;
  }

  unifier: string;
  allQueue: boolean;

  getUnifier() {
    return this.unifier;
  }

  shouldAbortAll() {
    return this.allQueue;
  }
}
