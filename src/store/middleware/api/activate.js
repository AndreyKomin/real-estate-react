/** @flow */
let activationCode: string = process.env.ACTIVATION_CODE || '';

export function setCode(actCode: string) {
  activationCode = actCode;
}

export function getCode(): string {
  return activationCode;
}

export default function activate(requestObject: Object) {
  if (!activationCode) return requestObject;
  const { query = {}, ...rest } = requestObject;

  return {
    ...rest,
    query: {
      ActivationCode: activationCode,
      ...query,
    },
  };
}
